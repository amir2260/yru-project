@extends ('layouts.template')

@section('title')
    รายละเอียดกิจกรรม
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){

//     $('.selectpicker').selectpicker({
//         liveSearch : true
//     });

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ action('ProjectController@info', $project->PROJECT_ID) }}';
    })
    
})
</script>

<div class="col-sm-12">
	@include('Project.projectinfo', ['project'=> $project])
</div>

{!! Form::model($indicator) !!}
	{{ Form::hidden('PROJECT_ID', $project->PROJECT_ID) }}
	{{ Form::hidden('INDICATOR_ID', $indicator->INDICATOR_ID) }}
<div class="col-sm-8">
    <div class="form-group {{ $errors->has('INDICATOR_GROUP_ID') ? ' has-error' : '' }}">
        <label for="INDICATOR_GROUP_ID">กลุ่มเป้าหมาย</label>
        {{ Form::select('INDICATOR_GROUP_ID', $groups,null, ['class'=>'form-control']) }}
        
        @if ($errors->has('INDICATOR_GROUP_ID'))
            <span class="text-danger"><strong>{{ $errors->first('INDICATOR_GROUP_ID') }}</strong></span>
        @endif

    </div>

    <div class="form-group {{ $errors->has('INDICATOR_TYPE_ID') ? ' has-error' : '' }}">
        <label for="INDICATOR_TYPE_ID">เกณฑ์ตัวชี้วัด</label>
        {{ Form::select('INDICATOR_TYPE_ID', $types,null, ['class'=>'form-control']) }}
        
        @if ($errors->has('INDICATOR_TYPE_ID'))
            <span class="text-danger"><strong>{{ $errors->first('INDICATOR_TYPE_ID') }}</strong></span>
        @endif

    </div>

    <div class="form-group {{ $errors->has('TARGET') ? ' has-error' : '' }}">
        <label for="TARGET">เป้าหมาย</label>
        {{ Form::number('TARGET', null, ['class'=>'form-control', 'placeholder'=>'เป้าหมายที่ต้องการ']) }}
        
        @if ($errors->has('TARGET'))
            <span class="text-danger"><strong>{{ $errors->first('TARGET') }}</strong></span>
        @endif
    </div>
    

    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
                <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">

        </div>
    </div>
</div>
{!! Form::close() !!}


@endsection
