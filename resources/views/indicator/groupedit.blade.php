@extends ('layouts.template')

@section('title')
    รายละเอียดกลุ่มเป้าหมาย
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){

    $('.selectpicker').selectpicker({
        liveSearch : true
    });

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ action('IndicatorController@indicator_group') }}';
    })

})
</script>
{!! Form::model($group) !!}
{!! Form::hidden('INDICATOR_GROUP_ID') !!}
<div class="col-sm-6">
    <div class="form-group {{ $errors->has('INDICATOR_GROUP_NAME') ? ' has-error' : '' }}">
        <label for="INDICATOR_GROUP_NAME">ชื่อ - สกุล</label>
        {{ Form::text('INDICATOR_GROUP_NAME', null, ['class' => 'form-control']) }}
        
        @if ($errors->has('INDICATOR_GROUP_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('INDICATOR_GROUP_NAME') }}</strong></span>
        @endif

    </div>    


    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
                <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">

        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
