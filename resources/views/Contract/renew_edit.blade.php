@extends ('layouts.template')

@section('title')
    สัญญาทุนการศึกษา
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}
</style>

<script type="text/javascript">
$(function(){
    $('#btnDelete').click(function(){
        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
            if (result){
                window.location = '{{ url('/contract/renew_delete', $renew->RENEW_ID ) }}';
            }
        })
    })

    $('#btnAdd').click(function(){
        window.location = '{{ url('/contract/renew_add') }}/{{ $contract->CONTRACT_ID }}';
    })


    $('#btnBack').click(function(){
        window.location = '{{ url('/contract/renew') }}/{{ $contract->CONTRACT_ID }}';
    })

    $('.datetimepicker').datetimepicker({ format: "YYYY-MM-DD", locale : "th"});

})
</script>

{{ Form::model($contract) }}
<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">ข้อมูลสัญญา</div>
        <div class="panel-body">
            <div class="col-sm-6">
                <div class="form-group {{ $errors->has('CONTRACT_ID') ? ' has-error' : '' }}">
                    <label for="CONTRACT_ID">ทุนการศึกษา</label>
                    {{ Form::text('FUND_ID', $contract->fund->FUND_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_NO') ? ' has-error' : '' }}">
                    <label for="CONTRACT_NO">เลขที่สัญญา</label>
                    {{ Form::text('CONTRACT_NO', old('CONTRACT_NO'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_NO'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_NO') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_DATE') ? ' has-error' : '' }}">
                    <label for="CONTRACT_DATE">วันที่สัญญา</label>
                    {{ Form::text('CONTRACT_DATE', $utils->thaidate($contract->CONTRACT_DATE), ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_START_DATE') ? ' has-error' : '' }}">
                    <label for="CONTRACT_START_DATE">วันที่รับทุน</label>
                    {{ Form::text('CONTRACT_START_DATE', $utils->thaidate($contract->CONTRACT_START_DATE), ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_START_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_START_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_FINISH_DATE') ? ' has-error' : '' }}">
                    <label for="CONTRACT_FINISH_DATE">วันที่สิ้นสุดรับทุน</label>
                    {{ Form::text('CONTRACT_FINISH_DATE', $utils->thaidate($contract->CONTRACT_FINISH_DATE), ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_FINISH_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_FINISH_DATE') }}</strong></span>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('STUDENT_TYPE_ID') ? ' has-error' : '' }}">
                    <label for="STUDENT_TYPE_ID">ประเภทผู้รับทุน</label>
                    {{ Form::text('STUDENT_TYPE_ID', $contract->studentType->STUDENT_TYPE_NAME, ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('STUDENT_TYPE_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('STUDENT_TYPE_ID') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="col-sm-6 border-left">
                <div class="form-group {{ $errors->has('CITIZEN_ID') ? ' has-error' : '' }}">
                    <label for="CITIZEN_ID">เลขบัตรประชาชน</label>
                    {{ Form::text('CITIZEN_ID', old('CITIZEN_ID'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('CITIZEN_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('CITIZEN_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('STUDENT_NAME') ? ' has-error' : '' }}">
                    <label for="STUDENT_NAME">ชื่อ - สกุล</label>
                    {{ Form::text('STUDENT_NAME', old('STUDENT_NAME'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('STUDENT_NAME'))
                        <span class="text-danger"><strong>{{ $errors->first('STUDENT_NAME') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('ADDRESS') ? ' has-error' : '' }}">
                    <label for="ADDRESS">ที่อยู่</label>
                    {{ Form::text('ADDRESS', old('ADDRESS'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('ADDRESS'))
                        <span class="text-danger"><strong>{{ $errors->first('ADDRESS') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('PHONE') ? ' has-error' : '' }}">
                    <label for="PHONE">หมายเลขโทรศัพย์</label>
                    {{ Form::text('PHONE', old('PHONE'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('PHONE'))
                        <span class="text-danger"><strong>{{ $errors->first('PHONE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('EMAIL') ? ' has-error' : '' }}">
                    <label for="EMAIL">ที่อยู่อีเมล์</label>
                    {{ Form::text('EMAIL', old('EMAIL'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('EMAIL'))
                        <span class="text-danger"><strong>{{ $errors->first('EMAIL') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('ADVISOR') ? ' has-error' : '' }}">
                    <label for="ADVISOR">ที่ปรึกษา</label>
                    {{ Form::text('ADVISOR', old('ADVISOR'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('ADVISOR'))
                        <span class="text-danger"><strong>{{ $errors->first('ADVISOR') }}</strong></span>
                    @endif
                </div>

            </div>

            <div class="col-sm-12">
                <div class="form-group">
                <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                    กลับ
                </button>
                </div>
            </div>

        </div>
    </div>
</div>
{{ Form::close() }}

{{ Form::model($renew) }}
{{ Form::hidden('CONTRACT_ID') }}
{{ Form::hidden('RENEW_ID') }}

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">บันทึกข้อมูลการขยายเวลา</div>
        <div class="panel-body">
            <div class="col-sm-6">
                <div class="form-group {{ $errors->has('RENEW_INDEX') ? ' has-error' : '' }}">
                    <label for="RENEW_INDEX">ครั้งที่</label>
                    {{ Form::number('RENEW_INDEX', old('RENEW_INDEX'), ['class'=>'form-control']) }}
                    @if ($errors->has('RENEW_INDEX'))
                        <span class="text-danger"><strong>{{ $errors->first('RENEW_INDEX') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('REQUEST_DATE') ? ' has-error' : '' }}">
                    <label for="REQUEST_DATE">วันที่ขอขยายเวลาศึกษาต่อ</label>
                    {{ Form::text('REQUEST_DATE', old('REQUEST_DATE'), ['class'=>'form-control datetimepicker']) }}
                    @if ($errors->has('REQUEST_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('REQUEST_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('START_DATE') ? ' has-error' : '' }}">
                    <label for="START_DATE">วันที่เริ่มขยายเวลา</label>
                    {{ Form::text('START_DATE', old('START_DATE'), ['class'=>'form-control datetimepicker']) }}
                    @if ($errors->has('START_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('START_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('END_DATE') ? ' has-error' : '' }}">
                    <label for="END_DATE">วันสิ้นสุดขยายเวลา</label>
                    {{ Form::text('END_DATE', old('END_DATE'), ['class'=>'form-control datetimepicker']) }}
                    @if ($errors->has('END_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('END_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('REASON') ? ' has-error' : '' }}">
                    <label for="REASON">เหตุผล</label>
                    {{ Form::textarea('REASON', old('REASON'), ['class'=>'form-control', 'rows'=>'3']) }}
                    @if ($errors->has('REASON'))
                        <span class="text-danger"><strong>{{ $errors->first('REASON') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-sm-6 border-left">
                <div class="form-group {{ $errors->has('ACCEPTOR') ? ' has-error' : '' }}">
                    <label for="ACCEPTOR">ผู้รับรอง</label>
                    {{ Form::text('ACCEPTOR', old('ACCEPTOR'), ['class'=>'form-control']) }}
                    @if ($errors->has('ACCEPTOR'))
                        <span class="text-danger"><strong>{{ $errors->first('ACCEPTOR') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('ACCEPTOR_DATE') ? ' has-error' : '' }}">
                    <label for="ACCEPTOR_DATE">วันที่รับรอง</label>
                    {{ Form::text('ACCEPTOR_DATE', old('ACCEPTOR_DATE'), ['class'=>'form-control datetimepicker']) }}
                    @if ($errors->has('ACCEPTOR_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('ACCEPTOR_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('APPROVER') ? ' has-error' : '' }}">
                    <label for="APPROVER">ผู้อนุมัติ</label>
                    {{ Form::text('APPROVER', old('APPROVER'), ['class'=>'form-control']) }}
                    @if ($errors->has('APPROVER'))
                        <span class="text-danger"><strong>{{ $errors->first('APPROVER') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('APPROVER_DATE') ? ' has-error' : '' }}">
                    <label for="APPROVER_DATE">วันที่อนุมัติ</label>
                    {{ Form::text('APPROVER_DATE', old('APPROVER_DATE'), ['class'=>'form-control datetimepicker']) }}
                    @if ($errors->has('APPROVER_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('APPROVER_DATE') }}</strong></span>
                    @endif
                </div>

            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <button class="btn btn-primary" type="button" id="btnAdd" class="btnAdd">บันทึกรายการขยายเวลา</button>
                    @if (isset($renew->RENEW_ID ) && $renew->RENEW_ID > 0)
                        <button class="btn btn-default" type="button" id="btnDelete" class="btnDelete">ลบรายการขยายเวลา</button>
                    @endif
                </div>
            </div>

        </div>
    </div>
</div>
{{ Form::close() }}

@foreach ($errors->all() as $error)
   <div class='text-danger'>{{ $error }}</div>
@endforeach



@endsection
