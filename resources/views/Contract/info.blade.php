@extends ('layouts.template')

@section('title')
    สัญญาทุนการศึกษา
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}
</style>

<script type="text/javascript">
$(function(){
    $('#btnDelete').click(function(){
        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
            if (result){
                // window.location = '{{ url('/contract/delete') }}/{{ $contract->CONTRACT_ID }}';
            }
        })
    })

    $('#btnEdit').click(function(){
        window.location = '{{ url('/contract/edit') }}/{{ $contract->CONTRACT_ID }}';
    })

    $('#btnBudget').click(function(){
        window.location = '{{ url('/contract/budget') }}/{{ $contract->CONTRACT_ID }}';
    })

    $('#btnProgress').click(function(){
        window.location = '{{ url('/contract/progress') }}/{{ $contract->CONTRACT_ID }}';
    })

    $('#btnRenew').click(function(){
        window.location = '{{ url('/contract/renew') }}/{{ $contract->CONTRACT_ID }}';
    })

    $('#btnBack').click(function(){
        window.location = '{{ url('/contract') }}';
    })

})
</script>

{{ Form::model($contract) }}
<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">ข้อมูลสัญญา</div>
        <div class="panel-body">

            <div class="col-sm-6">
                <div class="form-group {{ $errors->has('CONTRACT_NO') ? ' has-error' : '' }}">
                    <label for="CONTRACT_NO">เลขที่สัญญา</label>
                    {{ Form::text('CONTRACT_NO', old('CONTRACT_NO'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_NO'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_NO') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_DATE') ? ' has-error' : '' }}">
                    <label for="CONTRACT_DATE">วันที่สัญญา</label>
                    {{ Form::text('CONTRACT_DATE', $utils->thaidate($contract->CONTRACT_DATE), ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_ID') ? ' has-error' : '' }}">
                    <label for="CONTRACT_ID">ทุนการศึกษา</label>
                    {{ Form::text('FUND_ID', $contract->fund->FUND_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('PROJECT_ID') ? ' has-error' : '' }}">
                    <label for="PROJECT_ID">โครงการ</label>
                    {{ Form::text('PROJECT_ID', $contract->fund->project->PROJECT_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('PROJECT_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('PROJECT_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('PROJECT_ID') ? ' has-error' : '' }}">
                    <label for="PROJECT_ID">ปีการศึกษา</label>
                    {{ Form::text('PROJECT_ID', $contract->fund->eduTerm->EDU_TERM_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('PROJECT_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('PROJECT_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('PROJECT_ID') ? ' has-error' : '' }}">
                    <label for="PROJECT_ID">ประเภททุน</label>
                    {{ Form::text('PROJECT_ID', $contract->fund->fundType->FUND_TYPE_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('PROJECT_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('PROJECT_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('PROJECT_ID') ? ' has-error' : '' }}">
                    <label for="PROJECT_ID">สาขาวิชา</label>
                    {{ Form::text('PROJECT_ID', $contract->fund->program->PROGRAM_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('PROJECT_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('PROJECT_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('PROJECT_ID') ? ' has-error' : '' }}">
                    <label for="PROJECT_ID">สถานศึกษา</label>
                    {{ Form::text('PROJECT_ID', $contract->fund->school->SCHOOL_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('PROJECT_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('PROJECT_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('PROJECT_ID') ? ' has-error' : '' }}">
                    <label for="PROJECT_ID">ระดับการศึกษา</label>
                    {{ Form::text('PROJECT_ID', $contract->fund->eduLevel->EDU_LEVEL_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('PROJECT_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('PROJECT_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_START_DATE') ? ' has-error' : '' }}">
                    <label for="CONTRACT_START_DATE">วันที่รับทุน</label>
                    {{ Form::text('CONTRACT_START_DATE', $utils->thaidate($contract->CONTRACT_START_DATE), ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_START_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_START_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_FINISH_DATE') ? ' has-error' : '' }}">
                    <label for="CONTRACT_FINISH_DATE">วันที่สิ้นสุดรับทุน</label>
                    {{ Form::text('CONTRACT_FINISH_DATE', $utils->thaidate($contract->CONTRACT_FINISH_DATE), ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_FINISH_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_FINISH_DATE') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="col-sm-6 border-left">

                <div class="form-group {{ $errors->has('CITIZEN_ID') ? ' has-error' : '' }}">
                    <label for="CITIZEN_ID">เลขบัตรประชาชน</label>
                    {{ Form::text('CITIZEN_ID', old('CITIZEN_ID'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('CITIZEN_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('CITIZEN_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('STUDENT_NAME') ? ' has-error' : '' }}">
                    <label for="STUDENT_NAME">ชื่อ - สกุล</label>
                    {{ Form::text('STUDENT_NAME', old('STUDENT_NAME'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('STUDENT_NAME'))
                        <span class="text-danger"><strong>{{ $errors->first('STUDENT_NAME') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('STUDENT_TYPE_ID') ? ' has-error' : '' }}">
                    <label for="STUDENT_TYPE_ID">ประเภทผู้รับทุน</label>
                    {{ Form::text('STUDENT_TYPE_ID', $contract->studentType->STUDENT_TYPE_NAME, ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('STUDENT_TYPE_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('STUDENT_TYPE_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('ADDRESS') ? ' has-error' : '' }}">
                    <label for="ADDRESS">ที่อยู่</label>
                    {{ Form::text('ADDRESS', old('ADDRESS'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('ADDRESS'))
                        <span class="text-danger"><strong>{{ $errors->first('ADDRESS') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('PHONE') ? ' has-error' : '' }}">
                    <label for="PHONE">หมายเลขโทรศัพย์</label>
                    {{ Form::text('PHONE', old('PHONE'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('PHONE'))
                        <span class="text-danger"><strong>{{ $errors->first('PHONE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('EMAIL') ? ' has-error' : '' }}">
                    <label for="EMAIL">ที่อยู่อีเมล์</label>
                    {{ Form::text('EMAIL', old('EMAIL'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('EMAIL'))
                        <span class="text-danger"><strong>{{ $errors->first('EMAIL') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('ADVISOR') ? ' has-error' : '' }}">
                    <label for="ADVISOR">ที่ปรึกษา</label>
                    {{ Form::text('ADVISOR', old('ADVISOR'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('ADVISOR'))
                        <span class="text-danger"><strong>{{ $errors->first('ADVISOR') }}</strong></span>
                    @endif
                </div>
            </div>

        </div>
    </div>
</div>

{{ Form::close() }}

<div class="col-sm-12">
    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-default" id="btnProgress" name="btnProgress">
                รายงานความก้าวหน้า
            </button>
            <button type="button" class="btn btn-default" id="btnBudget" name="btnBudget">
                เบิกทุนการศึกษา
            </button>
            <button type="button" class="btn btn-default" id="btnRenew" name="btnRenew">
                ขยายระยะเวลาศึกษาต่อ
            </button>
            <button type="button" class="btn btn-default" id="btnEdit2" name="btnEdit2">
                แนบเอกสาร
            </button>

            &nbsp;

            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">
            <button type="button" class="btn btn-default" id="btnEdit" name="btnEdit">
                แก้ไข
            </button>
            <button type="button" class="btn btn-default" id="btnDelete" name="btnDelete">
                <span class="glyphicon glyphicon-remove-circle"></span> ลบ
            </button>
        </div>
    </div>
</div>


@endsection
