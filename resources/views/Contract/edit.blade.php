@extends ('layouts.template')

@section('title')
    สัญญาทุนการศึกษา
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){

    $('.datetimepicker').datetimepicker({ format: "YYYY-MM-DD", locale : "th"});

    $('.selectpicker').selectpicker({
        liveSearch : true
    });

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ url('/contract/info', $contract->CONTRACT_ID) }}';
    })

})
</script>
{!! Form::model($contract) !!}
{{ Form::hidden('CONTRACT_ID', $contract->CONTRACT_ID )}}
<div class="col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">ข้อมูลสัญญา</div>
        <div class="panel-body">
            <div class="form-group {{ $errors->has('FUND_ID') ? ' has-error' : '' }}">
                <label for="FUND_ID">ทุนการศึกษา</label>
                {{ Form::select('FUND_ID', $funds, $contract->FUND_ID, ['class'=>'form-control selectpicker'])}}
                @if ($errors->has('FUND_ID'))
                    <span class="text-danger"><strong>{{ $errors->first('FUND_ID') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('CONTRACT_NO') ? ' has-error' : '' }}">
                <label for="CONTRACT_NO">เลขที่สัญญา</label>
                {{ Form::text('CONTRACT_NO', old('CONTRACT_NO'), ['class'=>'form-control']) }}
                @if ($errors->has('CONTRACT_NO'))
                    <span class="text-danger"><strong>{{ $errors->first('CONTRACT_NO') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('CONTRACT_DATE') ? ' has-error' : '' }}">
                <label for="CONTRACT_DATE">วันที่สัญญา</label>
                {{ Form::text('CONTRACT_DATE', old('CONTRACT_DATE'), ['class'=>'form-control datetimepicker']) }}
                @if ($errors->has('CONTRACT_DATE'))
                    <span class="text-danger"><strong>{{ $errors->first('CONTRACT_DATE') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('CONTRACT_START_DATE') ? ' has-error' : '' }}">
                <label for="CONTRACT_START_DATE">วันที่รับทุน</label>
                {{ Form::text('CONTRACT_START_DATE', old('CONTRACT_START_DATE'), ['class'=>'form-control datetimepicker']) }}
                @if ($errors->has('CONTRACT_START_DATE'))
                    <span class="text-danger"><strong>{{ $errors->first('CONTRACT_START_DATE') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('CONTRACT_FINISH_DATE') ? ' has-error' : '' }}">
                <label for="CONTRACT_FINISH_DATE">วันที่สิ้นสุดรับทุน</label>
                {{ Form::text('CONTRACT_FINISH_DATE', old('CONTRACT_FINISH_DATE'), ['class'=>'form-control datetimepicker']) }}
                @if ($errors->has('CONTRACT_FINISH_DATE'))
                    <span class="text-danger"><strong>{{ $errors->first('CONTRACT_FINISH_DATE') }}</strong></span>
                @endif
            </div>


            <div class="form-group {{ $errors->has('STUDENT_TYPE_ID') ? ' has-error' : '' }}">
                <label for="STUDENT_TYPE_ID">ประเภทผู้รับทุน</label>
                {{ Form::select('STUDENT_TYPE_ID', $studentTypes, $contract->STUDENT_TYPE_ID, ['class'=>'form-control selectpicker'])}}
                @if ($errors->has('STUDENT_TYPE_ID'))
                    <span class="text-danger"><strong>{{ $errors->first('STUDENT_TYPE_ID') }}</strong></span>
                @endif
            </div>
        </div>
    </div>




{{-- ชื่อ
ที่อยู่
หมายเลขโทรศัพย์
ที่อยู่อีเมล์ --}}

</div>

<div class="col-sm-6">
    <div class="panel panel-default">
        <div class="panel-heading">ข้อมูลผู้รับทุน</div>
        <div class="panel-body">
            <div class="form-group {{ $errors->has('CITIZEN_ID') ? ' has-error' : '' }}">
                <label for="CITIZEN_ID">เลขบัตรประชาชน</label>
                {{ Form::text('CITIZEN_ID', old('CITIZEN_ID'), ['class'=>'form-control']) }}
                @if ($errors->has('CITIZEN_ID'))
                    <span class="text-danger"><strong>{{ $errors->first('CITIZEN_ID') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('STUDENT_NAME') ? ' has-error' : '' }}">
                <label for="STUDENT_NAME">ชื่อ - สกุล</label>
                {{ Form::text('STUDENT_NAME', old('STUDENT_NAME'), ['class'=>'form-control']) }}
                @if ($errors->has('STUDENT_NAME'))
                    <span class="text-danger"><strong>{{ $errors->first('STUDENT_NAME') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('ADDRESS') ? ' has-error' : '' }}">
                <label for="ADDRESS">ที่อยู่</label>
                {{ Form::text('ADDRESS', old('ADDRESS'), ['class'=>'form-control']) }}
                @if ($errors->has('ADDRESS'))
                    <span class="text-danger"><strong>{{ $errors->first('ADDRESS') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('PHONE') ? ' has-error' : '' }}">
                <label for="PHONE">หมายเลขโทรศัพย์</label>
                {{ Form::text('PHONE', old('PHONE'), ['class'=>'form-control']) }}
                @if ($errors->has('PHONE'))
                    <span class="text-danger"><strong>{{ $errors->first('PHONE') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('EMAIL') ? ' has-error' : '' }}">
                <label for="EMAIL">ที่อยู่อีเมล์</label>
                {{ Form::text('EMAIL', old('EMAIL'), ['class'=>'form-control']) }}
                @if ($errors->has('EMAIL'))
                    <span class="text-danger"><strong>{{ $errors->first('EMAIL') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('ADVISOR') ? ' has-error' : '' }}">
                <label for="ADVISOR">ที่ปรึกษา</label>
                {{ Form::text('ADVISOR', old('ADVISOR'), ['class'=>'form-control']) }}
                @if ($errors->has('ADVISOR'))
                    <span class="text-danger"><strong>{{ $errors->first('ADVISOR') }}</strong></span>
                @endif
            </div>


        </div>
    </div>
</div>

<div class="col-sm-12">
    <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
        <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
    </button>
    <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
        กลับ
    </button>
</div>

{!! Form::close() !!}

@endsection
