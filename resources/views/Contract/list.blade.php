@extends ('layouts.template')

@section('title')
    สัญญาทุนการศึกษา
@endsection

@section('content')
<style>
ul.pagination{
    margin: 1px;
}
</style>

{!! Form::open(['method' => 'post']) !!}
<div class="row">
    <div class="col-sm-4">
        <input type="text" name="TEXT_SEARCH" id="TEXT_SEARCH" value="{{ old('TEXT_SEARCH', $text_search) }}"
        class="form-control" placeholder="ป้อนคำค้นหา">
    </div>
    <div class="col-sm-3">
        <button type="submit" name="btnSearch" class="btn btn-default" >ค้นหา</button>
    </div>

    <div class="col-sm-5 text-right">
        <p>
            @if ($contracts->total() > 0)
                {{ $contracts->firstItem() }} - {{ $contracts->lastItem() }}
            @endif
            จาก {{ $contracts->total() }} รายการ
            <br>
            {{ $contracts->links() }}
            <a href="{{ url('contract/create') }}" class='btn btn-primary'>
                <span class='glyphicon glyphicon-plus-sign'></span> เพิ่มรายการ
            </a>
        </p>
    </div>
</div>
{!! Form::close() !!}

<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-hover">
            <tr class="active">
                <th>เลขบัตรประชาชน</th>
                <th>ชื่อ - สกุล ผู้รับทุน</th>
                <th>เลขที่สัญญา</th>
                <th>วันที่สัญญา</th>

                <th>ชื่อทุน</th>
                <th>ปีการศึกษา</th>
                <th>สาขาวิชา</th>
                <th>โครงการ</th>
                <th>ระดับ</th>
                <th>สถานะ</th>
            </tr>
            @foreach ($contracts as $contract)
                <tr>
                    <td>
                        <a href="{{ url('/contract/info') }}/{{ $contract->CONTRACT_ID }}">
                            {{ $contract->CITIZEN_ID }}
                        </a>
                    </td>
                    <td>{{ $contract->STUDENT_NAME }}</td>
                    <td>{{ $contract->CONTRACT_NO }}</td>
                    <td>{{ $utils->thaidate($contract->CONTRACT_DATE) }}</td>

                    <td>{{ $contract->fund->FUND_NAME }}</td>
                    <td>{{ $contract->fund->eduTerm->EDU_TERM_NAME }}</td>
                    <td>{{ $contract->fund->program->PROGRAM_NAME }}</td>
                    <td>{{ $contract->fund->project->PROJECT_NAME }}</td>
                    <td>{{ $contract->fund->eduLevel->EDU_LEVEL_NAME }}</td>
                    <td>{{ $contract->contractStatus->CONTRACT_STATUS_NAME }}</td>

                </tr>
            @endforeach
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 text-right">
        {{ $contracts->links() }}
    </div>
</div>

@endsection
