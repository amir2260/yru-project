@extends ('layouts.template')

@section('title')
    สัญญาทุนการศึกษา
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}
</style>

<script type="text/javascript">
$(function(){
    $('#btnDelete').click(function(){
        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
            if (result){
                // window.location = '{{ url('/contract/delete') }}/{{ $contract->CONTRACT_ID }}';
            }
        })
    })

    $('#btnEdit').click(function(){
        window.location = '{{ url('/contract/edit') }}/{{ $contract->CONTRACT_ID }}';
    })

    $('#btnBack').click(function(){
        window.location = '{{ url('/contract/info') }}/{{ $contract->CONTRACT_ID }}';
    })

    $('.datetimepicker').datetimepicker({ format: "YYYY-MM-DD", locale : "th"});

})
</script>

{{ Form::model($contract) }}
<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">ข้อมูลสัญญา</div>
        <div class="panel-body">
            <div class="col-sm-6">
                <div class="form-group {{ $errors->has('CONTRACT_ID') ? ' has-error' : '' }}">
                    <label for="CONTRACT_ID">ทุนการศึกษา</label>
                    {{ Form::text('FUND_ID', $contract->fund->FUND_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_NO') ? ' has-error' : '' }}">
                    <label for="CONTRACT_NO">เลขที่สัญญา</label>
                    {{ Form::text('CONTRACT_NO', old('CONTRACT_NO'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_NO'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_NO') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_DATE') ? ' has-error' : '' }}">
                    <label for="CONTRACT_DATE">วันที่สัญญา</label>
                    {{ Form::text('CONTRACT_DATE', $utils->thaidate($contract->CONTRACT_DATE), ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_START_DATE') ? ' has-error' : '' }}">
                    <label for="CONTRACT_START_DATE">วันที่รับทุน</label>
                    {{ Form::text('CONTRACT_START_DATE', $utils->thaidate($contract->CONTRACT_START_DATE), ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_START_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_START_DATE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('CONTRACT_FINISH_DATE') ? ' has-error' : '' }}">
                    <label for="CONTRACT_FINISH_DATE">วันที่สิ้นสุดรับทุน</label>
                    {{ Form::text('CONTRACT_FINISH_DATE', $utils->thaidate($contract->CONTRACT_FINISH_DATE), ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('CONTRACT_FINISH_DATE'))
                        <span class="text-danger"><strong>{{ $errors->first('CONTRACT_FINISH_DATE') }}</strong></span>
                    @endif
                </div>


                <div class="form-group {{ $errors->has('STUDENT_TYPE_ID') ? ' has-error' : '' }}">
                    <label for="STUDENT_TYPE_ID">ประเภทผู้รับทุน</label>
                    {{ Form::text('STUDENT_TYPE_ID', $contract->studentType->STUDENT_TYPE_NAME, ['class'=>'form-control datetimepicker', 'readonly'=>'']) }}
                    @if ($errors->has('STUDENT_TYPE_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('STUDENT_TYPE_ID') }}</strong></span>
                    @endif
                </div>
            </div>

            <div class="col-sm-6 border-left">
                <div class="form-group {{ $errors->has('CITIZEN_ID') ? ' has-error' : '' }}">
                    <label for="CITIZEN_ID">เลขบัตรประชาชน</label>
                    {{ Form::text('CITIZEN_ID', old('CITIZEN_ID'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('CITIZEN_ID'))
                        <span class="text-danger"><strong>{{ $errors->first('CITIZEN_ID') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('STUDENT_NAME') ? ' has-error' : '' }}">
                    <label for="STUDENT_NAME">ชื่อ - สกุล</label>
                    {{ Form::text('STUDENT_NAME', old('STUDENT_NAME'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('STUDENT_NAME'))
                        <span class="text-danger"><strong>{{ $errors->first('STUDENT_NAME') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('ADDRESS') ? ' has-error' : '' }}">
                    <label for="ADDRESS">ที่อยู่</label>
                    {{ Form::text('ADDRESS', old('ADDRESS'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('ADDRESS'))
                        <span class="text-danger"><strong>{{ $errors->first('ADDRESS') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('PHONE') ? ' has-error' : '' }}">
                    <label for="PHONE">หมายเลขโทรศัพย์</label>
                    {{ Form::text('PHONE', old('PHONE'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('PHONE'))
                        <span class="text-danger"><strong>{{ $errors->first('PHONE') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('EMAIL') ? ' has-error' : '' }}">
                    <label for="EMAIL">ที่อยู่อีเมล์</label>
                    {{ Form::text('EMAIL', old('EMAIL'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('EMAIL'))
                        <span class="text-danger"><strong>{{ $errors->first('EMAIL') }}</strong></span>
                    @endif
                </div>

                <div class="form-group {{ $errors->has('ADVISOR') ? ' has-error' : '' }}">
                    <label for="ADVISOR">ที่ปรึกษา</label>
                    {{ Form::text('ADVISOR', old('ADVISOR'), ['class'=>'form-control', 'readonly'=>'']) }}
                    @if ($errors->has('ADVISOR'))
                        <span class="text-danger"><strong>{{ $errors->first('ADVISOR') }}</strong></span>
                    @endif
                </div>

            </div>

            <div class="col-sm-12">
                <div class="form-group">
                <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                    กลับ
                </button>
                </div>
            </div>

        </div>
    </div>
</div>
{{ Form::close() }}

{{ Form::model($budget) }}
{{ Form::hidden('CONTRACT_ID') }}
{{-- `CONTRACT_ID` int(11) DEFAULT NULL,
  `BUDGET_TYPE_ID` int(11) DEFAULT NULL,
  `AMOUNT` decimal(15,3) DEFAULT NULL,
  `BUDGET_DATE` datetime DEFAULT NULL,
  `REF_NO` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `CREATE_BY` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUDGET_DESC` varchar(1024) COLLATE utf8_unicode_ci DEFAULT '',
 --}}

<div class="col-sm-4">
    <div class="panel panel-default">
        <div class="panel-heading">บันทึกรายการค่าใช้จ่าย</div>
        <div class="panel-body">
            <div class="form-group">
                <label for="BUDGET_TYPE_ID">ประเภทค่าใช้จ่าย</label>
                {{ Form::select('BUDGET_TYPE_ID', $budgetType, $contract->BUDGET_TYPE_ID, ['class'=>'form-control selectpicker'])}}
            </div>

            <div class="form-group {{ $errors->has('BUDGET_DATE') ? ' has-error' : '' }}">
                <label for="BUDGET_DATE">วันที่</label>
                {{ Form::text('BUDGET_DATE', old('BUDGET_DATE'), ['class'=>'form-control datetimepicker']) }}
                @if ($errors->has('BUDGET_DATE'))
                    <span class="text-danger"><strong>{{ $errors->first('BUDGET_DATE') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('BUDGET_DESC') ? ' has-error' : '' }}">
                <label for="BUDGET_DESC">รายละเอียด</label>
                {{ Form::text('BUDGET_DESC', old('BUDGET_DESC'), ['class'=>'form-control']) }}
                @if ($errors->has('BUDGET_DESC'))
                    <span class="text-danger"><strong>{{ $errors->first('BUDGET_DESC') }}</strong></span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('AMOUNT') ? ' has-error' : '' }}">
                <label for="AMOUNT">จำนวนเงิน</label>
                {{ Form::text('AMOUNT', old('AMOUNT'), ['class'=>'form-control']) }}
                @if ($errors->has('AMOUNT'))
                    <span class="text-danger"><strong>{{ $errors->first('AMOUNT') }}</strong></span>
                @endif
            </div>

            <div class="form-group">
                <button class="btn btn-primary" id="btnAdd" class="btnAdd">เพิ่มรายการค่าใช้จ่าย</button>
            </div>

        </div>
    </div>
</div>
{{ Form::close() }}

<div class="col-sm-8">
    <div class="text-right">
        รวมทั้งสิ้น {{ $contract->budgets->sum('AMOUNT') }} บาท
    </div>
    <table class="table table-bordered table-hover">
        <tr class="active">
            <th>วันที่</th>
            <th>ประเภทค่าใช้จ่าย</th>
            <th>รายละเอียด</th>
            <th class="text-right">จำนวนเงิน(บาท)</th>
        </tr>

        @foreach ($contract->budgets as $b)
            <tr>
                <td>{{ $utils->thaidate($b->BUDGET_DATE) }}</td>
                <td>{{ $b->budgetType->BUDGET_TYPE_NAME }}</td>
                <td>{{ $b->BUDGET_DESC }}</td>
                <td class="text-right">{{ $b->AMOUNT }}</td>
            </tr>
        @endforeach
    </table>
</div>




@endsection
