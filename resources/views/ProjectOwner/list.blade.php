@extends ('layouts.template')

@section('title')
  หน่วยงานเจ้าของโครงการ
@endsection

@section('content')
  <style>
  ul.pagination{
    margin: 1px;
  }
  </style>

  {!! Form::open(['method' => 'post']) !!}
  <div class="row">
    <div class="col-sm-4">
      <input type="text" name="TEXT_SEARCH" id="TEXT_SEARCH" value="{{ old('TEXT_SEARCH', $text_search) }}"
        class="form-control" placeholder="ป้อนคำค้นหา">
    </div>
    <div class="col-sm-3">
      <button type="submit" name="btnSearch" class="btn btn-default" >ค้นหา</button>
    </div>

    <div class="col-sm-5 text-right">
      <p>
        @if ($owners->total() > 0)
          {{ $owners->firstItem() }} - {{ $owners->lastItem() }}
        @endif
        จาก {{ $owners->total() }} รายการ
        <br>
        {{ $owners->links() }}
        <a href="{{ url('projectowner/create') }}" class='btn btn-primary'>
          <span class='glyphicon glyphicon-plus-sign'></span> เพิ่มรายการ
        </a>
      </p>
    </div>
  </div>
  {!! Form::close() !!}

  <div class="row">
    <div class="col-sm-12">
      <table class="table table-bordered table-hover">
        <tr class="active">
          <th>เจ้าของโครงการ</th>
        </tr>
        @foreach ($owners as $owner)
            <tr>
              <td>
                <a href="{{ url('/projectowner/info') }}/{{ $owner->PROJECT_OWNER_ID }}">
                  {{ $owner->PROJECT_OWNER_NAME }}
                </a>
              </td>
            </tr>
        @endforeach
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 text-right">
      {{ $owners->links() }}
    </div>
  </div>

@endsection
