@extends ('layouts.template')

@section('title')
    หน่วยงานเจ้าของโครงการ
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}
</style>

<script type="text/javascript">
$(function(){
    $('#btnDelete').click(function(){
        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
            if (result){
                // window.location = '{{ url('/projectowner/delete') }}/{{ old('PROJECT_ID', $owner->PROJECT_ID) }}';
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ url('/projectowner') }}';
    })

})
</script>

<div class="col-sm-6">


    <div class="form-group {{ $errors->has('PROJECT_OWNER_NAME') ? ' has-error' : '' }}">
        <label for="PROJECT_OWNER_NAME">หน่วยงานเจ้าของโครงการ</label>
        <input type="text" class="form-control" id="PROJECT_OWNER_NAME" name="PROJECT_OWNER_NAME" readonly
        value='{{ old('PROJECT_OWNER_NAME', $owner->PROJECT_OWNER_NAME) }}'>

        @if ($errors->has('PROJECT_OWNER_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('PROJECT_OWNER_NAME') }}</strong></span>
        @endif
    </div>


    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">
            <button type="button" class="btn btn-default" id="btnDelete" name="btnDelete">
                <span class="glyphicon glyphicon-remove-circle"></span> ลบ
            </button>
        </div>
    </div>
</div>


@endsection
