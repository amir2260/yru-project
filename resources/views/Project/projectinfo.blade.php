<div class="panel panel-default">
	<div class="panel-heading">
		<h4>รายละเอียดโครงการ</h4>
	</div>
	<div class="panel-body">
		
			<div class="col-sm-12">
			
			    <dl class="dl-horizontal">
			        <dt>รหัสโครงการ</dt>
			        <dd>
			            <div class="form-group {{ $errors->has('PROJECT_CODE') ? ' has-error' : '' }}">
			                {{ Form::text('PROJECT_CODE', $project->PROJECT_CODE , ['class'=>'form-control', 'readonly'=>'']) }}
			                @if ($errors->has('PROJECT_CODE'))
			                    <span class="text-danger"><strong>{{ $errors->first('PROJECT_CODE') }}</strong></span>
			                @endif
			            </div>
			        </dd>
			
			        <dt>โครงการ</dt>
			        <dd>
			            <div class="form-group {{ $errors->has('PROJECT_NAME') ? ' has-error' : '' }}">
			                {{ Form::text('PROJECT_NAME', $project->PROJECT_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
			
			                @if ($errors->has('PROJECT_NAME'))
			                    <span class="text-danger"><strong>{{ $errors->first('PROJECT_NAME') }}</strong></span>
			                @endif
			            </div>
			        </dd>
			
			        <dt>ปีการศึกษา</dt>
			        <dd>
			            <div class="form-group {{ $errors->has('TERM_ID') ? ' has-error' : '' }}">
			                {{ Form::text('TERM_ID', old ('TERM_ID', $project->term->TERM_NAME ), ['class'=>'form-control', 'readonly'=>'']) }}
			
			                @if ($errors->has('TERM_ID'))
			                    <span class="text-danger"><strong>{{ $errors->first('TERM_ID') }}</strong></span>
			                @endif
			            </div>
			        </dd>
			
			        <dt>หน่วยงาน</dt>
			        <dd>
			            <div class="form-group {{ $errors->has('DEPARTMENT_ID') ? ' has-error' : '' }}">
			                {{ Form::text('DEPARTMENT_ID', old ('DEPARTMENT_ID', $project->department->DEPARTMENT_DESC ), ['class'=>'form-control', 'readonly'=>'']) }}
			
			                @if ($errors->has('DEPARTMENT_ID'))
			                    <span class="text-danger"><strong>{{ $errors->first('DEPARTMENT_ID') }}</strong></span>
			                @endif
			            </div>
			        </dd>
			
			        <dt>งบประมาณที่ได้รับ(บาท)</dt>
			        <dd>
			            <div class="form-group {{ $errors->has('COST') ? ' has-error' : '' }}">
			                {{ Form::text('COST', old ('COST', number_format($project->COST, 2)), ['class'=>'form-control', 'readonly'=>'']) }}
			
			                @if ($errors->has('COST'))
			                    <span class="text-danger"><strong>{{ $errors->first('COST') }}</strong></span>
			                @endif
			            </div>
			        </dd>						       
			    </dl>
						    
			</div>		
		
				
	</div>
</div>
