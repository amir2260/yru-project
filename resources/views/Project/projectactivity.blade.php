@extends ('layouts.template')

@section('title')
    ข้อมูลโครงการ
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

ul, ol{
	margin-top: 0;
	margin-bottom: 0;
}
</style>

<script type="text/javascript">
$(function(){
    $('#btnDelete').click(function(){
        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
            if (result){
                window.location = '{{ url('/project/delete') }}/{{ old('PROJECT_ID', $project->PROJECT_ID) }}';
            }
        })
    })

    $('.removeLink').click(function(){
        var id = $(this).data('id');

        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
            if (result){
                window.location = '{{ url('/project/deleteaction') }}/' + id.toString();
            }
        })
    })

    $('#btnSend').click(function(){
        bootbox.confirm('ต้องการส่งไปยังกองนโยบายและแผน เพื่อทำการพิจารณาใช่หรือไม่ ', function(result){
            if (result){
                window.location = '{{ action('ProjectController@send', $project->PROJECT_ID) }}';
            }
        })
    })

    $('#btnApprove').click(function(){
        bootbox.confirm('อนุมัติโครงการ ใช่หรือไม่ ', function(result){
            if (result){
                window.location = '{{ action('ProjectController@approve', $project->PROJECT_ID) }}';
            }
        })
    })

    $('#btnReject').click(function(){
        bootbox.confirm('ไม่อนุมัติโครงการ ใช่หรือไม่ ', function(result){
            if (result){
                window.location = '{{ action('ProjectController@reject', $project->PROJECT_ID) }}';
            }
        })
    })

    $('#btnReturn').click(function(){
        bootbox.confirm('ส่งกลับเพื่อทำการปรับแก้รายละเอียด ใช่หรือไม่ ', function(result){
            if (result){
                window.location = '{{ action('ProjectController@returnProject', $project->PROJECT_ID) }}';
            }
        })
    })

    $('#btnCompleted').click(function(){
        bootbox.confirm('เสร็จสิ้นการดำเนินโครงการ ใช่หรือไม่ ', function(result){
            if (result){
                // window.location = '{{ action('ProjectController@returnProject', $project->PROJECT_ID) }}';
            }
        })
    })

    $('#btnCancel').click(function(){
        bootbox.confirm('ยกเลิกโครงการนี้ ใช่หรือไม่ ', function(result){
            if (result){
                // window.location = '{{ action('ProjectController@returnProject', $project->PROJECT_ID) }}';
            }
        })
    })

    $('#btnEdit').click(function(){
        window.location = '{{ url('/project/edit') }}/{{ old('PROJECT_ID', $project->PROJECT_ID) }}';
    })

    $('#btnAddActivity').click(function(){
    	window.location = '{{ action('ProjectController@addActivity', $project->PROJECT_ID) }}';
    })

    $('.btnEditActivity').click(function(){
        var id = $(this).data('id');
    	window.location = '{{ url('/project/editactivity') }}/' + id.toString();
    })



    $('#btnBack').click(function(){
        window.location = '{{ url('/project') }}';
    })

})
</script>


{{ Form::model($project) }}
<div class="panel panel-default">
	<div class="panel-body">
		<dl class="dl-horizontal">
		<dt>รหัสโครงการ</dt>
			<dd>
				<div class="form-group {{ $errors->has('PROJECT_CODE') ? ' has-error' : '' }}">
					{{ Form::text('PROJECT_CODE', old ('PROJECT_CODE'), ['class'=>'form-control', 'readonly'=>'']) }}
					@if ($errors->has('PROJECT_CODE'))
						<span class="text-danger"><strong>{{ $errors->first('PROJECT_CODE') }}</strong></span>
					@endif
				</div>
			</dd>

			<dt>โครงการ</dt>
			<dd>
				<div class="form-group {{ $errors->has('PROJECT_NAME') ? ' has-error' : '' }}">
					{{ Form::text('PROJECT_NAME', old ('PROJECT_NAME'), ['class'=>'form-control', 'readonly'=>'']) }}

					@if ($errors->has('PROJECT_NAME'))
						<span class="text-danger"><strong>{{ $errors->first('PROJECT_NAME') }}</strong></span>
					@endif
				</div>
			</dd>

			<dt>ปีการศึกษา</dt>
			<dd>
				<div class="form-group {{ $errors->has('TERM_ID') ? ' has-error' : '' }}">
					{{ Form::text('TERM_ID', old ('TERM_ID', $project->term->TERM_NAME ), ['class'=>'form-control', 'readonly'=>'']) }}

					@if ($errors->has('TERM_ID'))
						<span class="text-danger"><strong>{{ $errors->first('TERM_ID') }}</strong></span>
					@endif
				</div>
			</dd>

			<dt>หน่วยงาน</dt>
			<dd>
				<div class="form-group {{ $errors->has('DEPARTMENT_ID') ? ' has-error' : '' }}">
					{{ Form::text('DEPARTMENT_ID', old ('DEPARTMENT_ID', $project->department->DEPARTMENT_DESC ), ['class'=>'form-control', 'readonly'=>'']) }}

					@if ($errors->has('DEPARTMENT_ID'))
						<span class="text-danger"><strong>{{ $errors->first('DEPARTMENT_ID') }}</strong></span>
					@endif
				</div>
			</dd>

			<dt>งบประมาณที่ได้รับ(บาท)</dt>
			<dd>
				<div class="form-group {{ $errors->has('COST') ? ' has-error' : '' }}">
					{{ Form::text('COST', number_format($project->getRequestAmount(),2), ['class'=>'form-control', 'readonly'=>'']) }}

					@if ($errors->has('COST'))
						<span class="text-danger"><strong>{{ $errors->first('COST') }}</strong></span>
					@endif
				</div>
			</dd>
			<dt>งบประมาณที่ใช้ไป(บาท)</dt>
			<dd>
				<div class="form-group {{ $errors->has('COST') ? ' has-error' : '' }}">
					{{ Form::text('COST', number_format($project->getUsageAmount(),2), ['class'=>'form-control', 'readonly'=>'']) }}

					@if ($errors->has('COST'))
						<span class="text-danger"><strong>{{ $errors->first('COST') }}</strong></span>
					@endif
				</div>
			</dd>
			<dt>งบประมาณคงเหลือ(บาท)</dt>
			<dd>
				<div class="form-group {{ $errors->has('COST') ? ' has-error' : '' }}">
					{{ Form::text('COST', number_format($project->getRequestAmount() - $project->getUsageAmount(),2), ['class'=>'form-control', 'readonly'=>'']) }}

					@if ($errors->has('COST'))
						<span class="text-danger"><strong>{{ $errors->first('COST') }}</strong></span>
					@endif
				</div>
			</dd>

		</dl>
		<div class="col-sm-9">
            @if ($project->projectStatus->PROJECT_STATUS_ID == 1) {{-- รายการที่ยังไม่ส่ง --}}
                <button type="button" class="btn btn-primary btn-xs" id="btnSend" name="btnSend">
                    <span class="glyphicon glyphicon-send"></span>
    				ส่งเพื่อพิจารณาอนุมัติ
    			</button>
            @endif

            @if ($project->projectStatus->PROJECT_STATUS_ID == 2) {{-- รายการที่รอการอนุมัติ --}}
                <button type="button" class="btn btn-primary btn-xs" id="btnApprove" name="btnApprove">
                    <span class="glyphicon glyphicon-ok-circle"></span>
    				อนุมัติโครงการ
    			</button>

                <button type="button" class="btn btn-danger btn-xs" id="btnReject" name="btnReject">
                    <span class="glyphicon glyphicon-ban-circle"></span>
    				ไม่อนุมัติโครงการ
    			</button>

                <button type="button" class="btn btn-default btn-xs" id="btnReturn" name="btnReturn">
    				ส่งกลับเพื่อดำเนินการปรับแก้
    			</button>

            @endif

            @if ($project->projectStatus->PROJECT_STATUS_ID == 3) {{-- รายการที่ผ่านการอนุมัติ --}}
                <button type="button" class="btn btn-success btn-xs" id="btnCompleted" name="btnCompleted">
                    <span class="glyphicon glyphicon-ok-circle"></span>
    				ปิดโครงการ
    			</button>

                <button type="button" class="btn btn-danger btn-xs" id="btnCancel" name="btnCancel">
                    <span class="glyphicon glyphicon-ban-circle"></span>
    				ยกเลิกโครงการ
    			</button>

            @endif

            @if ($project->projectStatus->PROJECT_STATUS_ID == 1 || $project->projectStatus->PROJECT_STATUS_ID == 5) {{-- รายการที่ยังไม่ส่ง --}}
    			<button type="button" class="btn btn-default btn-xs" id="btnEdit" name="btnEdit">
    				<span class="glyphicon glyphicon-edit"></span> แก้ไขรายละเอียด
    			</button>
            @endif

			<button type="button" class="btn btn-default btn-xs" id="btnBack" name="btnBack">
				กลับ
			</button>
		</div>
		<div class="col-sm-3 text-right" >
            @if ($project->projectStatus->PROJECT_STATUS_ID == 1 || $project->projectStatus->PROJECT_STATUS_ID == 5) {{-- รายการที่ยังไม่ส่ง --}}
    			<button type="button" class="btn btn-default btn-xs" id="btnDelete" name="btnDelete">
    				<span class="glyphicon glyphicon-remove-circle"></span> ลบ
    			</button>
            @endif
		</div>

	</div>
</div>

<ul class="nav nav-tabs">
  	<li class="active"><a data-toggle="tab" href="#home">กิจกรรม</a></li>
  	<li><a data-toggle="tab" href="#menu1">รายละเอียดโครงการ</a></li>
  	<li><a data-toggle="tab" href="#menu2">ประวัติการดำเนินการ</a></li>
</ul>

<div class="tab-content">
  	<div id="home" class="tab-pane fade in active">
    	<h3>
			รายละเอียดการดำเนินกิจกรรม
            @if ($project->projectStatus->PROJECT_STATUS_ID == 1 || $project->projectStatus->PROJECT_STATUS_ID == 5) {{-- รายการที่ยังไม่ส่ง --}}
    			<button type="button" class="btn btn-default btn-xs" id="btnAddActivity" name="btnAddActivity">
    				<span class="glyphicon glyphicon-edit"></span> เพิ่มกิจกรรม
    	        </button>
            @endif
			<hr>
		</h3>
		<p>
    		@foreach ($project->activities as $ac)
                @if ($project->projectStatus->PROJECT_STATUS_ID == 1 || $project->projectStatus->PROJECT_STATUS_ID == 5) {{-- รายการที่ยังไม่ส่ง --}}
    				<div class="col-sm-12">
    					<h5>
    						{{ $ac->ACTIVITY_NAME }}
    						<br />
    						<button type="button" class="btn btn-default btn-xs btnEditActivity" data-id="{{ $ac->ACTIVITY_ID }}" >
    							แก้ไขชื่อกิจกรรม
    						</button>
    						<button type="button" class="btn btn-default btn-xs btnDeleteActivity" data-id="{{ $ac->ACTIVITY_ID }}" >
    							ลบกิจกรรม
    						</button>
    						|
    						<a href="{{ action('ProjectController@addActivityDetail', $ac->ACTIVITY_ID) }}" class="btn btn-default btn-xs" >
    							บันทึกรายการกิจกรรมและค่าใช้จ่าย
    						</a>
    					</h5>
    				</div>
                @endif

				@if ($ac->activityDetails->count() > 0)
				<div class="col-sm-12">
					<table class="table table-bordered">
						<thead>
							<tr class="active">
								<th rowspan="2" colspan="1" class="text-center col-sm-5">งบรายจ่าย</th>
                                <th rowspan="2" colspan="1" class="text-center col-sm-2">หลักสูตร</th>
								<th rowspan="1" colspan="5" class="text-center">งบรายจ่าย</th>
							</tr>
							<tr class="active">
								@foreach ($budgetGroups as $b)
									<th class="text-right" rowspan="1" colspan="1">{{ $b->BUDGET_GROUP_NAME }}</th>
								@endforeach
								<th class="text-right" rowspan="1" colspan="1">รวม</th>
							</tr>
						</thead>
						<tbody>

							@foreach ($budgetTypes as $bType)
							@if (
								$ac->activityDetails->filter(function($a) use ($bType){
									return $a->budgetDetail->BUDGET_TYPE_ID == $bType->BUDGET_TYPE_ID;
								})->count() > 0
							)

								<tr style="background-color:lightgray">
									<td><strong>{{ $bType->BUDGET_TYPE_NAME }}</strong></td>
                                    <td class="text-center">-</td>
									@foreach ($budgetGroups as $b)
										<td class="text-right" rowspan="1" colspan="1">
											<strong>
											{{
												number_format($ac->activityDetails->filter(function($a) use($bType){
													return ($a->budgetDetail->BUDGET_TYPE_ID == $bType->BUDGET_TYPE_ID);
												})->sum(function($s) use($b){
													return $s->activityBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('BUDGET_AMOUNT');
												}),2)
											}}
											</strong>
                                            <br>
                                            <small>
                                                <span class="text-danger">
                                                    ใช้ไป
                                                    {{
                                                        number_format(
                                                            $ac->activityDetails->filter(function($a) use($bType){
            													return ($a->budgetDetail->BUDGET_TYPE_ID == $bType->BUDGET_TYPE_ID);
            												})
                                                            ->sum(function ($d) use($b){
                                                                return $d->actions->sum(function ($act) use ($b){
                                                                        return $act->actionBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('AMOUNT');
                                                                    });
                                                            })
                                                        , 2)
                                                    }}
                                                </span>
                                                <br>
                                                <span class="text-primary">
                                                    คงเหลือ
                                                    {{
                                                        number_format(
                                                            $ac->activityDetails->filter(function($a) use($bType){
            													return ($a->budgetDetail->BUDGET_TYPE_ID == $bType->BUDGET_TYPE_ID);
            												})->sum(function($s) use($b){
            													return $s->activityBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('BUDGET_AMOUNT');
            												}) -
                                                            $ac->activityDetails->filter(function($a) use($bType){
            													return ($a->budgetDetail->BUDGET_TYPE_ID == $bType->BUDGET_TYPE_ID);
            												})
                                                            ->sum(function ($d) use($b){
                                                                return $d->actions->sum(function ($act) use ($b){
                                                                        return $act->actionBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('AMOUNT');
                                                                    });
                                                            })

                                                        , 2)
                                                    }}
                                                </span>
                                            </small>
										</td>
									@endforeach

									<td class="text-right" rowspan="1" colspan="1">
										<strong>
										{{
                                            number_format(
    											$ac->activityDetails->filter(function($a) use($bType){
    												return ($a->budgetDetail->BUDGET_TYPE_ID == $bType->BUDGET_TYPE_ID);
    											})->sum(function($s) use($b){
    												return $s->activityBudgets->sum('BUDGET_AMOUNT');
    											}),2)
										}}
										</strong>
                                        <br>
                                        <small>
                                            <span class="text-danger">
                                                ใช้ไป
                                                {{
                                                    number_format(
                                                        $ac->activityDetails->filter(function($a) use($bType){
                                                            return ($a->budgetDetail->BUDGET_TYPE_ID == $bType->BUDGET_TYPE_ID);
                                                        })
                                                        ->sum(function ($d) use($b){
                                                            return $d->actions->sum(function ($act) use ($b){
                                                                    return $act->actionBudgets->sum('AMOUNT');
                                                                });
                                                        })
                                                    , 2)
                                                }}
                                            </span>
                                            <br>
                                            <span class="text-primary">
                                                คงเหลือ
                                                {{
                                                    number_format(
                                                        $ac->activityDetails->filter(function($a) use($bType){
            												return ($a->budgetDetail->BUDGET_TYPE_ID == $bType->BUDGET_TYPE_ID);
            											})->sum(function($s) use($b){
            												return $s->activityBudgets->sum('BUDGET_AMOUNT');
            											}) -
                                                        $ac->activityDetails->filter(function($a) use($bType){
                                                            return ($a->budgetDetail->BUDGET_TYPE_ID == $bType->BUDGET_TYPE_ID);
                                                        })
                                                        ->sum(function ($d) use($b){
                                                            return $d->actions->sum(function ($act) use ($b){
                                                                    return $act->actionBudgets->sum('AMOUNT');
                                                                });
                                                        })

                                                    , 2)
                                                }}
                                            </span>
                                        </small>
									</td>
								</tr>

								@foreach ($bType->budgetDetails as $bDetail)
								@if ($ac->activityDetails->where('BUDGET_DETAIL_ID', $bDetail->BUDGET_DETAIL_ID)->count() > 0)
									<tr>
										<td><strong> &nbsp; &nbsp; {{ $bDetail->BUDGET_DETAIL_NAME }}</strong></td>
                                        <td class="text-center">-</td>
										@foreach ($budgetGroups as $b)
											<td class="text-right" rowspan="1" colspan="1">
												<strong>
												{{
													number_format(
														$ac->activityDetails->where('BUDGET_DETAIL_ID', $bDetail->BUDGET_DETAIL_ID)->sum(function ($d) use($b){
															return $d->activityBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('BUDGET_AMOUNT');
														}), 2)
												}}
												</strong>
                                                <br>
                                                <small>
                                                    <span class="text-danger">
                                                        ใช้ไป
                                                        {{
        													number_format(
                                                                $ac->activityDetails->where('BUDGET_DETAIL_ID', $bDetail->BUDGET_DETAIL_ID)->sum(function ($d) use($b){
                                                                    return $d->actions->sum(function ($act) use ($b){
                                                                            return $act->actionBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('AMOUNT');
                                                                        });
        														})
                                                            , 2)
        												}}
                                                    </span>
                                                    <br>
                                                    <span class="text-primary">
                                                        คงเหลือ
                                                        {{
        													number_format(
                                                                $ac->activityDetails->where('BUDGET_DETAIL_ID', $bDetail->BUDGET_DETAIL_ID)->sum(function ($d) use($b){
        															return $d->activityBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('BUDGET_AMOUNT');
        														})
                                                                - $ac->activityDetails->where('BUDGET_DETAIL_ID', $bDetail->BUDGET_DETAIL_ID)->sum(function ($d) use($b){
                                                                    return $d->actions->sum(function ($act) use ($b){
                                                                            return $act->actionBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('AMOUNT');
                                                                        });
        														})

                                                            , 2)
        												}}
                                                    </span>
                                                </small>
											</td>
										@endforeach

										<td class="text-right" rowspan="1" colspan="1">
											<strong>
											{{
												number_format($ac->activityDetails->where('BUDGET_DETAIL_ID', $bDetail->BUDGET_DETAIL_ID)->sum(function ($d){
														return $d->activityBudgets->sum('BUDGET_AMOUNT');
													}), 2)
											}}
											</strong>
                                            <br>
                                            <small>
                                                <span class="text-danger">
                                                    ใช้ไป
                                                    {{
                                                        number_format(
                                                            $ac->activityDetails->where('BUDGET_DETAIL_ID', $bDetail->BUDGET_DETAIL_ID)->sum(function ($d) use($b){
                                                                return $d->actions->sum(function ($act) use ($b){
                                                                        return $act->actionBudgets->sum('AMOUNT');
                                                                    });
                                                            })
                                                        , 2)
                                                    }}
                                                </span>
                                                <br>
                                                <span class="text-primary">
                                                    คงเหลือ
                                                    {{
                                                        number_format(
                                                            $ac->activityDetails->where('BUDGET_DETAIL_ID', $bDetail->BUDGET_DETAIL_ID)->sum(function ($d) use($b){
                                                                return $d->activityBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('BUDGET_AMOUNT');
                                                            })
                                                            - $ac->activityDetails->where('BUDGET_DETAIL_ID', $bDetail->BUDGET_DETAIL_ID)->sum(function ($d) use($b){
                                                                return $d->actions->sum(function ($act) use ($b){
                                                                        return $act->actionBudgets->sum('AMOUNT');
                                                                    });
                                                            })

                                                        , 2)
                                                    }}
                                                </span>
                                            </small>
										</td>
									</tr>

									<?php
									$activityDetails = $ac->activityDetails->filter(function($a) use($bDetail){
										return $a->BUDGET_DETAIL_ID == $bDetail->BUDGET_DETAIL_ID;
									})
									?>

									@foreach ($activityDetails as $de)
                                        {{-- แสดงชื่อกิจกรรม--}}
										<tr>
											<td>
												<ul>
                                                    <li>
                                                    {{ $de->ACTIVITY_DETAIL_NAME }}
                                                    @if ($project->projectStatus->PROJECT_STATUS_ID == 3)
														@if (Auth::user()->staff->canAccessActivity($de->ACTIVITY_DETAIL_ID))
                                                        <br>
														<small>
															<small></small>
                                                        		<a href="{{ action('ProjectController@addAction', $de->ACTIVITY_DETAIL_ID) }}">+รายงานการจัดกิจกรรม/การใช้งบระมาณ</a>
															</small>
														</small>
														@endif
                                                    @endif
                                                    </li>
                                                </ul>
											</td>
                                            <td>{{ isset($de->course) ? $de->course->COURSE_NAME : "" }}</td>
											@foreach ($budgetGroups as $b)
												<td class="text-right" rowspan="1" colspan="1">
													{{
														number_format($de->activityBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('BUDGET_AMOUNT'),2)
													}}
                                                    <br>

                                                    <small>
                                                        <span class="text-danger">
                                                        ใช้ไป
                                                        {{
                                                            number_format($de->actions->sum(function ($act) use ($b){
                                                                    return $act->actionBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('AMOUNT');
                                                                })
                                                            ,2)
                                                        }}
                                                        </span>
                                                        <br>
                                                        <span class="text-primary">
                                                        คงเหลือ
                                                        {{ number_format(
                                                            $de->activityBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('BUDGET_AMOUNT')
                                                            - $de->actions->sum(function ($act) use ($b){
                                                                    return $act->actionBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('AMOUNT');
                                                                })
                                                            ,2)
                                                        }}
                                                        </span>
                                                    </small>
												</td>
											@endforeach

											<td class="text-right" rowspan="1" colspan="1">
												{{ number_format($de->activityBudgets->sum('BUDGET_AMOUNT'),2) }}
                                                <br>
                                                <small>
                                                    <span class="text-danger">
                                                    ใช้ไป
                                                    {{
                                                        number_format($de->actions->sum(function ($act) use ($b){
                                                                return $act->actionBudgets->sum('AMOUNT');
                                                            })
                                                        ,2)
                                                    }}
                                                    </span>
                                                    <br>
                                                    <span class="text-primary">
                                                    คงเหลือ
                                                    {{ number_format(
                                                        $de->activityBudgets->sum('BUDGET_AMOUNT')
                                                        - $de->actions->sum(function ($act) use ($b){
                                                                return $act->actionBudgets->sum('AMOUNT');
                                                            })
                                                        ,2)
                                                    }}
                                                    </span>
                                                </small>
											</td>
										</tr>

                                        {{-- แสดงการใช้เงินหรือจัดกิจกรรม--}}
                                        @if ($de->actions->count() > 0)
                                            @foreach ($de->actions as $action)
                                            <tr>
                                                <td style="padding-left:60px">
                                                    <a href="#" data-id='{{ $action->ACTION_ID }}' class='removeLink'>
                                                        <span class="glyphicon glyphicon-trash"> </span>
                                                    </a>

                                                    <a href="{{ action('ProjectController@editAction', $action->ACTION_ID ) }}">
                                                        {{ $action->ACTION_DETAIL }}
                                                    </a><br>
                                                </td>
                                                <td>
                                                    {{--  {{ $action->course->COURSE_NAME }}  --}}
                                                </td>
                                                @foreach ($budgetGroups as $b)
    												<td class="text-right text-danger" rowspan="1" colspan="1">
    													{{
    														number_format($action->actionBudgets->where('BUDGET_GROUP_ID', $b->BUDGET_GROUP_ID)->sum('AMOUNT'),2)
    													}}
    												</td>
    											@endforeach

    											<td class="text-right text-danger" rowspan="1" colspan="1">
                                                    {{
                                                        number_format($action->actionBudgets->sum('AMOUNT'),2)
                                                    }}
    											</td>
                                            </tr>
                                            @endforeach

                                            {{-- <table class="table table-hover">
                                                <thead>
                                                    <tr class="active">
                                                        <th colspan="2">รายละเอียดกิจกรรม</th>
                                                        <th class="text-right">จำนวนเงิน(บาท)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($de->actions as $action)
                                                        <tr>
                                                            <td>
                                                                <span class="glyphicon glyphicon-trash"> </span>
                                                                <a href="#">
                                                                    {{ $action->ACTION_NAME }}
                                                                </a>
                                                            </td>
                                                            <td>bbbb</td>
                                                            <td class="text-right">250.00</td>
                                                        </tr>
                                                    @endforeach

                                                </tbody>
                                            </table> --}}
                                        @endif

									@endforeach
								@endif
								@endforeach

							@endif
							@endforeach



						</tbody>
					</table>
					<br />
				</div>
				@endif
			@endforeach
		</p>
  	</div>
  	<div id="menu1" class="tab-pane fade">
    	<h3>
			รายละเอียดโครงการ
		</h3>
		<hr>
		<p>
    	{!! Form::model($project) !!}
			{{ Form::hidden('PROJECT_ID', $project->PROJECT_ID )}}
			<div class="col-sm-12">

			    <dl class="dl-horizontal">
			        <dt>หลักการและเหตุผล</dt>
			        <dd>
			            <div class="form-group {{ $errors->has('REASON') ? ' has-error' : '' }}">
			                {{ Form::textarea('REASON', old ('REASON'), ['class'=>'form-control', 'readonly'=>'']) }}

			                @if ($errors->has('REASON'))
			                    <span class="text-danger"><strong>{{ $errors->first('REASON') }}</strong></span>
			                @endif
			            </div>
			        </dd>

			        <dt>ผลที่คาดว่าจะได้รับ</dt>
			        <dd>
			            <div class="form-group {{ $errors->has('RESPACT') ? ' has-error' : '' }}">
			                {{ Form::textarea('RESPACT', old ('RESPACT'), ['class'=>'form-control', 'readonly'=>'']) }}

			                @if ($errors->has('RESPACT'))
			                    <span class="text-danger"><strong>{{ $errors->first('RESPACT') }}</strong></span>
			                @endif
			            </div>
			        </dd>

			        @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 1) as $cType)
			            <dt>{{ $cType->CATEGORY_TYPE_NAME }}</dt>
			            <dd>
			                <div class="col-sm-12">
			                    <div class="form-group">
									<ul>
			                        @foreach ($project->categories as $c)
			                            @if ($c->category->CATEGORY_TYPE_ID == 1)
											@if (($c->IS_SELECTED == 1))
												<li>
													{{ $c->category->CATEGORY_NAME }}	
												</li>
											@endif			                            
			                            @endif
			                        @endforeach
									</ul>
			                    </div>
			                </div>
			            </dd>
			        @endforeach

			        @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 2) as $cType)
			            <dt>{{ $cType->CATEGORY_TYPE_NAME }}</dt>
			            <dd>
			                <div class="col-sm-12">
			                    <div class="form-group">
			                        <ul>
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 2)
												@if (($c->IS_SELECTED == 1))
			                                	<li>
			                                    	{{ $c->category->CATEGORY_NAME }}
			                                	</li>
												@endif
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                </div>
			            </dd>
			        @endforeach

			        @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 3) as $cType)
			            <dt>{{ $cType->CATEGORY_TYPE_NAME }}</dt>
			            <dd>
			                <div class="col-sm-12">
			                    <div class="form-group">
			                        <ul>
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 3)
												@if (($c->IS_SELECTED == 1))
												<li>
													{{ $c->category->CATEGORY_NAME }}
												</li>
												@endif
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                </div>
			            </dd>
			        @endforeach

			        <dt>ผลกระทบ(IMPACT)</dt>
			        <dd>
			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 4) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul>
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 4 && ($c->IS_SELECTED == 1))
			                                <li>
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 5) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul>
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 5 && ($c->IS_SELECTED == 1))
			                                <li>
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 6) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul>
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 6 && ($c->IS_SELECTED == 1))
			                                <li>
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>
			        </dd>

			        <dt>กลยุทธฺ์</dt>
			        <dd>
			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 7) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul>
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 7 && ($c->IS_SELECTED == 1))
			                                <li>
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 8) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul>
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 8 && ($c->IS_SELECTED == 1))
			                                <li>
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 9) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul>
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 9 && ($c->IS_SELECTED == 1))
			                                <li>
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 10) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul>
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 10 && ($c->IS_SELECTED == 1))
			                                <li>
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>
			        </dd>
			    </dl>

			</div>
		{!! Form::close() !!}
		</p>
  	</div>
  	<div id="menu2" class="tab-pane fade">
    	<p>
			<div class="col-sm-4 border-left">
				<div class="form-group">
					<label for="CREATE_BY">บันทึกโดย</label>
					{{ Form::text('CREATE_BY', $project->CREATE_BY, ['class'=>'form-control', 'readonly'=>'' ]) }}
				</div>

				<div class="form-group">
					<label for="CREATE_BY">วันที่</label>
					{{ Form::text('CREATE_BY', $project->CREATE_DATE, ['class'=>'form-control', 'readonly'=>'' ]) }}
				</div>
			</div>
		</p>
  	</div>
</div>
{{ Form::close() }}

@endsection
