@extends ('layouts.template')

@section('title')
    {{ $title }}
    <br>
    <small>{{ $desc }}</small>
@endsection

@section('content')
<style>
ul.pagination{
    margin: 1px;
}
</style>

{!! Form::open(['method' => 'post']) !!}
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="TEXT_SEARCH">คำค้นหา</label>
            {{ Form::text('TEXT_SEARCH', $search['TEXT_SEARCH'], ['class'=>'form-control']) }}
        </div>
        <div class="form-group">
            <label for="TERM_ID">ปีงบประมาณ</label>
            {{ Form::select('TERM_ID', $search['terms'], null, ['class'=>'form-control']) }}
        </div>
        <div class="form-group">
            <button type="submit" name="btnSearch" class="btn btn-default" >ค้นหา</button>
        </div>
    </div>

    <div class="col-sm-6 text-right">
        <p>
            @if ($projects->total() > 0)
                {{ $projects->firstItem() }} - {{ $projects->lastItem() }}
            @endif
            จาก {{ $projects->total() }} รายการ
            <br>
            {{ $projects->links() }}

        </p>
    </div>
</div>
{!! Form::close() !!}

<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-hover">
            <thead>
                <tr class="active">
                    <th>โครงการ</th>
                    <th>รหัสโครงการ</th>
                    <th>ปีการศึกษา</th>
                    <th>หน่วยงาน</th>
                    <th>สถานะ</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($projects as $project)
                <tr>
                    <td>
                        <a href="{{ action('ProjectController@projectActivity', $project->PROJECT_ID) }}">
                            {{ $project->PROJECT_NAME }}
                        </a>
                    </td>
                    <td>{{ $project->PROJECT_CODE }}</td>
                    <td>{{ $project->term->TERM_NAME }}</td>
                    <td>{{ $project->department->DEPARTMENT_DESC }}</td>
                    <td>{{ $project->projectStatus->PROJECT_STATUS_NAME }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 text-right">
        {{ $projects->links() }}
    </div>
</div>

@endsection
