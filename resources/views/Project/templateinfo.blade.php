@extends ('layouts.template')

@section('title')
    ข้อมูลกรอบโครงการ
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

ul, ol{
	margin-top: 0;
	margin-bottom: 0;
}
</style>

<script type="text/javascript">
$(function(){

    $('#btnCreate').click(function(){
        bootbox.confirm("ต้องการสร้างโครงการใหม่จากกรอบโครงการนี้ใช่หรือไม่", function(result){
            if (result){
                window.location = '{{ action('ProjectController@createFromTemplate', $project->PROJECT_ID) }}';
            }
        })

    })

    $('#btnBack').click(function(){
        window.location = '{{ action('ProjectController@selectTemplate') }}';
    })

})
</script>

{{ Form::model($project) }}
<div class="panel panel-default">
	<div class="panel-body">
		<dl class="dl-horizontal">
		<dt>รหัสโครงการ</dt>
			<dd>
				<div class="form-group {{ $errors->has('PROJECT_CODE') ? ' has-error' : '' }}">
					{{ Form::text('PROJECT_CODE', old ('PROJECT_CODE'), ['class'=>'form-control', 'readonly'=>'']) }}
					@if ($errors->has('PROJECT_CODE'))
						<span class="text-danger"><strong>{{ $errors->first('PROJECT_CODE') }}</strong></span>
					@endif
				</div>
			</dd>

			<dt>โครงการ</dt>
			<dd>
				<div class="form-group {{ $errors->has('PROJECT_NAME') ? ' has-error' : '' }}">
					{{ Form::text('PROJECT_NAME', old ('PROJECT_NAME'), ['class'=>'form-control', 'readonly'=>'']) }}

					@if ($errors->has('PROJECT_NAME'))
						<span class="text-danger"><strong>{{ $errors->first('PROJECT_NAME') }}</strong></span>
					@endif
				</div>
			</dd>

			<dt>โครงการปีการศึกษา</dt>
			<dd>
				<div class="form-group {{ $errors->has('TERM_ID') ? ' has-error' : '' }}">
					{{ Form::text('TERM_ID', old ('TERM_ID', $project->term->TERM_NAME ), ['class'=>'form-control', 'readonly'=>'']) }}

					@if ($errors->has('TERM_ID'))
						<span class="text-danger"><strong>{{ $errors->first('TERM_ID') }}</strong></span>
					@endif
				</div>
			</dd>


		</dl>
		<div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnCreate" name="btnCreate">
				สร้างโครงการจากกรอบโครงการนี้
			</button>
			<button type="button" class="btn btn-default" id="btnBack" name="btnBack">
				กลับ
			</button>
		</div>
		<div class="col-sm-6 text-right" >
		</div>

	</div>
</div>

<ul class="nav nav-tabs">
  	<li class="active"><a data-toggle="tab" href="#home">กิจกรรม</a></li>
  	<li><a data-toggle="tab" href="#menu1">รายละเอียดโครงการ</a></li>
</ul>

<div class="tab-content">
  	<div id="home" class="tab-pane fade in active">
		<p>
            <ul>
    		@foreach ($project->activities as $ac)
                <li>{{ $ac->ACTIVITY_NAME }}</li>
			@endforeach
            </ul>
		</p>
  	</div>
  	<div id="menu1" class="tab-pane fade">
		<p>
    	{!! Form::model($project) !!}
			{{ Form::hidden('PROJECT_ID', $project->PROJECT_ID )}}
			<div class="col-sm-12">

			    <dl class="dl-horizontal">

			        @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 1) as $cType)
			            <dt>{{ $cType->CATEGORY_TYPE_NAME }}</dt>
			            <dd>
			                <div class="col-sm-12">
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                        @foreach ($project->categories as $c)
			                            @if ($c->category->CATEGORY_TYPE_ID == 1)
			                            <li>
			                                {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                {{ $c->category->CATEGORY_NAME }}
			                            </li>
			                            @endif
			                        @endforeach
			                        </ul>
			                    </div>
			                </div>
			            </dd>
			        @endforeach

			        @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 2) as $cType)
			            <dt>{{ $cType->CATEGORY_TYPE_NAME }}</dt>
			            <dd>
			                <div class="col-sm-12">
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 2)
			                                <li>
			                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                </div>
			            </dd>
			        @endforeach

			        @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 3) as $cType)
			            <dt>{{ $cType->CATEGORY_TYPE_NAME }}</dt>
			            <dd>
			                <div class="col-sm-12">
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 3)
			                                <li>
			                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                </div>
			            </dd>
			        @endforeach

			        <dt>ผลกระทบ(IMPACT)</dt>
			        <dd>
			            <div class="col-sm-4">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 4) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 4)
			                                <li>
			                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-4">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 5) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 5)
			                                <li>
			                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-4">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 6) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 6)
			                                <li>
			                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>
			        </dd>

			        <dt>กลยุทธฺ์</dt>
			        <dd>
			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 7) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 7)
			                                <li>
			                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 8) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 8)
			                                <li>
			                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 9) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 9)
			                                <li>
			                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>

			            <div class="col-sm-3">
			                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 10) as $cType)
			                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
			                    <div class="form-group">
			                        <ul class="list-unstyled">
			                            @foreach ($project->categories as $c)
			                                @if ($c->category->CATEGORY_TYPE_ID == 10)
			                                <li>
			                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, ($c->IS_SELECTED == 1), ['disabled']) }}
			                                    {{ $c->category->CATEGORY_NAME }}
			                                </li>
			                                @endif
			                            @endforeach
			                        </ul>
			                    </div>
			                @endforeach
			            </div>
			        </dd>
			    </dl>

			</div>
		{!! Form::close() !!}
		</p>
  	</div>

</div>
{{ Form::close() }}





@endsection
