@extends ('layouts.template')

@section('title')
    ข้อมูลโครงการ
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){

//     $('.selectpicker').selectpicker({
//         liveSearch : true
//     });

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ action('ProjectController@info',  $project->PROJECT_ID) }}';
    })
})
</script>
{!! Form::model($project) !!}
{{ Form::hidden('PROJECT_ID', $project->PROJECT_ID )}}
<div class="col-sm-8">

    <dl class="dl-horizontal">
        <dt>รหัสโครงการ</dt>
        <dd>
            <div class="form-group {{ $errors->has('PROJECT_CODE') ? ' has-error' : '' }}">
                {{ Form::text('PROJECT_CODE', old ('PROJECT_CODE'), ['class'=>'form-control']) }}
                @if ($errors->has('PROJECT_CODE'))
                    <span class="text-danger"><strong>{{ $errors->first('PROJECT_CODE') }}</strong></span>
                @endif
            </div>
        </dd>

        <dt>โครงการ</dt>
        <dd>
            <div class="form-group {{ $errors->has('PROJECT_NAME') ? ' has-error' : '' }}">
                {{ Form::text('PROJECT_NAME', old ('PROJECT_NAME'), ['class'=>'form-control']) }}

                @if ($errors->has('PROJECT_NAME'))
                    <span class="text-danger"><strong>{{ $errors->first('PROJECT_NAME') }}</strong></span>
                @endif
            </div>
        </dd>

        <dt>ปีการศึกษา</dt>
        <dd>
            <div class="form-group {{ $errors->has('TERM_ID') ? ' has-error' : '' }}">
                {{ Form::select('TERM_ID', $terms, $project->TERM_ID, ['class'=>'form-control selectpicker']) }}

                @if ($errors->has('TERM_ID'))
                    <span class="text-danger"><strong>{{ $errors->first('TERM_ID') }}</strong></span>
                @endif
            </div>
        </dd>

        <dt>หน่วยงาน</dt>
        <dd>
            <div class="form-group {{ $errors->has('DEPARTMENT_ID') ? ' has-error' : '' }}">
                {{ Form::select('DEPARTMENT_ID', $departments, $project->DEPARTMENT_ID, ['class'=>'form-control selectpicker']) }}

                @if ($errors->has('DEPARTMENT_ID'))
                    <span class="text-danger"><strong>{{ $errors->first('DEPARTMENT_ID') }}</strong></span>
                @endif
            </div>
        </dd>

        <dt>งบประมาณที่ได้รับ(บาท)</dt>
        <dd>
            <div class="form-group {{ $errors->has('COST') ? ' has-error' : '' }}">
                {{ Form::number('COST', old ('COST'), ['class'=>'form-control']) }}

                @if ($errors->has('COST'))
                    <span class="text-danger"><strong>{{ $errors->first('COST') }}</strong></span>
                @endif
            </div>
        </dd>

        <dt>หลักการและเหตุผล</dt>
        <dd>
            <div class="form-group {{ $errors->has('REASON') ? ' has-error' : '' }}">
                {{ Form::textarea('REASON', old ('REASON'), ['class'=>'form-control']) }}

                @if ($errors->has('REASON'))
                    <span class="text-danger"><strong>{{ $errors->first('REASON') }}</strong></span>
                @endif
            </div>
        </dd>

        <dt>ผลที่คาดว่าจะได้รับ</dt>
        <dd>
            <div class="form-group {{ $errors->has('RESPACT') ? ' has-error' : '' }}">
                {{ Form::textarea('RESPACT', old ('RESPACT'), ['class'=>'form-control']) }}

                @if ($errors->has('RESPACT'))
                    <span class="text-danger"><strong>{{ $errors->first('RESPACT') }}</strong></span>
                @endif
            </div>
        </dd>

        @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 1) as $cType)
            <dt>{{ $cType->CATEGORY_TYPE_NAME }}</dt>
            <dd>
                <div class="col-sm-12">
                    <div class="form-group">
                        <ul class="list-unstyled">
                            
                            @foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 1)
                                <li>
                                	<?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </dd>
        @endforeach

        @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 2) as $cType)
            <dt>{{ $cType->CATEGORY_TYPE_NAME }}</dt>
            <dd>
                <div class="col-sm-12">
                    <div class="form-group">
                        <ul class="list-unstyled">
                            @foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 2)
                                <li>
                                    <?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </dd>
        @endforeach

        @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 3) as $cType)
            <dt>{{ $cType->CATEGORY_TYPE_NAME }}</dt>
            <dd>
                <div class="col-sm-12">
                    <div class="form-group">
                        <ul class="list-unstyled">
                            @foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 3)
                                <li>
                                    <?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </dd>
        @endforeach

        <dt>ผลกระทบ(IMPACT)</dt>
        <dd>
            <div class="col-sm-4">
                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 4) as $cType)
                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
                    <div class="form-group">
                        <ul class="list-unstyled">
                            @foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 4)
                                <li>
                                    <?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>

            <div class="col-sm-4">
                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 5) as $cType)
                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
                    <div class="form-group">
                        <ul class="list-unstyled">
                            @foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 5)
                                <li>
                                    <?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>

            <div class="col-sm-4">
                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 6) as $cType)
                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
                    <div class="form-group">
                        <ul class="list-unstyled">
                            @foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 6)
                                <li>
                                    <?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </dd>

        <dt>กลยุทธฺ์</dt>
        <dd>
            <div class="col-sm-3">
                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 7) as $cType)
                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
                    <div class="form-group">
                        <ul class="list-unstyled">
                            @foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 7)
                                <li>
                                    <?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>

            <div class="col-sm-3">
                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 8) as $cType)
                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
                    <div class="form-group">
                        <ul class="list-unstyled">
                            @foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 8)
                                <li>
                                    <?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>

            <div class="col-sm-3">
                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 9) as $cType)
                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
                    <div class="form-group">
                        <ul class="list-unstyled">
                        	@foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 9)
                                <li>
                                    <?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>

            <div class="col-sm-3">
                @foreach ($categoryTypes->where('CATEGORY_TYPE_ID', 10) as $cType)
                    <strong>{{ $cType->CATEGORY_TYPE_NAME }}</strong>
                    <div class="form-group">
                        <ul class="list-unstyled">
                        	@foreach ($categories as $c)
                                @if ($c->CATEGORY_TYPE_ID == 10)
                                <li>
                                    <?php
                                	$selected = $project->categories
	                                	->where('IS_SELECTED', '1')->where('CATEGORY_ID', $c->CATEGORY_ID)->first();                                	
                                	?>
                                		
                                    {{ Form::checkbox('CATEGORY_ID[]', $c->CATEGORY_ID, isset($selected)) }}
                                    {{ $c->CATEGORY_NAME }}
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </dd>


    </dl>





    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
                <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">

        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
