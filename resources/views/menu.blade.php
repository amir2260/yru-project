<ul class="nav navbar-nav">
    @if (Auth::guest())
        <li>
            <a href="{{ url('/login') }}" class="dropdown-toggle logOff" role="button" aria-haspopup="true" aria-expanded="false">
                เข้าสู่ระบบ
                <span class="glyphicon glyphicon-off"></span>
            </a>
        </li>
    @else
{{--
        @can('user.is_admin')
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                    ทุนการศึกษา<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{ action('ContractController@index') }}">สัญญาทุนการศึกษา</a></li>
                </ul>
            </li>
        @endcan


        @can('user.is_admin')
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                    รายงาน<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{ action('ContractController@all') }}">รายงานสถานะการศึกษา</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('ReportController@budget') }}">รายงานการใช้ทุนการศึกษา - รายสัญญา</a></li>
                    <li><a href="{{ action('FundController@index') }}">รายงานการใช้ทุนการศึกษา - รายโครงการ</a></li>
                    <li><a href="{{ action('ReportController@budget_month') }}">รายงานการใช้ทุนการศึกษา - ประจำเดือน</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('FundController@index') }}">รายงานผลการศึกษา - รายสัญญา</a></li>
                    <li><a href="{{ action('FundController@index') }}">รายงานผลการศึกษา - รายภาคการศึกษา</a></li>
                </ul>
            </li>
        @endcan --}}

        @can('user.is_admin')
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                    งบประมาณ/กิจกรรม<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{ action('ProjectController@projectlist') }}">บันทึกการจัดกิจกรรม/ใช้งบประมาณ</a></li>                    
                </ul>
            </li>
        @endcan

        @can('user.is_admin')
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                    โครงการ<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{ action('ProjectController@selectTemplate') }}">สร้างโครงการใหม่จากกรอบโครงการ</a></li>
                    <li><a href="{{ action('ProjectController@create') }}">สร้างโครงการใหม่</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('ProjectController@index') }}">ร่างโครงการ</a></li>
                    <li><a href="{{ action('ProjectController@returnlist') }}">โครงการรอการปรับแก้</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('ProjectController@pending') }}">โครงการรอการอนุมัติ</a></li>
                    <li><a href="{{ action('ProjectController@approvelist') }}">โครงการผ่านการอนุมัติ</a></li>
                    <li><a href="{{ action('ProjectController@rejectlist') }}">โครงการไม่ผ่านการอนุมัติ</a></li>
                </ul>
            </li>
        @endcan

        @can('user.is_admin')
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                    รายงาน<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{ action('ProjectOwnerController@index') }}">รายงานการใช้งบประมาณ - แยกตามหลักสูตร</a></li>
                    <li><a href="{{ action('ProjectOwnerController@index') }}">รายงานการใช้งบประมาณ - แยกหน่วยงาน</a></li>
                    <li><a href="{{ action('ProjectOwnerController@index') }}">รายงานการใช้งบประมาณ - แยกโครงการ</a></li>
                    <li><a href="{{ action('ProjectOwnerController@index') }}">รายงานการใช้งบประมาณ - แยกหมวดงบประมาณ</a></li>                    
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('ProjectOwnerController@index') }}">รายงานการจัดกิจกรรม - แยกตามหลักสูตร</a></li>
                    <li><a href="{{ action('ProjectOwnerController@index') }}">รายงานการจัดกิจกรรม - แยกหน่วยงาน</a></li>
                    <li><a href="{{ action('ProjectOwnerController@index') }}">รายงานการจัดกิจกรรม - แยกโครงการ</a></li>
                </ul>
            </li>
        @endcan

        @can('user.is_admin')
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                    ข้อมูลหลัก<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{ action('TermController@index') }}">ปีงบประมาณ</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('CategoryController@category01') }}">ผลผลิต</a></li>
                    <li><a href="{{ action('CategoryController@category02') }}">พันธกิจ</a></li>
                    <li><a href="{{ action('CategoryController@category03') }}">ประเด็นยุทธศาสตร์</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('CategoryController@category04') }}">ผลกระทบ - ยุทธศาสตร์การพัฒนาคุณภาพการศึกษา</a></li>
                    <li><a href="{{ action('CategoryController@category05') }}">ผลกระทบ - ยุทธศาสตร์การพัฒนาท้องถิ่น</a></li>
                    <li><a href="{{ action('CategoryController@category06') }}">ผลกระทบ - ยุทธศาสตร์การพัฒนาองค์การ</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('CategoryController@category07') }}">กลยุทธ์ - ยุทธศาสตร์การพัฒนาคุณภาพการศึกษา</a></li>
                    <li><a href="{{ action('CategoryController@category08') }}">กลยุทธ์ - ยุทธศาสตร์การพัฒนาท้องถิ่น</a></li>
                    <li><a href="{{ action('CategoryController@category08') }}">กลยุทธ์ - ยุทธศาสตร์การพัฒนาองค์การ</a></li>
                    <li><a href="{{ action('CategoryController@category10') }}">กลยุทธ์ - ยุทธศาสตร์การพัฒนาสู่ประชาคมอาเซียน</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('BudgetController@index') }}">งบรายจ่าย</a></li>
                    <li><a href="{{ action('BudgetController@budgettype') }}">ประเภทงบรายจ่าย</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('IndicatorController@indicator_group') }}">กลุ่มเป้าหมาย</a></li>
                    <li><a href="{{ action('IndicatorController@indicator_type') }}">หลักเกณฑ์ตัวชี้วัด</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{ action('CourseController@index') }}">หลักสูตร</a></li>
                </ul>
            </li>
        @endcan

        @can('user.is_admin')
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="false">
                    <span class="glyphicon glyphicon-cog"></span> ตั้งค่า<span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{ action('AdminController@index') }}">ผู้ดูแลระบบ</a></li>
                </ul>
            </li>
        @endcan

        {{-- <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"  aria-haspopup="true" aria-expanded="false">
        <span class="glyphicon glyphicon-list-alt"></span> รายงาน<span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
    <li><a href="{{ action('HomeController@soon') }}">รายงาน 1</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="{{ action('HomeController@soon') }}">รายงาน 2</a></li>
    <li><a href="{{ action('HomeController@soon') }}">รายงาน 3</a></li>
    <li><a href="{{ action('HomeController@soon') }}">รายงาน 4</a></li>
</ul>
</li> --}}

<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <span></span> {{ Auth::user()->name }} <span class="glyphicon glyphicon-off"></span>
    </a>
    <ul class="dropdown-menu">
        <li><a href="{{ action('HomeController@soon') }}">ข้อมูลส่วนบุคคล</a></li>
        <li role="separator" class="divider"></li>
        <li><a onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">ออกจากระบบ</a></li>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>

        </ul>
    </li>

@endif

</ul>
