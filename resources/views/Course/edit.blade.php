@extends ('layouts.template')

@section('title')
    ข้อมูลหลักสูตร
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}
</style>

<script type="text/javascript">
$(function(){

    $('.selectpicker').selectpicker({
        liveSearch : true
    });

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ action('CourseController@index') }}';
    })

})
</script>
{!! Form::model($course) !!}
{{ Form::hidden('COURSE_ID') }}
<div class="col-sm-12">
    <div class="form-group {{ $errors->has('COURSE_NAME') ? ' has-error' : '' }}">
        <label for="COURSE_NAME">หลักสูตร</label>
        {{ Form::text('COURSE_NAME', null, ['class'=>'form-control']) }}

        @if ($errors->has('COURSE_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('COURSE_NAME') }}</strong></span>
        @endif

    </div>

    <div class="form-group {{ $errors->has('DEPARTMENT_ID') ? ' has-error' : '' }}">
        <label for="DEPARTMENT_ID">หน่วยงาน</label>
        {{ Form::select('DEPARTMENT_ID', $departments, null, ['class'=>'form-control selectpicker']) }}
    </div>


    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
                <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">

        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
