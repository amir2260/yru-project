@extends ('layouts.template')

@section('title')
  หลักสูตร
@endsection

@section('content')
  <style>
  ul.pagination{
    margin: 1px;
  }
  </style>

  {!! Form::open(['method' => 'post']) !!}
  <div class="row">
    <div class="col-sm-4">
      {{-- <input type="text" name="TEXT_SEARCH" id="TEXT_SEARCH" value="{{ old('TEXT_SEARCH', $text_search) }}"
        class="form-control" placeholder="ป้อนคำค้นหา"> --}}
    </div>
    <div class="col-sm-3">
      {{-- <button type="submit" name="btnSearch" class="btn btn-default" >ค้นหา</button> --}}
    </div>

    <div class="col-sm-5 text-right">
      <p>
        {{-- @if ($projects->total() > 0)
          {{ $projects->firstItem() }} - {{ $projects->lastItem() }}
        @endif
        จาก {{ $projects->total() }} รายการ
        <br>
        {{ $projects->links() }} --}}
        <a href="{{ action('CourseController@create') }}" class='btn btn-primary'>
          <span class='glyphicon glyphicon-plus-sign'></span> เพิ่มรายการ
        </a>
      </p>
    </div>
  </div>
  {!! Form::close() !!}

  <div class="row">
    <div class="col-sm-12">
      <table class="table table-bordered table-hover">
        <thead>
          <tr class="active">
            <th>หลักสูตร</th>
            <th>คณะ</th>
          </tr>
        </thead>
        @foreach ($courses as $course)
            <tr>
              <td>
                  <a href="{{ action('CourseController@info', $course->COURSE_ID) }}">
                      {{ $course->COURSE_NAME }}
                  </a>
                 @if ($course->staffsInCourse->count() > 0)
                     <ul>
                            @foreach ($course->staffsInCourse as $item)
                                <li>{{ $item->staff->FULL_NAME }}</li>
                            @endforeach
                     </ul>
                 @endif
              </td>
              <td>{{ $course->department->DEPARTMENT_NAME }}</td>
            </tr>
        @endforeach
      </table>
    </div>
  </div>

  <div class="row">
    {{-- <div class="col-sm-12 text-right">
      {{ $projects->links() }}
    </div> --}}
  </div>

@endsection
