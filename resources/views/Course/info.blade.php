@extends ('layouts.template')

@section('title')
    โครงการ
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){
    $('#btnDelete').click(function(){
        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
            if (result){
                window.location = '{{ action('CourseController@delete', $course->COURSE_ID) }}';
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ action('CourseController@index') }}';
    })

    $('.selectpicker').selectpicker({
        liveSearch : true
    })

})
</script>

{!! Form::model($course) !!}
{{ Form::hidden('COURSE_ID') }}
<div class="col-sm-12">
    <div class="form-group {{ $errors->has('COURSE_NAME') ? ' has-error' : '' }}">
        <label for="COURSE_NAME">หลักสูตร</label>
        {{ Form::text('COURSE_NAME', null, ['class'=>'form-control', 'readonly'=>'true']) }}

        @if ($errors->has('COURSE_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('COURSE_NAME') }}</strong></span>
        @endif

    </div>

    <div class="form-group {{ $errors->has('DEPARTMENT_ID') ? ' has-error' : '' }}">
        <label for="DEPARTMENT_ID">หน่วยงาน</label>
        {{ Form::text('DEPARTMENT_ID', $course->department->DEPARTMENT_NAME, ['class'=>'form-control', 'readonly'=>'true']) }}
    </div>


    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnEdit" name="btnEdit">
                <span class="glyphicon glyphicon-ok-circle"></span> แก้ไข
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">
            <button type="button" class="btn btn-danger" id="btnDelete" name="btnDelete">
                ลบ
            </button>
        </div>
    </div>
</div>
{!! Form::close() !!}

<div class="col-sm-12">
    &nbsp;
</div>

{{ Form::open() }}
{{ Form::hidden('COURSE_ID', $course->COURSE_ID) }}
<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            ผู้มีสิทธิ์
        </div>
        <div class="panel-body">
            <table class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <td>ชื่อ</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($course->staffsInCourse as $item)
                        <tr>
                            <td>
                                <a href="#"><span class="glyphicon glyphicon-trash"></span></a>
                                {{ $item->staff->FULL_NAME }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <hr>
            <div class="form-group {{ $errors->has('STAFF_ID') ? ' has-error' : '' }}">
                <label for="STAFF_ID">เลือกบุคลากร</label>
                {{ Form::select('STAFF_ID', $staffs, null , ['class'=>'form-control selectpicker']) }}
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary" id="btnSave" name="btnSave">
                    <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
                </button>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}



@endsection
