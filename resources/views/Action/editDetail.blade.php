@extends ('layouts.template')

@section('title') 
    รายละเอียดกิจกรรม
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){

//     $('.selectpicker').selectpicker({
//         liveSearch : true
//     });

	$('.datepicker').datepicker({
    			language:'th-th'
    			,format:'dd/mm/yyyy'
    			,autoclose:true
    			,todayBtn:true
    			,todayHighlight:true
    		});

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ action('ProjectController@projectActivity', $activity->project->PROJECT_ID) }}';
    })

})
</script>

<div class="col-sm-12">
	@include('Project.projectinfo', ['project'=> $activity->project])
</div>

{!! Form::model($activity) !!}
    {{ Form::hidden('ACTION_ID', $action->ACTION_ID) }}
	{{ Form::hidden('ACTIVITY_ID', $activity->ACTIVITY_ID) }}
	{{ Form::hidden('ACTIVITY_DETAIL_ID', $activityDetail->ACTIVITY_DETAIL_ID) }}
<div class="col-sm-12">

    <div class="form-group {{ $errors->has('ACTIVITY_NAME') ? ' has-error' : '' }}">
        <label for="ACTIVITY_NAME">ชื่อกิจกรรม</label>
		{{ Form::text('ACTIVITY_NAME', $activity->ACTIVITY_NAME, ['class'=>'form-control', 'placeholder'=>'ชื่อกิจกรรม เช่น กิจกรรมที่ 1 การส่งเสริมการ...', 'readonly'=>'' ]) }}
        @if ($errors->has('ACTIVITY_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('ACTIVITY_NAME') }}</strong></span>
        @endif
    </div>	

    <div class="form-group {{ $errors->has('ACTIVITY_DETAIL_NAME') ? ' has-error' : '' }}">
        <label for="ACTIVITY_DETAIL_NAME">รายการ</label>
		{{ Form::text('ACTIVITY_DETAIL_NAME', $activityDetail->ACTIVITY_DETAIL_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
        @if ($errors->has('ACTIVITY_DETAIL_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('ACTIVITY_DETAIL_NAME') }}</strong></span>
        @endif
    </div>

    <div class="row">
    	<div class="col-sm-12">
	    	<div class="form-group {{ $errors->has('BUDGET_DETAIL_ID') ? ' has-error' : '' }}">
		        <label for="BUDGET_DETAIL_ID">หมวดงบประมาณ</label>
				{{ Form::text('BUDGET_DETAIL_ID'
					, $activityDetail->budgetDetail->budgetType->BUDGET_TYPE_NAME . " - " . $activityDetail->budgetDetail->BUDGET_DETAIL_NAME
					, ['class'=>'form-control', 'readonly'=>'']) }}

		        @if ($errors->has('BUDGET_DETAIL_ID'))
		            <span class="text-danger"><strong>{{ $errors->first('BUDGET_DETAIL_ID') }}</strong></span>
		        @endif
		    </div>
		</div>
	</div>

    <div class="row">
		<div class="col-sm-6">
			<div class="form-group {{ $errors->has('DEPARTMENT_ID') ? ' has-error' : '' }}">
				<label for="DEPARTMENT_ID">หลักสูตร</label>
				{{ Form::hidden('COUSE_ID', $course->COUSE_ID) }}
				{{ Form::text('COUSE_NAME', $course->COURSE_NAME, ['class'=>'form-control', 'readonly'=>'readonly']) }}

				{{--  {{ Form::select('DEPARTMENT_ID', $courses, null, ['class'=>'form-control']) }}
				@if ($errors->has('DEPARTMENT_ID'))
					<span class="text-danger"><strong>{{ $errors->first('DEPARTMENT_ID') }}</strong></span>
				@endif  --}}
			</div>
		
			<div class="form-group {{ $errors->has('ACTION_DATE') ? ' has-error' : '' }}">
		        <label for="ACTION_DATE">วันที่จัดกิจกรรม</label>
				{{ Form::text('ACTION_DATE', $action->ACTION_DATE, ['class'=>'form-control datepicker']) }}

		        @if ($errors->has('ACTION_DATE'))
		            <span class="text-danger"><strong>{{ $errors->first('ACTION_DATE') }}</strong></span>
		        @endif
		    </div>

	    	<div class="form-group {{ $errors->has('LOCATION') ? ' has-error' : '' }}">
		        <label for="LOCATION">สถานที่</label>
				{{ Form::text('LOCATION', $action->LOCATION, ['class'=>'form-control']) }}

		        @if ($errors->has('LOCATION'))
		            <span class="text-danger"><strong>{{ $errors->first('LOCATION') }}</strong></span>
		        @endif
		    </div>	    

			<div class="form-group {{ $errors->has('INDICATOR_ID') ? ' has-error' : '' }}">

		        <label for="INDICATOR_ID">ตัวชี้วัดความสำเร็จ</label>				

				
				<select class="form-control" id="INDICATOR_ID" class="form-control">
					@foreach ($project->indicators as $ind)
						<option value="s">{{ $ind->getName() }} ({{ $ind->getUnitName() }})</option>
					@endforeach
				</select>

		        @if ($errors->has('INDICATOR_ID'))
		            <span class="text-danger"><strong>{{ $errors->first('INDICATOR_ID') }}</strong></span>
		        @endif

		    </div>

			{{--  <div class="form-group {{ $errors->has('ACTION_NAME') ? ' has-error' : '' }}">
		        <label for="ACTION_NAME">ผลการดำเนินงาน</label>
				{{ Form::text('ACTION_NAME', $action->ACTION_NAME, ['class'=>'form-control']) }}

		        @if ($errors->has('ACTION_NAME'))
		            <span class="text-danger"><strong>{{ $errors->first('ACTION_NAME') }}</strong></span>
		        @endif
		    </div>  --}}
			<div class="form-group {{ $errors->has('ACTION_NAME') ? ' has-error' : '' }}">
		        <label for="ACTION_NAME">ผลสำเร็จตามตัวชี้วัด</label>
				{{ Form::text('ACTION_NAME', $action->ACTION_NAME, ['class'=>'form-control']) }}

		        @if ($errors->has('ACTION_NAME'))
		            <span class="text-danger"><strong>{{ $errors->first('ACTION_NAME') }}</strong></span>
		        @endif
		    </div>


		</div>
		<div class="col-sm-6 border-left">
			<div class="form-group {{ $errors->has('TARGET') ? ' has-error' : '' }}">
		        <label for="TARGET">เป้าหมาย</label>
				{{ Form::textarea('TARGET', $action->TARGET, ['class'=>'form-control', 'ROWS'=>'2']) }}

		        @if ($errors->has('TARGET'))
		            <span class="text-danger"><strong>{{ $errors->first('TARGET') }}</strong></span>
		        @endif
		    </div>

			<div class="form-group {{ $errors->has('ACTION_DETAIL') ? ' has-error' : '' }}">
		        <label for="ACTION_DETAIL">สรุปผลการดำเนินงาน</label>
				{{ Form::textarea('ACTION_DETAIL', $action->ACTION_DETAIL, ['class'=>'form-control']) }}

		        @if ($errors->has('ACTION_DETAIL'))
		            <span class="text-danger"><strong>{{ $errors->first('ACTION_DETAIL') }}</strong></span>
		        @endif
		    </div>
		</div>

		<div class="col-sm-12">
			<hr>


			{{--  <div class="form-group {{ $errors->has('KPI') ? ' has-error' : '' }}">

		        <label for="KPI">ตัวชี้วัดความสำเร็จ</label>
				{{ Form::textarea('KPI', $action->KPI, ['class'=>'form-control', 'ROWS'=>'2']) }}

		        @if ($errors->has('KPI'))
		            <span class="text-danger"><strong>{{ $errors->first('KPI') }}</strong></span>
		        @endif

		    </div>  --}}
	    	
		
	    	<div class="form-group {{ $errors->has('TROUBLE') ? ' has-error' : '' }}">
		        <label for="TROUBLE">ข้อบกพร่อง/ปัญหาอุปสรรค</label>
				{{ Form::textarea('TROUBLE', $action->TROUBLE, ['class'=>'form-control', 'ROWS'=>'2']) }}

		        @if ($errors->has('TROUBLE'))
		            <span class="text-danger"><strong>{{ $errors->first('TROUBLE') }}</strong></span>
		        @endif
		    </div>

			<div class="form-group {{ $errors->has('COMMENT') ? ' has-error' : '' }}">
		        <label for="COMMENT">ข้อเสนอแนะ</label>
				{{ Form::textarea('COMMENT', $action->COMMENT, ['class'=>'form-control']) }}

		        @if ($errors->has('COMMENT'))
		            <span class="text-danger"><strong>{{ $errors->first('COMMENT') }}</strong></span>
		        @endif
		    </div>
			<hr>
		</div>


        <div class="col-sm-6">
	    	<div class="form-group {{ $errors->has('REF_NO') ? ' has-error' : '' }}">
		        <label for="REF_NO">หมายเลขรายการอ้างอิง</label>
				{{ Form::text('REF_NO', $action->REF_NO, ['class'=>'form-control']) }}

		        @if ($errors->has('REF_NO'))
		            <span class="text-danger"><strong>{{ $errors->first('REF_NO') }}</strong></span>
		        @endif
		    </div>
		</div>

	</div>

	<div class="row">

    	@foreach ($budgetGroups as $group)
    		<div class="col-sm-6">
			    <div class="form-group {{ $errors->has('BUDGET_GROUP_' . $group->BUDGET_GROUP_ID ) ? ' has-error' : '' }}">
			        <label for="BUDGET_GROUP_{{ $group->BUDGET_GROUP_ID }}">{{ $group->BUDGET_GROUP_NAME }}</label>
					{{ Form::number ('BUDGET_GROUP_' . $group->BUDGET_GROUP_ID
                        , number_format($action->actionBudgets->where('BUDGET_GROUP_ID', $group->BUDGET_GROUP_ID)->sum('AMOUNT'),2)
                        , ['class'=>'form-control']) }}
			        @if ($errors->has('BUDGET_GROUP_'  . $group->BUDGET_GROUP_ID ))
			            <span class="text-danger"><strong>{{ $errors->first('BUDGET_GROUP_' . $group->BUDGET_GROUP_ID) }}</strong></span>
			        @endif
			    </div>
			</div>
    	@endforeach

        <div class="col-sm-6">
    		
    	</div>
    </div>

    <div class="row">
    	<hr />

	    <div class="col-sm-12">
	    	&nbsp;
	    </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
                <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">

        </div>
    </div>
</div>
{!! Form::close() !!}

    @foreach ($errors->all() as $error)
        <div>{{ $error }}</div>
    @endforeach


@endsection
