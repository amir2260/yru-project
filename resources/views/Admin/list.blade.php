@extends ('layouts.template')

@section('title')
  ผู้ดูแลระบบ
@endsection

@section('content')
  <style>
  ul.pagination{
    margin: 1px;
  }
  </style>

  {!! Form::open(['method' => 'post']) !!}
  <div class="row">
    <div class="col-sm-2">
      <input type="text" name="TEXT_SEARCH" id="TEXT_SEARCH" value="{{ old('TEXT_SEARCH', $text_search) }}"
        class="form-control" placeholder="ป้อนคำค้นหา">
    </div>
    <div class="col-sm-2">
      <button type="submit" name="btnSearch" class="btn btn-default" >ค้นหา</button>
    </div>

    <div class="col-sm-2 text-right">
      <p>
        @if ($admins->total() > 0)
          {{ $admins->firstItem() }} - {{ $admins->lastItem() }}
        @endif
        จาก {{ $admins->total() }} รายการ
        <br>
        {{ $admins->links() }}
        <a href="{{ url('admin/create') }}" class='btn btn-primary'>
          <span class='glyphicon glyphicon-plus-sign'></span> เพิ่มรายการ
        </a>
      </p>
    </div>
  </div>

  {!! Form::close() !!}

  <div class="row">
    <div class="col-sm-6">
      <table class="table table-bordered table-hover">
        <tr class="active">
          <th>ผู้ดูแลระบบ</th>
          <th>ผู้สร้าง</th>
          <th>วันที่สร้าง</th>
        </tr>
        @foreach ($admins as $admin)
            <tr>
              <td>
                <a href="{{ url('/admin/info') }}/{{ $admin->ADMIN_ID }}">
                  {{ $admin->staff->FULL_NAME }}
                </a>
              </td>
              <td>
                  {{ $admin->CREATE_BY }}
              </td>
              <td>
                  {{ $admin->CREATE_DATE }}
              </td>
            </tr>
        @endforeach
      </table>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12 text-right">
      {{ $admins->links() }}
    </div>
  </div>

@endsection
