@extends ('layouts.template')

@section('title')
  รายละเอียดสารบัญกลาง
@endsection

@section('content')
  <style>
    .table {
      margin-bottom : 2px;
    }
  </style>

  <script type="text/javascript">
    $(function(){
      $('#btnDelete').click(function(){
        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
          if (result){
            window.location = '{{ url('/admin/delete') }}/{{ old('ADMIN_ID', $admin->ADMIN_ID) }}';
          }
        })
      })

      $('#btnBack').click(function(){
        window.location = '{{ url('/admin') }}';
      })

    })
  </script>

    <div class="col-sm-6">
      <div class="form-group {{ $errors->has('ADMIN_NAME') ? ' has-error' : '' }}">
        <label for="ADMIN_NAME">ชื่อ - สกุล</label>
        <input type="text" class="form-control" id="ADMIN_NAME" name="ADMIN_NAME" readonly
          value='{{ old('FULL_NAME', $admin->staff->FULL_NAME) }}'>

        @if ($errors->has('ADMIN_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('ADMIN_NAME') }}</strong></span>
        @endif

      </div>


      <div class="row">
        <div class="col-sm-6">
          <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
            กลับ
          </button>
        </div>
        <div class="col-sm-6 text-right">
          <button type="button" class="btn btn-default" id="btnDelete" name="btnDelete">
            <span class="glyphicon glyphicon-remove-circle"></span> ลบ
          </button>
        </div>
      </div>
    </div>


@endsection
