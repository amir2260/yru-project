@extends ('layouts.template')

@section('title')
    รายละเอียดสารบัญกลาง
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){

    $('.selectpicker').selectpicker({
        liveSearch : true
    });

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ url('/manager') }}';
    })

    var staffs = {!! $staffs !!}
    $('#STAFF_NAME').typeahead({
        minLength: 2,
        hint: true,
        highlight:true,
        source: staffs
    });


})
</script>
{!! Form::open(['method' => 'post']) !!}
<div class="col-sm-6">
    <div class="form-group {{ $errors->has('STAFF_NAME') ? ' has-error' : '' }}">
        <label for="STAFF_NAME">ชื่อ - สกุล</label>
        <input type="text" class="form-control" id="STAFF_NAME" name="STAFF_NAME"
        value='{{ old('STAFF_NAME') }}'>

        @if ($errors->has('STAFF_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('STAFF_NAME') }}</strong></span>
        @endif

    </div>

    <div class="form-group {{ $errors->has('DEPARTMENT_ID') ? ' has-error' : '' }}">
        <label for="DEPARTMENT_ID">หน่วยงาน</label>
        <select name="DEPARTMENT_ID" id="DEPARTMENT_ID" class="form-control selectpicker">
            @foreach ($departments as $department)
                <option value="{{ $department->DEPARTMENT_ID }}">
                    {{ $department->DEPARTMENT_NAME }}
                </option>
                @foreach ($department->departments as $dd)
                    <option value="{{ $dd->DEPARTMENT_ID }}">
                        {{ $department->DEPARTMENT_NAME }} - {{ $dd->DEPARTMENT_NAME }}
                    </option>
                @endforeach
            @endforeach
        </select>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
                <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">

        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
