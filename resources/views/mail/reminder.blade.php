<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h4>แจ้งเตือนนัดหมาย : {{ $appoint->APPOINT_TITLE }}</h4>
    สถานที่ : {{ $appoint->LOCATION }} <br>
    วันที่ : {{ $utils->thaidate($appoint->START_DATE) }} <br>
    <br>
    ผู้ใช้สามารถ ตอบรับ หรือปฏิเสธ การเข้าร่วม หรือดูรายละเอียดเพิ่มเติมได้ที่ :
    <a href="{{ action('AppointController@info', $appoint->APPOINT_ID) }}">คลิกที่นี่</a>
    <br>
    <br>
    <br>
    <br>
    ** กรุณาไม่ทำการตอบกลับ เนื่องจากเป็นอีเมล์อัตโนมัติจากระบบ
    <br>
    <br>
  </body>
</html>
