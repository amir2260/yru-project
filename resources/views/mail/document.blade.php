<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h4>แจ้งเตือนกล่องเอกสารเข้า เมื่อเวลา : {{ $utils->thaidate(date ( "Y-m-d H:i:s" ), true) }}</h4>
    <br>
    ผู้ใช้สามารถเข้าดูรายการได้ที่ :
    <a href="{{ action('HomeController@index') }}">คลิกที่นี่</a>
    <br>
    <br>
    <br>
    <br>
    ** กรุณาไม่ทำการตอบกลับ เนื่องจากเป็นอีเมล์อัตโนมัติจากระบบ
    <br>
    <br>
  </body>
</html>
