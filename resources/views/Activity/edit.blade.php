@extends ('layouts.template')

@section('title')
    รายละเอียดกิจกรรม
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){

//     $('.selectpicker').selectpicker({
//         liveSearch : true
//     });

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ action('ProjectController@info', $project->PROJECT_ID) }}';
    })
    
})
</script>

<div class="col-sm-12">
	@include('Project.projectinfo', ['project'=> $project])
</div>

{!! Form::model($activity) !!}
	{{ Form::hidden('PROJECT_ID', $project->PROJECT_ID) }}
	{{ Form::hidden('ACTIVITY_ID', $activity->ACTIVITY_ID) }}
<div class="col-sm-8">
    <div class="form-group {{ $errors->has('ACTIVITY_NAME') ? ' has-error' : '' }}">
        <label for="ACTIVITY_NAME">ชื่อกิจกรรม</label>
        
		{{ Form::text('ACTIVITY_NAME', null, ['class'=>'form-control', 'placeholder'=>'ชื่อกิจกรรม เช่น กิจกรรมที่ 1 การส่งเสริมการ...' ]) }}

        @if ($errors->has('ACTIVITY_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('ACTIVITY_NAME') }}</strong></span>
        @endif

    </div>
    

    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
                <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">

        </div>
    </div>
</div>
{!! Form::close() !!}

@if (isset($activity->ACTIVITY_ID) && $activity->ACTIVITY_ID > 0 && false)
    {{ Form::open(['action' => action('ProjectController@addCourse') ]) }}
    {{ Form::hidden('ACTIVITY_ID', $activity->ACTIVITY_ID) }}
        <div class="col-sm-4 border-left">
            <div class="form-group">
                <label for="aa">หลักสูตรที่มีสิทธิ์</label>
                {{ Form::select('COURSE_ID', $courses->pluck('COURSE_NAME', 'COURSE_ID'), null, ['class'=>'form-control']) }}
                <small class="text-warning">**เลือกหลักสูตรที่ต้องการให้สิทธิ์ และกดปุ่มเพิ่มหลักสูตร</small>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary" id="btnSavea" name="btnSavea">
                    <span class="glyphicon glyphicon-ok-circle"></span> เพิ่มหลักสูตร
                </button>
            </div>
        </div>
    {{ Form::close() }}
@endif

@endsection
