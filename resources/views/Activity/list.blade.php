@extends ('layouts.template')

@section('title')
  ปีงบประมาณ
@endsection

@section('content')
  <style>
  ul.pagination{
    margin: 1px;
  }
  </style>

  {!! Form::open(['method' => 'post']) !!}
  <div class="row">
    <div class="col-sm-4">
      {{-- <input type="text" name="TEXT_SEARCH" id="TEXT_SEARCH" value="{{ old('TEXT_SEARCH', $text_search) }}"
        class="form-control" placeholder="ป้อนคำค้นหา"> --}}
    </div>
    <div class="col-sm-3">
      {{-- <button type="submit" name="btnSearch" class="btn btn-default" >ค้นหา</button> --}}
    </div>

    <div class="col-sm-5 text-right">
      <p>
        {{-- @if ($projects->total() > 0)
          {{ $projects->firstItem() }} - {{ $projects->lastItem() }}
        @endif
        จาก {{ $projects->total() }} รายการ
        <br>
        {{ $projects->links() }} --}}
        <a href="{{ url('/') }}" class='btn btn-primary'>
          <span class='glyphicon glyphicon-plus-sign'></span> เพิ่มรายการ
        </a>
      </p>
    </div>
  </div>
  {!! Form::close() !!}

  <div class="row">
    <div class="col-sm-12">
      <table class="table table-bordered table-hover">
        <tr class="active">
          <th>รายการ</th>
        </tr>
        @foreach ($terms as $term)
            <tr>
              <td>
                  {{ $term->TERM_NAME }}
              </td>
            </tr>
        @endforeach
      </table>
    </div>
  </div>

  <div class="row">
    {{-- <div class="col-sm-12 text-right">
      {{ $projects->links() }}
    </div> --}}
  </div>

@endsection
