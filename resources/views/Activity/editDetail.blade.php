@extends ('layouts.template')

@section('title')
    รายละเอียดกิจกรรม
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){

//     $('.selectpicker').selectpicker({
//         liveSearch : true
//     });

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ action('ProjectController@info', $activity->project->PROJECT_ID) }}';
    })

})
</script>

<div class="col-sm-12">
	@include('Project.projectinfo', ['project'=> $activity->project])
</div>

{!! Form::model($activityDetail) !!}
	{{ Form::hidden('ACTIVITY_ID', $activity->ACTIVITY_ID) }}
	{{ Form::hidden('ACTIVITY_DETAIL_ID', $activityDetail->ACTIVITY_DETAIL_ID) }}
<div class="col-sm-12">

    <div class="form-group {{ $errors->has('ACTIVITY_NAME') ? ' has-error' : '' }}">
        <label for="ACTIVITY_NAME">ชื่อกิจกรรม</label>
		{{ Form::text('ACTIVITY_NAME', $activity->ACTIVITY_NAME, ['class'=>'form-control', 'placeholder'=>'ชื่อกิจกรรม เช่น กิจกรรมที่ 1 การส่งเสริมการ...', 'readonly'=>'' ]) }}
        @if ($errors->has('ACTIVITY_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('ACTIVITY_NAME') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('ACTIVITY_DETAIL_NAME') ? ' has-error' : '' }}">
        <label for="ACTIVITY_DETAIL_NAME">รายการ</label>
		{{ Form::text('ACTIVITY_DETAIL_NAME', null, ['class'=>'form-control']) }}
        @if ($errors->has('ACTIVITY_DETAIL_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('ACTIVITY_DETAIL_NAME') }}</strong></span>
        @endif
    </div>


    <div class="row">
    	<div class="col-sm-6">
	    	<div class="form-group {{ $errors->has('BUDGET_DETAIL_ID') ? ' has-error' : '' }}">
		        <label for="BUDGET_DETAIL_ID">หมวดงบประมาณ</label>
				<select name="BUDGET_DETAIL_ID" id="BUDGET_DETAIL_ID" class="form-control">
					@foreach ($budgetDetails as $detail)
						<option value="{{ $detail->BUDGET_DETAIL_ID}}"	{{ ($detail->BUDGET_DETAIL_ID == $activityDetail->BUDGET_DETAIL_ID ) ? "selected" : "" }} >
							{{ $detail->budgetType->BUDGET_TYPE_NAME }} - {{ $detail->BUDGET_DETAIL_NAME }}
						</option>
					@endforeach
				</select>

		        @if ($errors->has('BUDGET_DETAIL_ID'))
		            <span class="text-danger"><strong>{{ $errors->first('BUDGET_DETAIL_ID') }}</strong></span>
		        @endif
		    </div>
    	</div>

    	<div class="col-sm-6">
    		<div class="form-group {{ $errors->has('TARGET_DATE') ? ' has-error' : '' }}">
		        <label for="TARGET_DATE">กำหนดใช้งบประมาณ</label>
				<select name="TARGET_DATE" id="TARGET_DATE" class="form-control">
					<option value="1">ไตรมาสที่ 1</option>
					<option value="2">ไตรมาสที่ 2</option>
					<option value="3">ไตรมาสที่ 3</option>
					<option value="4">ไตรมาสที่ 4</option>
				</select>
		        @if ($errors->has('TARGET_DATE'))
		            <span class="text-danger"><strong>{{ $errors->first('TARGET_DATE') }}</strong></span>
		        @endif
		    </div>
    	</div>

    	@foreach ($budgetGroups as $group)
    		<div class="col-sm-6">
			    <div class="form-group {{ $errors->has('BUDGET_GROUP_' . $group->BUDGET_GROUP_ID ) ? ' has-error' : '' }}">
			        <label for="BUDGET_GROUP_{{ $group->BUDGET_GROUP_ID }}">{{ $group->BUDGET_GROUP_NAME }}</label>
					{{ Form::number ('BUDGET_GROUP_' . $group->BUDGET_GROUP_ID, null, ['class'=>'form-control']) }}
			        @if ($errors->has('BUDGET_GROUP_'  . $group->BUDGET_GROUP_ID ))
			            <span class="text-danger"><strong>{{ $errors->first('BUDGET_GROUP_' . $group->BUDGET_GROUP_ID) }}</strong></span>
			        @endif
			    </div>
			</div>
    	@endforeach

        <div class="col-sm-6">
    		<div class="form-group {{ $errors->has('COURSE_ID') ? ' has-error' : '' }}">
		        <label for="COURSE_ID">หลักสูตร</label>
                {{ Form::select('COURSE_ID', $courses, null, ['class'=>'form-control']) }}
		        @if ($errors->has('COURSE_ID'))
		            <span class="text-danger"><strong>{{ $errors->first('COURSE_ID') }}</strong></span>
		        @endif
		    </div>
    	</div>
    </div>

    <div class="row">
    	<hr />

	    <div class="col-sm-12">
	    	&nbsp;
	    </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
                <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">

        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
