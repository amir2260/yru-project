<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ระบบสารสนเทศบริหารโครงการ - มหาวิทยาลัยราชภัฎยะลา</title>

	  <link href='https://fonts.googleapis.com/css?family=Kanit&subset=thai,latin' rel='stylesheet' type='text/css'>

    {!! Html::script('jquery-1.12.1.min.js') !!}

    {!! Html::script('bootstrap-3.3.6-dist/js/bootstrap.js') !!}
    {!! Html::style('bootstrap-3.3.6-dist/css/bootstrap.css') !!}
    {!! Html::style('bootstrap-3.3.6-dist/css/bootstrap-theme.css') !!}
    {{--  {!! Html::style('bootstrap-yeti/bootstrap.min.css') !!}  --}}

    {!! Html::script('bootbox.min.js') !!}
    {{-- {!! Html::script('typeahead.bundle.js') !!} --}}
    {!! Html::script('Bootstrap-3-Typeahead-master/bootstrap3-typeahead.js') !!}



    {!! Html::script('jojosati-bootstrap-datepicker-thai/js/bootstrap-datepicker.js') !!}
    {!! Html::script('jojosati-bootstrap-datepicker-thai/js/bootstrap-datepicker-thai.js') !!}
    {!! Html::script('jojosati-bootstrap-datepicker-thai/js/locales/bootstrap-datepicker.th.js') !!}
    {!! Html::style('jojosati-bootstrap-datepicker-thai/css/datepicker.css') !!}

    {!! Html::script('http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js') !!}
    {!! Html::script('bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js') !!}
    {!! Html::style('bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.css') !!}

    {!! Html::script('kartik-v-bootstrap-fileinput/js/fileinput.min.js') !!}
    {!! Html::script('kartik-v-bootstrap-fileinput/js/locales/th.js') !!}
    {!! Html::style('kartik-v-bootstrap-fileinput/css/fileinput.min.css') !!}


    {!! Html::script('is-loading-master/jquery.isloading.min.js') !!}

    {{-- {!! Html::script('pretty-event-calendar/src/calendar.js') !!}
    {!! Html::style('pretty-event-calendar/src/calendar.css') !!} --}}

    {!! Html::script('bootstrap-select-master/dist/js/bootstrap-select.js') !!}
    {!! Html::style('bootstrap-select-master/dist/css/bootstrap-select.css') !!}

    {{-- <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> --}}
    {{-- {!! Html::script('fancytree-master/src/jquery.fancytree.js') !!} --}}
    {{-- {!! Html::style('fancytree-master/src/skin-win8-n/ui.fancytree.css') !!}  --}}

    <script type="text/javascript">
      $(function(){
        
        $("input").addClass("input-sm");

        $(".btn").addClass("btn-sm");
        bootbox.setLocale('th');

      })
    </script>


    <!-- Custom styles for this template -->
    {!! Html::style('css/styles.css') !!}

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    {!! Html::script('js/ie-emulation-modes-warning.js') !!}


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    {!! Html::script('js/ie10-viewport-bug-workaround.js') !!}

    <style>
    body, h1, h2, h3, h4 {
		  font-family: 'Kanit', sans-serif;
		}

    body {
      padding-top: 50px;
		  padding-bottom: 20px;
    }

    .body-content {
      font-family: Arial, Helvetica, sans-serif;
    }

    pre {
     white-space: pre-wrap;       /* css-3 */
     white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
     white-space: -pre-wrap;      /* Opera 4-6 */
     white-space: -o-pre-wrap;    /* Opera 7 */
     word-wrap: break-word;       /* Internet Explorer 5.5+ */

     font : inherit;
    }

    ul, ol, li {
        /*list-style: none;*/
        /*padding: 0;
        margin: 0;*/
    }

    .calendar-display {
      font-size: 20px;
      text-indent: 10px;
    }

    .calendar-hd {
      padding: 5px 0;
      height: 40px;
      line-height: 30px;
    }

    </style>
  </head>

  <body>

	<div class="modal fade" id="modalUserInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Name here</h4>
	      </div>

	      <div class="modal-body">
	        <p>
	        	<a href="{{ url('/') }}account/changepwd" class="btn btn-default">เปลี่ยนรหัสผ่าน</a>
	        </p>
	      </div>
	    </div>
	  </div>
	</div>

    <nav class="navbar navbar-inverse navbar-fixed-top box-shadow--2dp" >
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          	<a class="navbar-brand visible-lg visible-xs" href="{{ url('/') }}" >
                <span class="glyphicon glyphicon-signal"></span> ระบบสารสนเทศบริหารโครงการ
	         </a>

             <a class="navbar-brand visible-md" href="{{ url('/') }}" style="color: #fff;">
                 <span class="glyphicon glyphicon-signal"></span> บริหารโครงการ
 	         </a>

             <a class="navbar-brand visible-sm" href="{{ url('/') }}" style="color: #fff;">
                 <span class="glyphicon glyphicon-send"></span>
 	         </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse  navbar-right">
          @include('menu')

        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <div class="container">

    	<div class="row">
    		<h4>&nbsp;</h4>

    		@hasSection('title')
	    		<div class="col-sm-12" style="background-color: gray; color:white;">
					<h3>
	              		@yield('title')
	              	</h3>
	          	</div>
            @endif

    		<div class="panel panel-default">
          		<div class="panel-body">
          			<div class="row  body-content">
          				<div class="col-sm-12">
	          				&nbsp; <br />
	          				@yield('content')
          				</div>
          			</div>

          		</div>
        	</div>
    	</div>

		<div class="row">
			<div class="col-sm-12">
		      	<footer class="text-muted">
              		ระบบสารสนเทศบริหารโครงการ มหาวิทยาลัยราชภัฎยะลา.
              		<br>© 2016 - Version 0.1a
                  <a target="_blank" href="https://docs.google.com/document/d/1Z9MAVHCALp0LG8aL4T-S9ZvUHL17c0uxZKH2rSDW_sM/edit">docs</a>
		      	</footer>
	      	</div>
    	</div>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

  </body>
</html>
