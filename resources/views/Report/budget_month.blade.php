@extends ('layouts.template')

@section('title')
    รายงานการใช้ทุนการศึกษา - ประจำเดือน
@endsection

@section('content')
<style>
ul.pagination{
    margin: 1px;
}
</style>

{!! Form::open(['method' => 'post']) !!}
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <label for="TEXT_SEARCH">เดือน</label>
            <input type="text" name="TEXT_SEARCH" id="TEXT_SEARCH" value="{{ old('TEXT_SEARCH') }}"
            class="form-control" placeholder="ป้อนคำค้นหา">
        </div>

        {{-- <div class="form-group">
            <label for="TEXT_SEARCH">คำค้นหา</label>
            <input type="text" name="TEXT_SEARCH" id="TEXT_SEARCH" value="{{ old('TEXT_SEARCH', $search->TEXT_SEARCH) }}"
            class="form-control" placeholder="ป้อนคำค้นหา">
        </div> --}}

        {{-- <div class="form-group">
            <label for="EDU_TERM_ID">ปีที่รับทุนการศึกษา</label>
            {{ Form::select('EDU_TERM_ID', $eduTerms, $search->EDU_TERM_ID, ['class'=>'form-control']) }}
        </div> --}}

        <div class="form-group">
            <button type="submit" name="btnSearch" class="btn btn-default" >ค้นหา</button>
        </div>

    </div>
    <div class="col-sm-3">

    </div>

    <div class="col-sm-5 text-right">
        <p>
            @if ($budgets->total() > 0)
                {{ $budgets->firstItem() }} - {{ $budgets->lastItem() }}
            @endif
            จาก {{ $budgets->total() }} รายการ
            <br>
            เป็นเงินทั้งสิ้น : {{ number_format($budgets->sum('AMOUNT'),2) }} บาท
        </p>
    </div>
</div>
{!! Form::close() !!}

<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-hover">
            <tr class="active">
                <th>เลขที่ใบเบิก</th>
                <th>วันที่</th>
                <th>รายการ</th>
                <th>ชื่อทุน</th>

                <th>ผู้รับเงิน</th>
                <th class="text-right">จำนวนเงิน(บาท)</th>
                <th>หมายเหตุ</th>
                <th>สถานะรายการ</th>
            </tr>
            @foreach ($budgets as $budget)
                <tr>
                    <td>{{ $budget->REF_NO }}</td>
                    <td>{{ $utils->thaidate($budget->BUDGET_DATE) }}</td>
                    <td>{{ $budget->budgetType->BUDGET_TYPE_NAME }}</td>
                    <td>{{ $budget->contract->fund->FUND_NAME }}</td>

                    <td>{{ $budget->contract->STUDENT_NAME }}</td>
                    <td  class="text-right">{{ number_format($budget->AMOUNT,2) }}</td>
                    <td>-</td>
                    <td>ปกติ</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 text-right">
        {{ $budgets->links() }}
    </div>
</div>

@endsection
