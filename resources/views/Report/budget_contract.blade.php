@extends ('layouts.template')

@section('title')
    รายงานการใช้ทุนการศึกษา - รายสัญญา
@endsection

@section('content')
<style>
ul.pagination{
    margin: 1px;
}
</style>

{!! Form::open(['method' => 'post']) !!}
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label for="STUDENT_ID">ผู้รับทุน</label>
            {{ Form::text('STUDENT_ID', '[' . $contract->CITIZEN_ID . '] ' . $contract->STUDENT_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
        </div>
        <div class="form-group">
            <label for="STUDENT_ID">ทุน</label>
            {{ Form::text('STUDENT_ID', $contract->fund->FUND_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
        </div>

        <div class="form-group">
            <label for="STUDENT_ID">โครงการ</label>
            {{ Form::text('STUDENT_ID', $contract->fund->project->PROJECT_NAME, ['class'=>'form-control', 'readonly'=>'']) }}
        </div>
    </div>
</div>
{!! Form::close() !!}

<div class="row">
    <div class="col-sm-12 text-right">
        รวม : {{ number_format($contract->budgets->count()) }} รายการ
        <br>
        เป็นเงินทั้งสิ้น : {{ number_format($contract->budgets->sum('AMOUNT'),2) }} บาท
    </div>
    <div class="col-sm-12">
        <table class="table table-bordered table-hover">
            <tr class="active">
                <th>เลขที่ใบเบิก</th>
                <th>วันที่</th>
                <th>รายการ</th>
                <th>ชื่อทุน</th>

                <th>ผู้รับเงิน</th>
                <th class="text-right">จำนวนเงิน(บาท)</th>
                <th>หมายเหตุ</th>
                <th>สถานะรายการ</th>
            </tr>
            @foreach ($contract->budgets as $budget)
                <tr>
                    <td>{{ $budget->REF_NO }}</td>
                    <td>{{ $utils->thaidate($budget->BUDGET_DATE) }}</td>
                    <td>{{ $budget->budgetType->BUDGET_TYPE_NAME }}</td>
                    <td>{{ $budget->contract->fund->FUND_NAME }}</td>

                    <td>{{ $budget->contract->STUDENT_NAME }}</td>
                    <td  class="text-right">{{ number_format($budget->AMOUNT,2) }}</td>
                    <td>-</td>
                    <td>ปกติ</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 text-right">
        {{-- {{ $budgets->links() }} --}}
    </div>
</div>

@endsection
