@extends ('layouts.template')

@section('title')
    โครงการ
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}
</style>

<script type="text/javascript">
$(function(){
    $('#btnDelete').click(function(){
        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
            if (result){
                // window.location = '{{ url('/project/delete') }}/{{ old('PROJECT_ID', $project->PROJECT_ID) }}';
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ url('/project') }}';
    })

})
</script>

<div class="col-sm-12">

    <div class="panel panel-default">
        <div class="panel-body">

            <div class="form-group {{ $errors->has('PROJECT_CODE') ? ' has-error' : '' }}">
                <label for="PROJECT_CODE">รหัสโครงการ</label>
                <input type="text" class="form-control" id="PROJECT_CODE" name="PROJECT_CODE" readonly
                value='{{ old('PROJECT_CODE', $project->PROJECT_CODE) }}'>

                @if ($errors->has('PROJECT_CODE'))
                    <span class="text-danger"><strong>{{ $errors->first('PROJECT_CODE') }}</strong></span>
                @endif

            </div>

            <div class="form-group {{ $errors->has('PROJECT_NAME') ? ' has-error' : '' }}">
                <label for="PROJECT_NAME">รหัสโครงการ</label>
                <input type="text" class="form-control" id="PROJECT_NAME" name="PROJECT_NAME" readonly
                value='{{ old('FULL_NAME', $project->PROJECT_NAME) }}'>

                @if ($errors->has('PROJECT_NAME'))
                    <span class="text-danger"><strong>{{ $errors->first('PROJECT_NAME') }}</strong></span>
                @endif

            </div>

            <div class="form-group {{ $errors->has('PROJECT_OWNER_NAME') ? ' has-error' : '' }}">
                <label for="PROJECT_OWNER_NAME">หน่วยงานเจ้าของโครงการ</label>
                <input type="text" class="form-control" id="PROJECT_OWNER_NAME" name="PROJECT_OWNER_NAME" readonly
                value='{{ old('PROJECT_OWNER_NAME', $project->projectOwner->PROJECT_OWNER_NAME) }}'>

                @if ($errors->has('PROJECT_OWNER_NAME'))
                    <span class="text-danger"><strong>{{ $errors->first('PROJECT_OWNER_NAME') }}</strong></span>
                @endif
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                        กลับ
                    </button>
                </div>
                <div class="col-sm-6 text-right">
                    <button type="button" class="btn btn-default" id="btnDelete" name="btnDelete">
                        <span class="glyphicon glyphicon-remove-circle"></span> ลบ
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>

    <div class="col-sm-12">
        <table class="table table-bordered table-hover">
            <tr class="active">
                <th>ชื่อทุน</th>
                <th>ปีการศึกษา</th>
                <th>สาขาวิชา</th>
                <th>ระดับ</th>
                <th class="text-center">จำนวน(ทุน)</th>
                <th>สถานะ</th>
            </tr>
            @foreach ($project->funds as $fund)
                <tr>
                    <td>
                        <a href="{{ url('/fund/info') }}/{{ $fund->FUND_ID }}">
                            {{ $fund->FUND_NAME }}
                        </a>
                    </td>
                    <td>{{ $fund->eduTerm->EDU_TERM_NAME }}</td>
                    <td>{{ $fund->program->PROGRAM_NAME }}</td>
                    <td>{{ $fund->eduLevel->EDU_LEVEL_NAME }}</td>
                    <td class="text-center">{{ $fund->REQUEST_AMOUNT }}</td>
                    <td>{{ $fund->fundStatus->FUND_STATUS_NAME }}</td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection
