@extends ('layouts.template')

@section('content')

    <script>
    $(function(){
        $('#appoint').calendar({
            // view: 'year',
            width: 400,
            height: 240,
            weekArray: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
            monthArray: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
            // startWeek: 0,
            // selectedRang: [new Date(), null],
            data: [
                @foreach ($keys as $key)
                <?php
                $val = "";
                foreach ($appoints_all[$key] as $item){
                    $val .= $item['APPOINT_TITLE'] . ", " . $utils->thaidate($item['START_DATE'], true) . "<br/>";
                }
                ?>

                {
                    date : '{!! $key !!}',
                    value : '{!! $val !!}'
                },
                @endforeach
                {
                    date: '2009/01/21',
                    value: '-'
                }
            ], onSelected: function (view, date, data) {
                console.log('view:' + view)
                console.log('date:' + date)
                console.log('data:' + (data || 'None'));
            }
        });
    })
    </script>

    <style>
    ul, ol, li {
        list-style: none;
        padding-left: 0px;
    }

    element.style {
        width: 400px;
        height: inherit;;
    }
    </style>

    <div class="col-sm-12 row">
        <div class="col-sm-12">
            <h1 class='text-info'>บริหารโครงการ</h1>
            <h4 class="text-muted">ระบบสารสนเทศบริหารโครงการ - มหาวิทยาลัยราชภัฏยะลา</h4>
            <hr>
        </div>

        <div class="col-sm-8">
            <table class="table table-bordered table-hover">
                <tr class='active'>
                    <td>ประเภทการลา</td>
                    <td class="text-center">ใช้สิทธิ์แล้ว(วัน)</td>
                    <td class="text-center">คงเหลือ(วัน)</td>
                </tr>
                @foreach ($leaveTypes as $leaveType)
                    @if ($leaveType->LEAVE_TYPE_ID <= 3)
                    <tr>
                        <td>{{ $leaveType->LEAVE_TYPE_NAME }}</td>
                        <?php
                        // var_dump($staff->documents->where('LEAVE_TYPE_ID', $leaveType->LEAVE_TYPE_ID));
                        ?>
                        <td class="text-center">
                            {{ $staff->documents->where('LEAVE_TYPE_ID', $leaveType->LEAVE_TYPE_ID)
                                ->where('DOC_STATUS_ID', 5 )
                                ->sum('LEAVE_AMOUNT') }}
                        </td>
                        <td class="text-center">
                            {{
                                $leaveTypeLimits->where('LEAVE_TYPE_ID', $leaveType->LEAVE_TYPE_ID)->sum('LIMIT_PER_YEAR')
                                - $staff->documents->where('LEAVE_TYPE_ID', $leaveType->LEAVE_TYPE_ID)
                                    ->where('DOC_STATUS_ID', 5 )
                                    ->sum('LEAVE_AMOUNT')
                            }}
                        </td>
                    </tr>
                    @endif
                @endforeach
            </table>

            <h4>
                <a href="{{ action('DocumentController@inbox') }}">
                    เอกสารเข้า
                </a>
            </h4>

            <table class="table table-bordered table-hover">
                <tr class="active">
                    <th>#</th>
                    <th>เรื่อง</th>
                    <th>ประเภทเอกสาร</th>
                    <th>เลขที่</th>
                    <th>ลงวันที่</th>
                    <th>ถึง</th>
                    <th>ผู้ร่าง</th>
                    <th>ผู้พิมพ์</th>
                    <th>ดำเนินการ</th>
                </tr>
                @foreach ($documents->sortByDesc('SEND_DATE') as $document)
                    {{-- @can('document.access_item', $document) --}}

                    <tr>
                        <td>
                            <?php
                            $itemRead = $document->paths->filter(function($d){
                                return $d->STAFF_ID == Auth::user()->id && isset($d->OPEN_DATE);
                            });

                            ?>
                            {!!
                                ($itemRead->isEmpty() && $document->STAFF_ID != Auth::user()->id) ?  "<span class='glyphicon glyphicon-envelope'></span>" : ""
                                !!}
                            </td>
                            <td>
                                <a href="{{ url('/document/info') }}/{{ $document->DOC_ID }}">
                                    {{ $document->DOC_TITLE }}
                                </a>
                                @if (isset($document->files) && count($document->files) > 0)
                                    <ul>
                                        @foreach ($document->files as $fileItem)
                                            <li><small>{{ $fileItem->FILE_NAME }}</small></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </td>
                            <td>{{ $document->docType->DOC_TYPE_NAME }}</td>
                            <td>{{ $document->DOC_NO }}</td>
                            <td>{{ $utils->thaidate($document->DOC_DATE) }}</td>
                            <td>{{ $document->TO_NAME }}</td>
                            <td>{{ $document->FROM_NAME }}</td>
                            <td>{{ $document->CREATE_BY }}
                                <br><small>ส่ง {{ $utils->thaidate($document->SEND_DATE, true) }}</small>
                            </td>
                            <td>
                                <?php
                                $action = $document->paths->where('STAFF_ID', Auth::user()->id)->first();
                                ?>
                                {{
                                    isset($action) ? $action->pathAction->PATH_ACTION_NAME : ''
                                }}
                        </td>
                    </tr>
                    {{-- @endcan --}}
                @endforeach
            </table>

            </div>






            <div class="col-sm-4 border-left">
                <h4>
                    <a href="{{ action('AppointController@index') }}">
                        นัดหมาย
                    </a>
                </h4>
                <small class="text-muted">รายการนัดหมายในอีก 7 วัน</small>

                @if (isset($appoints) && count($appoints) > 0)
                    <div id="appoint"></div>

                    <table class="table table-hover">
                        @foreach($appoints as $appoint)
                            <?php $attender = $appoint->attenders->where('ATTENDER_STAFF_ID', $staff->STAFF_ID)->first(); ?>

                            @if (isset($attender))
                                @if($attender->ATTENDER_STATUS_ID == 1 && $appoint->APPOINT_STATUS_ID != 2)
                                    <tr class="success">
                                    @else
                                        <tr>
                                        @endif
                                    @else
                                        <tr>
                                        @endif

                                        <td>
                                            @if ($appoint->APPOINT_STATUS_ID == 2)
                                                <del>
                                                @endif
                                                <a href="{{ url('/appoint/info') }}/{{ $appoint->APPOINT_ID }}">
                                                    {{ $appoint->APPOINT_TITLE }}
                                                </a>
                                                @if ($appoint->APPOINT_STATUS_ID == 2)
                                                </del> <span class="text-danger">(รายการถูกยกเลิก)</span>
                                            @endif
                                            <br>
                                            <small>
                                                {{ $utils->thaidate($appoint->START_DATE, true) }} - {{ $utils->thaidate($appoint->END_DATE, true) }}
                                            </small>
                                        </td>
                                        <td>
                                            @if ($attender == null)
                                                ผู้นัดหมาย
                                            @else
                                                {{ $attender->attenderStatus->ATTENDER_STATUS_NAME }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @else
                            <small>- ไม่มีรายการนัดหมาย</small>
                        @endif
                    </div>
                </div>

            @endsection
