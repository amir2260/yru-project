@extends('layouts.template')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
          <br>
          <br>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3">
        </div>
        <div class="col-md-6">
            <h2 class='text-info text-center'>&nbsp;</h2>
            <div class="panel panel-default">
                <div class="panel-heading">เข้าสู่ระบบ</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label"> ชื่อเข้าใช้</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">รหัสผ่าน</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
{{--  
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> จดจำผู้เข้าใช้
                                    </label>
                                </div>
                            </div>
                        </div>  --}}

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    เข้าสู่ระบบ
                                </button>
                            </div>
                        </div>
                    </form>

                       @foreach ($errors->all() as $error)
                          <div class='text-danger'>{{ $error }}</div>
                      @endforeach

                </div>
            </div>
        </div>
        <div class="col-sm-3">

        </div>
    </div>
</div>
@endsection
