@extends ('layouts.template')

@section('title')
    ทุนการศึกษา
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}
</style>

<script type="text/javascript">
$(function(){
    $('#btnDelete').click(function(){
        bootbox.confirm('ต้องการลบข้อมูลรายการนี้ ', function(result){
            if (result){
                // window.location = '{{ url('/fund/delete') }}/{{ old('FUND_ID', $fund->FUND_ID) }}';
            }
        })
    })

    $('#btnClose').click(function(){
        bootbox.confirm('ต้องการปิดการรับทุนสำหรับทุนรายการนี้ ', function(result){
            if (result){
                window.location = '{{ url('/fund/deactive') }}/{{ old('FUND_ID', $fund->FUND_ID) }}';
            }
        })
    })

    $('#btnEdit').click(function(){
        window.location = '{{ url('/fund/edit') }}/{{ old('FUND_ID', $fund->FUND_ID) }}';
    })

    $('#btnBack').click(function(){
        window.location = '{{ url('/fund') }}';
    })

})
</script>

<div class="col-sm-6">


    <div class="form-group {{ $errors->has('FUND_NAME') ? ' has-error' : '' }}">
        <label for="FUND_NAME">ชื่อทุน</label>
        <input type="text" class="form-control" id="FUND_NAME" name="FUND_NAME"
        value='{{ old('FUND_NAME', $fund->FUND_NAME) }}' readonly="">

        @if ($errors->has('FUND_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('FUND_NAME') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('PROJECT_ID') ? ' has-error' : '' }}">
        <label for="PROJECT_NAME">ภายใต้โครงการ</label>
        <input type="text" class="form-control" id="PROJECT_NAME" name="PROJECT_NAME"
        value='{{ old('PROJECT_NAME', $fund->project->PROJECT_NAME) }}' readonly="">

        @if ($errors->has('PROJECT_ID'))
            <span class="text-danger"><strong>{{ $errors->first('PROJECT_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('FUND_TYPE_ID') ? ' has-error' : '' }}">
        <label for="FUND_TYPE_ID">ประเภททุน</label>
        <input type="text" class="form-control" id="PROJECT_NAME" name="PROJECT_NAME"
        value='{{ old('PROJECT_NAME', $fund->fundType->FUND_TYPE_NAME) }}' readonly="">

        @if ($errors->has('FUND_TYPE_ID'))
            <span class="text-danger"><strong>{{ $errors->first('FUND_TYPE_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('PROGRAM_ID') ? ' has-error' : '' }}">
        <label for="PROGRAM_ID">สาขาวิชา</label>
        <input type="text" class="form-control" id="PROJECT_NAME" name="PROJECT_NAME"
        value='{{ old('PROJECT_NAME', $fund->program->PROGRAM_NAME) }}' readonly="">

        @if ($errors->has('PROGRAM_ID'))
            <span class="text-danger"><strong>{{ $errors->first('PROGRAM_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('SCHOOL_ID') ? ' has-error' : '' }}">
        <label for="SCHOOL_ID">สถานศึกษา</label>
        <input type="text" class="form-control" id="PROJECT_NAME" name="PROJECT_NAME"
        value='{{ old('PROJECT_NAME', $fund->school->SCHOOL_NAME) }}' readonly="">

        @if ($errors->has('SCHOOL_ID'))
            <span class="text-danger"><strong>{{ $errors->first('SCHOOL_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('EDU_LEVEL_ID') ? ' has-error' : '' }}">
        <label for="EDU_LEVEL_ID">ระดับการศึกษา</label>
        <input type="text" class="form-control" id="PROJECT_NAME" name="PROJECT_NAME"
        value='{{ old('PROJECT_NAME', $fund->eduLevel->EDU_LEVEL_NAME) }}' readonly="">

        @if ($errors->has('EDU_LEVEL_ID'))
            <span class="text-danger"><strong>{{ $errors->first('EDU_LEVEL_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('REQUEST_AMOUNT') ? ' has-error' : '' }}">
        <label for="REQUEST_AMOUNT">จำนวนที่เปิดรับ</label>
        {{ Form::number('REQUEST_AMOUNT', $fund->REQUEST_AMOUNT, ['class'=>'form-control', 'readonly'=>""]) }}
        @if ($errors->has('REQUEST_AMOUNT'))
            <span class="text-danger"><strong>{{ $errors->first('REQUEST_AMOUNT') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('FUND_STATUS_ID') ? ' has-error' : '' }}">
        <label for="FUND_STATUS_ID">สถานะ</label>
        {{ Form::text('FUND_STATUS_ID', $fund->fundStatus->FUND_STATUS_NAME, ['class'=>'form-control', 'readonly'=>""]) }}
        @if ($errors->has('FUND_STATUS_ID'))
            <span class="text-danger"><strong>{{ $errors->first('FUND_STATUS_ID') }}</strong></span>
        @endif
    </div>


    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-default" id="btnEdit" name="btnEdit">
                แก้ไข
            </button>
            <button type="button" class="btn btn-default" id="btnClose" name="btnClose">
                ปิดการรับ
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">
            <button type="button" class="btn btn-default" id="btnDelete" name="btnDelete">
                <span class="glyphicon glyphicon-remove-circle"></span> ลบ
            </button>
        </div>
    </div>
</div>


@endsection
