@extends ('layouts.template')

@section('title')
    ทุนการศึกษา
@endsection

@section('content')
<style>
ul.pagination{
    margin: 1px;
}
</style>

{!! Form::open(['method' => 'post']) !!}
<div class="row">
    <div class="col-sm-4">
        <input type="text" name="TEXT_SEARCH" id="TEXT_SEARCH" value="{{ old('TEXT_SEARCH', $text_search) }}"
        class="form-control" placeholder="ป้อนคำค้นหา">
    </div>
    <div class="col-sm-3">
        <button type="submit" name="btnSearch" class="btn btn-default" >ค้นหา</button>
    </div>

    <div class="col-sm-5 text-right">
        <p>
            @if ($funds->total() > 0)
                {{ $funds->firstItem() }} - {{ $funds->lastItem() }}
            @endif
            จาก {{ $funds->total() }} รายการ
            <br>
            {{ $funds->links() }}
            <a href="{{ url('fund/create') }}" class='btn btn-primary'>
                <span class='glyphicon glyphicon-plus-sign'></span> เพิ่มรายการ
            </a>
        </p>
    </div>
</div>
{!! Form::close() !!}

<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-hover">
            <tr class="active">
                <th>ชื่อทุน</th>
                <th>ปีการศึกษา</th>
                <th>สาขาวิชา</th>
                <th>โครงการ</th>
                <th>ระดับ</th>
                <th class="text-center">จำนวน(ทุน)</th>
                <th>สถานะ</th>
            </tr>
            @foreach ($funds as $fund)
                <tr>
                    <td>
                        <a href="{{ url('/fund/info') }}/{{ $fund->FUND_ID }}">
                            {{ $fund->FUND_NAME }}
                        </a>
                    </td>
                    <td>{{ $fund->eduTerm->EDU_TERM_NAME }}</td>
                    <td>{{ $fund->program->PROGRAM_NAME }}</td>
                    <td>{{ $fund->project->PROJECT_NAME }}</td>
                    <td>{{ $fund->eduLevel->EDU_LEVEL_NAME }}</td>
                    <td class="text-center">{{ $fund->REQUEST_AMOUNT }}</td>
                    <td>{{ $fund->fundStatus->FUND_STATUS_NAME }}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 text-right">
        {{ $funds->links() }}
    </div>
</div>

@endsection
