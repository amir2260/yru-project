@extends ('layouts.template')

@section('title')
    ทุนการศึกษา
@endsection

@section('content')
<style>
.table {
    margin-bottom : 2px;
}

</style>

<script type="text/javascript">
$(function(){

    $('.selectpicker').selectpicker({
        liveSearch : true
    });

    $('#btnSave').click(function(){
        bootbox.confirm('ต้องการบันทึกข้อมูลรายการนี้ ', function(result){
            if (result){
                $('form').submit();
            }
        })
    })

    $('#btnBack').click(function(){
        window.location = '{{ url('/fund/info') }}/{{ old('FUND_ID', $fund->FUND_ID) }}';
    })



})
</script>
{!! Form::open(['method' => 'post']) !!}
{{ Form::hidden('FUND_ID', $fund->FUND_ID )}}
<div class="col-sm-6">
    <div class="form-group {{ $errors->has('FUND_NAME') ? ' has-error' : '' }}">
        <label for="FUND_NAME">ชื่อทุน</label>
        <input type="text" class="form-control" id="FUND_NAME" name="FUND_NAME"
        value='{{ old('FUND_NAME', $fund->FUND_NAME) }}'>

        @if ($errors->has('FUND_NAME'))
            <span class="text-danger"><strong>{{ $errors->first('FUND_NAME') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('PROJECT_ID') ? ' has-error' : '' }}">
        <label for="PROJECT_ID">ภายใต้โครงการ</label>
        {{ Form::select('PROJECT_ID', $projects, $fund->PROJECT_ID, ['class'=>'form-control selectpicker'])}}
        @if ($errors->has('PROJECT_ID'))
            <span class="text-danger"><strong>{{ $errors->first('PROJECT_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('EDU_TERM_ID') ? ' has-error' : '' }}">
        <label for="EDU_TERM_ID">ปีการศึกษา</label>
        {{ Form::select('EDU_TERM_ID', $terms, $fund->EDU_TERM_ID, ['class'=>'form-control selectpicker'])}}
        @if ($errors->has('EDU_TERM_ID'))
            <span class="text-danger"><strong>{{ $errors->first('EDU_TERM_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('FUND_TYPE_ID') ? ' has-error' : '' }}">
        <label for="FUND_TYPE_ID">ประเภททุน</label>
        {{ Form::select('FUND_TYPE_ID', $fundTypes, $fund->FUND_TYPE_ID, ['class'=>'form-control selectpicker'])}}
        @if ($errors->has('FUND_TYPE_ID'))
            <span class="text-danger"><strong>{{ $errors->first('FUND_TYPE_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('PROGRAM_ID') ? ' has-error' : '' }}">
        <label for="PROGRAM_ID">สาขาวิชา</label>
        {{ Form::select('PROGRAM_ID', $programs, $fund->PROGRAM_ID, ['class'=>'form-control selectpicker'])}}
        @if ($errors->has('PROGRAM_ID'))
            <span class="text-danger"><strong>{{ $errors->first('PROGRAM_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('SCHOOL_ID') ? ' has-error' : '' }}">
        <label for="SCHOOL_ID">สถานศึกษา</label>
        {{ Form::select('SCHOOL_ID', $schools, $fund->SCHOOL_ID, ['class'=>'form-control selectpicker'])}}
        @if ($errors->has('SCHOOL_ID'))
            <span class="text-danger"><strong>{{ $errors->first('SCHOOL_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('EDU_LEVEL_ID') ? ' has-error' : '' }}">
        <label for="EDU_LEVEL_ID">ระดับการศึกษา</label>
        {{ Form::select('EDU_LEVEL_ID', $eduLevels, $fund->EDU_LEVEL_ID, ['class'=>'form-control selectpicker'])}}
        @if ($errors->has('EDU_LEVEL_ID'))
            <span class="text-danger"><strong>{{ $errors->first('EDU_LEVEL_ID') }}</strong></span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('REQUEST_AMOUNT') ? ' has-error' : '' }}">
        <label for="REQUEST_AMOUNT">จำนวนที่เปิดรับ</label>
        {{ Form::number('REQUEST_AMOUNT', $fund->REQUEST_AMOUNT, ['class'=>'form-control']) }}
        @if ($errors->has('REQUEST_AMOUNT'))
            <span class="text-danger"><strong>{{ $errors->first('REQUEST_AMOUNT') }}</strong></span>
        @endif
    </div>



    <div class="row">
        <div class="col-sm-6">
            <button type="button" class="btn btn-primary" id="btnSave" name="btnSave">
                <span class="glyphicon glyphicon-ok-circle"></span> บันทึก
            </button>
            <button type="button" class="btn btn-default" id="btnBack" name="btnBack">
                กลับ
            </button>
        </div>
        <div class="col-sm-6 text-right">

        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection
