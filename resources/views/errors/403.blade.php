@extends ('layouts.template')

@section('title')
  ERROR : 403
@endsection

@section('content')
  <strong>Unauthorized action.</strong>
  <br>
  <span class='text-muted'>
    สิทธิ์ในการเข้าถึงข้อมูลไม่เพียงพอ หรือไม่มีสิทธิ์ในกระบวนการดังกล่าว
  </span>
@endsection
