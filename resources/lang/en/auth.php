<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'ชื่อเข้าใช้ หรือ รหัสผ่านไม่ถูกต้อง หรือ ไม่สามารถเชื่อมต่อกับระบบระบุตัวตน',
    'throttle' => 'มีการเข้าใช้งานผิดพลาดเกินจำนวนครั้งที่ทำหนด เข้าใช้งานอีกครั้งได้ในอีก :seconds seconds.',

];
