<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\Staff;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Validator::extend('valid_staffname', function($attribute, $value, $parameters) {

        $staff= Staff::where('FULL_NAME', $value)->first();
        if (isset($staff)){
          return true;
        }

        return false;

      });

      Validator::extend('valid_invalid_path', function($attribute, $value, $parameters) {
        return false;
      });

      Validator::extend('valid_duplicate_path', function($attribute, $value, $parameters) {
        return false;
      });


      Validator::extend('valid_request', function($attribute, $value, $parameters) {
        return true;
      });

      // Validator::extend('valid_withdraw', function($attribute, $value, $parameters) {
      //   // หา member
      //   $member = Member::where('MEMBER_NAME', $value)->first();
      //   $total_balance = $member->deposits->sum('DEPOSIT_AMOUNT');
      //
      //   if (!isset($member)){
      //     return false;
      //   }
      //   // TODO : must ............
      //   // $incomp = Member::where (['MEMBER_ID'=> $member->MEMBER_ID])
      //   //   ->first()
      //   //   ->deposits()->where(['ACTION_TYPE' => 'ฝาก'])->sum('DEPOSIT_AMOUNT');
      //   //
      //   // $excomp = Member::where (['MEMBER_ID'=> $member->MEMBER_ID])
      //   //   ->first()
      //   //   ->deposits()->where(['ACTION_TYPE' => 'ถอน'])->sum('DEPOSIT_AMOUNT');
      //
      //   // $incomp = isset($incomp) ? $incomp : 0;
      //   // $excomp = isset($excomp) ? $excomp : 0;
      //
      //   if ($total_balance > 35000){
      //     return true;
      //   }
      //   return false;
      // });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
