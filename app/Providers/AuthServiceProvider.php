<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Model\Staff;
use App\Model\Manager;
use App\Model\Admin;

class AuthServiceProvider extends ServiceProvider
{
    /**
    * The policy mappings for the application.
    *
    * @var array
    */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
    * Register any authentication / authorization services.
    *
    * @return void
    */
    public function boot()
    {
        $this->registerPolicies();

        /**
        * Custom policy by Amir.
        */

        // ตรวจสอบว่า เป็นสารบัญกลางหรือไม่
        Gate::define('user.manager', function ($user, $department = null)
        {
            $managers = Manager::where('STAFF_ID', $user->id)->get();
            if (isset($managers) && count($managers) > 0){
                if (!isset($department)){
                    return true;
                }

                foreach ($managers as $manager){
                    if ($manager->DEPARTMENT_ID == $department->DEPARTMENT_ID){
                        return true;
                    }
                }
            }
            return false;
        });

        // Is Admin
        Gate::define('user.is_admin', function ($user)
        {
            $admin = Admin::where('STAFF_ID', $user->id)->first();

            if (isset($admin)){
                return true;
            }
            return false;
        });

        Gate::define('appoint.is_owner', function ($user, $appoint)
        {
            if (!isset($user) || !isset($appoint)) return false;
            if ($appoint->CREATE_STAFF_ID == $user->id) return true;

            return false;
        });

        Gate::define('appoint.is_attender', function ($user, $appoint)
        {
            if (!isset($user) || !isset($appoint)) return false;

            $attenders = $appoint->attenders->where('ATTENDER_STAFF_ID', $user->id);
            if (isset($attenders) && count($attenders) > 0){
                return true;
            }
            return false;
        });

        // สารบรรณกลาง
        // Gate::define('user.is_manager', function ($user)
        // {
        //     $admin = Manager::where('STAFF_ID', $user->id)->first();
        //
        //     if (isset($admin)){
        //         return true;
        //     }
        //     return false;
        // });


        // ตรวจสอบว่า มีสิทธิ์แก้ไขเส้นทางดังกล่าวหรือไม่
        Gate::define('path.access_item', function ($user, $path)
        {
            if (!isset($path)) return false;

            // ถ้าเป็นเจ้าของเส้นทาง ต้องเข้าถึงได้
            if ($user->id == $path->STAFF_ID)return true;

            // ถ้าไม่ได้เป็นเจ้าของ แต่อยู่ในหน่วยงาน (กรณีเส้นทางโดยสารบัญกลาง) ต้องสามารถเข้าถึงได้
            if (isset($path->DEPARTMENT_ID)){
                $staff = Staff::where('STAFF_ID', $user->id)->first();
                return $this->canAccessPath($staff->department, $path);
            }
            return false;
        });


        Gate::define('contract.access_item', function ($user, $contract)
        {
            if (!isset($contract)) return false;

            // ถ้าเป็นเจ้าของเส้นทาง ต้องเข้าถึงได้
            if ($user->id == $contract->STAFF_ID) return true;

            return false;
        });


        Gate::define('box.access_item', function ($user, $box)
        {
            if (!isset($box)) return false;

            // ถ้าเป็นเจ้าของเส้นทาง ต้องเข้าถึงได้
            if ($user->id == $box->STAFF_ID) return true;

            if (isset($box->DEPARTMENT_ID) && $box->DEPARTMENT_ID > 0){
                $manager = Manager::where('STAFF_ID', $user->id)
                ->where('DEPARTMENT_ID', $box->DEPARTMENT_ID)->first();

                if (isset($manager)) return true;
            }

            return false;
        });


        // ตรวจสอบว่า มีสิทธิ์แก้ไขในเส้นทางดังกล่าวหรือไม่
        Gate::define('path.edit_item', function ($user, $path) {
            if (!isset($path)) return false;

            // ถ้าเป็นเจ้าของเส้นทาง ต้องได้
            if ($user->id == $path->STAFF_ID)
            return true;

            // ถ้าเส้นทางเป็นเส้นทางที่สร้างโดยสารบัญกลาง (เส้นทางหน่วยงาน)
            if (isset($path->DEPARTMENT_ID)){ // ต้องเป็นเส้นทางหน่วยงานที่สร้างโดยสารบัญกลาง
                $staff = Staff::where('STAFF_ID', $user->id)->first();

                // ผู้แก้ไข ต้องเป็นสารบัญกลางเท่านั้น
                if (isset($staff->managers) && count($staff->managers) > 0){
                    // และต้องเป็นสารบัญกลางของหน่วยงานนั้นๆ
                    if ($staff->manager[0]->DEPARTMENT_ID == $path->DEPARTMENT_ID){
                        return true;
                    }
                }
            }
            return false;

        });

        Gate::define('document.edit_item', function ($user, $doc)
        {
            if (!isset($doc)) return false;
            if ($user->id == $doc->STAFF_ID && ($doc->DOC_STATUS_ID == 1 || $doc->DOC_STATUS_ID == 3)){
                return true;
            }
            return false;
        });

        Gate::define('document.can_getback', function ($user, $doc)
        {
            if (!isset($doc)) return false;
            if ($user->id == $doc->STAFF_ID && $doc->DOC_STATUS_ID == 2){

                foreach ($doc->paths as $p){
                    if ($p->PATH_ACTION_ID == 3 && isset($p->COMPLETE_DATE)){
                        return false;
                    }
                }
                return true;
            }else{
                return false;
            }
            return false;
        });


        Gate::define('document.can_send', function ($user, $doc)
        {
            // TODO : ต้องเลือกเส้นทาง หรือผู้รับ ถึงจะส่งได้
            if (!isset($doc)) return false;
            if ($user->id == $doc->STAFF_ID && $doc->DOC_STATUS_ID == 1
            && isset($doc->paths) && count($doc->paths) > 0){
                return true;
            }

            return false;
        });


        Gate::define('document.can_tobox', function ($user, $doc)
        {
            if (!isset($doc)) return false;
            if (($doc->DOC_STATUS_ID == 2 || $doc->DOC_STATUS_ID == 5)){
                return true;
            }
            return false;
        });


        Gate::define('document.access_item', function ($user, $doc)
        {
            if (!isset($doc)) return false;

            if ($doc->STAFF_ID == $user->id) return true;

            foreach ($doc->paths as $p){
                if ($p->STAFF_ID == $user->id && isset($p->COMPLETE_DATE)){
                    // อยู่ในเส้นทาง และผ่านมาแล้ว
                    return true;
                }
            }

            // ได้รับการแชร์
            $docShare = $doc->shares->where('TO_STAFF_ID', $user->id)->first();
            if ($docShare != null) return true;

            // ไม่อยู่ในเส้นทาง
            $userItem = $doc->paths->where('STAFF_ID', $user->id)->first();
            if (!isset($userItem)) return false;

            // อยู่ในเส้นทาง ในลำดับที่ก่อนลำดับปัจจุบัน
            $currentSeq = $doc->paths->filter(function($p){
                return $p->PATH_ACTION_ID > 1 && !isset($p->COMPLETE_DATE);
            })->min('ITEM_SEQ');

            if (!isset($currentSeq)){
            	return true;
            }

            if ($userItem->ITEM_SEQ <= $currentSeq){
                return true;
            }

            return false;
        });

        Gate::define('document.is_owner', function ($user, $doc)
        {
            if (!isset($doc)) return false;
            if ($user->id == $doc->STAFF_ID){
                return true;
            }
            return false;
        });

        Gate::define('document.can_comment', function ($user, $doc)
        {
            // return true;
            if (!isset($doc)) return false;
            if ($doc->DOC_STATUS_ID == 1 || $doc->DOC_STATUS_ID == 3 || $doc->DOC_STATUS_ID == 4 ) return false;

            $userItem = $doc->paths->where('STAFF_ID', $user->id)->first();
            // if (isset($userItem) && $userItem->PATH_ACTION_ID == 3){
            //     return true;
            // }

            if (!isset($userItem)) return false;
            if (isset($userItem->COMPLETE_DATE)) return false;
            if ($userItem->PATH_ACTION_ID == 1) return false;

            $currentSeq = $doc->paths->filter(function($p){
                return $p->PATH_ACTION_ID > 1 && !isset($p->COMPLETE_DATE);
            })->min('ITEM_SEQ');

            if ($userItem->ITEM_SEQ == $currentSeq && ($userItem->PATH_ACTION_ID == 2 || $userItem->PATH_ACTION_ID == 3)){
                return true;
            }

            return true;

            // หาลำดับถัดไปเพื่อตรวจดูว่าได้ทำรายการหรือยัง เพราะหากยัง จะต้องสามารถคอมเม้นได้ (กรณีมีผู้คอมเม้นหลายคน)
            $nextSeq = $doc->paths->filter(function($p){
                return $p->PATH_ACTION_ID > 1  && $p->ITEM_SEQ > $currentSeq;
            })->min('ITEM_SEQ');

            $nextItem = $doc->paths->where('ITEM_SEQ', $nextSeq)->first();

            if (!isset($nextItem)) return true;
            if (isset($nextItem) && !isset($nextItem->COMPLETE_DATE)) return true;

            return false;
        });

        Gate::define('document.can_approve', function ($user, $doc)
        {
            // return true;
            if (!isset($doc)) return false;
            if ($doc->DOC_STATUS_ID == 1 || $doc->DOC_STATUS_ID == 3 || $doc->DOC_STATUS_ID == 4 ) return false;

            $userItem = $doc->paths->where('STAFF_ID', $user->id)->first();
            if (!isset($userItem)) return false;
            if ($userItem->PATH_ACTION_ID != 3) return false;

            $currentSeq = $doc->paths->filter(function($p){
                return $p->PATH_ACTION_ID > 1 && !isset($p->COMPLETE_DATE);
            })->min('ITEM_SEQ');


            if ($userItem->ITEM_SEQ == $currentSeq && $userItem->PATH_ACTION_ID == 3){
                return true;
            }

            return false;
        });

        Gate::define('document.can_add_file', function ($user, $doc)
        {
            if (!isset($doc)) return false;
            return true;
        });

    }


    private function canAccessPath($department, $path){
        if ($department->DEPARTMENT_ID == $path->DEPARTMENT_ID){
            return true;
        }else{
            if ($department->DEPARTMENT_ID != $department->MASTER_DEPARTMENT_ID){
                return $this->canAccessPath($department->masterDepartment, $path);
            }
        }
        return false;
    }
}
