<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Utils
{
  function monththai($month)
  {
		$strMonthCut=array(
  		"",  "มกราคม",  "กุมภาพันธ์",  "มีนาคม",  "เมษายน",  "พฤษภาคม",  "มิถุนายน",   "กรกฎาคม",  "สิงหาคม",  "กันยายน",  "ตุลาคม",  "พฤศจิกายน",  "ธันวาคม"
		);

		return $strMonthCut[$month];
  }


	function thainumber($number){
		$txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า','สิบ');
		$txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน');

		$number = str_replace(",","",$number);
		$number = str_replace(" ","",$number);
		$number = str_replace("บาท","",$number);
		$number = explode(".",$number);
		if(sizeof($number)>2){
			return 'ทศนิยมหลายตัวนะจ๊ะ';
			exit;
		}

		$strlen = strlen($number[0]);
		$convert = '';

		for($i=0;$i<$strlen;$i++){
			$n = substr($number[0], $i,1);
			if($n!=0){
				if($i==($strlen-1) AND $n==1){ $convert .= 'เอ็ด'; }
				elseif($i==($strlen-2) AND $n==2){  $convert .= 'ยี่'; }
				elseif($i==($strlen-2) AND $n==1){ $convert .= ''; }
				else{ $convert .= $txtnum1[$n]; }
				$convert .= $txtnum2[$strlen-$i-1];
			}
		}

		$convert .= 'บาท';
		if($number[1]=='0' OR $number[1]=='00' OR $number[1]==''){
			$convert .= 'ถ้วน';
		}else{
			$strlen = strlen($number[1]);
			for($i=0;$i<$strlen;$i++){
			$n = substr($number[1], $i,1);
				if($n!=0){
				if($i==($strlen-1) AND $n==1){$convert
				.= 'เอ็ด';}
				elseif($i==($strlen-2) AND
				$n==2){$convert .= 'ยี่';}
				elseif($i==($strlen-2) AND
				$n==1){$convert .= '';}
				else{ $convert .= $txtnum1[$n];}
				$convert .= $txtnum2[$strlen-$i-1];
				}
			}
			$convert .= 'สตางค์';
		}
		return $convert;
	}

	public function reformat_citizenid($citizenid){

		if (strlen($citizenid) == 13){
			$result = substr($citizenid, 0, 1)
				. '-' . substr($citizenid, 1, 4)
				. '-' . substr($citizenid, 5, 5)
				. '-' . substr($citizenid, 10, 2)
				. '-' . substr($citizenid, 12, 1);

				return $result;
		}

		return $citizenid;
	}

	public function thaidate($strDate, $showtime = null)
  {
		if (strtotime($strDate) != null && $strDate != '0000-00-00 00:00:00'){
    	$strYear = date("Y",strtotime($strDate))+543;
    	$strMonth= date("n",strtotime($strDate));
    	$strDay= date("j",strtotime($strDate));
    	$strHour= date("H",strtotime($strDate));
    	$strMinute= date("i",strtotime($strDate));
    	$strSeconds= date("s",strtotime($strDate));
    	$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    	$strMonthThai=$strMonthCut[$strMonth];

    	if ($showtime != null)
    		return "$strDay $strMonthThai $strYear, $strHour:$strMinute";

    	return "$strDay $strMonthThai $strYear";
		}
		return "-";
  }

	function thaifulldate($strDate, $showtime = null)
  {
		if (strtotime($strDate) != null && $strDate != '0000-00-00 00:00:00'){
    	$strYear = date("Y",strtotime($strDate))+543;
    	$strMonth= date("n",strtotime($strDate));
    	$strDay= date("j",strtotime($strDate));
    	$strHour= date("H",strtotime($strDate));
    	$strMinute= date("i",strtotime($strDate));
    	$strSeconds= date("s",strtotime($strDate));

			$strMonthCut=array(
    		"",  "มกราคม",  "กุมภาพันธ์",  "มีนาคม",  "เมษายน",  "พฤษภาคม",  "มิถุนายน",   "กรกฎาคม",  "สิงหาคม",  "กันยายน",  "ตุลาคม",  "พฤศจิกายน",  "ธันวาคม"
			);

    	$strMonthThai=$strMonthCut[$strMonth];

    	if ($showtime != null)
    		return "$strDay $strMonthThai $strYear, $strHour:$strMinute";

    	return "$strDay $strMonthThai พ.ศ.$strYear";
		}
		return "-";
  }


  // convert thaidate(dd/mm/yyyy) to UTC yyyy/mm/dd (-543)
	function thaiToUTC($date){
  	if (isset($date) && !empty($date)){

  		$d = explode('/', $date);

  		try{
    		if (count($d) == 3)
    			$d[2] -= 543;

    		return $d[2] . '/' . $d[1] . '/' . $d[0];
  		} catch(Exception $e){}
  	}
  	return null;
  }

  // yyy/mm/dd to dd/mm/yyyy+543
  function utcToThai($date){

  	if (isset($date) && !empty($date)){

  		$strYear = date("Y",strtotime($date))+543;
  		$strMonth= date("m",strtotime($date));
  		$strDay= date("d",strtotime($date));
  		$strHour= date("H",strtotime($date));
  		$strMinute= date("i",strtotime($date));
  		$strSeconds= date("s",strtotime($date));

  		//return $date;
  		//return "$strYear/$strMonth/$strDay";
			return "$strDay/$strMonth/$strYear";

  		/*

  		$d = explode('-', $date);

  		try{
  			if (count($d) == 3)
  				$d[0] += 543;

  				return $d[0] . '/' . $d[1] . '/' . $d[2];
  		} catch(Exception $e){}
  		*/
  	}
  	return null;
  }

	function showdate($date, $isThaiFormat = false){
		if (isset($date) && !empty($date) && !($date == '0000-00-00 00:00:00')){

			if ($isThaiFormat){ // dd/mm/yyyy+543 convert to yyyy/mm/dd
				$date = $this->thaiToUTC($date);
			}

  		$strYear = date("Y",strtotime($date)) + 543;
  		$strMonth= date("m",strtotime($date));
  		$strDay= date("d",strtotime($date));
  		$strHour= date("H",strtotime($date));
  		$strMinute= date("i",strtotime($date));
  		$strSeconds= date("s",strtotime($date));

  		//return $date;
  		//return "$strYear/$strMonth/$strDay";
			return "$strDay/$strMonth/$strYear";

			//return $this->utcToThai($date);

  		/*

  		$d = explode('-', $date);

  		try{
  			if (count($d) == 3)
  				$d[0] += 543;

  				return $d[0] . '/' . $d[1] . '/' . $d[2];
  		} catch(Exception $e){}
  		*/
  	}
  	return null;
	}

	// for CI validattion
	function valid_date($date)
	{
			$date1 = $this->thaiToUTC($date);
			$valid = DateTime::createFromFormat("Y/m/d", $date1);

			if ($valid){
				return true;
			}else{
				$this->form_validation->set_message('valid_date', '%s รูปแบบวันที่ไม่ถูกต้อง');
				return false;
			}
	}

}
