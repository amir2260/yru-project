<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Fund;
use App\Model\FundType;
use App\Model\Staff;

use App\Model\Program;
use App\Model\School;
use App\Model\EduLevel;
use App\Model\EduTerm;
use App\Model\Project;


class _FundController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $funds = Fund::paginate(20);

        return View('Fund.list')->with([
            'funds' => $funds ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request){
        $search = $request['TEXT_SEARCH'];

        $funds = Fund::where('FUND_NAME', 'like', '%' . $search . '%')
        ->orWhereHas('project', function($q) use ($search) {
            $q->where('PROJECT_NAME', 'like', '%' . $search . '%');
        })
        ->orWhereHas('program', function($q) use ($search) {
            $q->where('PROGRAM_NAME', 'like', '%' . $search . '%');
        })->paginate(20);

        return View('Fund.list')->with([
            'funds' => $funds ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function info($id){
        $fund = Fund::find($id);
        return View("Fund.info")->with([
            'fund' => $fund
        ]);
    }

    public function edit($id){
        $fund = Fund::find($id);
        $fundTypes = FundType::pluck('FUND_TYPE_NAME', 'FUND_TYPE_ID');
        $programs = Program::pluck('PROGRAM_NAME', 'PROGRAM_ID');
        $schools = School::pluck('SCHOOL_NAME', 'SCHOOL_ID');
        $eduLevels = EduLevel::pluck('EDU_LEVEL_NAME', 'EDU_LEVEL_ID');
        $projects = Project::pluck('PROJECT_NAME', 'PROJECT_ID');
        $terms = EduTerm::pluck('EDU_TERM_NAME', 'EDU_TERM_ID');

        return View('Fund.edit')->with([
            'fund' => $fund
            ,'fundTypes' => $fundTypes
            ,'programs' => $programs
            ,'schools' => $schools
            ,'eduLevels' => $eduLevels
            ,'projects' => $projects
            ,'terms' => $terms
        ]);
    }

    public function delete($id){
        $fund = Fund::find($id);
        if (isset($fund)){
            $fund->delete();
        }

        return redirect('/fund');
    }

    public function deactive($id){
        $fund = Fund::find($id);
        if (isset($fund)){
            $fund->FUND_STATUS_ID = 1;
            $fund->UPDATE_BY = Auth::user ()->name;
			$fund->UPDATE_DATE = date ( "Y-m-d H:i:s" );
            $fund->save();
        }

        return redirect('/fund');
    }


    public function create(){
        $fund = new Fund;

        $fundTypes = FundType::pluck('FUND_TYPE_NAME', 'FUND_TYPE_ID');
        $programs = Program::pluck('PROGRAM_NAME', 'PROGRAM_ID');
        $schools = School::pluck('SCHOOL_NAME', 'SCHOOL_ID');
        $eduLevels = EduLevel::pluck('EDU_LEVEL_NAME', 'EDU_LEVEL_ID');
        $projects = Project::pluck('PROJECT_NAME', 'PROJECT_ID');
        $terms = EduTerm::pluck('EDU_TERM_NAME', 'EDU_TERM_ID');


        return View('Fund.edit')->with([
            'fund' => $fund
            ,'fundTypes' => $fundTypes
            ,'programs' => $programs
            ,'schools' => $schools
            ,'eduLevels' => $eduLevels
            ,'projects' => $projects
            ,'terms' => $terms
        ]);
    }

    public function save(Request $request){

        $this->validate($request, [
            'FUND_NAME' => 'required'
            ,'PROJECT_ID' => 'required'
            ,'FUND_TYPE_ID' => 'required'
            ,'PROGRAM_ID' => 'required'
            ,'SCHOOL_ID' => 'required'
            ,'EDU_LEVEL_ID' => 'required'
            ,'REQUEST_AMOUNT' => 'required'
        ]);

        $fund = new Fund;
        if (isset($request['FUND_ID']) && $request['FUND_ID'] > 0){
            $fund = Fund::find($request['FUND_ID']);
        }

        if (!isset($fund)){
            $fund = new Fund;
            $fund->CREATE_BY = Auth::user ()->name;
			$fund->CREATE_DATE = date ( "Y-m-d H:i:s" );
        }

        $fund->FUND_NAME = $request['FUND_NAME'];
        $fund->PROJECT_ID = $request['PROJECT_ID'];
        $fund->EDU_TERM_ID = $request['EDU_TERM_ID'];
        $fund->FUND_TYPE_ID = $request['FUND_TYPE_ID'];
        $fund->PROGRAM_ID = $request['PROGRAM_ID'];
        $fund->SCHOOL_ID = $request['SCHOOL_ID'];
        $fund->EDU_LEVEL_ID = $request['EDU_LEVEL_ID'];
        $fund->REQUEST_AMOUNT = $request['REQUEST_AMOUNT'];

        $fund->UPDATE_BY = Auth::user ()->name;
        $fund->UPDATE_DATE = date ( "Y-m-d H:i:s" );
        $fund->save();

        return redirect('/fund');
    }



    // public function memberinfo($id){
    //     $member = Member::find($id);
    //     $staffTypes = StaffType::all();
    //
    //     return View('Member.info')->with(
    //         [
    //             'member'=>$member
    //             ,'stafftypes' => $staffTypes]
    //         );
    //
    //     }
    //
    //     public function memberedit($id){
    //         $member = Member::find($id);
    //         $staffTypes = StaffType::all();
    //
    //         return View('Member.register')->with(
    //             [
    //                 'member'=>$member
    //                 ,'stafftypes' => $staffTypes]
    //             );
    //
    //         }
    //
    //         public function memberdelete($id){
    //             $member = Member::find($id);
    //             if ($member){
    //                 $member->delete();
    //             }
    //
    //             return redirect('/member');
    //         }

            // public function memberjson(){
            //   $members = Member::all();
            //   return response()->json($members);
            // }

        }
