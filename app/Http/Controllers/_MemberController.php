<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Member;
use App\StaffType;

class MemberController extends Controller {

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function register(){
    $member = new Member;
    $staffTypes = StaffType::all();

    return View('Member.register')->with(
      [
        'member'=>$member
        ,'stafftypes' => $staffTypes]
    );
  }

  public function register_do(Request $request){

    $this->validate($request, [
        'CITIZEN_ID' => 'required',
        'MEMBER_NAME' => 'required',
    ]);

    $member = new Member;
    if (isset($request['MEMBER_ID']) && $request['MEMBER_ID'] > 0){
      $member = Member::find($request['MEMBER_ID']);
    }else {
      $member->CREATE_BY = Auth::user()->email;
      $member->CREATE_DATE = date("Y-m-d H:i:s");
    }

    //$member->MEMBER_ID = $request['MEMBER_ID'];
    //$member->STAFF_ID = $request['STAFF_ID'];
    $member->CITIZEN_ID = $request['CITIZEN_ID'];
    $member->MEMBER_NAME = $request['MEMBER_NAME'];
    $member->ADDRESS = $request['ADDRESS'];
    $member->PHONE = $request['PHONE'];
    $member->EMAIL = $request['EMAIL'];
    $member->STAFF_TYPE_ID = $request['STAFF_TYPE_ID'];
    $member->MEMBER_STATUS_ID = $request['MEMBER_STATUS_ID'];

    $member->save();


    return redirect('/member');
  }

  public function index(Request $request){
    $members = Member::paginate(20);
    return View('Member.list')->with([
        'members' => $members ,
        'text_search' => $request['TEXT_SEARCH']
      ]);
  }

  public function index_post(Request $request){
    $members = Member::where('MEMBER_NAME', 'like', '%' . $request['TEXT_SEARCH'] . '%')
      ->orWhere('CITIZEN_ID', 'like', '%' . $request['TEXT_SEARCH'] . '%')
      ->orWhere('PHONE', 'like', '%' . $request['TEXT_SEARCH'] . '%')
      ->orWhere('EMAIL', 'like', '%' . $request['TEXT_SEARCH'] . '%')
      ->paginate(20);

    return View('Member.list')->with([
        'members' => $members ,
        'text_search' => $request['TEXT_SEARCH']
      ]);
  }

  public function memberinfo($id){
    $member = Member::find($id);
    $staffTypes = StaffType::all();

    return View('Member.info')->with(
      [
        'member'=>$member
        ,'stafftypes' => $staffTypes]
    );

  }

  public function memberedit($id){
    $member = Member::find($id);
    $staffTypes = StaffType::all();

    return View('Member.register')->with(
      [
        'member'=>$member
        ,'stafftypes' => $staffTypes]
    );

  }

  public function memberdelete($id){
    $member = Member::find($id);
    if ($member){
      $member->delete();
    }

    return redirect('/member');
  }

  // public function memberjson(){
  //   $members = Member::all();
  //   return response()->json($members);
  // }

}
