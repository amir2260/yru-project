<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Project;
use App\Model\Department;
use App\Model\Staff;
use App\Model\Term;
use App\Model\CategoryType;
use App\Model\ProjectCategory;
use App\Model\Category;
use App\Model\Activity;
use App\Model\ActivityDetail;
use App\Model\Action;
use App\Model\ActionBudget;
use App\Model\BudgetGroup;
use App\Model\BudgetDetail;
use App\Model\BudgetType;
use App\Model\ActivityBudget;
use App\Model\Course;
use App\Model\Indicator;
use App\Model\IndicatorGroup;
use App\Model\IndicatorType;


class ProjectController extends Controller {

	public function __construct() {
		$this->middleware ( 'auth' );
	}

	public function index(Request $request) {
		$projectModel = new Project;
		$projects = $projectModel->getByStatus(Auth::user()->id, 1);

		return View ( 'Project.list' )->with ( [
				'projects' => $projects->paginate(20),
				'title' => 'ร่างโครงการ',
				'desc' => 'ร่างโครงการ รอการนำเสนอ',
				'search' => [
						'TEXT_SEARCH' => '',
						'terms' => Term::pluck ( 'TERM_NAME', 'TERM_ID' )
				]
		] );
	}

	public function pending(Request $request) {
		$projectModel = new Project;
		$projects = $projectModel->getByStatus(Auth::user()->id, 2);

		return View ( 'Project.list' )->with ( [
				'projects' => $projects->paginate(20),
				'title' => 'โครงการรอการอนุมัติ',
				'desc' => 'โครงการที่ถูกนำเสนอ และรอการอนุมัติ',
				'search' => [
						'TEXT_SEARCH' => '',
						'terms' => Term::pluck ( 'TERM_NAME', 'TERM_ID' )
				]
		] );
	}

	public function approvelist(Request $request) {
		$projectModel = new Project;
		$projects = $projectModel->getByStatus(Auth::user()->id, 3);

		return View ( 'Project.list' )->with ( [
				'projects' => $projects->paginate(20),
				'title' => 'โครงการผ่านการอนุมัติ',
				'desc' => 'รายละเอียดโครงการทั้งหมดที่ผ่านการอนุมัติ',
				'search' => [
						'TEXT_SEARCH' => '',
						'terms' => Term::pluck ( 'TERM_NAME', 'TERM_ID' )
				]
		] );
	}

	public function projectlist(Request $request) {
		$projectModel = new Project;
		$projects = $projectModel->getByStatus(Auth::user()->id, 3);

		return View ( 'Project.projectlist' )->with ( [
				'projects' => $projects->paginate(20),
				'title' => 'บันทึกการจัดกิจกรรม/ใช้งบประมาณ',
				'desc' => 'เลือกโครงการเพื่อทำการบันทึกผลการใช้งบประมาณ หรือจัดกิจกรรม',
				'search' => [
						'TEXT_SEARCH' => '',
						'terms' => Term::pluck ( 'TERM_NAME', 'TERM_ID' )
				]
		] );
	}

	public function rejectlist(Request $request) {
		$projectModel = new Project;
		$projects = $projectModel->getByStatus(Auth::user()->id, 4);

		return View ( 'Project.list' )->with ( [
				'projects' => $projects->paginate(20),
				'title' => 'โครงการไม่ผ่านการอนุมัติ',
				'desc' => 'รายละเอียดโครงการทั้งหมดที่ไม่ผ่านการอนุมัติ',
				'search' => [
						'TEXT_SEARCH' => '',
						'terms' => Term::pluck ( 'TERM_NAME', 'TERM_ID' )
				]
		] );
	}

	public function returnlist(Request $request) {
		$projectModel = new Project;
		$projects = $projectModel->getByStatus(Auth::user()->id, 5);

		return View ( 'Project.list' )->with ( [
				'projects' => $projects->paginate(20),
				'title' => 'โครงการรอการปรับแก้',
				'desc' => 'โครงการที่ไม่ผ่านการพิจารณา และมีการเสนอให้มีการปรับแก้รายละเอียด',
				'search' => [
						'TEXT_SEARCH' => '',
						'terms' => Term::pluck ( 'TERM_NAME', 'TERM_ID' )
				]
		] );
	}

	public function index_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];

		$projects = Project::where('IS_TEMPLATE', 1)
			->where ( 'PROJECT_CODE', 'like', '%' . $search . '%' )->orWhere ( 'PROJECT_NAME', 'like', '%' . $search . '%' )->orWhereHas ( 'projectOwner', function ($q) use ($search) {
				$q->where ( 'PROJECT_OWNER_NAME', 'like', '%' . $search . '%' );
		} )->paginate ( 20 );

		return View ( 'Project.list' )->with ( [
				'projects' => $projects,
				'text_search' => $request ['TEXT_SEARCH']
		] );
	}

	public function selectTemplate(Request $request) {
		$projects = Project::where('IS_TEMPLATE', 1)->paginate ( 20 );

		return View ( 'Project.selecttemplate' )->with ( [
				'projects' => $projects,
				'search' => [
						'TEXT_SEARCH' => '',
						'terms' => Term::pluck ( 'TERM_NAME', 'TERM_ID' )
				]
		] );
	}

	public function selectTemplate_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];

		$projects = Project::where ( 'PROJECT_CODE', 'like', '%' . $search . '%' )->where('IS_TEMPLATE', 1)
			->orWhere ( 'PROJECT_NAME', 'like', '%' . $search . '%' )->orWhereHas ( 'projectOwner', function ($q) use ($search) {
				$q->where ( 'PROJECT_OWNER_NAME', 'like', '%' . $search . '%' );
		} )->paginate ( 20 );

		return View ( 'Project.selecttemplate' )->with ( [
				'projects' => $projects,
				'text_search' => $request ['TEXT_SEARCH']
		] );
	}

	public function info($id) {
		$project = Project::find ( $id );

		// dd($project->getRequestAmount());
		return View ( "Project.info" )->with ( [
				'project' => $project,
				'categoryTypes' => CategoryType::all ()
				,'budgetGroups' => BudgetGroup::all()
				,'budgetTypes' => BudgetType::all()
				,'budgetDetails' => BudgetDetail::all()
		] );
	}

	public function projectActivity($id) {
		$project = Project::find ( $id );
		return View ( "Project.projectActivity" )->with ( [
				'project' => $project,
				'categoryTypes' => CategoryType::all ()
				,'budgetGroups' => BudgetGroup::all()
				,'budgetTypes' => BudgetType::all()
				,'budgetDetails' => BudgetDetail::all()
		] );
	}

	public function templateInfo($id) {
		$project = Project::find ( $id );
		return View ( "Project.templateinfo" )->with ( [
				'project' => $project,
				'categoryTypes' => CategoryType::all ()
				,'budgetGroups' => BudgetGroup::all()
				,'budgetTypes' => BudgetType::all()
				,'budgetDetails' => BudgetDetail::all()
		] );
	}

	public function createFromTemplate($id){

		$project = Project::find($id);
		if (isset($project)){
			// $newProject->PROJECT_ID = 0;
			// $newProject = $project->replicate();
			// $newProject->save();
			$newProject = $project->replicate();
			$newProject->PROJECT_ID = null;
			$newProject->PROJECT_CODE = "";
			$newProject->IS_TEMPLATE = 0;

			// TODO : อย่าลืม
			//$newProject->DEPARTMENT_ID = 0;
			$newProject->save();

			$id = $newProject->PROJECT_ID;

			foreach ($project->activities as $item){
				echo "1 <br />";
				$newItem = $item->replicate();
				$newItem->ACTIVITY_ID = null;
				$newItem->PROJECT_ID = $id;
				$newItem->save();
			}

			foreach ($project->projectOwners as $item){
				echo "2 <br />";
				$newItem = $item->replicate();
				$newItem->PROJECT_OWNER_ID = null;
				$newItem->PROJECT_ID = $id;
				$newItem->save();
			}

			foreach ($project->categories as $item){
				echo "3 <br />";
				$newItem = $item->replicate();
				$newItem->PROJECT_CATEGORY_ID = null;
				$newItem->PROJECT_ID = $id;
				$newItem->save();
			}
			return redirect()->action('ProjectController@info', $id);
		}

		return redirect()->back();
	}

	public function delete($id) {
		$project = Project::find ( $id );
		if (isset ( $project )) {
			$project->delete ();
		}

		return redirect ( '/project' );
	}

	public function create() {
		$project = new Project ();
		$staffs = Staff::pluck ( 'FULL_NAME', 'STAFF_ID' );
		$departments = Department::where ( 'DEPARTMENT_ID', '<>', 0 )->pluck ( 'DEPARTMENT_DESC', 'DEPARTMENT_ID' );

		return View ( 'Project.edit' )->with ( [
				'project' => $project,
				'staffs' => $staffs,
				'terms' => Term::pluck ( 'TERM_NAME', 'TERM_ID' ),
				'departments' => $departments,
				'categories' => Category::all(),
				'categoryTypes' => CategoryType::all ()
		] );
	}

	public function edit($id) {
		$project = Project::find ( $id );

		$staffs = Staff::pluck ( 'FULL_NAME', 'STAFF_ID' );
		$departments = Department::where ( 'DEPARTMENT_ID', '<>', 0 )->pluck ( 'DEPARTMENT_DESC', 'DEPARTMENT_ID' );
		$categories = Category::whereHas('projectCategories', function($projectCategories) use ($id) {
			$projectCategories->where('PROJECT_ID', $id);
		})->get();

		return View ( "Project.edit" )->with ( [
				'project' => $project,
				'staffs' => $staffs,
				'terms' => Term::pluck ( 'TERM_NAME', 'TERM_ID' ),
				'departments' => $departments,
				'categories' => $categories,
				'categoryTypes' => CategoryType::all ()
		] );
	}

	public function save(Request $request) {
		$this->validate ( $request, [
				'PROJECT_NAME' => 'required',
				'PROJECT_CODE' => 'required',
				'DEPARTMENT_ID' => 'required',
				'COST' => 'required|numeric',
				'REASON' => 'required',
				'TERM_ID' => 'required',
				'RESPACT' => 'required'
		] );

		$project = null;
		$isNew = false;
		if (isset ( $request ['PROJECT_ID'] ) && $request ['PROJECT_ID'] > 0) {
			$project = Project::find ( $request ['PROJECT_ID'] );
		}

		if (! isset ( $project )) {
			$isNew = true;
			$project = new Project ();

			$project->PROJECT_STATUS_ID = 1; // is draff
			$project->CREATE_BY = Auth::user ()->name;
			$project->CREATE_DATE = date ( "Y-m-d H:i:s" );
		}

		$project->UPDATE_BY = Auth::user ()->name;
		$project->UPDATE_DATE = date ( "Y-m-d H:i:s" );

		$project->PROJECT_NAME = $request ['PROJECT_NAME'];
		$project->PROJECT_CODE = $request ['PROJECT_CODE'];
		$project->DEPARTMENT_ID = $request ['DEPARTMENT_ID'];
		$project->COST = $request ['COST'];
		$project->REASON = $request ['REASON'];
		$project->TERM_ID = $request ['TERM_ID'];
		$project->RESPACT = $request ['RESPACT'];

		$project->save ();

		$selectList = $request ['CATEGORY_ID'];
		if ($isNew) {
			// // add all to project.
			$categoryTypes = CategoryType::all ();

			foreach ( $categoryTypes as $t ) {
				foreach ( $t->categories as $c ) {
					$projectCategory = new ProjectCategory ();

					$projectCategory->PROJECT_ID = $project->PROJECT_ID;
					$projectCategory->CATEGORY_ID = $c->CATEGORY_ID;
					$projectCategory->IS_SELECTED = 0;

					if (isset ( $selectList )) {
						foreach ( $selectList as $select ) {
							if ($select == $c->CATEGORY_ID) {
								$projectCategory->IS_SELECTED = 1;
							}
						}
					}

					$projectCategory->save ();
				}
			}
		}else{
			foreach ($project->categories as $c) {
				$isSelect = 0;

				if (isset ( $selectList )) {
					foreach ( $selectList as $select ) {
						if ($select == $c->CATEGORY_ID) {
							$isSelect = 1;
						}
					}
				}

				$c->IS_SELECTED = $isSelect;
				$c->save();
			}
		}

		return redirect()->action('ProjectController@info', $project->PROJECT_ID);
	}


	public function addActivity($id){
		// id = project_id
		$project = Project::find($id);
		if (isset($project)){
			$activity = new Activity;

			return view('Activity.edit')->with([
					'project'=>$project
					, 'activity'=>$activity
			]);
		}
	}

	public function editActivity($id){
		// id = activity_id

		$activity = Activity::find($id);
		if (isset($activity)){
			$project = $activity->project;

			$courseModel = new Course;
			$courses = $courseModel->gets();

			return view('Activity.edit')->with([
					'project'=>$project
					, 'activity'=>$activity
					, 'courses' => $courses
			]);
		}
	}

	public function deleteActivity($id){
		// id = activity_id

		$activity = Activity::find($id);
		if (isset($activity)){
			$activity->delete();
			return redirect()->action('ProjectController@info', $activity->PROJECT_ID);
		}
	}

	public function saveActivity(Request $request){
		$this->validate ( $request, [
				'ACTIVITY_NAME' => 'required'
				,'PROJECT_ID' => 'required'
		] );

		$activity = new Activity;
		$activity->CREATE_BY = Auth::user ()->name;

		$item = Activity::find($request['ACTIVITY_ID']);
		if (isset($item)){
			$activity = $item;
		}

		$activity->PROJECT_ID= $request['PROJECT_ID'];
		$activity->ACTIVITY_NAME = $request['ACTIVITY_NAME'];
		$activity->UPDATE_BY = Auth::user ()->name;

		$activity->save();

		return redirect()->action('ProjectController@editActivity', $activity->ACTIVITY_ID);
	}

	// public function addCourse(Request $request){
	// 	$this->validate ( $request, [
	// 			'COURSE_ID' => 'required'
	// 			,'ACTIVITY_ID' => 'required'
	// 	] );

	// 	$cou = new Activity;
	// 	$activity->CREATE_BY = Auth::user ()->name;

	// 	$item = Activity::find($request['ACTIVITY_ID']);
	// 	if (isset($item)){
	// 		$activity = $item;
	// 	}

	// 	$activity->PROJECT_ID= $request['PROJECT_ID'];
	// 	$activity->ACTIVITY_NAME = $request['ACTIVITY_NAME'];
	// 	$activity->UPDATE_BY = Auth::user ()->name;

	// 	$activity->save();

	// 	return redirect()->action('ProjectController@editActivity', $activity->ACTIVITY_ID);
	// }


	public function addActivityDetail($id){
		// $id = activity_id

		$activity= Activity::find($id);
		if (isset($activity)){
			$activityDetail = new ActivityDetail;
			$staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

			return view('Activity.editDetail')->with([
					'activityDetail' => $activityDetail
					, 'activity'=>$activity
					, 'budgetDetails' => BudgetDetail::all()
					, 'budgetGroups' => BudgetGroup::all()
					, 'courses' => Course::pluck('COURSE_NAME', 'COURSE_ID') // TODO : ผิด ... ต้องเอาหลักสูตรจากผู้ที่ Login
			]);
		}
	}

	public function editActivityDetail($id){
		// id = activity_detail_id
		$activityDetail = ActivityDetail::find($id);
		if (isset($activityDetail)){
			$project = $activityDetail->activity->project;			


			return view('Activity.editDetail')->with([
					'project'=>$project					
					, 'activityDetail'=> $activityDetail
					, 'budgetDetails' => BudgetDetail::pluck('BUDGET_DETAIL_NAME', 'BUDGET_DETAIL_ID')
					, 'budgetGroups' => BudgetGroup::pluck('BUDGET_GROUP_NAME', 'BUDGET_GROUP_ID')
			]);
		}
	}

	public function deleteActivityDetail($id){
		// id = activity_detail_id
		$activityDetail = ActivityDetail::find($id);
		if (isset($activityDetail)){
			$activityDetail->delete();
			return redirect()->action('ProjectController@info', $activityDetail->activity->PROJECT_ID);
		}
	}

    public function saveActivityDetail(Request $request){

        $validate = ['ACTIVITY_DETAIL_NAME' => 'required'];

        $budgetGroups = BudgetGroup::all();
        foreach ($budgetGroups as $g){
            $name = 'BUDGET_GROUP_' . $g->BUDGET_GROUP_ID;
            $validate += [$name => 'numeric|required'];
        }

        $this->validate ( $request, $validate );

        $activity = Activity::find($request['ACTIVITY_ID']);
        $detailId =  $request['BUDGET_DETAIL_ID'];

        if (isset($activity)){
            // บันทึกกิจกรรมที่เพิ่มเข้ามา
            $activityDetail = new ActivityDetail;

            $activityDetail->CREATE_BY = Auth::user ()->name;
            $activityDetail->ACTIVITY_ID = $request['ACTIVITY_ID'];

            $item = ActivityDetail::find($request['ACTIVITY_DETAIL_ID']);
            if (isset($item)) $activityDetail= $item;

            $activityDetail->ACTIVITY_DETAIL_NAME= $request['ACTIVITY_DETAIL_NAME'];
            $activityDetail->BUDGET_DETAIL_ID = $request['BUDGET_DETAIL_ID'];
            $activityDetail->UPDATE_BY = Auth::user ()->name;
            $activityDetail->COURSE_ID = $request['COURSE_ID'];
            $activityDetail->save();

            // บันทึกจำนวนเงินที่ต้องใช้ในกิจกรรมดังกล่าว ในหมวดเงินต่างๆ
            $total = 0;
            foreach ($budgetGroups as $g){
                $name = 'BUDGET_GROUP_' . $g->BUDGET_GROUP_ID;

                if ($request->has($name)){
                    // ถ้ามีการป้อนเข้ามา บันทึกซะ
                    $total += $request[$name];

                    $activityBudget = new ActivityBudget;
                    $activityBudget->ACTIVITY_DETAIL_ID = $activityDetail->ACTIVITY_DETAIL_ID;
                    $activityBudget->BUDGET_GROUP_ID = $g->BUDGET_GROUP_ID;
                    $activityBudget->CREATE_BY = Auth::user ()->name;

                    // หาค่า เคยมีการคีย์รายการนี้หรือยัง
                    $item = ActivityBudget::where('ACTIVITY_DETAIL_ID', $activityDetail->ACTIVITY_DETAIL_ID)
                        ->where('BUDGET_GROUP_ID', $g->BUDGET_GROUP_ID)
                        ->first();

                    if (isset($item)){
                        // ถ้ามีแล้ว ก็ให้เป็นการแก้ไข
                        $activityBudget = $item;
                    }

                    $activityBudget->BUDGET_AMOUNT= $request[$name];
                    $activityBudget->UPDATE_BY = Auth::user ()->name;

                    $activityBudget->save();
                }
            }



        }

        return redirect()->action('ProjectController@info', $activity->PROJECT_ID);


        /*


        $item = ActivityDetail::find($request['ACTIVITY_DETAIL_ID']);
        if (isset($item)){
            $activityDetail= $item;
        }




        $activityDetail->ACTIVITY_ID= $request['ACTIVITY_ID'];
        $activityDetail->ACTIVITY_DETAIL_NAME= $request['ACTIVITY_DETAIL_NAME'];

        $activityDetail->BUDGET_AMOUNT= $request['BUDGET_AMOUNT'];
        $activityDetail->BUDGET_GROUP_ID= $request['BUDGET_GROUP_ID'];
        $activityDetail->BUDGET_DETAIL_ID= $request['BUDGET_DETAIL_ID'];

        $activityDetail->UPDATE_BY = Auth::user ()->name;

        $activityDetail->save();


        */
    }


    public function send($id){
		$project = Project::find ( $id );
		if (isset ( $project )) {
			$project->PROJECT_STATUS_ID = 2;
			$project->UPDATE_BY = Auth::user ()->name;
			$project->UPDATE_DATE = date ( "Y-m-d H:i:s" );

			$project->save ();
		}

		return redirect()->action('ProjectController@index');
	}

    public function approve($id){
		$project = Project::find ( $id );
		if (isset ( $project )) {
			$project->PROJECT_STATUS_ID = 3;
			$project->UPDATE_BY = Auth::user ()->name;
			$project->UPDATE_DATE = date ( "Y-m-d H:i:s" );

			$project->save ();
		}

		return redirect()->action('ProjectController@index');
	}

	public function reject($id){
		$project = Project::find ( $id );
		if (isset ( $project )) {
			$project->PROJECT_STATUS_ID = 4;
			$project->UPDATE_BY = Auth::user ()->name;
			$project->UPDATE_DATE = date ( "Y-m-d H:i:s" );

			$project->save ();
		}
		return redirect()->action('ProjectController@index');
	}

	public function returnProject($id){
		$project = Project::find ( $id );
		if (isset ( $project )) {
			$project->PROJECT_STATUS_ID = 5;
			$project->UPDATE_BY = Auth::user ()->name;
			$project->UPDATE_DATE = date ( "Y-m-d H:i:s" );

			$project->save ();
		}
		return redirect()->action('ProjectController@index');
	}

	public function addAction($id){

		// id = ACTIVITY_DETAIL_ID
        $activityDetail =  ActivityDetail::find($id);
        if (isset($activityDetail)){
			$project = $activityDetail->activity->project;
			
			// dd (Auth::user()->staff);
			// $staff = Auth::user()->staff;
			// dd ($staff->getCourses()->pluck('COURSE_NAME', 'COURSE_ID'));

			// dd ($activityDetail->course->pluck('COURSE_NAME', 'COURSE_ID'));
			
            return view('Action.editDetail')->with([
                'project'=>$project
                ,'activity'=>$activityDetail->activity
				, 'action' => new Action
                , 'activityDetail'=> $activityDetail
                , 'budgetDetails' => BudgetDetail::all()
                , 'budgetGroups' => BudgetGroup::all()
                , 'course' => $activityDetail->course // TODO : ผิด ... ต้องเอาหลักสูตรจากผู้ที่ Login
            ]);

        }

	}

	public function editAction($id){

		// id = ACTION_ID
		$action = Action::find($id);
		if (isset($action)){
			$activityDetail	 = $action->activityDetail;
            $project = $activityDetail->activity->project;

            return view('Action.editDetail')->with([
                'project'=>$project
                ,'activity'=>$activityDetail->activity
				, 'action' => $action
                , 'activityDetail'=> $activityDetail
                , 'budgetDetails' => BudgetDetail::all()
                , 'budgetGroups' => BudgetGroup::all()
                , 'course' => $activityDetail->course // TODO : ผิด ... ต้องเอาหลักสูตรจากผู้ที่ Login
            ]);
        }
	}


	public function deleteAction($id){

		// id = ACTION_ID
		$action = Action::find($id);
		if (isset($action)){
			$action->delete();
        }

		$activityDetail = ActivityDetail::find($action->ACTIVITY_DETAIL_ID);
        return redirect()->action('ProjectController@projectActivity', $activityDetail->activity->PROJECT_ID);
	}


    public function saveAction(Request $request){

        $validate = ['ACTION_DETAIL' => 'required'
                        , 'ACTION_NAME' => 'required'
                    ];

//        $budgetGroups = BudgetGroup::all();
//        foreach ($budgetGroups as $g){
//            $name = 'BUDGET_GROUP_' . $g->BUDGET_GROUP_ID;
//            $validate += [$name => 'numeric|required'];
//        }

        $this->validate ( $request, $validate );

        $activityDetail = ActivityDetail::find($request['ACTIVITY_DETAIL_ID']);
        $activityDetailId =  $request['ACTIVITY_DETAIL_ID'];

        if (isset($activityDetail)){
            // บันทึกกิจกรรมที่เพิ่มเข้ามา
            $action = new Action;

			$item = Action::find ($request->input('ACTION_ID'));

			if (isset($item)){
				$action = $item;
				$action->CREATE_BY = Auth::user ()->name;
			}

            $action->ACTIVITY_DETAIL_ID = $request['ACTIVITY_DETAIL_ID'];

            $item = Action::find($request['ACTION_ID']);
            if (isset($item)) $action= $item;

            $action->ACTION_NAME= $request['ACTION_NAME'];
			$action->ACTION_DETAIL= $request['ACTION_DETAIL'];
			
			$action->LOCATION= $request['LOCATION'];
			$action->TARGET= $request['TARGET'];
			$action->TROUBLE= $request['TROUBLE'];
			$action->COMMENT= $request['COMMENT'];
			$action->KPI= $request['KPI'];

            $action->REF_NO = $request['REF_NO'];
            $action->UPDATE_BY = Auth::user ()->name;
            $action->DEPARTMENT_ID = $request['DEPARTMENT_ID'];


            $action->save();

            // บันทึกจำนวนเงินที่ต้องใช้ในกิจกรรมดังกล่าว ในหมวดเงินต่างๆ
            $total = 0;

			$budgetGroups = BudgetGroup::all();
            foreach ($budgetGroups as $g){
                $name = 'BUDGET_GROUP_' . $g->BUDGET_GROUP_ID;

                if ($request->has($name)){
                    // ถ้ามีการป้อนเข้ามา บันทึกซะ
                    $total += $request[$name];

                    $actionBudget = new ActionBudget;
                    $actionBudget->ACTION_ID = $action->ACTION_ID;
                    $actionBudget->BUDGET_GROUP_ID = $g->BUDGET_GROUP_ID;
                    $actionBudget->CREATE_BY = Auth::user ()->name;

                    // หาค่า เคยมีการคีย์รายการนี้หรือยัง
                    $item = ActionBudget::where('ACTION_ID', $action->ACTION_ID)
                        ->where('BUDGET_GROUP_ID', $g->BUDGET_GROUP_ID)
                        ->first();

					// dd ($actionBudget);

                    if (isset($item)){
                        // ถ้ามีแล้ว ก็ให้เป็นการแก้ไข
                        $actionBudget = $item;
                    }

                    $actionBudget->AMOUNT= $request[$name];
                    $actionBudget->UPDATE_BY = Auth::user ()->name;

                    $actionBudget->save();
                }
            }

        }

		$activityDetail = ActivityDetail::find($request['ACTIVITY_DETAIL_ID']);
        return redirect()->action('ProjectController@projectActivity', $activityDetail->activity->PROJECT_ID);


        /*


        $item = ActivityDetail::find($request['ACTIVITY_DETAIL_ID']);
        if (isset($item)){
            $activityDetail= $item;
        }




        $activityDetail->ACTIVITY_ID= $request['ACTIVITY_ID'];
        $activityDetail->ACTIVITY_DETAIL_NAME= $request['ACTIVITY_DETAIL_NAME'];

        $activityDetail->BUDGET_AMOUNT= $request['BUDGET_AMOUNT'];
        $activityDetail->BUDGET_GROUP_ID= $request['BUDGET_GROUP_ID'];
        $activityDetail->BUDGET_DETAIL_ID= $request['BUDGET_DETAIL_ID'];

        $activityDetail->UPDATE_BY = Auth::user ()->name;

        $activityDetail->save();


        */
	}
	

	public function addIndicator($id){
		// id = project_id
		$project = Project::find($id);
		if (isset($project)){
			$indicator = new Indicator;
			$groups = IndicatorGroup::pluck('INDICATOR_GROUP_NAME', 'INDICATOR_GROUP_ID');
			$types = IndicatorType::pluck('INDICATOR_TYPE_NAME', 'INDICATOR_TYPE_ID');

			return view('indicator.edit')->with([
					'project'=>$project
					, 'indicator'=>$indicator
					, 'groups'=>$groups
					, 'types'=>$types
			]);
		}
	}

	public function editIndicator($id){
		// id = activity_id

		$indicator = Indicator::find($id);
		if (isset($indicator)){
			$project = $indicator->project;

			return view('indicator.edit')->with([
					'project'=>$project
					, 'indicator'=>$indicator
			]);
		}
	}

	public function deleteIndicator($id){
		// id = activity_id

		$indicator = Indicator::find($id);
		if (isset($indicator)){
			$indicator->delete();
			return redirect()->action('ProjectController@info', $indicator->PROJECT_ID);
		}
	}

	public function saveIndicator(Request $request){
		$this->validate ( $request, [
				'PROJECT_ID' => 'required'
		] );

		$indicator = new Indicator;

		$item = Indicator::find($request['INDICATOR_ID']);
		if (isset($item)){
			$indicator = $item;
		}

		$indicator->PROJECT_ID= $request['PROJECT_ID'];
		$indicator->INDICATOR_GROUP_ID = $request['INDICATOR_GROUP_ID'];
		$indicator->INDICATOR_TYPE_ID = $request['INDICATOR_TYPE_ID'];
		$indicator->TARGET = $request['TARGET'];
		$indicator->save();

		return redirect()->action('ProjectController@info', $indicator->PROJECT_ID);
	}



}
