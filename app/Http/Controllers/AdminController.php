<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Admin;
use App\Model\Department;
use App\Model\Staff;


class AdminController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $admins = Admin::paginate(20);

        return View('Admin.list')->with([
            'admins' => $admins ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request){
        $search = $request['TEXT_SEARCH'];

        $admins = Admin::whereHas('staff', function($q) use ($search) {
            $q->where('FULL_NAME', 'like', '%' . $search . '%');
        })->paginate(20);

        return View('Admin.list')->with([
            'admins' => $admins ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function info($id){
        $admin = Admin::find($id);
        return View("Admin.info")->with([
            'admin' => $admin
        ]);
    }

    public function delete($id){
        $admin = Admin::find($id);
        if (isset($admin)){
            $admin->delete();
        }

        return redirect('/admin');
    }


    public function create()
    {
        $admin = new Admin;
        $staff_items = Staff::all();
        // $departments = Department::where('MASTER_DEPARTMENT_ID', 0)->get();

        $staffs = array();
        foreach ($staff_items as $item){
            $staffs[] = $item->FULL_NAME;
        }

        return View('Admin.edit')->with([
            'admin' => $admin
            , 'staffs' => json_encode($staffs)
            // , 'departments'=>$departments
        ]);
    }

    public function save(Request $request){

        $this->validate($request, [
            'STAFF_NAME' => 'required|valid_staffname',
        ]);

        $staff = Staff::where('FULL_NAME', $request['STAFF_NAME'])->first();

        $admin = Admin::where('STAFF_ID', $staff->STAFF_ID)->first();

        if (!isset($admin)){
            $admin = new Admin;

            $admin->STAFF_ID = $staff->STAFF_ID;
            $admin->IS_ADMIN = 1;

            $admin->CREATE_BY = Auth::user()->name;
            $admin->CREATE_DATE = date("Y-m-d H:i:s");

            $admin->save();
        }

        return redirect('/admin');
    }



    // public function memberinfo($id){
    //   $member = Member::find($id);
    //   $staffTypes = StaffType::all();
    //
    //   return View('Member.info')->with(
    //     [
    //       'member'=>$member
    //       ,'stafftypes' => $staffTypes]
    //   );
    //
    // }
    //
    // public function memberedit($id){
    //   $member = Member::find($id);
    //   $staffTypes = StaffType::all();
    //
    //   return View('Member.register')->with(
    //     [
    //       'member'=>$member
    //       ,'stafftypes' => $staffTypes]
    //   );
    //
    // }
    //
    // public function memberdelete($id){
    //   $member = Member::find($id);
    //   if ($member){
    //     $member->delete();
    //   }
    //
    //   return redirect('/member');
    // }

}
