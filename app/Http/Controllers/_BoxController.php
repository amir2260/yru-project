<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Box;
use App\Model\Staff;
use App\Model\Manager;


class BoxController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (!Auth::user()) {
            abort(403, 'Unauthorized action.');
        }

        $boxs = Box::where('PARRENT_BOX_ID', 0)
        ->where('STAFF_ID', Auth::user()->id )->orderBy('BOX_NAME')->paginate(20);

        return View('Box.list')->with([
            'boxs' => $boxs ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request)
    {
        if (!Auth::user()) {
            abort(403, 'Unauthorized action.');
        }

        $search = $request['TEXT_SEARCH'];

        $boxs = Box::where('STAFF_ID', Auth::user()->id )
        ->where('BOX_NAME', 'like' , '%' . $search . '%')
        ->orderBy('BOX_NAME')->paginate(20);

        return View('Box.list')->with([
            'boxs' => $boxs ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function info($id)
    {
        if (!Auth::user()) {
            abort(403, 'Unauthorized action.');
        }

        $box = Box::find($id);
        $boxs = Box::where('PARRENT_BOX_ID', 0)
        ->where('STAFF_ID', Auth::user()->id )->orderBy('BOX_NAME')->paginate(20);

        if (!Auth::user()->can('box.access_item', $box)) {
            abort(403, 'Unauthorized action.');
        }

        return View("Box.info")->with([
            'box' => $box,
            'boxs' => $boxs
        ]);
    }

    public function managerinfo($id)
    {
        if (!Auth::user()) {
            abort(403, 'Unauthorized action.');
        }

        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();
        $box = Box::find($id);
        if (!Auth::user()->can('box.access_item', $box)) abort(403, 'Unauthorized action.');

        $boxs = Box::where('PARRENT_BOX_ID', 0)
        ->where('DEPARTMENT_ID', $staff->managers[0]->DEPARTMENT_ID )
        ->orderBy('BOX_NAME')->paginate(20);

        return View('Box.managerinfo')->with([
            'staff' => $staff,
            'box' => $box,
            'boxs' => $boxs
            // 'text_search' => $request['TEXT_SEARCH']
        ]);



        // return View("Box.managerinfo")->with([
        //     'box' => $box,
        //     'boxs' => $boxs
        // ]);
    }


    public function edit($id)
    {
        if (!Auth::user()) {
            abort(403, 'Unauthorized action.');
        }

        $box = Box::find($id);

        if (!Auth::user()->can('box.access_item', $box)) {
            abort(403, 'Unauthorized action.');
        }

        return View("Box.edit")->with([
            'box' => $box
        ]);
    }


    public function delete($id)
    {
        if (!Auth::user()) {
            abort(403, 'Unauthorized action.');
        }

        $box = Box::find($id);

        if (!Auth::user()->can('box.access_item', $box)) {
            abort(403, 'Unauthorized action.');
        }


        // TODO : ลบตู้แล้ว ลบส่วนที่เกี่ยวข้องด้วย
        $box->delete();

        if (isset($box->DEPARTMENT_ID) && $box->DEPARTMENT_ID > 0)
            return redirect()->action('BoxController@manager');

        return redirect()->action('BoxController@index');
    }


    public function create(){
        $box = new Box;

        return View('Box.edit')->with([
            'box' => $box
        ]);
    }

    public function save(Request $request){

        $this->validate($request, [
            'BOX_NAME' => 'required',
        ]);

        $box = Box::find($request['BOX_ID']);
        $id = 0;
        if (!isset($box)){
            $box = new Box;

            $box->STAFF_ID = Auth::user()->id;
            $box->BOX_NAME = $request['BOX_NAME'];

            if (isset($request['PARRENT_BOX_ID'])){
                $box->PARRENT_BOX_ID = $request['PARRENT_BOX_ID'];
            }else {
                $box->PARRENT_BOX_ID = 0;
            }
            $box->CREATE_BY = Auth::user()->name;
            $box->CREATE_DATE = date("Y-m-d H:i:s");
            $box->UPDATE_BY = Auth::user()->name;
            $box->UPDATE_DATE = date("Y-m-d H:i:s");
            $box->save();

            $id = $box->BOX_ID;
        }else{
            $id = $box->BOX_ID;

            $box->BOX_NAME = $request['BOX_NAME'];
            $box->UPDATE_BY = Auth::user()->name;
            $box->UPDATE_DATE = date("Y-m-d H:i:s");

            $box->save();
        }

        return redirect('/box/info/' . $id);
    }

    public function managersave(Request $request){

        $this->validate($request, [
            'BOX_NAME' => 'required',
        ]);

        $box = Box::find($request['BOX_ID']);
        $id = 0;
        if (!isset($box)){
            $box = new Box;

            $manager = Manager::where('STAFF_ID', Auth::user()->id)->first();

            $box->DEPARTMENT_ID = $manager->DEPARTMENT_ID;
            $box->BOX_NAME = $request['BOX_NAME'];

            if (isset($request['PARRENT_BOX_ID'])){
                $box->PARRENT_BOX_ID = $request['PARRENT_BOX_ID'];
            }else {
                $box->PARRENT_BOX_ID = 0;
            }
            $box->CREATE_BY = Auth::user()->name;
            $box->CREATE_DATE = date("Y-m-d H:i:s");
            $box->UPDATE_BY = Auth::user()->name;
            $box->UPDATE_DATE = date("Y-m-d H:i:s");
            $box->save();

            $id = $box->BOX_ID;
        }else{
            $id = $box->BOX_ID;

            $box->BOX_NAME = $request['BOX_NAME'];
            $box->UPDATE_BY = Auth::user()->name;
            $box->UPDATE_DATE = date("Y-m-d H:i:s");

            $box->save();
        }

        return redirect('/box/managerinfo/' . $id);
    }

    public function addStaff(Request $request)
    {
        $id = $request['BOX_ID'];
        $box = Box::find($id);

        if (!Auth::user()->can('box.access_item', $box)){
            abort(403, 'Unauthorized action.');
        }

        $this->validate($request, [
            'STAFF_ID' => 'required',
        ]);

        // validate STAFF_ID ต้องซ้ำไม่ได้
        $isDuplicate = false;
        foreach ($box->details as $item){

            // if (isset($item->STAFF_ID) || isset($item->DEPARTMENT_ID)){
            if ($item->STAFF_ID == $request['STAFF_ID']){
                $isDuplicate = true;
            }
            // }
        }

        if ($isDuplicate){
            // TODO : validate ไม่ทำงาน งงเหมือนกัน
            $this->validate($request
            , ['STAFF_ID' => 'valid_duplicate_path'], ['valid_duplicate_path' => 'บุคลไม่สามารถซ้ำกันได้']
        );
    }else{
        $item = new BoxDetail;
        $item->BOX_ID = $box->BOX_ID;
        $item->STAFF_ID = $request['STAFF_ID'];

        $item->CREATE_BY = Auth::user()->name;
        $item->CREATE_DATE = date("Y-m-d H:i:s");
        $item->save();
    }

    return redirect('/box/info/' . $box->BOX_ID);
}



public function manager(Request $request)
{
    if (!Auth::user()) {
        abort(403, 'Unauthorized action.');
    }

    $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

    $boxs = Box::where('PARRENT_BOX_ID', 0)
    ->where('DEPARTMENT_ID', $staff->managers[0]->DEPARTMENT_ID )
    ->orderBy('BOX_NAME')->paginate(20);

    return View('Box.manager')->with([
        'staff' => $staff,
        'boxs' => $boxs ,
        'text_search' => $request['TEXT_SEARCH']
    ]);
}

public function manager_post(Request $request)
{
    if (!Auth::user()) {
        abort(403, 'Unauthorized action.');
    }

    $search = $request['TEXT_SEARCH'];
    $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

    $boxs = Box::where('DEPARTMENT_ID', $staff->managers[0]->DEPARTMENT_ID )
    ->where('BOX_NAME', 'like' , '%' . $search . '%')
    ->orderBy('BOX_NAME')->paginate(20);

    return View('Box.manager')->with([
        'staff' => $staff,
        'boxs' => $boxs ,
        'text_search' => $request['TEXT_SEARCH']
    ]);
}



}
