<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Holiday;
use App\Model\Department;
use App\Model\Staff;
use App\Utils;


class HolidayController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $selected_year = date('Y');
        $holidays = Holiday::whereYear('HOLIDAY_DATE' , '=', $selected_year)->paginate(20);

        return View('holiday.list')->with([
            'holidays' => $holidays ,
            'selected_year' => $selected_year,
            'utils' => new Utils
        ]);
    }

    public function index_post(Request $request){
        $selected_year = $request['SELECTED_YEAR'];

        $holidays = Holiday::whereYear('HOLIDAY_DATE' , '=', $selected_year)->paginate(20);

        return View('holiday.list')->with([
            'holidays' => $holidays ,
            'selected_year' => $selected_year,
            'utils' => new Utils
        ]);
    }

    public function info($id){
        $holiday = Holiday::find($id);
        return View("Holiday.info")->with([
            'holiday' => $holiday,
            'utils' => new Utils
        ]);
    }

    public function edit($id){
        $holiday = Holiday::find($id);
        return View("Holiday.edit")->with([
            'holiday' => $holiday,
            'utils' => new Utils
        ]);
    }

    public function delete($id){
        $holiday = Holiday::find($id);
        if (isset($holiday)){
            $holiday->delete();
        }

        return redirect('/holiday');
    }


    public function create(){
        $holiday = new Holiday;

        return View('holiday.edit')->with([
            'holiday' => $holiday,
            'utils' => new Utils
        ]);
    }

    public function save(Request $request){

        $this->validate($request, [
            'HOLIDAY_NAME' => 'required',
            'HOLIDAY_DATE' => 'required',
        ]);

        $holiday = null;
        if (isset($request['HOLIDAY_ID'])){
            $holiday = Holiday::find($request['HOLIDAY_ID']);
        }

        if (!isset($holiday)){
            $holiday = new Holiday;
            $holiday->CREATE_BY = Auth::user()->name;
            $holiday->CREATE_DATE = date("Y-m-d H:i:s");
        }

        $holiday->HOLIDAY_NAME = $request['HOLIDAY_NAME'];
        $holiday->HOLIDAY_DATE = $request['HOLIDAY_DATE'];

        $holiday->save();

        return redirect('/holiday');
    }



    // public function memberinfo($id){
    //     $member = Member::find($id);
    //     $staffTypes = StaffType::all();
    //
    //     return View('Member.info')->with(
    //         [
    //             'member'=>$member
    //             ,'stafftypes' => $staffTypes]
    //         );
    //
    //     }
    //
    //     public function memberedit($id){
    //         $member = Member::find($id);
    //         $staffTypes = StaffType::all();
    //
    //         return View('Member.register')->with(
    //             [
    //                 'member'=>$member
    //                 ,'stafftypes' => $staffTypes]
    //             );
    //
    //         }
    //
    //         public function memberdelete($id){
    //             $member = Member::find($id);
    //             if ($member){
    //                 $member->delete();
    //             }
    //
    //             return redirect('/member');
    //         }

            // public function memberjson(){
            //   $members = Member::all();
            //   return response()->json($members);
            // }

        }
