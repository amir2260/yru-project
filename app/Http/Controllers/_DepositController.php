<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Deposit;
use App\Member;
use App\TransactionType;

class DepositController extends Controller {

  private $pagecount = 20;

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function addnew(){
    $deposit = new Deposit;
    $members = Member::select('MEMBER_NAME')->get();

    $transactionType = TransactionType::getDeposits();

    $membername_list = array();
    foreach ($members as $member){
      $membername_list[] = $member->MEMBER_NAME;
    }

    return View('Deposit.edit')->with([
        'deposit' => $deposit
        , 'membername_list' => $membername_list
        , 'transactionType' => $transactionType
    ]);
  }



  public function deposit_save(Request $request){

    $this->validate($request, [
        'MEMBER_NAME' => 'required|valid_membername',
        'DEPOSIT_AMOUNT' => 'required|numeric|min:1',
        'TRANSACTION_TYPE_ID' => 'required',
      ], ['MEMBER_NAME.valid_membername' => 'ชื่อสมาชิกไม่ถูกต้อง']
    );

    $member = Member::where('MEMBER_NAME', $request['MEMBER_NAME']);

    $deposit = new Deposit;
    if (isset($request['DEPOSIT_ID']) && $request['DEPOSIT_ID'] > 0){
      $deposit = Deposit::find($request['DEPOSIT_ID']);
    }else {
      $deposit->DEPOSIT_CODE = date("YmdHis");
      $deposit->MEMBER_ID = $member->MEMBER_ID;

      $deposit->CREATE_BY = Auth::user()->email;
      $deposit->CREATE_DATE = date("Y-m-d H:i:s");
    }

    $deposit->UPDATE_BY = Auth::user()->email;
    $deposit->UPDATE_DATE = date("Y-m-d H:i:s");

    $deposit->DEPOSIT_STATUS = 1;
    $deposit->DEPOSIT_AMOUNT = $request['DEPOSIT_AMOUNT'];
    $deposit->TRANSACTION_TYPE_ID = $request['TRANSACTION_TYPE_ID'];

    $deposit->save();

    return redirect('/deposit');
  }



  public function withdraw(){
    $deposit = new Deposit;
    $members = Member::select('MEMBER_NAME')->get();

    $membername_list = array();
    foreach ($members as $member){
      $membername_list[] = $member->MEMBER_NAME;
    }

    return View('Deposit.withdraw')->with([
        'deposit' => $deposit
        , 'membername_list' => $membername_list
    ]);
  }



  public function withdraw_save(Request $request){

    $this->validate($request, [
        'MEMBER_NAME' => 'required|valid_membername|valid_withdraw',
        'DEPOSIT_AMOUNT' => 'required|numeric|min:1',
      ], [
        'MEMBER_NAME.valid_membername' => 'ชื่อสมาชิกไม่ถูกต้อง',
        'MEMBER_NAME.valid_withdraw' => 'จำนวนเงินคงเหลือหลังจากถอนแล้ว ต้องไม่น้อยกว่า 35,000 บาท'
      ]
    );

    $member = Member::where('MEMBER_NAME', $request['MEMBER_NAME'])->first();
    $deposit = new Deposit;

    if (isset($request['DEPOSIT_ID']) && $request['DEPOSIT_ID'] > 0){
      $deposit = Deposit::find($request['DEPOSIT_ID']);
    }else {
      $deposit->DEPOSIT_CODE = date("YmdHis");
      $deposit->MEMBER_ID = $member->MEMBER_ID;

      $deposit->CREATE_BY = Auth::user()->email;
      $deposit->CREATE_DATE = date("Y-m-d H:i:s");
    }

    $deposit->UPDATE_BY = Auth::user()->email;
    $deposit->UPDATE_DATE = date("Y-m-d H:i:s");

    $deposit->DEPOSIT_STATUS = 1;
    $deposit->DEPOSIT_AMOUNT = 0 - $request['DEPOSIT_AMOUNT'];
    $deposit->TRANSACTION_TYPE_ID = 5; // ถอน

    $deposit->save();

    return redirect('/deposit');
  }



  public function index(Request $request){
    $search = "";
    $deposits = Deposit::orderBy('CREATE_DATE', 'desc');

    return View('Deposit.list')->with([
        'deposits' => $deposits->paginate($this->pagecount)
        ,'text_search' => $request['TEXT_SEARCH']
      ]);
  }



  public function index_post(Request $request){
      $search = $request['TEXT_SEARCH'];
      $deposits = Deposit::whereHas('member', function($q) use ($search){
        $q->where('MEMBER_NAME', 'like', '%' . $search . '%');
      })
      ->orWhereHas('transactionType', function($q) use ($search){
        $q->where('TRANSACTION_TYPE_NAME', 'like', '%' . $search . '%');
      })
      ->orderBy('CREATE_DATE', 'desc');


      return View('Deposit.list')->with([
          'deposits' => $deposits->paginate($this->pagecount)
          ,'text_search' => $request['TEXT_SEARCH']
        ]);
  }


  public function memberlist(Request $request){
    $members = Member::orderBy('MEMBER_NAME')->paginate($this->pagecount);

    return View('Deposit.memberlist')->with([
        'members' => $members,
        'text_search' => $request['TEXT_SEARCH']
      ]);
  }

  public function memberlist_post(Request $request){
    $members = Member::where('MEMBER_NAME', 'like', '%' . $request['TEXT_SEARCH'] . '%')
      ->orWhere('CITIZEN_ID', 'like', '%' . $request['TEXT_SEARCH'] . '%')
      ->paginate($this->pagecount);


    return View('Deposit.memberlist')->with([
        'members' => $members,
        'text_search' => $request['TEXT_SEARCH']
      ]);
  }


  /**
   * [info description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function info($id){
    $deposit = Deposit::find($id);
    if ($deposit->transactionType->TRANSACTION_TYPE == 1){
      return redirect('/deposit/editincome/' . $id);
    } else {
      return redirect('/deposit/editwithdraw/' . $id);
    }
  }

  /**
   * แสดงการเคลื่อนไหว บช เงินฝากสะสม รายบุคคล
   * @param  [type] $memberid [description]
   * @return [type]           [description]
   */
  public function listbymember($memberid){

    $member = Member::find($memberid);
    $deposits = Deposit::where('MEMBER_ID', $memberid)->orderBy('CREATE_DATE','desc')->paginate($this->pagecount);

    return View('Deposit.listbymember')->with([
        'deposits' => $deposits
        ,'member'=>$member
      ]);
  }


  /**
   * แก้ไขรายการเงินฝากสะสม
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function editincome($id){
    $deposit = Deposit::find($id);
    $transactionType = TransactionType::getDeposits();

    return View('Deposit.editincome')->with([
        'deposit' => $deposit
        , 'transactionType' => $transactionType
    ]);
  }

  /**
   * แก้ไขรายการถอน
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function editwithdraw($id){
    $deposit = Deposit::find($id);
    $deposit->DEPOSIT_AMOUNT = abs($deposit->DEPOSIT_AMOUNT); // ทำให้เป็นบวก

    return View('Deposit.editwithdraw')->with([
        'deposit' => $deposit
    ]);
  }


  /**
   * ลบรายการเงินฝากสะสม
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function delete($id){
    $deposit = Deposit::find($id);
    if ($deposit){
      $deposit->delete();
    }
    return redirect('/deposit');
  }

}
