<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Project;
use App\Model\Department;
use App\Model\Staff;
use App\Model\Category;
use App\Model\CategoryType;


class CategoryController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function category01(){
        $categoryType = CategoryType::find(1);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

    public function category02(){
        $categoryType = CategoryType::find(2);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

    public function category03(){
        $categoryType = CategoryType::find(3);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

    public function category04(){
        $categoryType = CategoryType::find(4);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

    public function category05(){
        $categoryType = CategoryType::find(5);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

    public function category06(){
        $categoryType = CategoryType::find(6);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

    public function category07(){
        $categoryType = CategoryType::find(7);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

    public function category08(){
        $categoryType = CategoryType::find(8);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

    public function category09(){
        $categoryType = CategoryType::find(9);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

    public function category10(){
        $categoryType = CategoryType::find(10);
        return View('Category.list')->with([
            'categoryType'=> $categoryType
        ]);
    }

}
