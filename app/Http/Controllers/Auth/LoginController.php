<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /* 
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $redirectAfterLogout = '/home';
    // protected $redirectTo = '/amir';
    // protected $redirectAfterLogout = '/amir';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }


    public function logout(Request $request){
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect($this->redirectAfterLogout);
    }

    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');

        return $credentials;
        //return array_add($credentials, 'active', '1');
        // return true;
    }

    public function username(){
      return "username";
    }

    // protected function credentials(Request $request)
    // {
    //   $usernameInput = trim($request->{'$this->username()'});
    //   $usernameColumn = 'username'; //filter_var($usernameInput, FILTER_VALIDATE_EMAIL) ? 'email' : $this->username();
    //
    //   return [$usernameColumn => $usernameInput, 'password' => $request->password];
    // }

}
