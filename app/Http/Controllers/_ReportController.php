<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Contract;
use App\Model\ContractType;
use App\Model\Staff;

use App\Model\Program;
use App\Model\School;
use App\Model\EduLevel;
use App\Model\EduTerm;
use App\Model\Project;
use App\Model\Fund;
// use App\Model\Student;
use App\Model\StudentType;
use App\Model\Budget;
use App\Model\BudgetType;
use App\Model\Progress;
use App\Model\ProgressStatus;
use App\Model\Renew;

use App\Utils;


class _ReportController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function budget_month(Request $request){
        $budgets = Budget::paginate(20);
        return View('Report.budget_month')->with([
            'budgets' => $budgets
            ,'utils' => new Utils
        ]);
    }

    public function budget_month_post(Request $request){

    }

    // public function index(Request $request){
    //     $contracts = Contract::paginate(20);
    //
    //     return View('Contract.list')->with([
    //         'contracts' => $contracts
    //         , 'utils' => new Utils
    //         , 'text_search' => $request['TEXT_SEARCH']
    //     ]);
    // }
    //
    // public function index_post(Request $request){
    //     $search = $request['TEXT_SEARCH'];
    //
    //     $contracts = Contract::where('CONTRACT_NO', 'like', '%' . $search . '%')
    //         ->orWhereHas('student', function($student) use ($search) {
    //             $student->where('NAME_TH', 'like', '%' . $search . '%')
    //             ->orWhere('NAME_EN', 'like', '%' . $search . '%')
    //             ->orWhere('CITIZEN_ID', 'like', '%' . $search . '%');
    //         })
    //         ->orWhereHas('fund', function($fund) use ($search) {
    //             $fund->where('FUND_NAME', 'like', '%' . $search . '%')
    //             ->orWhereHas('program', function($program) use ($search){
    //                 $program->where('PROGRAM_NAME', 'like', '%' . $search . '%');
    //             })->orWhereHas('project', function($project) use ($search){
    //                 $project->where('PROJECT_NAME', 'like', '%' . $search . '%');
    //             });
    //         })
    //     ->paginate(20);
    //
    //     return View('Contract.list')->with([
    //         'contracts' => $contracts
    //         , 'utils' => new Utils
    //         , 'text_search' => $request['TEXT_SEARCH']
    //     ]);
    // }

    public function budget(Request $request){
        $contracts = Contract::paginate(20);
        $eduTerms = EduTerm::pluck('EDU_TERM_NAME', 'EDU_TERM_ID');

        return View('Report.budget')->with([
            'contracts' => $contracts
            , 'utils' => new Utils
            , 'eduTerms' => $eduTerms
            , 'search' => json_decode(json_encode(['TEXT_SEARCH' => '', 'EDU_TERM_ID' => '0' ]))
        ]);
    }

    public function budget_post(Request $request){
        $search = $request;
        $eduTerms = EduTerm::pluck('EDU_TERM_NAME', 'EDU_TERM_ID');

        $contracts = Contract::where('CONTRACT_NO', 'like', '%' . $search . '%')
            ->orWhereHas('fund', function($fund) use ($search) {
                $fund->where('FUND_NAME', 'like', '%' . $search['TEXT_SEARCH'] . '%')
                ->orWhere('EDU_TERM_ID', $search['EDU_TERM_ID'])
                ->orWhereHas('program', function($program) use ($search){
                    $program->where('PROGRAM_NAME', 'like', '%' . $search['TEXT_SEARCH'] . '%');
                })->orWhereHas('project', function($project) use ($search){
                    $project->where('PROJECT_NAME', 'like', '%' . $search['TEXT_SEARCH'] . '%');
                });
            })
        ->paginate(20);

        return View('Contract.all')->with([
            'contracts' => $contracts
            , 'utils' => new Utils
            , 'search' => $request
            , 'eduTerms' => $eduTerms
            , 'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function budget_contract(Request $request, $id){
        $contract = Contract::find($id);
        return View('Report.budget_contract')->with([
            'contract' => $contract
            ,'utils' => new Utils
        ]);
    }

    public function info($id){
        $contract = Contract::find($id);

        return View("Contract.info")->with([
            'contract' => $contract
            ,'utils' => new Utils
        ]);
    }


}
