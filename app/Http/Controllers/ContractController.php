<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Contract;
use App\Model\ContractType;
use App\Model\Staff;

use App\Model\Program;
use App\Model\School;
use App\Model\EduLevel;
use App\Model\EduTerm;
use App\Model\Project;
use App\Model\Fund;
// use App\Model\Student;
use App\Model\StudentType;
use App\Model\Budget;
use App\Model\BudgetType;
use App\Model\Progress;
use App\Model\ProgressStatus;
use App\Model\Renew;

use App\Utils;


class ContractController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $contracts = Contract::paginate(20);

        return View('Contract.list')->with([
            'contracts' => $contracts
            , 'utils' => new Utils
            , 'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request){
        $search = $request['TEXT_SEARCH'];

        $contracts = Contract::where('CONTRACT_NO', 'like', '%' . $search . '%')
            ->orWhereHas('student', function($student) use ($search) {
                $student->where('NAME_TH', 'like', '%' . $search . '%')
                ->orWhere('NAME_EN', 'like', '%' . $search . '%')
                ->orWhere('CITIZEN_ID', 'like', '%' . $search . '%');
            })
            ->orWhereHas('fund', function($fund) use ($search) {
                $fund->where('FUND_NAME', 'like', '%' . $search . '%')
                ->orWhereHas('program', function($program) use ($search){
                    $program->where('PROGRAM_NAME', 'like', '%' . $search . '%');
                })->orWhereHas('project', function($project) use ($search){
                    $project->where('PROJECT_NAME', 'like', '%' . $search . '%');
                });
            })
        ->paginate(20);

        return View('Contract.list')->with([
            'contracts' => $contracts
            , 'utils' => new Utils
            , 'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function all(Request $request){
        $contracts = Contract::paginate(20);
        $eduTerms = EduTerm::pluck('EDU_TERM_NAME', 'EDU_TERM_ID');

        return View('Contract.all')->with([
            'contracts' => $contracts
            , 'utils' => new Utils
            , 'eduTerms' => $eduTerms
            , 'search' => json_decode(json_encode(['TEXT_SEARCH' => '', 'EDU_TERM_ID' => '0' ]))
        ]);
    }

    public function all_post(Request $request){
        $search = $request;
        $eduTerms = EduTerm::pluck('EDU_TERM_NAME', 'EDU_TERM_ID');

        $contracts = Contract::where('CONTRACT_NO', 'like', '%' . $search . '%')
            ->orWhereHas('fund', function($fund) use ($search) {
                $fund->where('FUND_NAME', 'like', '%' . $search['TEXT_SEARCH'] . '%')
                ->orWhere('EDU_TERM_ID', $search['EDU_TERM_ID'])
                ->orWhereHas('program', function($program) use ($search){
                    $program->where('PROGRAM_NAME', 'like', '%' . $search['TEXT_SEARCH'] . '%');
                })->orWhereHas('project', function($project) use ($search){
                    $project->where('PROJECT_NAME', 'like', '%' . $search['TEXT_SEARCH'] . '%');
                });
            })
        ->paginate(20);

        return View('Contract.all')->with([
            'contracts' => $contracts
            , 'utils' => new Utils
            , 'search' => $request
            , 'eduTerms' => $eduTerms
            , 'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function info($id){
        $contract = Contract::find($id);

        return View("Contract.info")->with([
            'contract' => $contract
            ,'utils' => new Utils
        ]);
    }

    public function edit($id){
        $contract = Contract::find($id);

        return View('Contract.edit')->with([
            'contract' => $contract
            , 'funds' => Fund::where('FUND_STATUS_ID', 0)->pluck('FUND_NAME', 'FUND_ID')
            , 'studentTypes' => StudentType::pluck('STUDENT_TYPE_NAME', 'STUDENT_TYPE_ID')
            ,'utils' => new Utils
        ]);
    }

    public function delete($id){
        $contract = Contract::find($id);
        if (isset($contract)){
            $contract->delete();
        }

        return redirect('/contract');
    }

    public function renew_delete($id){
        $renew = Renew::find($id);
        if (isset($renew)){
            $renew->delete();
        }

        return redirect()->action('ContractController@renew', [$renew->CONTRACT_ID]);
    }


    public function create(){
        $contract = new Contract;

        // $contractTypes = ContractType::pluck('CONTRACT_TYPE_NAME', 'CONTRACT_TYPE_ID');
        // $programs = Program::pluck('PROGRAM_NAME', 'PROGRAM_ID');
        // $schools = School::pluck('SCHOOL_NAME', 'SCHOOL_ID');
        // $eduLevels = EduLevel::pluck('EDU_LEVEL_NAME', 'EDU_LEVEL_ID');
        // $projects = Project::pluck('PROJECT_NAME', 'PROJECT_ID');
        // $terms = EduTerm::pluck('EDU_TERM_NAME', 'EDU_TERM_ID');


        return View('Contract.edit')->with([
            'contract' => $contract
            , 'funds' => Fund::where('FUND_STATUS_ID', 0)->pluck('FUND_NAME', 'FUND_ID')
            // , 'students' => Student::pluck('NAME_TH', 'STUDENT_ID')
            , 'studentTypes' => StudentType::pluck('STUDENT_TYPE_NAME', 'STUDENT_TYPE_ID')
            // ,'contractTypes' => $contractTypes
            // ,'programs' => $programs
            // ,'schools' => $schools
            // ,'eduLevels' => $eduLevels
            // ,'projects' => $projects
            // ,'terms' => $terms
        ]);
    }

    public function save(Request $request){

        //  `CONTRACT_STATUS_ID` int(11) DEFAULT NULL,
        //  `ADVISOR` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,


        $this->validate($request, [
            'STUDENT_TYPE_ID' => 'required'
            ,'CONTRACT_DATE' => 'required'
            ,'CONTRACT_NO' => 'required'
            ,'CONTRACT_START_DATE' => 'required'
            ,'CONTRACT_FINISH_DATE' => 'required'
            ,'FUND_ID' => 'required'
            ,'CITIZEN_ID' => 'required'
            ,'STUDENT_NAME' => 'required'
            ,'ADDRESS' => 'required'
            ,'PHONE' => 'required'
            ,'EMAIL' => 'required'
            ,'ADVISOR' => 'required'
        ]);

        $contract = null;
        if (isset($request['CONTRACT_ID']) && $request['CONTRACT_ID'] > 0){
            $contract = Contract::find($request['CONTRACT_ID']);
        }

        if (!isset($contract)){
            $contract = new Contract;
            $contract->FUND_STATUS_ID = $request['FUND_STATUS_ID']; // เปิดปกติ
            $contract->CONTRACT_STATUS_ID = 3; // ระหว่างศึกษา
            $contract->CREATE_BY = Auth::user ()->name;
			$contract->CREATE_DATE = date ( "Y-m-d H:i:s" );
        }

        $contract->STUDENT_TYPE_ID = $request['STUDENT_TYPE_ID'];
        $contract->CONTRACT_DATE = $request['CONTRACT_DATE'];
        $contract->CONTRACT_NO = $request['CONTRACT_NO'];
        $contract->CONTRACT_START_DATE = $request['CONTRACT_START_DATE'];
        $contract->CONTRACT_FINISH_DATE = $request['CONTRACT_FINISH_DATE'];
        $contract->FUND_ID = $request['FUND_ID'];
        $contract->CITIZEN_ID = $request['CITIZEN_ID'];
        $contract->STUDENT_NAME = $request['STUDENT_NAME'];

        $contract->ADDRESS = $request['ADDRESS'];
        $contract->PHONE = $request['PHONE'];
        $contract->EMAIL = $request['EMAIL'];
        $contract->ADVISOR = $request['ADVISOR'];

        $contract->UPDATE_BY = Auth::user ()->name;
        $contract->UPDATE_DATE = date ( "Y-m-d H:i:s" );
        $contract->save();

        return redirect('/contract');
    }

    public function budget(Request $request, $id){
        $contract = Contract::find($id);

        $budget = new Budget;
        $budget->CONTRACT_ID = $contract->CONTRACT_ID;

        return View("Contract.budget")->with([
            'contract' => $contract
            ,'budget' => $budget
            ,'budgetType' => BudgetType::pluck('BUDGET_TYPE_NAME', 'BUDGET_TYPE_ID')
            ,'utils' => new Utils
        ]);
    }

    public function renew(Request $request, $id){
        $contract = Contract::find($id);

        return View("Contract.renew")->with([
            'contract' => $contract
            ,'utils' => new Utils
        ]);
    }

    public function renew_add(Request $request, $id){
        $contract = Contract::find($id);

        $renew = new Renew;

        $renew->CONTRACT_ID = $contract->CONTRACT_ID;
        $renew->RENEW_INDEX = count($contract->renews) + 1;
        $renew->REQUEST_DATE = date ( "Y-m-d" );
        $renew->START_DATE = date ( "Y-m-d" );

        return View("Contract.renew_edit")->with([
            'contract' => $contract
            ,'renew' => $renew
            ,'utils' => new Utils
        ]);
    }

    public function renew_edit(Request $request, $id){
        $renew = Renew::find($id);
        $contract = $renew->contract;

        return View("Contract.renew_edit")->with([
            'contract' => $contract
            ,'renew' => $renew
            ,'utils' => new Utils
        ]);
    }

    public function progress(Request $request, $id){
        $contract = Contract::find($id);

        $budget = new Budget;
        $budget->CONTRACT_ID = $contract->CONTRACT_ID;

        return View("Contract.progress")->with([
            'contract' => $contract
            ,'budget' => $budget
            ,'progressStatus' => ProgressStatus::pluck('PROGRESS_STATUS_NAME', 'PROGRESS_STATUS_ID')
            ,'utils' => new Utils
        ]);
    }

    public function budget_save(Request $request){

        $this->validate($request, [
            'AMOUNT' => 'required'
            ,'BUDGET_DATE' => 'required'
            ,'BUDGET_DESC' => 'required'
        ]);

        $budget = new Budget;
        $budget->CREATE_BY = Auth::user ()->name;
        $budget->CREATE_DATE = date ( "Y-m-d H:i:s" );

        $budget->CONTRACT_ID = $request['CONTRACT_ID'];
        $budget->AMOUNT = $request['AMOUNT'];
        $budget->BUDGET_DATE = $request['BUDGET_DATE'];
        $budget->BUDGET_DESC = $request['BUDGET_DESC'];
        $budget->BUDGET_TYPE_ID = $request['BUDGET_TYPE_ID'];

        $budget->save();

        return redirect('/contract/budget/' . $request['CONTRACT_ID']);
    }

    public function progress_save(Request $request){

        $this->validate($request, [
            'GPA' => 'required'
            ,'PROGRESS_DESC' => 'required'
            ,'PROGRESS_DATE' => 'required'
        ]);

        $progress = new Progress;
        $progress->CREATE_BY = Auth::user ()->name;
        $progress->CREATE_DATE = date ( "Y-m-d H:i:s" );

        $progress->CONTRACT_ID = $request['CONTRACT_ID'];
        $progress->GPA = $request['GPA'];
        $progress->PROGRESS_DATE = $request['PROGRESS_DATE'];
        $progress->PROGRESS_DESC = $request['PROGRESS_DESC'];
        $progress->PROGRESS_STATUS_ID = $request['PROGRESS_STATUS_ID'];

        $progress->save();

        return redirect('/contract/progress/' . $request['CONTRACT_ID']);
    }

    public function renew_save(Request $request){

        $this->validate($request, [
            'RENEW_INDEX' => 'required'
            ,'REQUEST_DATE' => 'required'
            ,'START_DATE' => 'required'
            ,'END_DATE' => 'required'
            ,'REASON' => 'required'
            ,'ACCEPTOR' => 'required'
            ,'ACCEPTOR_DATE' => 'required'
            ,'APPROVER' => 'required'
            ,'APPROVER_DATE' => 'required'
        ]);

        $renew = new Renew;
        $renew->CREATE_BY = Auth::user ()->name;
        $renew->CREATE_DATE = date ( "Y-m-d H:i:s" );

        if (isset($request['RENEW_ID']) && $request['RENEW_ID'] > 0){
            $renew = Renew::find($request['RENEW_ID']);
        }

        $renew->UPDATE_BY = Auth::user ()->name;
        $renew->UPDATE_DATE = date ( "Y-m-d H:i:s" );

        $renew->CONTRACT_ID = $request['CONTRACT_ID'];
        $renew->RENEW_INDEX = $request['RENEW_INDEX'];
        $renew->REQUEST_DATE = $request['REQUEST_DATE'];
        $renew->START_DATE = $request['START_DATE'];
        $renew->END_DATE = $request['END_DATE'];
        $renew->REASON = $request['REASON'];
        $renew->ACCEPTOR = $request['ACCEPTOR'];
        $renew->ACCEPTOR_DATE = $request['ACCEPTOR_DATE'];
        $renew->APPROVER = $request['APPROVER'];
        $renew->APPROVER_DATE = $request['APPROVER_DATE'];

        $renew->save();

        return redirect('/contract/renew/' . $request['CONTRACT_ID']);
    }

}
