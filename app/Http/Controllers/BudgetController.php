<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Project;
use App\Model\Department;
use App\Model\Staff;
use App\Model\Category;
use App\Model\CategoryType;
use App\Model\Budget;
use App\Model\BudgetType;
use App\Model\BudgetDetail;


class BudgetController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $budgets = BudgetDetail::paginate(20);
        return View('Budget.list')->with([
            'budgets'=> $budgets
        ]);
    }


    public function budgetType(){
        $budgetTypes = BudgetType::paginate(20);
        return View('Budget.budgettype')->with([
            'budgetTypes'=> $budgetTypes
        ]);
    }

}
