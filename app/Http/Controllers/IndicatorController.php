<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Project;
use App\Model\Department;
use App\Model\Staff;
use App\Model\IndicatorType;
use App\Model\IndicatorGroup;


class IndicatorController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function indicator_group(){
        $groups = IndicatorGroup::all();
        return View('indicator.grouplist')->with([
            'groups'=> $groups
        ]);
    }
    
    public function add_group(){
        $group = new IndicatorGroup;
        return View('indicator.groupedit')->with([
            'group'=> $group
        ]);
    }
    
    public function edit_group($id){
        $group = IndicatorGroup::find($id);

        return View('indicator.groupedit')->with([
            'group'=> $group
        ]);
    }
    
    public function save_group(Request $request){
        $this->validate ( $request, [
            'INDICATOR_GROUP_NAME' => 'required'
        ] );

        $indicator = new IndicatorGroup;

        $item = IndicatorGroup::find($request['INDICATOR_GROUP_ID']);
        if (isset($item)){
            $indicator = $item;
        }

        $indicator->INDICATOR_GROUP_NAME = $request['INDICATOR_GROUP_NAME'];
        $indicator->save();

        return redirect()->action('IndicatorController@indicator_group');
    }
    
    /// indicator type
	public function indicator_type(){
        $types = IndicatorType::all();
        return View('indicator.typelist')->with([
            'types'=> $types
        ]);
    }

    public function add_type(){
        $type = new IndicatorGroup;
        return View('indicator.typeedit')->with([
            'type'=> $type
        ]);
    }
    
    public function edit_type($id){
        $type = IndicatorType::find($id);

        return View('indicator.typeedit')->with([
            'type'=> $type
        ]);
    }
    
    public function save_type(Request $request){
        $this->validate ( $request, [
            'INDICATOR_TYPE_NAME' => 'required'
            ,'UNIT_NAME' => 'required'
        ] );

        $indicator = new IndicatorType;

        $item = IndicatorType::find($request['INDICATOR_TYPE_ID']);
        if (isset($item)){
            $indicator = $item;
        }

        $indicator->INDICATOR_TYPE_NAME = $request['INDICATOR_TYPE_NAME'];
        $indicator->UNIT_NAME = $request['UNIT_NAME'];
        $indicator->save();

        return redirect()->action('IndicatorController@indicator_type');
    }


}
