<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\RequestForm;
use App\Member;
use App\RequestStatus;
use App\LoanType;
use App\Payment;
use App\RequestDetail;
use App\Utils;

class RequestformController extends Controller {

  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * [index description]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function index(Request $request){
    $requestforms = RequestForm::paginate(20);
    return View('RequestForm.list')->with([
        'requestForms' => $requestforms ,
        'text_search' => $request['TEXT_SEARCH'],
        'utils' => new Utils
      ]);
  }

  /**
   * [index_post description]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function index_post(Request $request){

    $search = "";
    if (isset($request['TEXT_SEARCH'])){
      $search = $request['TEXT_SEARCH'];
    }

    $requestforms = RequestForm::where('REQUEST_CODE', 'like', '%' . $search . '%')
      ->orWhereHas('member', function($q) use ($search){
        $q->where('MEMBER_NAME', 'like', '%' . $search . '%')
          ->orWhere('CITIZEN_ID', 'like', '%' . $search . '%')
          ->orWhereHas('staffType', function($st) use ($search){
            $st->where('STAFF_TYPE_NAME', 'like', '%' . $search . '%');
          });
      })
      ->orWhereHas('loanType', function($q) use ($search){
        $q->where('LOAN_TYPE_NAME', 'like', '%' . $search . '%');
      })
      ->orWhereHas('requestStatus', function($q) use ($search){
        $q->where('REQUEST_STATUS_NAME', 'like', '%' . $search . '%');
      })
      ->paginate(20);

    return View('RequestForm.list')->with([
        'requestForms' => $requestforms ,
        'text_search' => $search,
        'utils' => new Utils
      ]);
  }

  public function pending(Request $request){
    $requestforms = RequestForm::where('REQUEST_STATUS_ID', '1')->paginate(20);
    return View('RequestForm.pending')->with([
        'requestForms' => $requestforms ,
        'text_search' => $request['TEXT_SEARCH'],
        'utils' => new Utils
      ]);
  }

  /**
   * [index_post description]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function pending_post(Request $request){

    $search = "";
    if (isset($request['TEXT_SEARCH'])){
      $search = $request['TEXT_SEARCH'];
    }

    $requestforms = RequestForm::where('REQUEST_STATUS_ID', '1')
      ->where (function ($m) use ($search){
        $m->where('REQUEST_CODE', 'like', '%' . $search . '%')
        ->orWhereHas('member', function($q) use ($search){
            $q->where('MEMBER_NAME', 'like', '%' . $search . '%')
              ->orWhere('CITIZEN_ID', 'like', '%' . $search . '%')
              ->orWhereHas('staffType', function($st) use ($search){
                  $st->where('STAFF_TYPE_NAME', 'like', '%' . $search . '%');
              });
        })->orWhereHas('loanType', function($q) use ($search){
          $q->where('LOAN_TYPE_NAME', 'like', '%' . $search . '%');
        })
        ->orWhereHas('requestStatus', function($q) use ($search){
          $q->where('REQUEST_STATUS_NAME', 'like', '%' . $search . '%');
        });
      })->paginate(20);

    return View('RequestForm.pending')->with([
        'requestForms' => $requestforms ,
        'text_search' => $search,
        'utils' => new Utils
      ]);
  }


  /**
   * [create description]
   * @return [type] [description]
   */
  public function create(){
    $requestform = new RequestForm;

    $member_list = Member::all();
    $requestStatus = RequestStatus::all();
    $loanType = LoanType::all();

    $members = array();
    foreach ($member_list as $member){
      $members[] = $member->MEMBER_NAME;
    }

    return View('Requestform.edit')->with(
      [
        'requestForm'=>$requestform,
        'members'=>$members,
        'requestStatus_list'=>$requestStatus,
        'loanType_list'=>$loanType,
        'utils' => new Utils
      ]
    );
  }

  /**
   * [requestform_save description]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function requestform_save(Request $request){

    $this->validate($request, [
        'REQUEST_CODE' => 'required',
        'REQUEST_DATE' => 'required',
        'MEMBER_NAME' => 'required|valid_membername',
        'REQUEST_AMOUNT' => 'required|numeric|min:1000|valid_request',
    ]);

    $members = Member::all();
    $member = null;

    foreach ($members as $item){
      if ($item->MEMBER_NAME == $request['MEMBER_NAME']){
        $member = $item;
      }
    }

    $requestform = new RequestForm;
    if (isset($request['REQUEST_ID']) && $request['REQUEST_ID'] > 0){
      $requestform = RequestForm::find($request['REQUEST_ID']);
    }else {
      $requestform->CREATE_BY = Auth::user()->email;
      $requestform->CREATE_DATE = date("Y-m-d H:i:s");
    }

    $utils = new Utils;

    $requestform->UPDATE_BY = Auth::user()->email;
    $requestform->UPDATE_DATE = date("Y-m-d H:i:s");

    $requestform->MEMBER_ID = $member->MEMBER_ID;
    $requestform->REQUEST_CODE = $request['REQUEST_CODE'];
    $requestform->REQUEST_DATE = $utils->thaiToUTC($request['REQUEST_DATE']);
    $requestform->REQUEST_AMOUNT = $request['REQUEST_AMOUNT'];
    $requestform->LOAN_TYPE_ID = $request['LOAN_TYPE_ID'];
    $requestform->REQUEST_PAYMENT_COUNT = $request['REQUEST_PAYMENT_COUNT'];
    $requestform->REQUEST_STATUS_ID = 1;

    $requestform->save();

    return redirect('/requestform');
  }


  /**
   * [approve description]
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function approve(Request $request){

    $this->validate($request, [
        'REQUEST_DATE' => 'required',
        'REQUEST_AMOUNT' => 'required',
        'REQUEST_PAYMENT_COUNT' => 'required|numeric|valid_request',
    ]);

    $utils = new Utils;

    $requestform = RequestForm::find($request['REQUEST_ID']);

    $requestform->CREATE_BY = Auth::user()->email;
    $requestform->CREATE_DATE = date("Y-m-d H:i:s");

    $requestform->APPROVE_DATE = $utils->thaiToUTC($request['REQUEST_DATE']);
    $requestform->APPROVE_AMOUNT = $request['REQUEST_AMOUNT'];
    $requestform->APPROVE_PAYMENT_COUNT = $request['REQUEST_PAYMENT_COUNT'];
    $requestform->REQUEST_STATUS_ID = 2;

    $requestform->save();

    // insert payment table.
    $request_amount = $request['REQUEST_AMOUNT'];
    $loan_type_id = $request['LOAN_TYPE_ID'];
    $pay_amount = $request['REQUEST_PAYMENT_COUNT'];

    $permonth = $request_amount / $pay_amount;
    $items = array();
    $balance = $request_amount;

    $month = date('Y-m-d H:i:s');

    for ($i = 0; $i < $pay_amount; $i++){
      $items[] = [
        'month' => $i+1 ,
        'balance' => $balance,
        'base_per_month' => $permonth,
        'interest' => $balance * 5 /100
      ];

      $detail = new RequestDetail;

      $detail->REQUEST_ID = $request['REQUEST_ID'];
      $detail->BATCH_MONTH = date('m' ,strtotime("+1 months", strtotime($month)));
      $detail->BATCH_YEAR = date('Y' ,strtotime("+1 months", strtotime($month)));
      $detail->BASE_AMOUNT = $permonth;
      $detail->INTEREST_RATE = 0.05;
      $detail->INTEREST_AMOUNT = $balance * ($detail->INTEREST_RATE/12);
      $detail->BALANCE_BEFORE_AMOUNT = $balance;


      $balance = $balance - $permonth;
      $month = date('Y-m-d H:i:s' ,strtotime("+1 months", strtotime($month)));

      //$detail->PAYMENT_DATE = "";
      //$detail->PAYMENT_TYPE = "";
      //$detail->PAYMENT_AMOUNT = "";
      //$
      $detail->BALANCE_AFTER_AMOUNT = $balance;
      $detail->UPDATE_DATE = date("Y-m-d H:i:s");
      $detail->UPDATE_BY = Auth::user()->email;

      $detail->save();
    }

    return redirect('/requestform');
  }

  public function makepayment(Request $request){

    $this->validate($request, [
        'PAYMENT_DATE' => 'required',
        'PAYMENT_AMOUNT' => 'required',
    ]);

    $utils = new Utils;

    $requestform = RequestForm::find($request['REQUEST_ID']);

    $payment = new Payment;
    $payment->CREATE_BY = Auth::user()->email;
    $payment->CREATE_DATE = date("Y-m-d H:i:s");

    $payment->REQUEST_ID = $requestform->REQUEST_ID;
    $payment->PAYMENT_DATE = $utils->thaiToUTC($request['PAYMENT_DATE']);
    $payment->PAYMENT_AMOUNT = $request['PAYMENT_AMOUNT'];

    $payment->INTEREST_AMOUNT = $payment->PAYMENT_AMOUNT * 5 / 105;
    $payment->BASE_AMOUNT = $payment->PAYMENT_AMOUNT - $payment->INTEREST_AMOUNT;

    $payment->save();

    return redirect('/requestform/info/' . $requestform->REQUEST_ID);
  }

  /**
   * [info description]
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function info($id){
    $requestform = RequestForm::find($id);


    return View('Requestform.info')->with(
      [
        'requestForm'=>$requestform ,
        'utils' => new Utils
      ]
    );

  }

  public function edit($id){

    $requestform = RequestForm::find($id);

    $member_list = Member::all();
    $requestStatus = RequestStatus::all();
    $loanType = LoanType::all();

    $members = array();
    foreach ($member_list as $member){
      $members[] = $member->MEMBER_NAME;
    }

    return View('Requestform.edit')->with(
      [
        'requestForm'=>$requestform,
        'members'=>$members,
        'requestStatus_list'=>$requestStatus,
        'loanType_list'=>$loanType,
        'utils' => new Utils
      ]
    );

  }

  public function delete($id){
    $requestform = RequestForm::find($id);
    if ($requestform){
      $requestform->delete();
    }

    return redirect('/requestform');
  }

  public function cancel($id){
    $requestform = RequestForm::find($id);
    if ($requestform){
      $requestform->CREATE_BY = Auth::user()->email;
      $requestform->CREATE_DATE = date("Y-m-d H:i:s");

      $requestform->REQUEST_STATUS_ID = 98;

      $requestform->save();
    }
    return redirect('/requestform');
  }

  public function reject($id){
    $requestform = RequestForm::find($id);
    if ($requestform){
      $requestform->CREATE_BY = Auth::user()->email;
      $requestform->CREATE_DATE = date("Y-m-d H:i:s");

      $requestform->REQUEST_STATUS_ID = 99;

      $requestform->save();
    }
    return redirect('/requestform');
  }

  public function previewdetail(Request $request){
    $request_amount = $request['REQUEST_AMOUNT'];

    if ($request_amount > 1000){
      $loan_type_id = $request['LOAN_TYPE_ID'];
      $pay_amount = $request['REQUEST_PAYMENT_COUNT'];

      if ($loan_type_id == 2 && $pay_amount > 4){
        echo "<div class='text-danger'>เงินกู้ฉุกเฉิน ต้องชำระภายใน 4 งวด(2,500 บาท)</div>";
      }else {

        $permonth = $request_amount / $pay_amount;
        $items = array();
        $balance = $request_amount;

        for ($i = 0; $i < $pay_amount; $i++){
          $items[] = [
            'month' => $i+1 ,
            'balance' => $balance,
            'base_per_month' => $permonth,
            'interest' => $balance * (0.05 / 12)
          ];

          $balance = $balance - $permonth;
        }

        return View('Requestform.preview')->with([
          'items' => json_decode(json_encode($items))
        ]);
      }
    }else{
      echo "<div class='text-danger'>จำนวนเงินข้อกู้น้อยเกินไป</div>";
    }
  }

}
