<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\ProjectOwner;
use App\Model\Department;
use App\Model\Staff;


class _ProjectOwnerController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $owners = ProjectOwner::paginate(20);

        return View('ProjectOwner.list')->with([
            'owners' => $owners ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request){
        $search = $request['TEXT_SEARCH'];

        $owners = ProjectOwner::where('PROJECT_OWNER_NAME', 'like', '%' . $search . '%')
            ->paginate(20);

        return View('ProjectOwner.list')->with([
            'owners' => $owners ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function info($id){
        $owner = ProjectOwner::find($id);
        return View("ProjectOwner.info")->with([
            'owner' => $owner
        ]);
    }

    public function delete($id){
        $owner = ProjectOwner::find($id);
        if (isset($owner)){
            $owner->delete();
        }

        return redirect('/projectowner');
    }


    // public function create(){
    //     $owner = new ProjectOwner;
    //     $staff_items = Staff::all();
    //     $departments = Department::where('MASTER_DEPARTMENT_ID', 0)
    //         ->where('DEPARTMENT_ID', '<>', 0)->get();
    //
    //     $staffs = array();
    //     foreach ($staff_items as $item){
    //         $staffs[] = $item->FULL_NAME;
    //     }
    //
    //     return View('ProjectOwner.edit')->with([
    //         'project' => $owner
    //         , 'staffs' => json_encode($staffs)
    //         , 'departments'=>$departments
    //     ]);
    // }
    //
    // public function save(Request $request){
    //
    //     $this->validate($request, [
    //         'STAFF_NAME' => 'required|valid_staffname',
    //     ]);
    //
    //     $staff = Staff::where('FULL_NAME', $request['STAFF_NAME'])->first();
    //
    //     $owner = ProjectOwner::where('STAFF_ID', $staff->STAFF_ID);
    //     if (isset($owner)){
    //         $owner->delete();
    //     }
    //
    //     $owner = ProjectOwner::where('STAFF_ID', $staff->STAFF_ID)
    //     ->where('DEPARTMENT_ID', $request['DEPARTMENT_ID'])->first();
    //
    //     if (!isset($owner)){
    //         $owner = new ProjectOwner;
    //
    //         $owner->STAFF_ID = $staff->STAFF_ID;
    //         $owner->DEPARTMENT_ID = $request['DEPARTMENT_ID'];
    //
    //         $owner->CREATE_BY = Auth::user()->name;
    //         $owner->CREATE_DATE = date("Y-m-d H:i:s");
    //
    //         $owner->save();
    //     }
    //
    //     return redirect('/project');
    // }
    //


    // public function memberinfo($id){
    //     $member = Member::find($id);
    //     $staffTypes = StaffType::all();
    //
    //     return View('Member.info')->with(
    //         [
    //             'member'=>$member
    //             ,'stafftypes' => $staffTypes]
    //         );
    //
    //     }
    //
    //     public function memberedit($id){
    //         $member = Member::find($id);
    //         $staffTypes = StaffType::all();
    //
    //         return View('Member.register')->with(
    //             [
    //                 'member'=>$member
    //                 ,'stafftypes' => $staffTypes]
    //             );
    //
    //         }
    //
    //         public function memberdelete($id){
    //             $member = Member::find($id);
    //             if ($member){
    //                 $member->delete();
    //             }
    //
    //             return redirect('/member');
    //         }

            // public function memberjson(){
            //   $members = Member::all();
            //   return response()->json($members);
            // }

        }
