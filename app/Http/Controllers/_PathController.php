<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Path;
use App\Model\PathDetail;
use App\Model\PathAction;
use App\Model\Department;
use App\Model\Staff;

class PathController extends Controller {
	public function __construct() {
		$this->middleware ( 'auth' );
	}
	public function publicpath(Request $request) {
		$departments = Department::with ( 'masterDepartment' )->paginate ( 20 );
		;
		return View ( 'Path.publicpath' )->with ( [ 
				'departments' => $departments,
				'text_search' => $request ['TEXT_SEARCH'] 
		] );
	}


	public function publicpath_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];
		
		$departments = Department::with ( 'masterDepartment' )->where ( 'DEPARTMENT_NAME', 'like', '%' . $search . '%' )->paginate ( 20 );
		;
		
		return View ( 'Path.publicpath' )->with ( [ 
				'departments' => $departments,
				'text_search' => $request ['TEXT_SEARCH'] 
		] );
	}


	public function managerpath(Request $request) {
		$pathModel = new Path;
		$paths = $pathModel->getManagerPaths()
			->paginate ( 20 );

		return View ( 'Path.managerpath' )->with ( [ 
				'paths' => $paths,
				'title' => 'เส้นทางเอกสารหน่วยงาน (โดยสารบรรณกลาง)',
				'text_search' => $request ['TEXT_SEARCH'] 
		] );
	}


	public function managerpath_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];
		
		$pathModel = new Path;
		$paths = $pathModel->getManagerPaths()
			->where ( 'PATH_NAME', 'like', '%' . $search . '%' )
			->paginate ( 20 );
		
		return View ( 'Path.managerpath' )->with ( [ 
				'paths' => $paths,
				'title' => 'เส้นทางเอกสารหน่วยงาน (โดยสารบรรณกลาง)',
				'text_search' => $request ['TEXT_SEARCH'] 
		] );
	}


	public function personalpath(Request $request) {

		$pathModel = new Path;
		$paths = $pathModel->getPersonalPaths(Auth::user()->id)->paginate ( 20 );

		// $paths = Path::with ( 'department', 'staff' )->where ( function ($q) {
		// 	$q->whereNull ( 'DEPARTMENT_ID' )->orWhere ( 'DEPARTMENT_ID', 0 );
		// } )->where ( 'STAFF_ID', Auth::user ()->id )->paginate ( 20 );
		
		return View ( 'Path.managerpath' )->with ( [ 
				'paths' => $paths,
				'title' => 'เส้นทางส่วนบุคคล',
				'text_search' => $request ['TEXT_SEARCH'] 
		] );
	}


	public function personalpath_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];
		
		$pathModel = new Path;
		$paths = $pathModel->getPersonalPaths(Auth::user()->id)
			->where ( 'PATH_NAME', 'like', '%' . $search . '%' )->paginate ( 20 );;

		// $paths = Path::with ( 'department', 'staff' )->where ( function ($query) {
		// 	$query->whereNull ( 'DEPARTMENT_ID' )->orWhere ( 'DEPARTMENT_ID', 0 );
		// } )->where ( 'STAFF_ID', Auth::user ()->id )->where ( 'PATH_NAME', 'like', '%' . $search . '%' )->paginate ( 20 );
		
		return View ( 'Path.managerpath' )->with ( [ 
				'paths' => $paths,
				'title' => 'เส้นทางส่วนบุคคล',
				'text_search' => $request ['TEXT_SEARCH'] 
		] );
	}


	public function remove(Request $request, $id) {
		$path = Path::find ( $id );
		
		if (isset ( $path )) {
			$isDepartment = isset ( $path->DEPARTMENT_ID );
			
			if (! Auth::user ()->can ( 'path.edit_item', $path )) {
				abort ( 403, 'Unauthorized action.' );
			}
			
			PathDetail::where ( 'PATH_ID', $id )->delete ();
			$path->delete ();
			
			if ($isDepartment) {
				return redirect ( '/path/managerpath' );
			} else {
				return redirect ( '/path/personalpath' );
			}
		}
	}


	public function edit(Request $request, $id) {
		$path = Path::find ( $id );
		if (! Auth::user ()->can ( 'path.edit_item', $path )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		return View ( 'Path.edit' )->with ( [ 
				'path' => $path 
		] );
	}


	public function removeItem(Request $request, $id) {
		$detail = PathDetail::find ( $id );
		
		if (! Auth::user ()->can ( 'path.edit_item', $detail->path )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		if (isset ( $detail )) {
			$pathId = $detail->PATH_ID;
			
			$detail->delete ();
			return redirect ( '/path/pathdetail/' . $pathId );
		}
	}


	public function addStaff(Request $request) {
		$id = $request ['PATH_ID'];
		$path = Path::find ( $id );
		
		if (! Auth::user ()->can ( 'path.edit_item', $path )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$this->validate ( $request, [ 
				'STAFF_ID' => 'required' 
		] );
		
		// validate STAFF_ID ต้องซ้ำไม่ได้
		$isDuplicate = false;
		foreach ( $path->details as $item ) {
			
			if (isset ( $item->STAFF_ID ) || isset ( $item->DEPARTMENT_ID )) {
				if ($item->STAFF_ID == $request ['STAFF_ID']) {
					$isDuplicate = true;
				}
				
				if (isset ( $item->department->exStaff )) {
					// dd ($item->department->exStaff);
					if ($item->department->exStaff->STAFF_ID == $request ['STAFF_ID']) {
						$isDuplicate = true;
					}
				}
			}
		}
		
		if ($isDuplicate) {
			// TODO : validate ไม่ทำงาน งงเหมือนกัน
			$this->validate ( $request, [ 
					'PATH_ACTION_ID' => 'valid_duplicate_path' 
			], [ 
					'valid_duplicate_path' => 'บุคล หรือหน่วยงาน ไม่สามารถซ้ำกันได้' 
			] );
		} else {
			$item = new PathDetail ();
			$item->PATH_ID = $path->PATH_ID;
			$item->DEPARTMENT_ID = null;
			$item->STAFF_ID = $request ['STAFF_ID'];
			$item->IS_TEMP = 0;
			$item->PATH_ACTION_ID = 1;
			$item->ITEM_SEQ = 10;
			
			$item->CREATE_BY = Auth::user ()->name;
			$item->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$item->UPDATE_BY = Auth::user ()->name;
			$item->UPDATE_DATE = date ( "Y-m-d H:i:s" );
			$item->save ();
		}
		
		return redirect ( '/path/pathdetail/' . $path->PATH_ID );
	}


	public function addDepartment(Request $request) {
		$id = $request ['PATH_ID'];
		$path = Path::find ( $id );
		
		if (! Auth::user ()->can ( 'path.edit_item', $path )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$this->validate ( $request, [ 
				'DEPARTMENT_ID' => 'required' 
		] );
		
		// validate DEPARTMENT_ID ต้องซ้ำไม่ได้
		$isDuplicate = false;
		foreach ( $path->details as $item ) {
			
			if (isset ( $item->DEPARTMENT_ID ) & $item->DEPARTMENT_ID == $request ['DEPARTMENT_ID']) {
				$isDuplicate = true;
				break;
			}
		}
		
		if ($isDuplicate) {
			$this->validate ( $request, [ 
					'PATH_ACTION_ID' => 'valid_duplicate_path' 
			], [ 
					'valid_duplicate_path' => 'บุคล หรือหน่วยงาน ไม่สามารถซ้ำกันได้' 
			] );
		} else {
			$item = new PathDetail ();
			$item->PATH_ID = $path->PATH_ID;
			$item->DEPARTMENT_ID = $request ['DEPARTMENT_ID'];
			$item->STAFF_ID = null;
			$item->IS_TEMP = 0;
			$item->PATH_ACTION_ID = 1;
			$item->ITEM_SEQ = 10;
			
			$item->CREATE_BY = Auth::user ()->name;
			$item->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$item->UPDATE_BY = Auth::user ()->name;
			$item->UPDATE_DATE = date ( "Y-m-d H:i:s" );
			$item->save ();
		}
		
		return redirect ( '/path/pathdetail/' . $path->PATH_ID );
	}


	public function editItem(Request $request, $id) {
		$detail = PathDetail::find ( $id );
		if (! Auth::user ()->can ( 'path.edit_item', $detail->path )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$pathActions = PathAction::pluck ( 'PATH_ACTION_NAME', 'PATH_ACTION_ID' );
		
		return View ( 'Path.edit' )->with ( [ 
				'path' => $detail->path,
				'detail' => $detail,
				'pathActions' => $pathActions 
		] );
	}


	public function updateItem(Request $request) {
		$id = $request ['PATH_DETAIL_ID'];
		$detail = PathDetail::find ( $id );
		
		if (! Auth::user ()->can ( 'path.edit_item', $detail->path )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		if (isset ( $detail )) {
			$path = $detail->path;
			
			// do validate
			$approveCount = 0;
			if ($request ['PATH_ACTION_ID'] == 3) {
				$approveCount ++;
				foreach ( $path->details as $item ) {
					if ($item->PATH_DETAIL_ID != $detail->PATH_DETAIL_ID) {
						if ($item->PATH_ACTION_ID == 3) {
							$approveCount ++;
						}
					}
				}
			}
			
			// ต้องมี อนุมัติ อนุญาติ เพียงจุดเดียวเท่านั้น
			if ($approveCount > 1) {
				$this->validate ( $request, [ 
						'PATH_ACTION_ID' => 'valid_invalid_path' 
				], [ 
						'valid_invalid_path' => 'สิทธิ์อนุมัติ/อนุญาติ มีได้เพียงตำแหน่งเดียวเท่านั้น' 
				] );
			}
			
			// do update
			$detail->ITEM_SEQ = $request ['ITEM_SEQ'];
			$detail->PATH_ACTION_ID = $request ['PATH_ACTION_ID'];
			$detail->save ();
		}
		return redirect ( '/path/pathdetail/' . $detail->PATH_ID );
	}


	public function rename(Request $request, $id) {
		$path = Path::find ( $id );
		if (! Auth::user ()->can ( 'path.edit_item', $path )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		return View ( 'Path.rename' )->with ( [ 
				'path' => $path 
		] );
	}


	public function rename_post(Request $request, $id) {
		$this->validate ( $request, [ 
				'PATH_NAME' => 'required' 
		] );
		
		$path = Path::find ( $id );
		
		if (! Auth::user ()->can ( 'path.edit_item', $path )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$path->PATH_NAME = $request ['PATH_NAME'];
		$path->save ();
		
		return redirect ( '/path/pathdetail/' . $id );
	}


	public function publicdetail(Request $request, $id) {
		$department = Department::find ( $id );
		
		return View ( 'Path.publicdetail' )->with ( [ 
				'department' => $department 
		] );
	}


	public function index_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];
		
		$managers = Manager::whereHas ( 'staff', function ($q) use ($search) {
			$q->where ( 'FULL_NAME', 'like', '%' . $search . '%' );
		} )->orWhereHas ( 'department', function ($q) use ($search) {
			$q->where ( 'DEPARTMENT_NAME', 'like', '%' . $search . '%' );
		} )->paginate ( 20 );
		
		return View ( 'Manager.list' )->with ( [ 
				'managers' => $managers,
				'text_search' => $request ['TEXT_SEARCH'] 
		] );
	}
	
	/**
	 * [publictodepartment description]
	 * 
	 * @param Request $request
	 *        	[description]
	 * @param [type] $id
	 *        	id of department (DEPARTMENT_ID)
	 * @return [type] [description]
	 */
	public function publictodepartment(Request $request, $id) {
		if (! Auth::user ()->can ( 'user.manager' )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		
		// เส้นทางหน่วยงาน สร้างได้โดยสารบัญกลางเท่านั้น
		$department = Department::find ( $id );
		
		if (isset ( $department )) {
			
			$path = new Path ();
			
			// DEPARTMENT_ID ต้องเป็นของสารบัญกลางที่สร้าง
			$path->DEPARTMENT_ID = $staff->managers [0]->department->DEPARTMENT_ID;
			$path->STAFF_ID = Auth::user ()->id;
			$path->PATH_NAME = $department->DEPARTMENT_NAME . " (คัดลอก " . date ( 'Y-m-d H:i:s' ) . ")";
			$path->PATH_DESC = "";
			
			$path->CREATE_BY = Auth::user ()->name;
			$path->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$path->UPDATE_BY = Auth::user ()->name;
			$path->UPDATE_DATE = date ( "Y-m-d H:i:s" );
			$path->save ();
			
			$this->copyPathDetail ( $path->PATH_ID, $department, 1 );
			
			return redirect ( '/path/pathdetail/' . $path->PATH_ID );
		}
	}


	public function publictopersonal(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		
		// เส้นทางหน่วยงาน สร้างได้โดยสารบัญกลางเท่านั้น
		$department = Department::find ( $id );
		
		if (isset ( $department )) {
			
			$path = new Path ();
			
			// DEPARTMENT_ID ต้องไม่มีค่า ถ้าหากเป็นบุคคลทั่วไป
			// $path->DEPARTMENT_ID = $staff->managers[0]->department->DEPARTMENT_ID;
			$path->STAFF_ID = Auth::user ()->id;
			$path->PATH_NAME = $department->DEPARTMENT_NAME . " (คัดลอก " . date ( 'Y-m-d H:i:s' ) . ")";
			$path->PATH_DESC = "";
			
			$path->CREATE_BY = Auth::user ()->name;
			$path->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$path->UPDATE_BY = Auth::user ()->name;
			$path->UPDATE_DATE = date ( "Y-m-d H:i:s" );
			$path->save ();
			
			$this->copyPathDetail ( $path->PATH_ID, $department, 1 );
			
			return redirect ( '/path/pathdetail/' . $path->PATH_ID );
		}
	}


	public function copytodepartment(Request $request, $id) {
		
		// เส้นทางหน่วยงาน สร้างได้โดยสารบัญกลางเท่านั้น
		if (! Auth::user ()->can ( 'user.manager' )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		$sourcePath = Path::find ( $id );
		
		if (isset ( $sourcePath )) {
			
			$path = new Path ();
			
			// DEPARTMENT_ID ต้องเป็นของสารบัญกลางที่สร้าง
			$path->DEPARTMENT_ID = $staff->managers [0]->department->DEPARTMENT_ID;
			$path->STAFF_ID = Auth::user ()->id;
			$path->PATH_NAME = $sourcePath->PATH_NAME . " (คัดลอก " . date ( 'Y-m-d H:i:s' ) . ")";
			$path->PATH_DESC = "";
			
			$path->CREATE_BY = Auth::user ()->name;
			$path->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$path->UPDATE_BY = Auth::user ()->name;
			$path->UPDATE_DATE = date ( "Y-m-d H:i:s" );
			$path->save ();
			
			foreach ( $sourcePath->details as $detail ) {
				$item = new PathDetail ();
				$item->PATH_ID = $path->PATH_ID;
				$item->DEPARTMENT_ID = $detail->DEPARTMENT_ID;
				$item->STAFF_ID = $detail->STAFF_ID;
				$item->IS_TEMP = $detail->IS_TEMP;
				$item->PATH_ACTION_ID = $detail->PATH_ACTION_ID;
				$item->ITEM_SEQ = $detail->ITEM_SEQ;
				
				$item->CREATE_BY = Auth::user ()->name;
				$item->CREATE_DATE = date ( "Y-m-d H:i:s" );
				$item->UPDATE_BY = Auth::user ()->name;
				$item->UPDATE_DATE = date ( "Y-m-d H:i:s" );
				
				$item->save ();
			}
			
			return redirect ( '/path/pathdetail/' . $path->PATH_ID );
		}
	}


	public function copytopersonal(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		$sourcePath = Path::find ( $id );
		
		if (isset ( $sourcePath )) {
			
			$path = new Path ();
			
			// DEPARTMENT_ID ต้องเป็นของสารบัญกลางที่สร้าง
			$path->DEPARTMENT_ID = null; // $staff->managers[0]->department->DEPARTMENT_ID;
			$path->STAFF_ID = Auth::user ()->id;
			$path->PATH_NAME = $sourcePath->PATH_NAME . " (คัดลอก " . date ( 'Y-m-d H:i:s' ) . ")";
			$path->PATH_DESC = "";
			
			$path->CREATE_BY = Auth::user ()->name;
			$path->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$path->UPDATE_BY = Auth::user ()->name;
			$path->UPDATE_DATE = date ( "Y-m-d H:i:s" );
			$path->save ();
			
			foreach ( $sourcePath->details as $detail ) {
				$item = new PathDetail ();
				$item->PATH_ID = $path->PATH_ID;
				$item->DEPARTMENT_ID = $detail->DEPARTMENT_ID;
				$item->STAFF_ID = $detail->STAFF_ID;
				$item->IS_TEMP = $detail->IS_TEMP;
				$item->PATH_ACTION_ID = $detail->PATH_ACTION_ID;
				$item->ITEM_SEQ = $detail->ITEM_SEQ;
				
				$item->CREATE_BY = Auth::user ()->name;
				$item->CREATE_DATE = date ( "Y-m-d H:i:s" );
				$item->UPDATE_BY = Auth::user ()->name;
				$item->UPDATE_DATE = date ( "Y-m-d H:i:s" );
				
				$item->save ();
			}
			
			return redirect ( '/path/pathdetail/' . $path->PATH_ID );
		}
	}


	public function pathdetail($id) {
		$path = Path::find ( $id );
		
		if (isset ( $path )) {
			
			if (Auth::user ()->can ( 'path.access_item', $path )) {
				$department = Department::find ( $id );
				$staffs = Staff::select ( 'STAFF_ID', 'FULL_NAME' )->get (); // ->pluck('FULL_NAME', 'STAFF_ID');
				                                                         // $staffs = Staff::all()->pluck('FULL_NAME');
				$departments = Department::select ( 'DEPARTMENT_ID', 'DEPARTMENT_DESC' )->get ();
				
				// dd($staffs);
				
				return View ( 'Path.pathdetail' )->with ( [ 
						'path' => $path,
						'staffs' => $staffs,
						'departments' => $departments 
				] );
			} else {
				abort ( 403 );
			}
		}
	}


	private function copyPathDetail($pathId, $department, $index) {
		$item = new PathDetail ();
		$item->PATH_ID = $pathId;
		$item->DEPARTMENT_ID = $department->DEPARTMENT_ID;
		$item->STAFF_ID = null;
		$item->IS_TEMP = 0;
		$item->PATH_ACTION_ID = 1;
		$item->ITEM_SEQ = $index;
		
		$item->CREATE_BY = Auth::user ()->name;
		$item->CREATE_DATE = date ( "Y-m-d H:i:s" );
		$item->UPDATE_BY = Auth::user ()->name;
		$item->UPDATE_DATE = date ( "Y-m-d H:i:s" );
		
		$item->save ();
		
		if ($department->DEPARTMENT_ID != $department->MASTER_DEPARTMENT_ID) {
			$this->copyPathDetail ( $pathId, $department->masterDepartment, $index + 1 );
		}
	}
	
}
