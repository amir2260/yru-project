<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Contract;
use App\Model\ContractDetail;
use App\Model\Staff;

class ContractController extends Controller {
	public function __construct() {
		$this->middleware ( 'auth' );
	}
	
	public function index(Request $request) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$contracts = Contract::where ( 'STAFF_ID', Auth::user ()->id )->orderBy ( 'CONTRACT_NAME' )->paginate ( 20 );
		
		return View ( 'Contract.list' )->with ( [ 
				'contracts' => $contracts,
				'text_search' => $request ['TEXT_SEARCH'] 
		] );
	}
	
	public function index_post(Request $request) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$search = $request ['TEXT_SEARCH'];
		
		$contracts = Contract::where ( 'STAFF_ID', Auth::user ()->id )->where ( 'CONTRACT_NAME', 'like', '%' . $search . '%' )->orderBy ( 'CONTRACT_NAME' )->paginate ( 20 );
		
		return View ( 'Contract.list' )->with ( [ 
				'contracts' => $contracts,
				'text_search' => $request ['TEXT_SEARCH'] 
		] );
	}
	public function info($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$contract = Contract::find ( $id );
		
		if (! Auth::user ()->can ( 'contract.access_item', $contract )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$staffs = Staff::select ( 'STAFF_ID', 'FULL_NAME' )->get ();
		return View ( "Contract.info" )->with ( [ 
				'contract' => $contract,
				'staffs' => $staffs 
		] );
	}
	public function edit($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$contract = Contract::find ( $id );
		
		if (! Auth::user ()->can ( 'contract.access_item', $contract )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		return View ( "Contract.edit" )->with ( [ 
				'contract' => $contract 
		] );
	}
	public function delete($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$contract = Contract::find ( $id );
		
		if (! Auth::user ()->can ( 'contract.access_item', $contract )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		ContractDetail::where ( 'CONTRACT_ID', $contract->CONTRACT_ID )->delete ();
		$contract->delete ();
		
		return redirect ()->action ( 'ContractController@index' );
	}
	public function create() {
		$contract = new Contract ();
		
		return View ( 'Contract.edit' )->with ( [ 
				'contract' => $contract 
		] );
	}
	public function save(Request $request) {
		$this->validate ( $request, [ 
				'CONTRACT_NAME' => 'required' 
		] );
		
		$contract = Contract::find ( $request ['CONTRACT_ID'] );
		$id = 0;
		if (! isset ( $contract )) {
			$contract = new Contract ();
			
			$contract->STAFF_ID = Auth::user ()->id;
			$contract->CONTRACT_NAME = $request ['CONTRACT_NAME'];
			$contract->CREATE_BY = Auth::user ()->name;
			$contract->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$contract->UPDATE_BY = Auth::user ()->name;
			$contract->UPDATE_DATE = date ( "Y-m-d H:i:s" );
			$contract->save ();
			
			$id = $contract->CONTRACT_ID;
		} else {
			$id = $contract->CONTRACT_ID;
			
			$contract->CONTRACT_NAME = $request ['CONTRACT_NAME'];
			$contract->UPDATE_BY = Auth::user ()->name;
			$contract->UPDATE_DATE = date ( "Y-m-d H:i:s" );
			
			$contract->save ();
		}
		
		return redirect ( '/contract/info/' . $id );
	}
	public function addStaff(Request $request) {
		$id = $request ['CONTRACT_ID'];
		$contract = Contract::find ( $id );
		
		if (! Auth::user ()->can ( 'contract.access_item', $contract )) {
			abort ( 403, 'Unauthorized action.' );
		}
		
		$this->validate ( $request, [ 
				'STAFF_ID' => 'required' 
		] );
		
		// validate STAFF_ID ต้องซ้ำไม่ได้
		$isDuplicate = false;
		foreach ( $contract->details as $item ) {
			
			// if (isset($item->STAFF_ID) || isset($item->DEPARTMENT_ID)){
			if ($item->STAFF_ID == $request ['STAFF_ID']) {
				$isDuplicate = true;
			}
			// }
		}
		
		if ($isDuplicate) {
			// TODO : validate ไม่ทำงาน งงเหมือนกัน
			$this->validate ( $request, [ 
					'STAFF_ID' => 'valid_duplicate_path' 
			], [ 
					'valid_duplicate_path' => 'บุคลไม่สามารถซ้ำกันได้' 
			] );
		} else {
			$item = new ContractDetail ();
			$item->CONTRACT_ID = $contract->CONTRACT_ID;
			$item->STAFF_ID = $request ['STAFF_ID'];
			
			$item->CREATE_BY = Auth::user ()->name;
			$item->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$item->save ();
		}
		
		return redirect ( '/contract/info/' . $contract->CONTRACT_ID );
	}
}
