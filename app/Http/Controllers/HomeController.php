<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Staff;
use App\Model\Appoint;
use App\Model\Document;
use App\Model\LeaveType;
use App\Model\LeaveTypeLimit;
use App\Utils;

// use GuzzleHttp\Client;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// $this->middleware('auth');
	}


	public function index($condition, $start, $limit)
	{
		if (Auth::user()){
			// echo "abc";
			// $aa = Staff::paginate(10);

			// Staff::where($condition)->paginate(20);
			// Staff::where($condition)->offset($start)->paginate(20);
			// Staff::where($condition)->offset($start)->limit($limit20);

			// dd ($aa);

			return View('Home.index');
		}else{
			return redirect ('/login');
		}
	}

	public function soon(){
		return View('Home.soon');
	}
}
