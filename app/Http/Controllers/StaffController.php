<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Department;
use App\Model\Staff;



class StaffController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $staffs = Staff::orderBy('FIRST_NAME')->paginate(20);

        return View('Staff.list')->with([
            'staffs' => $staffs ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request){
        $search = $request['TEXT_SEARCH'];

        $staffs = Staff::where ('FULL_NAME', 'like' , '%' . $search . '%')
        ->orWhere ('POSITION_NAME', 'like' , '%' . $search . '%')
        ->orWhereHas('department', function($q) use ($search) {
            $q->where('DEPARTMENT_NAME', 'like', '%' . $search . '%')
            ->orWhere('DEPARTMENT_DESC', 'like', '%' . $search . '%');
        })->orderBy('FIRST_NAME')->paginate(20);

        return View('Staff.list')->with([
            'staffs' => $staffs ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function info($id){
        $staff = Staff::where('STAFF_ID', $id)->first();
        return View("Staff.info")->with([
            'staff' => $staff
        ]);
    }

    public function edit($id){
        $staff = Staff::where('STAFF_ID', $id)->first();
        return View("Staff.edit")->with([
            'staff' => $staff
        ]);
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'EMAIL' => 'required|email'
        ]);

        $id = $request['STAFF_ID'];
        $staff = Staff::where('STAFF_ID', $id)->first();

        if (!isset($staff)){
            abort(503);
        }

        $staff->EMAIL = $request['EMAIL'];
        $staff->save();

        return redirect('/staff/info/' . $staff->STAFF_ID);
    }


}
