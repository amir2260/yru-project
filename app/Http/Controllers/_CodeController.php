<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\CodeManager;
use App\Model\Document;
use App\Model\DocumentCode;
use App\Model\Department;
use App\Model\Staff;
use App\Utils;


class CodeController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){

        if (!Auth::user()->can('user.is_manager')) abort(403);

        $codes = CodeManager::get();
        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();
        $departmentId = $staff->managers[0]->department->DEPARTMENT_ID;

        $codeIn = CodeManager::where('DEPARTMENT_ID', $departmentId)
            ->where('CODE_TYPE_ID', 1)
            ->first();

        if (!isset($codeIn)){
            $codeIn = new CodeManager;
            $codeIn->CODE_TYPE_ID = 1;
            $codeIn->DEPARTMENT_ID = $staff->managers[0]->department->DEPARTMENT_ID;

            $codeIn->CREATE_BY = Auth::user()->name;
            $codeIn->CREATE_DATE = date("Y-m-d H:i:s");
        }

        $codeOut = CodeManager::where('DEPARTMENT_ID', $departmentId)
            ->where('CODE_TYPE_ID', 2)
            ->first();
        if (!isset($codeOut)){
            $codeOut = new CodeManager;
            $codeOut->CODE_TYPE_ID = 2;
            $codeIn->DEPARTMENT_ID = $staff->managers[0]->department->DEPARTMENT_ID;

            $codeOut->CREATE_BY = Auth::user()->name;
            $codeOut->CREATE_DATE = date("Y-m-d H:i:s");
        }

        return View('Code.list')->with([
            'codes' => $codes ,
            'staff' => $staff ,
            'codeIn' => $codeIn,
            'codeOut' => $codeOut,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request){

        if (!Auth::user()->can('user.is_manager')) abort(403);

        $this->validate($request, [
            'IN_CODE' => 'required'
        ]);

        $departmentId = $request['DEPARTMENT_ID'];

        $codeIn = CodeManager::where('DEPARTMENT_ID', $departmentId)
            ->where('CODE_TYPE_ID', 1)
            ->first();

        if (!isset($codeIn)){
            $codeIn = new CodeManager;
            $codeIn->CODE_TYPE_ID = 1;
            $codeIn->DEPARTMENT_ID = $departmentId;

            $codeIn->CREATE_BY = Auth::user()->name;
            $codeIn->CREATE_DATE = date("Y-m-d H:i:s");
        }
        $codeIn->CODE_PREFIX = $request['IN_CODE'];
        $codeIn->save();

        $codeOut = CodeManager::where('DEPARTMENT_ID', $departmentId)
            ->where('CODE_TYPE_ID', 2)
            ->first();
        if (!isset($codeOut)){
            $codeOut = new CodeManager;
            $codeOut->CODE_TYPE_ID = 2;
            $codeOut->DEPARTMENT_ID = $departmentId;

            $codeOut->CREATE_BY = Auth::user()->name;
            $codeOut->CREATE_DATE = date("Y-m-d H:i:s");
        }
        $codeOut->CODE_PREFIX = $request['OUT_CODE'];
        $codeOut->save();

        return redirect('/code');
    }

    public function pending(Request $request){
        if (!Auth::user()->can('user.is_manager')) abort(403);

        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

        $codeModel = new CodeManager;
        $documents = $codeModel->getDocumentsPending($staff->managers[0]->DEPARTMENT_ID);

        return View('Code.pending')->with([
            'text_search' => ''
            ,'utils' => new Utils
            ,'documents'=> $documents->paginate(20)
        ]);
    }

    public function codein(Request $request){
        if (!Auth::user()->can('user.is_manager')) abort(403);

        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

        $codeModel = new CodeManager;
        $documents = $codeModel->getDocumentsIn($staff->managers[0]->DEPARTMENT_ID);
        // $codeReserves = $codeModel->getReserveCodes($staff->managers[0]->DEPARTMENT_ID);

        return View('Code.codeout')->with([
            'text_search' => ''
            ,'utils' => new Utils
            ,'title' => 'สมุดคุมรายการหนังสือเข้า'
            ,'documents'=> $documents->paginate(20)
        ]);
    }

    public function codein_post(Request $request){
        if (!Auth::user()->can('user.is_manager')) abort(403);

        $search = $request['TEXT_SEARCH'];
        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

        $codeModel = new CodeManager;
        $documents = $codeModel->getDocumentsIn($staff->managers[0]->DEPARTMENT_ID)
            ->where ( function ($s) use ($search) {
                $s->where ( 'DOC_NO', 'like', '%' . $search . '%' )
                ->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )
                ->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )
                ->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )
                ->orWhereHas ( 'status', function ($q) use ($search) {
                    $q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
                });
        })
            ;

        return View('Code.codeout')->with([
            'text_search' => $search
            ,'utils' => new Utils
            ,'title' => 'สมุดคุมรายการหนังสือเข้า'
            ,'documents'=> $documents->paginate(20)
        ]);
    }

    public function codeout(Request $request){
        if (!Auth::user()->can('user.is_manager')) abort(403);

        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

        $codeModel = new CodeManager;
        $documents = $codeModel->getDocumentsOut($staff->managers[0]->DEPARTMENT_ID);
        // $codeReserves = $codeModel->getReserveCodes($staff->managers[0]->DEPARTMENT_ID);

        return View('Code.codeout')->with([
            'text_search' => ''
            ,'utils' => new Utils
            ,'title' => 'สมุดคุมรายการหนังสือออก'
            ,'documents'=> $documents->paginate(20)
        ]);
    }

    public function codeout_post(Request $request){
        if (!Auth::user()->can('user.is_manager')) abort(403);

        $search = $request['TEXT_SEARCH'];
        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

        $codeModel = new CodeManager;
        $documents = $codeModel->getDocumentsOut($staff->managers[0]->DEPARTMENT_ID)
            ->where ( function ($s) use ($search) {
                $s->where ( 'DOC_NO', 'like', '%' . $search . '%' )
                ->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )
                ->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )
                ->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )
                ->orWhereHas ( 'status', function ($q) use ($search) {
                    $q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
                });
        })
            ;

        return View('Code.codeout')->with([
            'text_search' => $search
            ,'utils' => new Utils
            ,'title' => 'สมุดคุมรายการหนังสือออก'
            ,'documents'=> $documents->paginate(20)
        ]);
    }

    public function reservelist(Request $request){
        if (!Auth::user()->can('user.is_manager')) abort(403);

        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

        $codeModel = new CodeManager;
        $documents = $codeModel->getDocumentsOut($staff->managers[0]->DEPARTMENT_ID);
        $codeReserves = $codeModel->getReserveCodes($staff->managers[0]->DEPARTMENT_ID);

        return View('Code.reservelist')->with([
            'text_search' => ''
            ,'utils' => new Utils
            ,'documents'=> $documents->paginate(20)
            ,'codeReserves' => $codeReserves->get()
        ]);
    }

    public function reserve(Request $request){
        if (!Auth::user()->can('user.is_manager')) abort(403);

        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();
        $code = CodeManager::where('DEPARTMENT_ID', $staff->managers[0]->DEPARTMENT_ID)
            ->where('CODE_TYPE_ID', 1)->first();

        $codeModel = new CodeManager;
        if (!isset($code)){
        	$codeModel->create($staff->managers[0]->department);
        }


        $itemNew = new DocumentCode;
        $itemNew->DOC_NO_INDEX = $codeModel->nextCode($staff->managers[0]->DEPARTMENT_ID);
        $itemNew->CREATE_BY = Auth::user()->name;
        $itemNew->CREATE_DATE = date ( "Y-m-d H:i:s" );
        $itemNew->CODE_ID = $code->CODE_ID;
        $itemNew->save();

        return redirect()->action('CodeController@reservelist');
    }

    // สร้างเลขที่เอกสาร
    public function getcode(Request $request, $id){
        if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'user.is_manager' ))	abort ( 403, 'Unauthorized action.' );

        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

        $codeModel = new CodeManager;
        $codes = $codeModel->getReserveCodes($staff->managers[0]->DEPARTMENT_ID);

		return View ( "Code.getcode" )->with ( [
				'document' => $doc
				, 'codes' => $codes->get()
				// 'staffs' => $staffs,
				// 'departments' => $departments,
				// 'managerPaths' => $managerPaths,
				// 'personalPaths' => $personalPaths
				, 'utils' => new Utils
		] );
    }

    // สร้างเลขที่เอกสาร
    public function addcode(Request $request, $codeid, $docid){
        if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $docid );

		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'user.is_manager' ))	abort ( 403, 'Unauthorized action.' );

        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();

        // จอง
        if ($codeid > 0){
            $code = DocumentCode::find ($codeid);
            if(isset($code)){
                $doc->DOC_NO = $code->code->CODE_PREFIX . '-' . $code->DOC_NO_INDEX . '/2560';
                $doc->save();

                $code->DOC_ID = $doc->DOC_ID;
                $code->save();
            }
        }else{
			// สร้างรหัส auto
            $code = CodeManager::where('DEPARTMENT_ID', $staff->managers[0]->DEPARTMENT_ID)->first();

            // if not exists , must create
            $codeModel = new CodeManager;
            if (!($code)){
            	$codeModel->createCodeOfDepartment($staff->managers[0]->department, $staff);
            }

            $code = CodeManager::where('DEPARTMENT_ID', $staff->managers[0]->DEPARTMENT_ID)->first();

            $itemNew = new DocumentCode;
            $itemNew->DOC_ID = $doc->DOC_ID;
            $itemNew->DOC_NO_INDEX = $codeModel->nextCode($staff->managers[0]->DEPARTMENT_ID);
            $itemNew->CREATE_BY = Auth::user()->name;
            $itemNew->CREATE_DATE = date ( "Y-m-d H:i:s" );
            $itemNew->CODE_ID = $code->CODE_ID;
            $itemNew->save();

            $doc->DOC_NO = $code->CODE_PREFIX . '-' . $itemNew->DOC_NO_INDEX . '/2560';
            $doc->save();
        }

        return redirect()->action('CodeController@pending');
    }


}
