<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Document;
use App\Model\DocumentComment;
use App\Model\Department;
use App\Model\Staff;
use App\Model\DocumentPrefix;
use App\Model\DocumentFooter;
use App\Model\DocumentPriority;
use App\Model\DocumentType;
use App\Model\DocumentStatus;
use App\Model\DocumentFile;
use App\Model\FileItem;
use App\Model\Box;
use App\Model\Path;
use App\Model\PathAction;
use App\Model\DocumentInBox;
use App\Model\DocumentPath;
use App\Model\ApplicationLog;
use App\Model\DocumentReceiveCode;
use App\Model\LeaveType;
use App\Model\LeaveTypeLimit;

use App\Utils;


class LeaveController extends Controller {
	public function __construct() {
		$this->middleware ( 'auth' );
	}


	public function limit(Request $request){
		$selected_year = date('Y');
		$leaveLimits = LeaveTypeLimit::where('YEAR_ID' , '=', $selected_year)->paginate(20);

		return View('leave.limitlist')->with([
			'leaveLimits' => $leaveLimits ,
			'selected_year' => $selected_year,
			'utils' => new Utils
		]);
	}

	public function limit_post(Request $request){
		$selected_year = $request['SELECTED_YEAR'];
		$leaveLimits = LeaveTypeLimit::where('YEAR_ID' , '=', $selected_year)->paginate(20);

		return View('leave.limitlist')->with([
			'leaveLimits' => $leaveLimits ,
			'selected_year' => $selected_year,
			'utils' => new Utils
		]);
	}

	public function limitedit(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$limit = LeaveTypeLimit::find($id);

		return View ( 'Leave.limitedit' )->with ( [
				'limit' => $limit,
				'utils' => new Utils
		] );
	}

	public function limitsave(Request $request) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$this->validate ( $request, [
				'LIMIT_PER_TIME' => 'required',
				'LIMIT_PER_YEAR' => 'required',
		] );

		$limit = LeaveTypeLimit::find($request['LEAVE_TYPE_LIMIT_ID']);
		if (!isset($limit)){
			$limit = new LeaveTypeLimit;
			$limit->LEAVE_TYPE_ID = $request['LEAVE_TYPE_ID'];
			$limit->YEAR_ID = $request['YEAR_ID'];
			$limit->CREATE_BY = Auth::user ()->name;
			$limit->CREATE_DATE = date ( "Y-m-d H:i:s" );
		}
		$limit->LIMIT_PER_TIME = $request['LIMIT_PER_TIME'];
		$limit->LIMIT_PER_YEAR = $request['LIMIT_PER_YEAR'];
		$limit->save();

		return redirect ()->action ( 'LeaveController@limit');
	}



	public function sick() {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = new Document ();
		$doc->LEAVE_TYPE_ID = 1; // ลาป่วย
		$doc->DOC_TITLE = "ขอลาป่วย";

		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		$masterDepartments = $staff->getMasterDepartments();

		return View ( 'Leave.edit' )->with ( [
				'document' => $doc,
				'title' => 'แบบใบลาป่วย',
				'staff' => $staff,
				'department' => $masterDepartments->last(),
				'leaveTypes' => LeaveType::get(),
				'leaveTypeLimits' => LeaveTypeLimit::get()
		] );
	}

	public function errand() {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = new Document ();
		$doc->LEAVE_TYPE_ID = 2; // ลากิจส่วนตัว
		$doc->DOC_TITLE = "ขอลากิจส่วนตัว";

		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		$masterDepartments = $staff->getMasterDepartments();

		return View ( 'Leave.edit' )->with ( [
				'document' => $doc,
				'title' => 'แบบใบลากิจส่วนตัว',
				'staff' => $staff,
				'department' => $masterDepartments->last(),
				'leaveTypes' => LeaveType::get(),
				'leaveTypeLimits' => LeaveTypeLimit::get()
		] );
	}


	public function annual() {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = new Document ();
		$doc->LEAVE_TYPE_ID = 3; // ลากิจส่วนตัว
		$doc->DOC_TITLE = "ขอลาพักผ่อน";
		$doc->DOC_CONTENT = "-";

		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		$masterDepartments = $staff->getMasterDepartments();

		return View ( 'Leave.edit' )->with ( [
				'document' => $doc,
				'title' => 'แบบใบลาพักผ่อน',
				'staff' => $staff,
				'department' => $masterDepartments->last(),
				'leaveTypes' => LeaveType::get(),
				'leaveTypeLimits' => LeaveTypeLimit::get()
		] );
	}


	public function edit(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (isset ( $doc )) {
			if (! Auth::user ()->can ( 'document.access_item', $doc )) {
				abort ( 403, 'Unauthorized action.' );
			}
		} else {
			abort ( 403, 'Unauthorized action.' );
		}


		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		$masterDepartments = $staff->getMasterDepartments();

		return View ( 'Leave.edit' )->with ( [
				'document' => $doc,
				'title' => 'แบบใบลาป่วย',
				'staff' => $staff,
				'department' => $masterDepartments->last(),
				'leaveTypes' => LeaveType::get(),
				'leaveTypeLimits' => LeaveTypeLimit::get()
		] );
	}


	public function info(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );
		if (isset ( $doc )) {
			if (! Auth::user ()->can ( 'document.access_item', $doc )) {
				abort ( 403, 'Unauthorized action.' );
			}
		}

		$staff = Staff::where('STAFF_ID',  Auth::user ()->id)->first();
		$ddlist = $staff->getMasterDepartments();
		$list = array();
		foreach ($ddlist as $d){
			if ($d->DEPARTMENT_ID)
				$list[] += $d->DEPARTMENT_ID;
		}

		$docReceices = DocumentReceiveCode::where('DOC_ID', $id)
		->whereHas('code', function($c) use ($list){
			$c->whereIn('DEPARTMENT_ID', $list);
		})->get();


		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		$utils = new Utils;

		return View ( 'Leave.info' )->with ( [
				'document' => $doc,
				'staff' => $staff,
				'utils' => $utils,
				'docReceices' => $docReceices,
				'leaveTypes' => LeaveType::get(),
				'leaveTypeLimits' => LeaveTypeLimit::get()
		] );
	}


	public function save(Request $request) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$id = $request ['DOC_ID'];
		$doc = Document::find ( $id );
		if (isset ( $doc )) {
			if (! Auth::user ()->can ( 'document.edit_item', $doc )) {
				abort ( 403, 'Unauthorized action.' );
			}
		} else {
			$doc = new Document ();
		}

		$this->validate ( $request, [
				'WRITE_AT' => 'required',
				'DOC_DATE' => 'required',
				'DOC_TITLE' => 'required',
				'TO_NAME' => 'required',
				'DOC_CONTENT' => 'required',
				'FROM_NAME' => 'required',
				'FROM_POSITION' => 'required',
				'FROM_DEPARTMENT' => 'required',
				'FROM_DATE' => 'required',
				'TO_DATE' => 'required',
				'FROM_CONTACT_ADDR' => 'required'
		] );

		// TODO : do validate.
		$doc->STAFF_ID = Auth::user ()->id;
		$doc->WRITE_AT = $request ['WRITE_AT'];
// 		$doc->DOC_NO = $request ['DOC_NO']; // ใบลาไม่มีเลขที่
		$doc->DOC_DATE = $request ['DOC_DATE'];
		// $doc->DOC_PREFIX_ID = $request ['DOC_PREFIX_ID']; // ใบลาไม่มีคำนำหน้า
		$doc->DOC_TITLE = $request ['DOC_TITLE'];
		$doc->TO_NAME = $request ['TO_NAME'];
		$doc->DOC_CONTENT = $request ['DOC_CONTENT'];
		// $doc->DOC_COMMENT = $request ['DOC_COMMENT']; // ใบลาไม่มี comment
		$doc->FROM_NAME = $request ['FROM_NAME'];
		$doc->FROM_POSITION = $request ['FROM_POSITION'];
		$doc->FROM_DEPARTMENT = $request ['FROM_DEPARTMENT'];
		// $doc->DOC_FOOTER_ID = $request ['DOC_FOOTER_ID']; // ใบลาไม่มี footer
		$doc->DOC_STATUS_ID = 1; // 1 = draff
		$doc->DOC_TYPE_ID = 2; // 4 =  ใบลา
		$doc->LEAVE_TYPE_ID = $request['LEAVE_TYPE_ID']; // ประเภทใบลา
		$doc->DOC_PRIORITY_ID = $request ['DOC_PRIORITY_ID'];

		$doc->UPDATE_BY = Auth::user ()->name;
		$doc->UPDATE_DATE = date ( "Y-m-d H:i:s" );
		$doc->DEPARTMENT_ID = $request['DEPARTMENT_ID'];

		$doc->FROM_DATE = $request['FROM_DATE'];
		$doc->TO_DATE = $request['TO_DATE'];
		$doc->FROM_CONTACT_ADDR = $request['FROM_CONTACT_ADDR'];
		$doc->LEAVE_AMOUNT = (strtotime($doc->TO_DATE) - strtotime($doc->FROM_DATE) ) / ( 60 * 60 * 24 );


		if (! (isset ( $doc->DOC_ID ) && $doc->DOC_ID > 0)) {
			$doc->CREATE_BY = Auth::user ()->name;
			$doc->CREATE_DATE = date ( "Y-m-d H:i:s" );
		}
		$doc->save ();

		$this->log('document', 'create', $doc->DOC_ID,  'บันทึกข้อมูลเอกสาร');

		return redirect ()->action ( 'DocumentController@info', $doc->DOC_ID );
	}




}
