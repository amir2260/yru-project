<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Project;
use App\Model\Department;
use App\Model\Staff;
use App\Model\Category;
use App\Model\CategoryType;
use App\Model\Budget;
use App\Model\Term;


class TermController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $terms = Term::paginate(20);
        return View('Term.list')->with([
            'terms'=> $terms
        ]);
    }


}
