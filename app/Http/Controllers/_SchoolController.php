<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\School;
use App\Model\SchoolType;
use App\Model\Staff;

use App\Model\Program;
use App\Model\EduLevel;
use App\Model\Project;


class _SchoolController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $schools = School::paginate(20);

        return View('School.list')->with([
            'schools' => $schools ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request){
        $search = $request['TEXT_SEARCH'];

        $schools = School::where('SCHOOL_NAME', 'like', '%' . $search . '%')
            ->paginate(20);

        return View('School.list')->with([
            'schools' => $schools ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function info($id){
        $school = School::find($id);
        return View("School.info")->with([
            'school' => $school
        ]);
    }

    public function edit($id){
        $school = School::find($id);

        return View("School.edit")->with([
            'school' => $school
        ]);
    }

    public function delete($id){
        $school = School::find($id);
        if (isset($school)){
            $school->delete();
        }

        return redirect('/school');
    }


    public function create(){
        $school = new School;

        return View('School.edit')->with([
            'school' => $school
        ]);
    }

    public function save(Request $request){

        $this->validate($request, [
            'SCHOOL_NAME' => 'required'
            , 'SCHOOL_ADDRESS' => 'required'
        ]);

        $school = new School;
        if (isset($request['SCHOOL_ID']) && $request['SCHOOL_ID'] > 0){
            $school = School::find($request['SCHOOL_ID']);
        }

        if (!isset($school)){
            $school = new School;
        }

        $school->SCHOOL_NAME = $request['SCHOOL_NAME'];
        $school->SCHOOL_ADDRESS = $request['SCHOOL_ADDRESS'];
        $school->CONTACT_NAME = $request['CONTACT_NAME'];
        $school->PHONE = $request['PHONE'];
        $school->EMAIL = $request['EMAIL'];
        $school->save();

        return redirect('/school');
    }



    // public function memberinfo($id){
    //     $member = Member::find($id);
    //     $staffTypes = StaffType::all();
    //
    //     return View('Member.info')->with(
    //         [
    //             'member'=>$member
    //             ,'stafftypes' => $staffTypes]
    //         );
    //
    //     }
    //
    //     public function memberedit($id){
    //         $member = Member::find($id);
    //         $staffTypes = StaffType::all();
    //
    //         return View('Member.register')->with(
    //             [
    //                 'member'=>$member
    //                 ,'stafftypes' => $staffTypes]
    //             );
    //
    //         }
    //
    //         public function memberdelete($id){
    //             $member = Member::find($id);
    //             if ($member){
    //                 $member->delete();
    //             }
    //
    //             return redirect('/member');
    //         }

            // public function memberjson(){
            //   $members = Member::all();
            //   return response()->json($members);
            // }

        }
