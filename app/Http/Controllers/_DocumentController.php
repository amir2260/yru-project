<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Document;
use App\Model\DocumentComment;
use App\Model\Department;
use App\Model\Staff;
use App\Model\Manager;
use App\Model\DocumentPrefix;
use App\Model\DocumentFooter;
use App\Model\DocumentPriority;
use App\Model\DocumentType;
use App\Model\DocumentStatus;
use App\Model\DocumentFile;
use App\Model\FileItem;
use App\Model\Box;
use App\Model\Path;
use App\Model\PathAction;
use App\Model\DocumentInBox;
use App\Model\DocumentPath;
use App\Model\DocumentShare;
use App\Model\ApplicationLog;
use App\Model\DocumentReceiveCode;
use App\Model\DocumentCode;

use Mail;
use DB;
use App\Utils;




class DocumentController extends Controller {
	public function __construct() {
		$this->middleware ( 'auth' );
	}


	public function index(Request $request) {
		// $documents = Document::with ( 'status' )->where ( 'STAFF_ID', Auth::user ()->id )->paginate ( 20 );

		$documents = Document::whereHas ('paths', function($q){
			return false;
			// $q->where('STAFF_ID', Auth::user()->id)->min('DOC_PATH_ID') > 10;
			$q->where('STAFF_ID', 30);
		});


		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้เอกสารทั้งหมด',
				'documents' => $documents->paginate ( 20 ),
				'text_search' => $request ['TEXT_SEARCH']
				, 'utils' => new Utils
		] );
	}


	public function index_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];

		$documents = Document::with ( 'status' )
			->where ( 'STAFF_ID', Auth::user ()->id )
			->where ( function ($s) use ($search) {
			$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
				$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
			} )->orWhereHas ( 'files', function ($q) use ($search) {
				$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
			} );
		} )->paginate ( 20 );

		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้เอกสารทั้งหมด',
				'documents' => $documents,
				'text_search' => $request ['TEXT_SEARCH']
				, 'utils' => new Utils
		] );
	}


	public function trash(Request $request) {
		$documents = Document::with ( 'status' )
			->where ( 'STAFF_ID', Auth::user ()->id )
			->where ( 'DOC_STATUS_ID', 4 )
			;

		$departments = Document::with ( 'status' )
			->where ( 'STAFF_ID', Auth::user ()->id )
			->where ( 'DOC_STATUS_ID', 4 )
			->groupBy('DEPARTMENT_ID')
			->select('DEPARTMENT_ID', DB::raw('count(*) as total'))
			->get();

		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้เอกสารร่าง',
				'documents' => $documents->paginate ( 20 )
				, 'departments' => $departments
				, 'search' => ['TEXT_SEARCH'=>'', 'FROM_DATE'=>'', 'TO_DATE'=>'', 'DEPARTMENT_ID'=>'']
				, 'utils' => new Utils
		] );
	}


	public function trash_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];

		$documents = Document::with ( 'status' )
		->where ( 'DOC_STATUS_ID', 4 )
		->where ( 'STAFF_ID', Auth::user ()->id )
		->where ( function ($s) use ($search) {
			$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
				$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
			} )->orWhereHas ( 'files', function ($q) use ($search) {
				$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
			} );
		} );

		$departments = Document::with ( 'status' )
		->where ( 'DOC_STATUS_ID', 4 )
		->where ( 'STAFF_ID', Auth::user ()->id )
		->where ( function ($s) use ($search) {
			$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
				$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
			} )->orWhereHas ( 'files', function ($q) use ($search) {
				$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
			} );
		} );

		if (isset($request['DEPARTMENT_ID']) && $request['DEPARTMENT_ID'] > 0){
			$documents = $documents->where('DEPARTMENT_ID', $request['DEPARTMENT_ID']);
			$departments = $departments->where('DEPARTMENT_ID', $request['DEPARTMENT_ID']);
		}

		if (isset($request['FROM_DATE']) && $request['FROM_DATE'] !='' && isset($request['TO_DATE']) && $request['TO_DATE'] != ''){
			$documents = $documents->whereBetween('DOC_DATE', array($request['FROM_DATE'], $request['TO_DATE']));
			$departments = $departments->whereBetween('DOC_DATE', array($request['FROM_DATE'], $request['TO_DATE']));
		}

		$departments = $departments->groupBy('DEPARTMENT_ID')
			->select('DEPARTMENT_ID', DB::raw('count(*) as total'))
			->get();

		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้ถังขยะ',
				'documents' => $documents->paginate ( 20 )
				, 'departments' => $departments
				, 'search' => $request
				, 'utils' => new Utils
		] );
	}


	public function box(Request $request, $type, $id = 0) {

		//$type = 'personal', 'automate', 'department'

		$docTypeId = 0;
		$docModel = new Document;
		$document_all = $docModel->getAccessList(Auth::user()->id);
		$dd = $document_all->get();
		$documents = $document_all;

		// ตู้เอกสารอัตโนมัติ
		$docTypes = DocumentType::get();
		$boxs = Box::where ( 'PARRENT_BOX_ID', 0 )->where ( 'STAFF_ID', Auth::user ()->id )->orderBy ( 'BOX_NAME' )->paginate ( 20 );

		// ตู้เอกสารหน่วยงาน
		$boxModel = new Box;
		$boxDepartments = $boxModel->getBoxDepartments(Auth::user()->id)->get();

		$box = new Box ();
		$box->BOX_ID = 0;

// 		$depModel = new Department;
// 		$staffDepartments = $depModel->getStaffDepartments(Auth::user()->id);
		$staff = Staff::where('STAFF_ID', Auth::user()->id)->first();
		$staffDepartments = $staff->getMasterDepartments();

		if ($type == 'personal'){
			// $documents = null;

			if (isset($id) && $id == 0){
				$box = $boxs->first();
			}else{
				$box = Box::find ( $id );
			}

			if (! isset ( $box )) {
				$documents = $document_all->whereHas ( 'documentInBox', function ($documentInBox) use ($box) {
					$documentInBox->where ( 'STAFF_ID', Auth::user ()->id );
				} );
			} else {
				$documents = $document_all->whereHas ( 'documentInBox', function ($documentInBox) use ($box) {
					$documentInBox->where ( 'BOX_ID', $box->BOX_ID )->where ( 'STAFF_ID', Auth::user ()->id );
				} );
			}
		} if ($type == 'automate'){
			$docTypeId = $id;
			$documents = $document_all->where('DOC_TYPE_ID', $id)
				->where('STAFF_ID', '<>', Auth::user()->id);

		} if ($type == 'department'){
			$box = Box::find ( $id );

			$documents = Document::whereHas('documentInBox', function($documentInBox) use ($id){
				$documentInBox->where('BOX_ID', $id);
			});
		}

		// TODO : เอกสารกล่องหน่วยงาน น่าจะมีข้อผิดพลาดเนื่องจากบางรายการสิทธิ์ "เมื่อคลิก" สิทธิ์อาจจะยังไม่ถึง
		return View ( 'Document.box' )->with ( [
				'title' => 'ตู้เอกสารจัดเก็บ',
				'documents' => $documents->paginate ( 20 ),
				'document_all' => $dd,
				'docTypes' => $docTypes,
				'docTypeId' => $docTypeId,
				'boxs' => $boxs,
				'staffDepartments' => $staffDepartments,
				'boxDepartments' => $boxDepartments,
				'box' => $box,
				'text_search' => $request ['TEXT_SEARCH']
				, 'utils' => new Utils
		] );
	}


	public function box_post(Request $request, $type, $id=0) {
		$search = $request ['TEXT_SEARCH'];

		$documents = null;
		$box = Box::find ( $id );

		////
		$docTypeId = 0;
		$docModel = new Document;
		$document_all = $docModel->getAccessList(Auth::user()->id);
		$dd = $document_all->get();
		$documents = $document_all;

		// ตู้เอกสารอัตโนมัติ
		$docTypes = DocumentType::get();
		$boxs = Box::where ( 'PARRENT_BOX_ID', 0 )->where ( 'STAFF_ID', Auth::user ()->id )->orderBy ( 'BOX_NAME' )->paginate ( 20 );

		// ตู้เอกสารหน่วยงาน
		$boxModel = new Box;
		$boxDepartments = $boxModel->getBoxDepartments(Auth::user()->id)->get();

		$box = new Box ();
		$box->BOX_ID = 0;

// 		$depModel = new Department;
// 		$staffDepartments = $depModel->getStaffDepartments(Auth::user()->id);
		$staff = Staff::where('STAFF_ID', Auth::user()->id)->first();
		$staffDepartments = $staff->getMasterDepartments();

		if ($type == 'personal'){
			// $documents = null;

			if (isset($id) && $id == 0){
				$box = $boxs->first();
			}else{
				$box = Box::find ( $id );
			}

			if (! isset ( $box )) {
				$documents = $document_all->whereHas ( 'documentInBox', function ($documentInBox) use ($box) {
					$documentInBox->where ( 'STAFF_ID', Auth::user ()->id );
				} );
			} else {
				$documents = $document_all->whereHas ( 'documentInBox', function ($documentInBox) use ($box) {
					$documentInBox->where ( 'BOX_ID', $box->BOX_ID )->where ( 'STAFF_ID', Auth::user ()->id );
				} );
			}
		} if ($type == 'automate'){
			$docTypeId = $id;
			$documents = $document_all->where('DOC_TYPE_ID', $id)
				->where('STAFF_ID', '<>', Auth::user()->id);

		} if ($type == 'department'){
			$box = Box::find ( $id );

			$documents = Document::whereHas('documentInBox', function($documentInBox) use ($id){
				$documentInBox->where('BOX_ID', $id);
			});
		}

		////

		$documents = $documents->where ( function ($s) use ($search) {
			$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
				$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
			} )->orWhereHas ( 'files', function ($q) use ($search) {
				$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
			} );
		} );

		return View ( 'Document.box' )->with ( [
				'title' => 'ตู้เอกสารจัดเก็บ',
				'documents' => $documents->paginate ( 20 ),
				'document_all' => $dd,
				'docTypes' => $docTypes,
				'docTypeId' => $docTypeId,
				'boxs' => $boxs,
				'staffDepartments' => $staffDepartments,
				'boxDepartments' => $boxDepartments,
				'box' => $box,
				'text_search' => $request ['TEXT_SEARCH']
				, 'utils' => new Utils
		] );
	}


	public function inbox(Request $request) {

		$docModel = new Document;
		$documents = $docModel->getInboxList(Auth::user()->id);

		$departments = $docModel->getInboxList(Auth::user()->id)->groupBy('DEPARTMENT_ID')->get(['DEPARTMENT_ID']);

		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้เอกสารเข้า'
				, 'documents' => $documents->paginate ( 20 )
				, 'departments' => $departments
				, 'search' => ['TEXT_SEARCH'=>'', 'FROM_DATE'=>'', 'TO_DATE'=>'', 'DEPARTMENT_ID'=>'']
				, 'utils' => new Utils
		] );
	}


	public function inbox_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];

		$docModel = new Document;

		$documents = $docModel->getInboxList(Auth::user()->id)
			->where ( function ($s) use ($search) {
			$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
				$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
			} )->orWhereHas ( 'files', function ($q) use ($search) {
				$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
			} );
		} );

		$departments = $docModel->getInboxList(Auth::user()->id)
			->where ( function ($s) use ($search) {
			$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
				$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
			} )->orWhereHas ( 'files', function ($q) use ($search) {
				$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
			} );
		} );

		if (isset($request['DEPARTMENT_ID']) && $request['DEPARTMENT_ID'] > 0){
			$documents = $documents->where('DEPARTMENT_ID', $request['DEPARTMENT_ID']);
			$departments = $departments->where('DEPARTMENT_ID', $request['DEPARTMENT_ID']);
		}

		if (isset($request['FROM_DATE']) && $request['FROM_DATE'] !='' && isset($request['TO_DATE']) && $request['TO_DATE'] != ''){
			$documents = $documents->whereBetween('DOC_DATE', array($request['FROM_DATE'], $request['TO_DATE']));
			$departments = $departments->whereBetween('DOC_DATE', array($request['FROM_DATE'], $request['TO_DATE']));
		}

		$departments = $departments->groupBy('DEPARTMENT_ID')
			->select('DEPARTMENT_ID', DB::raw('count(*) as total'))
			->get();

		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้เอกสารเข้า',
				'documents' => $documents->paginate ( 20 )
				, 'departments' => $departments
				, 'search' => $request
				, 'utils' => new Utils
		] );
	}


	public function outbox(Request $request) {

		$docModel = new Document;
		$documents = $docModel->getSendList(Auth::user()->id)
			->paginate ( 20 );

		$departments = $docModel->getSendList(Auth::user()->id)
			->groupBy('DEPARTMENT_ID')
			->select('DEPARTMENT_ID', DB::raw('count(*) as total'))
			->get();

		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้เอกสารออก',
				'documents' => $documents
				, 'departments' => $departments
				, 'search' => ['TEXT_SEARCH'=>'', 'FROM_DATE'=>'', 'TO_DATE'=>'', 'DEPARTMENT_ID'=>'']
				, 'utils' => new Utils
		] );
	}


	public function outbox_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];


		$docModel = new Document;
		$documents = $docModel->getSendList(Auth::user()->id)
			->where ( function ($s) use ($search) {
				$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
					$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
				} )->orWhereHas ( 'files', function ($q) use ($search) {
					$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
				});
			});

		$departments = $docModel->getSendList(Auth::user()->id)
			->where ( function ($s) use ($search) {
			$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
				$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
			} )->orWhereHas ( 'files', function ($q) use ($search) {
				$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
			} );
		} );

		if (isset($request['DEPARTMENT_ID']) && $request['DEPARTMENT_ID'] > 0){
			$documents = $documents->where('DEPARTMENT_ID', $request['DEPARTMENT_ID']);
			$departments = $departments->where('DEPARTMENT_ID', $request['DEPARTMENT_ID']);
		}

		if (isset($request['FROM_DATE']) && $request['FROM_DATE'] !='' && isset($request['TO_DATE']) && $request['TO_DATE'] != ''){
			$documents = $documents->whereBetween('DOC_DATE', array($request['FROM_DATE'], $request['TO_DATE']));
			$departments = $departments->whereBetween('DOC_DATE', array($request['FROM_DATE'], $request['TO_DATE']));
		}

		$departments = $departments->groupBy('DEPARTMENT_ID')
			->select('DEPARTMENT_ID', DB::raw('count(*) as total'))
			->get();

		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้เอกสารออก',
				'documents' => $documents->paginate ( 20 )
				, 'departments' => $departments
				, 'search' => $request
				, 'utils' => new Utils
		] );
	}


	public function draff(Request $request) {

		$docModel = new Document;
		$documents = $docModel->getDraffList(Auth::user()->id)
			->paginate ( 20 );

		$departments = $docModel->getDraffList(Auth::user()->id)
			->groupBy('DEPARTMENT_ID')
			->select('DEPARTMENT_ID', DB::raw('count(*) as total'))
			->get();

		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้เอกสารร่าง',
				'documents' => $documents
				, 'departments' => $departments
				, 'search' => ['TEXT_SEARCH'=>'', 'FROM_DATE'=>'', 'TO_DATE'=>'', 'DEPARTMENT_ID'=>'']
				, 'utils' => new Utils
		] );
	}


	public function draff_post(Request $request) {
		$search = $request ['TEXT_SEARCH'];

		$docModel = new Document;
		$documents = $docModel->getDraffList(Auth::user()->id)
			->where ( function ($s) use ($search) {
				$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )
					->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )
					->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )
					->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )
					->orWhereHas ( 'status', function ($q) use ($search) {
						$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
					} )->orWhereHas ( 'files', function ($q) use ($search) {
						$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
					} );
				} );

		$departments = $docModel->getDraffList(Auth::user()->id)
			->where ( function ($s) use ($search) {
			$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'FROM_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
				$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
			} )->orWhereHas ( 'files', function ($q) use ($search) {
				$q->where ( 'FILE_NAME', 'like', '%' . $search . '%' );
			} );
		} );

		if (isset($request['DEPARTMENT_ID']) && $request['DEPARTMENT_ID'] > 0){
			$documents = $documents->where('DEPARTMENT_ID', $request['DEPARTMENT_ID']);
			$departments = $departments->where('DEPARTMENT_ID', $request['DEPARTMENT_ID']);
		}

		if (isset($request['FROM_DATE']) && $request['FROM_DATE'] !='' && isset($request['TO_DATE']) && $request['TO_DATE'] != ''){
			$documents = $documents->whereBetween('DOC_DATE', array($request['FROM_DATE'], $request['TO_DATE']));
			$departments = $departments->whereBetween('DOC_DATE', array($request['FROM_DATE'], $request['TO_DATE']));
		}

		$departments = $departments->groupBy('DEPARTMENT_ID')
			->select('DEPARTMENT_ID', DB::raw('count(*) as total'))
			->get();

		return View ( 'Document.index' )->with ( [
				'title' => 'ตู้เอกสารร่าง',
				'documents' => $documents->paginate ( 20 )
				, 'departments' => $departments
				, 'search' => $request
				, 'utils' => new Utils
		] );
	}


	// public function owner(Request $request) {
	// 	$documents = Document::with ( 'status' )->where ( 'STAFF_ID', Auth::user ()->id )->where ( 'DOC_STATUS_ID', '>', 1 )->paginate ( 20 );
	//
	// 	return View ( 'Document.ownerlist' )->with ( [
	// 			'documents' => $documents,
	// 			'text_search' => $request ['TEXT_SEARCH']
	// 			, 'utils' => new Utils
	// 	] );
	// }
	//
	//
	// public function owner_post(Request $request) {
	// 	$search = $request ['TEXT_SEARCH'];
	//
	// 	$documents = Document::with ( 'status' )->where ( 'STAFF_ID', Auth::user ()->id )->where ( 'DOC_STATUS_ID', '>', 1 )->where ( function ($s) use ($search) {
	// 		$s->where ( 'DOC_NO', 'like', '%' . $search . '%' )->orWhere ( 'TO_NAME', 'like', '%' . $search . '%' )->orWhere ( 'DOC_TITLE', 'like', '%' . $search . '%' )->orWhereHas ( 'status', function ($q) use ($search) {
	// 			$q->where ( 'DOC_STATUS_NAME', 'like', '%' . $search . '%' );
	// 		} );
	// 	} )->paginate ( 20 );
	//
	// 	return View ( 'Document.ownerlist' )->with ( [
	// 			'documents' => $documents,
	// 			'text_search' => $request ['TEXT_SEARCH']
	// 			, 'utils' => new Utils
	// 	] );
	// }


	public function upload(Request $request) {
		$docId = $request ['DOC_ID'];
		$doc = Document::find ( $docId );

		if (! Auth::user ()->can ( 'document.edit_item', $doc ))
			abort ( '401' );

		$this->validate ( $request, [
				'FILE_UPLOAD' => 'required|mimes:pdf,jpg,jpeg,docx|max:4086',
				'DOC_ID' => 'required'
		], [
				'mimes' => 'ประเภทเอกสารต้องเป็น pdf, jpg, jpeg, docx',
				'max' => 'ขนาดไฟล์ต้องไม่เกิน 4086 KB'
		] );

		$file = $request->file ( 'FILE_UPLOAD' );

		// Store to disk.
		$file->store ( 'documents' );

		// Store to database.
		$fileItem = new FileItem ();
		$fileItem->FILE_DATA = file_get_contents ( $file->getRealPath () );
		$fileItem->FILE_MIME = $file->getMimeType ();
		$fileItem->FILE_SIZE = $file->getSize ();
		$fileItem->CREATE_BY = Auth::user ()->name;
		$fileItem->CREATE_DATE = date ( "Y-m-d H:i:s" );
		$fileItem->save ();

		$docFile = new DocumentFile ();
		$docFile->DOC_ID = $docId;
		$docFile->FILE_ID = $fileItem->FILE_ID;
		$docFile->FILE_NAME = $file->getClientOriginalName ();
		$docFile->CREATE_BY = Auth::user ()->name;
		$docFile->CREATE_DATE = date ( "Y-m-d H:i:s" );
		$docFile->save ();

		$this->log('document', 'upload', $doc->DOC_ID,  'นำขึ้นเอกสาร : ' . $docFile->FILE_NAME);

		return redirect ()->back ();
	}


	public function comment(Request $request) {

		$docId = $request ['DOC_ID'];
		$doc = Document::find ( $docId );

		if (! Auth::user ()->can ( 'document.can_comment', $doc ))
			abort ( '401' );

		$this->validate ( $request, [
				'STAFF_COMMENT' => 'required',
				'STAFF_NAME' => 'required',
				'STAFF_POSITION' => 'required',
				'DOC_ID' => 'required'
		] );

		$comment = new DocumentComment ();
		$comment->DOC_ID = $docId;
		$comment->STAFF_COMMENT = $request ['STAFF_COMMENT'];
		$comment->STAFF_NAME = $request ['STAFF_NAME'];
		$comment->STAFF_POSITION = $request ['STAFF_POSITION'];

		$comment->CREATE_BY = Auth::user ()->name;
		$comment->CREATE_DATE = $doc->CREATE_DATE = date ( "Y-m-d H:i:s" );
		$comment->save ();

		if (isset($request['clickValue']) && $request['clickValue'] == 'return'){
			// ตีกลับ
			$doc->DOC_STATUS_ID = 1; // กลับสู่จุดเริ่มต้น
			$doc->UPDATE_BY = Auth::user ()->name;
			$doc->UPDATE_DATE = date ( "Y-m-d H:i:s" );
			$doc->save ();

			DocumentPath::where('DOC_ID', $doc->DOC_ID)->update(['OPEN_DATE' => null, 'COMPLETE_DATE' => null]);
			$this->log('document', 'return', $doc->DOC_ID,  'ส่งกลับเอกสาร');

		}else{
			$actionList = $doc->paths->where('STAFF_ID', Auth::user()->id);
			foreach ($actionList as $action){
				if ($action->PATH_ACTION_ID == 2){
					$action->COMPLETE_DATE = date ( "Y-m-d H:i:s" );
					$action->save();
				}

				if (isset($request['DOC_STATUS_ID']) && $action->PATH_ACTION_ID == 3){
					$action->COMPLETE_DATE = date ( "Y-m-d H:i:s" );
					$action->save();

					$doc->DOC_STATUS_ID = $request['DOC_STATUS_ID'];
					$doc->UPDATE_BY = Auth::user ()->name;
					$doc->UPDATE_DATE = date ( "Y-m-d H:i:s" );
					$doc->save ();

				}
			}
			$this->log('document', 'comment', $doc->DOC_ID,  'บันทึกความคิดเห็น');
		}

		return redirect ()->back ();
	}


	public function tobox_post(Request $request) {
		$docId = $request ['DOC_ID'];
		$boxId = $request ['BOX_ID'];

		$doc = Document::find ( $docId );
		$box = Box::find ( $boxId );

		if (! isset ( $doc )) {
			abort ( 403 );
		}

		if ( isset ( $box )) {

			$inbox = DocumentInBox::where ( 'DOC_ID', $docId )
				->where ( 'STAFF_ID', Auth::user ()->id )
				->whereHas('box', function($box) {
					$box->whereNull('DEPARTMENT_ID');
				})
				->first ();

			if (isset ( $inbox )) {
				$inbox->delete ();
			}

			$documentInBox = new DocumentInBox ();
			$documentInBox->DOC_ID = $docId;
			$documentInBox->BOX_ID = $boxId;
			$documentInBox->STAFF_ID = Auth::user ()->id;
			$documentInBox->CREATE_BY = Auth::user ()->name;
			$documentInBox->CREATE_DATE = $doc->CREATE_DATE = date ( "Y-m-d H:i:s" );

			$documentInBox->save ();
		}

		return redirect ()->action ( 'DocumentController@info', $doc->DOC_ID );
	}


	public function todepartmentbox_post(Request $request) {
		$docId = $request ['DOC_ID'];
		$boxId = $request ['BOX_ID'];

		$doc = Document::find ( $docId );
		$box = Box::find ( $boxId );

		if (! isset ( $doc )) {
			abort ( 403 );
		}

		if ( isset ( $box )) {
			$manager = Manager::where('STAFF_ID', Auth::user()->id)->first();

			$inbox = DocumentInBox::where ( 'DOC_ID', $docId )
				->whereHas ('box', function($box) use ($manager){
					$box->where('DEPARTMENT_ID', $manager->DEPARTMENT_ID);
				})->first();


				// ->whereHas ('box', fundtion($box) use ($manager) {
				// 	$box->where('DEPARTMENT_ID', $manager->DEPARTMENT_ID);
				// })
				// ->first ();

			if (isset ( $inbox )) {
				$inbox->delete ();
			}

			$documentInBox = new DocumentInBox ();
			$documentInBox->DOC_ID = $docId;
			$documentInBox->BOX_ID = $boxId;
			$documentInBox->STAFF_ID = Auth::user ()->id;
			$documentInBox->CREATE_BY = Auth::user ()->name;
			$documentInBox->CREATE_DATE = $doc->CREATE_DATE = date ( "Y-m-d H:i:s" );

			$documentInBox->save ();
		}

		return redirect ()->action ( 'DocumentController@info', $doc->DOC_ID );
	}


	public function openFile(Request $request, $id) {
		$docFile = DocumentFile::find ( $id );

		if (! Auth::user ()->can ( 'document.access_item', $docFile->document ))
			abort ( 401 );
		if (! isset ( $docFile ))
			abort ( 401 );

		$this->log('document', 'openFile', $docFile->DOC_ID,  'เปิดเอกสารแนบ ' . $docFile->FILE_NAME);

			// open file
		$response = \Response::make ( $docFile->fileItem->FILE_DATA, 200 );
		$response->header ( 'Content-Disposition', 'attachment; filename="' . $docFile->FILE_NAME . '"' );

		return $response;
	}


	public function removeFile(Request $request, $id) {
		$docFile = DocumentFile::find ( $id );

		if (! Auth::user ()->can ( 'document.edit_item', $docFile->document ))
			abort ( 401 );
		if (! isset ( $docFile ))
			abort ( 401 );

		$this->log('document', 'removeFile', $docFile->DOC_ID,  'ลบเอกสารแนบ ' . $docFile->FILE_NAME);

		$fileItem = $docFile->fileItem;
		if ($fileItem) {
			$fileItem->delete ();
		}
		$docFile->delete ();


		return redirect ()->back ();
	}


	public function send(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action1.' );
		}

		$doc = Document::find ( $id );

		if (! Auth::user ()->can ( 'document.edit_item', $doc )) {
			abort ( 403, 'Unauthorized action2.' );
		}


		// TODO : สถานะต้องเป็นฉบับร่างเท่านั้นถึงจะส่งได้
		// TODO : ต้องมีการกำหนดผู้รับ หรือเส้นทาง
		// do validate.
		if (isset ( $doc->paths ) && count ( $doc->paths ) > 0) {
			// ส่งเมล์
			foreach($doc->paths as $item){
				if (isset($item->staff->EMAIL) && $item->staff->EMAIL != "") {
					$utils = new Utils;

					try {
						Mail::send('mail.document', ['document' => $doc, 'utils' => $utils ], function ($message) use ($item, $doc) {
							$subject = "[eDocument] แจ้งกล่องเอกสารเข้า - ";

							$message->from('amir.h@yru.ac.th', 'eDocument');
							$message->to($item->staff->EMAIL);
							$message->subject($subject);
						});
					} catch (Exception $e) {
					}
				}
			}
		}

		// $minId = $doc->paths
		// 			->where('')
		// 			->min('ITEM_SEQ');

		$doc->DOC_STATUS_ID = 2; // 2 = ระหว่างดำเนินการ
		$doc->UPDATE_BY = Auth::user ()->name;
		$doc->SEND_DATE = date ( "Y-m-d H:i:s" );
		$doc->UPDATE_DATE = date ( "Y-m-d H:i:s" );
		$doc->save ();

		$this->log('document', 'send', $doc->DOC_ID,  'ส่งเอกสาร ' );

		// TODO : บันทึก Log. tb_doc_history
		return redirect ()->action ( 'DocumentController@inbox' );
	}


	public function remove(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action1.' );
		}

		$doc = Document::find ( $id );

		if (! Auth::user ()->can ( 'document.edit_item', $doc )) {
			abort ( 403, 'Unauthorized action2.' );
		}


		$doc->DOC_STATUS_ID = 4; // 2 = ระหว่างดำเนินการ
		$doc->UPDATE_BY = Auth::user ()->name;
		$doc->SEND_DATE = date ( "Y-m-d H:i:s" );
		$doc->UPDATE_DATE = date ( "Y-m-d H:i:s" );
		$doc->save ();

		$this->log('document', 'remove', $doc->DOC_ID,  'ลบเอกสาร ' );

		// TODO : บันทึก Log. tb_doc_history
		return redirect ()->action ( 'DocumentController@inbox' );
	}


	public function getback(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action1.' );
		}

		$doc = Document::find ( $id );

		if (! Auth::user ()->can ( 'document.can_getback', $doc )) {
			abort ( 403, 'Unauthorized action2.' );
		}


		$doc->DOC_STATUS_ID = 1; // กลับสู่จุดเริ่มต้น
		$doc->UPDATE_BY = Auth::user ()->name;
		$doc->UPDATE_DATE = date ( "Y-m-d H:i:s" );
		$doc->save ();

		$this->log('document', 'getback', $doc->DOC_ID,  'ดึงกลับเอกสาร ' );

		DocumentPath::where('DOC_ID', $doc->DOC_ID)->update(['OPEN_DATE' => null, 'COMPLETE_DATE' => null]);

		return redirect ()->action ( 'DocumentController@inbox' );
	}


	public function info(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (!Auth::user ()->can('document.access_item', $doc)) {
			abort ( 403, 'Unauthorized action.' );
		}

		$actionList = $doc->paths->where('STAFF_ID', Auth::user()->id);
		foreach ($actionList as $action){
			$action->OPEN_DATE = date ( "Y-m-d H:i:s" );
			$action->save();
		}

		$this->log('document', 'info', $doc->DOC_ID,  'เปิดเอกสาร ' );

		// ลงเลขรับ
		// ตรวจสอบ ว่ามีเลขส่งหรือไม่
		$docCode = DocumentCode::where('DOC_ID', $id)->first();
		if  (isset($docCode)){
// 			$deptModel = new Department;
// 			$deptList = $deptModel->getStaffDepartments(Auth::user()->id);
			$staff = Staff::where('STAFF_ID', Auth::user()->id)->first();
			$deptList = $staff->getMasterDepartments();

			// ถ้าหน่วยงานที่ส่งมา อยู่คนละคณะ (ต้องออกเลขรับ)
			$isDepartment = false;
			foreach($deptList as $dept){
				if ($dept->DEPARTMENT_ID == $docCode->code->DEPARTMENT_ID){
					$isDepartment = true;
				}
			}

			if (!$isDepartment){

				// ลงเลขรับ
				$lastDept = $deptList->last(); // ลงเลขรับต้นสังกัดสูงสุด
				$docReceive = DocumentReceiveCode::where('DOC_ID', $doc->DOC_ID)
					->whereHas('code', function($c) use ($lastDept){
						$c->where('DEPARTMENT_ID', $lastDept->DEPARTMENT_ID);
					})->first();


				if (!isset($docReceive)){
					$receiveModel = new DocumentReceiveCode;
					$receiveModel->AddCode(Auth::user()->id, $doc, $lastDept);

				}
			}
		}

		if ($doc->DOC_TYPE_ID == 1) {
			return redirect ()->action ( 'NoteController@info', $id );
		}else if ($doc->DOC_TYPE_ID == 2) {
			return redirect ()->action ( 'LeaveController@info', $id );
		}else if ($doc->DOC_TYPE_ID == 3) {
			return redirect ()->action ( 'MemoController@info', $id );
		}else if ($doc->DOC_TYPE_ID == 4) {
			return redirect ()->action ( 'CommandController@info', $id );
		}
	}


	public function tobox($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );
		if (! isset ( $doc ))
			abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.access_item', $doc ))
			abort ( 403, 'Unauthorized action.' );

		$boxs = Box::where ( 'PARRENT_BOX_ID', 0 )->where ( 'STAFF_ID', Auth::user ()->id )->orderBy ( 'BOX_NAME' )->paginate ( 20 );

		return View ( "Document.tobox" )->with ( [
				'document' => $doc,
				'boxs' => $boxs
		] );
	}

	public function todepartmentbox($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );
		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.access_item', $doc ))		abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'user.is_manager'))	abort ( 403, 'Unauthorized action.' );

		$staff = Staff::where('STAFF_ID', Auth::user()->id)->first();
		$boxs = Box::where ( 'PARRENT_BOX_ID', 0 )
				->where ( 'DEPARTMENT_ID', $staff->managers[0]->DEPARTMENT_ID)
				->orderBy ( 'BOX_NAME' )->paginate ( 20 );

		return View ( "Document.tobox" )->with ( [
				'document' => $doc,
				'boxs' => $boxs
		] );
	}


	public function setuser($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );
		if (! isset ( $doc ))
			abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.edit_item', $doc ))
			abort ( 403, 'Unauthorized action.' );

			// $boxs = Box::where('PARRENT_BOX_ID', 0)
			// ->where('STAFF_ID', Auth::user()->id )->orderBy('BOX_NAME')->paginate(20);

		$staffs = Staff::all ();
		$departments = Department::all ();

		return View ( "Document.setuser" )->with ( [
				'document' => $doc,
				'staffs' => $staffs,
				'departments' => $departments
				, 'utils' => new Utils
		// 'boxs' => $boxs
		] );
	}


	public function selectpath($id, $pathId) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.edit_item', $doc ))	abort ( 403, 'Unauthorized action.' );

		$pathSelect = null;
		if (isset($pathId) && $pathId > 0){
			$pathSelect = Path::find($pathId);
		}

		$pathModel = new Path;
		$managerPaths = $pathModel->getManagerPaths()->get();
		$personalPaths = $pathModel->getPersonalPaths(Auth::user()->id)->get();

		$staffs = Staff::all ();
		$departments = Department::all ();

		return View ( "Document.selectpath" )->with ( [
				'document' => $doc,
				'pathSelect' => $pathSelect,
				// 'staffs' => $staffs,
				// 'departments' => $departments,
				'managerPaths' => $managerPaths,
				'personalPaths' => $personalPaths
				, 'utils' => new Utils
		] );
	}


	public function selectpath_post(Request $request, $id, $pathId) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );
		if (! Auth::user ( 'document.edit_item', $doc ))	abort ( 403, 'Unauthorized action.' );

		$pathSelect = Path::find($pathId);

		if (!isset($doc) && !isset($pathSelect)){
			abort ( 403, 'Unauthorized action.' );
		}

		// delete before ..
		DocumentPath::where('DOC_ID', $doc->DOC_ID)->delete();

		// foreach save ...
		foreach ($pathSelect->details as $item){

			$docPath = new DocumentPath;
			$docPath->DOC_ID = $doc->DOC_ID;
			$docPath->STAFF_ID = $item->STAFF_ID;
			$docPath->DEPARTMENT_ID = $item->DEPARTMENT_ID;
			$docPath->ITEM_SEQ = $item->ITEM_SEQ;
			$docPath->PATH_ACTION_ID = $item->PATH_ACTION_ID;
			$docPath->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$docPath->CREATE_BY = Auth::user()->name;

			$docPath->save();
		}

		// and redirect to where
		return redirect()->action('DocumentController@updatepath', $doc->DOC_ID);
	}


	public function updatepath($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.edit_item', $doc ))	abort ( 403, 'Unauthorized action.' );


		$staffs = Staff::all ();
		$departments = Department::all ();

		return View ( "Document.updatepath" )->with ( [
				'document' => $doc,
				// 'pathSelect' => $pathSelect,
				'staffs' => $staffs,
				'departments' => $departments
				, 'utils' => new Utils
				// 'managerPaths' => $managerPaths,
				// 'personalPaths' => $personalPaths
		] );
	}


	public function share($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.access_item', $doc ))	abort ( 403, 'Unauthorized action.' );


		$staffs = Staff::all ();
		$departments = Department::all ();

		return View ( "Document.share" )->with ( [
				'document' => $doc,
				'staffs' => $staffs,
				'departments' => $departments
				, 'utils' => new Utils
		] );
	}


	public function share_post(Request $request) {
		$id = $request ['DOC_ID'];
		$document = Document::find ( $id );

		if (! Auth::user ()->can ( 'document.access_item', $document )) {
			abort ( 403, 'Unauthorized action.' );
		}

		$this->validate ( $request, [
				'STAFF_ID' => 'required'
		] );

		$existItem = $document->shares->where('TO_STAFF_ID', $request['STAFF_ID'])->first();

		if (isset($existItem)){
			$this->validate ( $request, [
					'STAFF_ID' => 'valid_duplicate_path'
			], [
					'valid_duplicate_path' => 'บุคล ไม่สามารถซ้ำกันได้'
			] );
		}

		$item = new DocumentShare ();
		$item->DOC_ID = $document->DOC_ID;
		$item->TO_STAFF_ID = $request ['STAFF_ID'];
		$item->FROM_STAFF_ID = Auth::user ()->id;
		$item->CREATE_BY = Auth::user ()->name;
		$item->CREATE_DATE = date ( "Y-m-d H:i:s" );
		$item->save ();

		$this->log('document', 'share', $document->DOC_ID,  'มีการแบ่งปันเอกสาร ' );

		return redirect()->action('DocumentController@share', $document->DOC_ID);
	}


	public function removeshare(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action1.' );
		}

		$docShare = DocumentShare::find ( $id );

		if (!Auth::user ()->can ( 'document.access_item', $docShare->document )) {
			abort ( 403, 'Unauthorized action.' );
		}
		if ($docShare->FROM_STAFF_ID != Auth::user()->id){
			abort ( 403, 'Unauthorized action.' );
		}

		$docShare->delete ();
		$this->log('document', 'share', $docShare->DOC_ID,  'มีการยกเลิกการแบ่งปันเอกสาร ' );
		return redirect ()->action ( 'DocumentController@share', $docShare->DOC_ID );
	}



	public function dochistory($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.edit_item', $doc ))	abort ( 403, 'Unauthorized action.' );


		$staffs = Staff::all ();
		$departments = Department::all ();

		$logs = ApplicationLog::where('CONTROLLER_NAME', 'document')
				->where('DATA_ID', $id)->get();

		return View ( "Document.dochistory" )->with ( [
				'document' => $doc,
				// 'pathSelect' => $pathSelect,
				'staffs' => $staffs,
				'departments' => $departments,
				'logs' => $logs
				, 'utils' => new Utils
				// 'managerPaths' => $managerPaths,
				// 'personalPaths' => $personalPaths
		] );
	}

	public function commentHistory($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.access_item', $doc ))	abort ( 403, 'Unauthorized action.' );

		$logs = ApplicationLog::where('CONTROLLER_NAME', 'document')
				->where('DATA_ID', $id)->get();

		return View ( "Document.commenthistory" )->with ( [
				'document' => $doc
				, 'utils' => new Utils
		] );
	}

	public function pathhistory($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.edit_item', $doc ))	abort ( 403, 'Unauthorized action.' );


		$staffs = Staff::all ();
		$departments = Department::all ();

		return View ( "Document.pathhistory" )->with ( [
				'document' => $doc,
				// 'pathSelect' => $pathSelect,
				'staffs' => $staffs,
				'departments' => $departments
				, 'utils' => new Utils
				// 'managerPaths' => $managerPaths,
				// 'personalPaths' => $personalPaths
		] );
	}


	public function resetpath($id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (! isset ( $doc ))	abort ( 403, 'Unauthorized action.' );
		if (! Auth::user ( 'document.edit_item', $doc ))	abort ( 403, 'Unauthorized action.' );

		DocumentPath::where('DOC_ID', $doc->DOC_ID)->delete();
		return redirect()->action('DocumentController@updatepath', $doc->DOC_ID);
	}


	public function editPathItem(Request $request, $id) {
		$detail = DocumentPath::find ( $id );
		if (! Auth::user ()->can ( 'document.edit_item', $detail->document )) {
			abort ( 403, 'Unauthorized action.' );
		}

		$pathActions = PathAction::pluck ( 'PATH_ACTION_NAME', 'PATH_ACTION_ID' );

		return View ( 'Document.editpathitem' )->with ( [
				'document' => $detail->document,
				'detail' => $detail,
				'pathActions' => $pathActions
		] );
	}

	public function updatePathItem(Request $request) {
		$id = $request ['DOC_PATH_ID'];
		$detail = DocumentPath::find ( $id );

		if (! Auth::user ()->can ( 'document.edit_item', $detail->document )) {
			abort ( 403, 'Unauthorized action.' );
		}

		if (isset ( $detail )) {
			$path = $detail->document;

			// do validate
			$approveCount = 0;
			if ($request ['PATH_ACTION_ID'] == 3) {
				$approveCount ++;
				foreach ( $path->paths as $item ) {
					if ($item->DOC_PATH_ID != $detail->DOC_PATH_ID) {
						if ($item->PATH_ACTION_ID == 3) {
							$approveCount ++;
						}
					}
				}
			}

			// ต้องมี อนุมัติ อนุญาติ เพียงจุดเดียวเท่านั้น
			if ($approveCount > 1) {
				$this->validate ( $request, [
						'PATH_ACTION_ID' => 'valid_invalid_path'
				], [
						'valid_invalid_path' => 'สิทธิ์อนุมัติ/อนุญาติ มีได้เพียงตำแหน่งเดียวเท่านั้น'
				] );
			}

			// do update
			$detail->ITEM_SEQ = $request ['ITEM_SEQ'];
			$detail->PATH_ACTION_ID = $request ['PATH_ACTION_ID'];
			$detail->save ();
		}

		return redirect()->action('DocumentController@updatepath', $detail->DOC_ID);

		//return redirect ( '/path/pathdetail/' . $detail->PATH_ID );
	}


	public function removePathItem(Request $request, $id) {
		$detail = DocumentPath::find ( $id );

		if (! Auth::user ()->can ( 'document.edit_item', $detail->document )) {
			abort ( 403, 'Unauthorized action.' );
		}

		if (isset ( $detail )) {
			$docId = $detail->DOC_ID;
			$detail->delete ();

			return redirect()->action('DocumentController@updatepath', $detail->DOC_ID);
		}
	}


	public function addStaffItem(Request $request) {
		$id = $request ['DOC_ID'];
		$document = Document::find ( $id );

		if (! Auth::user ()->can ( 'document.edit_item', $document )) {
			abort ( 403, 'Unauthorized action.' );
		}

		$this->validate ( $request, [
				'STAFF_ID' => 'required'
		] );

		// validate STAFF_ID ต้องซ้ำไม่ได้
		$isDuplicate = false;
		foreach ( $document->paths as $item ) {

			if (isset ( $item->STAFF_ID ) || isset ( $item->DEPARTMENT_ID )) {
				if ($item->STAFF_ID == $request ['STAFF_ID']) {
					$isDuplicate = true;
				}

				if (isset ( $item->department->exStaff )) {
					// dd ($item->department->exStaff);
					if ($item->department->exStaff->STAFF_ID == $request ['STAFF_ID']) {
						$isDuplicate = true;
					}
				}
			}
		}

		if ($isDuplicate) {
			// TODO : validate ไม่ทำงาน งงเหมือนกัน
			$this->validate ( $request, [
					'STAFF_ID' => 'valid_duplicate_path'
			], [
					'valid_duplicate_path' => 'บุคล หรือหน่วยงาน ไม่สามารถซ้ำกันได้'
			] );
		} else {
			$item = new DocumentPath ();
			$item->DOC_ID = $document->DOC_ID;
			$item->DEPARTMENT_ID = null;
			$item->STAFF_ID = $request ['STAFF_ID'];
			// $item->IS_TEMP = 0;
			$item->PATH_ACTION_ID = 1;
			$item->ITEM_SEQ = 10;

			$item->CREATE_BY = Auth::user ()->name;
			$item->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$item->save ();
		}

		return redirect()->action('DocumentController@updatepath', $document->DOC_ID);
	}


	public function addDepartmentItem(Request $request) {
		$id = $request ['DOC_ID'];
		$document = Document::find ( $id );

		if (! Auth::user ()->can ( 'document.edit_item', $document )) {
			abort ( 403, 'Unauthorized action.' );
		}

		$this->validate ( $request, [
				'DEPARTMENT_ID' => 'required'
		] );

		// validate DEPARTMENT_ID ต้องซ้ำไม่ได้
		$isDuplicate = false;
		foreach ( $document->paths as $item ) {

			if (isset ( $item->DEPARTMENT_ID ) & $item->DEPARTMENT_ID == $request ['DEPARTMENT_ID']) {
				$isDuplicate = true;
				break;
			}
		}

		if ($isDuplicate) {
			$this->validate ( $request, [
					'DEPARTMENT_ID' => 'valid_duplicate_path'
			], [
					'valid_duplicate_path' => 'บุคล หรือหน่วยงาน ไม่สามารถซ้ำกันได้'
			] );
		} else {
			$item = new DocumentPath ();
			$item->DOC_ID = $document->DOC_ID;
			$item->DEPARTMENT_ID = $request ['DEPARTMENT_ID'];
			$item->STAFF_ID = null;
			// $item->IS_TEMP = 0;
			$item->PATH_ACTION_ID = 1;
			$item->ITEM_SEQ = 10;

			$item->CREATE_BY = Auth::user ()->name;
			$item->CREATE_DATE = date ( "Y-m-d H:i:s" );
			$item->save ();
		}

		return redirect()->action('DocumentController@updatepath', $document->DOC_ID);
	}



	// public function info($id){
	// $document = Document::find($id);
	// return View("Document.info")->with([
	// 'document' => $document
	// ]);
	// }
	//
	// public function delete($id){
	// $document = Document::find($id);
	// if (isset($document)){
	// $document->delete();
	// }
	//
	// return redirect('/document');
	// }
	//
	//
	// public function create(){
	// $document = new Document;
	// $staff_items = Staff::all();
	// $departments = Department::where('MASTER_DEPARTMENT_ID', 0)->get();
	//
	// $staffs = array();
	// foreach ($staff_items as $item){
	// $staffs[] = $item->FULL_NAME;
	// }
	//
	// return View('Document.edit')->with([
	// 'document' => $document
	// , 'staffs' => json_encode($staffs)
	// , 'departments'=>$departments
	// ]);
	// }
	//
	// public function save(Request $request){
	//
	// $this->validate($request, [
	// 'STAFF_NAME' => 'required|valid_staffname',
	// ]);
	//
	// $staff = Staff::where('FULL_NAME', $request['STAFF_NAME'])->first();
	//
	// $document = Document::where('STAFF_ID', $staff->STAFF_ID)
	// ->where('DEPARTMENT_ID', $request['DEPARTMENT_ID'])->first();
	//
	// if (!isset($document)){
	// $document = new Document;
	//
	// $document->STAFF_ID = $staff->STAFF_ID;
	// $document->DEPARTMENT_ID = $request['DEPARTMENT_ID'];
	//
	// $document->CREATE_BY = Auth::user()->name;
	// $document->CREATE_DATE = date("Y-m-d H:i:s");
	//
	// $document->save();
	// }
	//
	// return redirect('/document');
	// }
	//
	//
	//
	// public function memberinfo($id){
	// $member = Member::find($id);
	// $staffTypes = StaffType::all();
	//
	// return View('Member.info')->with(
	// [
	// 'member'=>$member
	// ,'stafftypes' => $staffTypes]
	// );
	//
	// }
	//
	// public function memberedit($id){
	// $member = Member::find($id);
	// $staffTypes = StaffType::all();
	//
	// return View('Member.register')->with(
	// [
	// 'member'=>$member
	// ,'stafftypes' => $staffTypes]
	// );
	//
	// }
	//
	// public function memberdelete($id){
	// $member = Member::find($id);
	// if ($member){
	// $member->delete();
	// }
	//
	// return redirect('/member');
	// }

	// public function memberjson(){
	// $members = Member::all();
	// return response()->json($members);
	// }
}
