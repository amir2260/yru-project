<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Appoint;
use App\Model\Location;
use App\Model\Staff;
use App\Model\Contract;
use App\Model\AppointAttender;
use App\Model\ApplicationLog;

//use Illuminate\Support\Facades\Mail;
use Mail;
use App\Utils;

class AppointController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        if (!Auth::user()) {
            abort(401);
        }

        $appoints = Appoint::where(function ($q) {
            $q->where('CREATE_STAFF_ID', Auth::user()->id);
            $q->orWhereHas('attenders', function ($attenders) {
                $attenders->where('ATTENDER_STAFF_ID', Auth::user()->id);
            });
        })
    ->orderBy('START_DATE', 'desc')->paginate(20);

        $utils = new Utils;

        return View('Appoint.list')->with([
        'appoints' => $appoints ,
        'text_search' => $request['TEXT_SEARCH'],
        'utils' => $utils
      ]);
    }


    public function index_post(Request $request)
    {
        if (!Auth::user()) {
            abort(401);
        }

        $search = $request['TEXT_SEARCH'];
        $utils = new Utils;

        $appoints = Appoint::where(function ($q) {
            $q->where('CREATE_STAFF_ID', Auth::user()->id);
            $q->orWhereHas('attenders', function ($attenders) {
                $attenders->where('ATTENDER_STAFF_ID', Auth::user()->id);
            });
        })
            ->where(function ($q) use ($search) {
                $q->where('APPOINT_TITLE', 'like', '%' . $search . '%')
              ->orWhere('APPOINT_DESC', 'like', '%' . $search . '%')
              ->orWhere('LOCATION', 'like', '%' . $search . '%')
              ->orWhere('OWNER_NAME', 'like', '%' . $search . '%');
            })->orderBy('START_DATE', 'desc')->paginate(20);

        return View('Appoint.list')->with([
        'appoints' => $appoints ,
        'text_search' => $request['TEXT_SEARCH'],
        'utils' => $utils
      ]);
    }

    public function info($id)
    {
        if (!Auth::user()) {
            abort(401);
        }

        $appoint = Appoint::find($id);

        if (!(Auth::user()->can('appoint.is_owner', $appoint) || Auth::user()->can('appoint.is_attender', $appoint))) {
            abort(401);
        }
    //if (!Auth::user()->can('appoint.is_attender'))   abort(401);
    // if (!Auth::user()->can('appoint.is_owner'))   abort(401);

    $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();
        $appoint = Appoint::find($id);
        $contracts = Contract::where('STAFF_ID', $staff->STAFF_ID)->orderBy('CONTRACT_NAME')->get();

        $staff_items = Staff::all();
        $staffs = array();
        foreach ($staff_items as $item) {
            $staffs[] = $item->FULL_NAME;
        }

        if ($appoint->CREATE_STAFF_ID != Auth::user()->id) {
            $this->log('appoint', 'info', $appoint->APPOINT_ID, 'ดูรายการนัดหมาย');
        }

        return View("Appoint.info")->with([
      'appoint' => $appoint,
      'contracts' => $contracts,
      'staffs' => json_encode($staffs)
    ]);
    }


    public function history($id)
    {
        if (!Auth::user()) {
            abort(401);
        }

        $appoint = Appoint::find($id);

        if (!(Auth::user()->can('appoint.is_owner', $appoint) || Auth::user()->can('appoint.is_attender', $appoint))) {
            abort(401);
        }

        $staff = Staff::where('STAFF_ID', Auth::user()->id)->first();
        $appoint = Appoint::find($id);

        $logs = ApplicationLog::where('CONTROLLER_NAME', 'appoint')->where('DATA_ID', $id)->orderBy('LOG_DATE')->get();


        return View("Appoint.history")->with([
      'appoint' => $appoint,
      'logs' => $logs
    ]);
    }


    public function sendmail($id)
    {
        if (!Auth::user()) {
            abort(401);
        }

        $appoint = Appoint::find($id);
        if (!Auth::user()->can('appoint.is_owner', $appoint)) {
            abort(401);
        }
    //if (!Auth::user()->can('appoint.is_attender'))   abort(401);

    if (isset($appoint)) {
        foreach ($appoint->attenders as $item) {
            // do send email
	        if (isset($item->EMAIL) && $item->EMAIL != "") {
	            $utils = new Utils;

	            try {
	                Mail::send('mail.reminder', ['appoint' => $appoint, 'utils' => $utils ], function ($message) use ($item, $appoint) {
	                    $subject = "[eDocument] แจ้งนัดหมาย - " . $appoint->APPOINT_TITLE;

	                    $message->from('amir.h@yru.ac.th', 'eDocument');
	                    $message->to($item->EMAIL);
	                    $message->subject($subject);
	                });
	            } catch (Exception $e) {
	            }
	        }
        }
    }
        return redirect()->action('AppointController@info', [ 'id' => $id]);
    }


    public function cancel($id)
    {
        if (!Auth::user()) {
            abort(401);
        }

        $appoint = Appoint::find($id);
        if (!Auth::user()->can('appoint.is_owner', $appoint)) {
            abort(401);
        }

        $appoint->APPOINT_STATUS_ID = 2;
        $appoint->save();

        $this->log('appoint', 'cancel', $appoint->APPOINT_ID, 'ยกเลิกนัดหมาย');

        return redirect('/appoint');
    }


    public function remove_attender($id)
    {
        if (!Auth::user()) {
            abort(401);
        }

        $attender = AppointAttender::find($id);
        if (isset($attender)) {
            if (!Auth::user()->can('appoint.is_owner', $attender->appoint)) {
                abort(401);
            }
        }

        $appointid = $attender->APPOINT_ID;
        if (isset($attender)) {
            $name = $attender->ATTENDER_NAME;
            $attender->delete();

            $this->log('appoint', 'remove_attender', $attender->APPOINT_ID, 'ลบผู้เข้าร่วมนัดหมาย : ' . $name);
        }

        return redirect('/appoint/info/' . $appointid);
    }


    public function create()
    {
        if (!Auth::user()) {
            abort(401);
        }

        $appoint = new Appoint;

        $staff_items = Staff::all();
        $staffs = array();
        foreach ($staff_items as $item) {
            $staffs[] = $item->FULL_NAME;
        }

        $location_items = Location::all();
        $locations = array();
        foreach ($location_items as $item) {
            $locations[] = $item->LOCATION_NAME;
        }

        $appoint->OWNER_NAME = Auth::user()->name;
        return View('Appoint.edit')->with([
      'appoint' => $appoint
      , 'staffs' => json_encode($staffs)
      , 'locations'=> json_encode($locations)
    ]);
    }

    public function add_attender(Request $request)
    {
        if (!Auth::user()) {
            abort(401);
        }

        $appoint = Appoint::find($request['APPOINT_ID']);
        if (!(Auth::user()->can('appoint.is_owner', $appoint) || Auth::user()->can('appoint.is_attender', $appoint))) {
            abort(401);
        }
    //if (!Auth::user()->can('appoint.is_attender'))   abort(401);

    $this->validate($request, [
        'ATTENDER_NAME' => 'required',
        'APPOINT_ID' => 'required'
    ]);

        $attender = new AppointAttender;

        $valid = true;

        $staff = Staff::where('FULL_NAME', $request['ATTENDER_NAME'])->first();

        if (isset($staff)) {
            $appoint = AppointAttender::where('ATTENDER_STAFF_ID', $staff->STAFF_ID)
        ->where('APPOINT_ID', $request['APPOINT_ID'])
        ->first();

            if (isset($appoint)) {
                // if exists, do not insert.
        $valid = false;
            } else {
                $attender->ATTENDER_STAFF_ID = $staff->STAFF_ID;
                $attender->EMAIL = $staff->EMAIL;
            }
        }

        if ($valid) {
            $attender->APPOINT_ID = $request['APPOINT_ID'];
            $attender->ATTENDER_STATUS_ID = 0;
            $attender->ATTENDER_NAME = $request['ATTENDER_NAME'];
      //$attender->EMAIL =

      $attender->UPDATE_BY = Auth::user()->name;
            $attender->UPDATE_DATE = date("Y-m-d H:i:s");
            $attender->CREATE_BY = Auth::user()->name;
            $attender->CREATE_DATE = date("Y-m-d H:i:s");

            $attender->save();

            $this->log('appoint', 'add_attender', $attender->APPOINT_ID, 'เพิ่มผู้เข้าร่วมนัดหมาย : ' . $attender->ATTENDER_NAME);
        }
        return redirect('/appoint/info/' . $request['APPOINT_ID']);
    }


    public function copy_appoint(Request $request)
    {
        if (!Auth::user()) {
            abort(401);
        }
    // if (!(Auth::user()->can('appoint.is_owner') || Auth::user()->can('appoint.is_attender')))   abort(401);
    //if (!Auth::user()->can('appoint.is_attender'))   abort(401);

    $appoint = Appoint::find($request['APPOINT_ID']);
        if (!Auth::user()->can('appoint.is_owner', $appoint)) {
            abort(401);
        }

        $this->validate($request, [
        'END_LOOP_DATE' => 'required',
    ]);

        $appoint = Appoint::find($request['APPOINT_ID']);
        $start = $appoint->START_DATE;
        $end = $appoint->END_DATE;
        $endloop = $request['END_LOOP_DATE'];

        if (isset($request['D_ALL']) && $request['D_ALL'] == 1) {
            while (strtotime($start) < strtotime($endloop)) {
                $start =  date('Y-m-d H:i:s', strtotime($start . "+1 days"));
                $end =  date('Y-m-d H:i:s', strtotime($end . "+1 days"));

                $item = Appoint::where('PARENT_ID', $appoint->APPOINT_ID)
          ->where('START_DATE', $start)->first();

                if (!isset($item)) {
                    $appoint_copy = $appoint->replicate();
                    $appoint_copy->PARENT_ID = $appoint->APPOINT_ID;
                    $appoint_copy->START_DATE = $start;
                    $appoint_copy->END_DATE = $end;

                    $appoint_copy->UPDATE_BY = Auth::user()->name;
                    $appoint_copy->UPDATE_DATE = date("Y-m-d H:i:s");
                    $appoint_copy->CREATE_BY = Auth::user()->name;
                    $appoint_copy->CREATE_DATE = date("Y-m-d H:i:s");

                    $appoint_copy->save();

                    foreach ($appoint->attenders as $attender) {
                        $attender_copy = $attender->replicate();
                        $attender_copy->APPOINT_ID = $appoint_copy->APPOINT_ID;
                        $attender_copy->UPDATE_BY = Auth::user()->name;
                        $attender_copy->UPDATE_DATE = date("Y-m-d H:i:s");
                        $attender_copy->CREATE_BY = Auth::user()->name;
                        $attender_copy->CREATE_DATE = date("Y-m-d H:i:s");

                        $attender_copy->save();
                    }
                }
            }
        } else {
            while (strtotime($start) < strtotime($endloop)) {
                $start =  date('Y-m-d H:i:s', strtotime($start . "+1 days"));
                $end =  date('Y-m-d H:i:s', strtotime($end . "+1 days"));

                $toSave = false;

                if (date('d', strtotime($start)) == $request['D_MONTH']) {
                    $toSave = true;
                }

                if (isset($request['D_01']) && $request['D_01'] == 1 && date('N', strtotime($start)) == 7) {
                    $toSave = true;
                }

                if (isset($request['D_02']) && $request['D_02'] == 1 && date('N', strtotime($start)) == 1) {
                    $toSave = true;
                }

                if (isset($request['D_03']) && $request['D_03'] == 1 && date('N', strtotime($start)) == 2) {
                    $toSave = true;
                }

                if (isset($request['D_04']) && $request['D_04'] == 1 && date('N', strtotime($start)) == 3) {
                    $toSave = true;
                }

                if (isset($request['D_05']) && $request['D_05'] == 1 && date('N', strtotime($start)) == 4) {
                    $toSave = true;
                }

                if (isset($request['D_06']) && $request['D_06'] == 1 && date('N', strtotime($start)) == 5) {
                    $toSave = true;
                }

                if (isset($request['D_07']) && $request['D_07'] == 1 && date('N', strtotime($start)) == 6) {
                    $toSave = true;
                }

                if ($toSave) {
                    // find before save .. parent_id and start_date
          $item = Appoint::where('PARENT_ID', $appoint->APPOINT_ID)
            ->where('START_DATE', $start)->first();

                    if (!isset($item)) {
                        $appoint_copy = $appoint->replicate();
                        $appoint_copy->PARENT_ID = $appoint->APPOINT_ID;
                        $appoint_copy->START_DATE = $start;
                        $appoint_copy->END_DATE = $end;
                        $appoint_copy->save();

                        foreach ($appoint->attenders as $attender) {
                            $attender_copy = $attender->replicate();
                            $attender_copy->APPOINT_ID = $appoint_copy->APPOINT_ID;

                            $attender_copy->save();
                        }
                    }
                }
            }
        }

        if (isset($request['D_MONTH']) && ($request['D_MONTH'] > 0)) {
            while (strtotime($start) < strtotime($endloop)) {
                $start =  date('Y-m-d H:i:s', strtotime($start . "+1 days"));
                $end =  date('Y-m-d H:i:s', strtotime($end . "+1 days"));

                if (date('d', strtotime($start)) == $request['D_MONTH']) {
                    $appoint_copy = $appoint->replicate();
                    $appoint_copy->PARENT_ID = $appoint->APPOINT_ID;
                    $appoint_copy->START_DATE = $start;
                    $appoint_copy->END_DATE = $end;
                    $appoint_copy->save();

                    foreach ($appoint->attenders as $attender) {
                        $attender_copy = $attender->replicate();
                        $attender_copy->APPOINT_ID = $appoint_copy->APPOINT_ID;

                        $attender_copy->save();
                    }
                }
            }
        }

        if (isset($request['D_MONTH']) && ($request['D_MONTH'] > 0)) {
            while (strtotime($start) < strtotime($endloop)) {
                $start =  date('Y-m-d H:i:s', strtotime($start . "+1 days"));
                $end =  date('Y-m-d H:i:s', strtotime($end . "+1 days"));

                if (date('d', strtotime($start)) == $request['D_MONTH']) {
                    $appoint_copy = $appoint->replicate();
                    $appoint_copy->PARENT_ID = $appoint->APPOINT_ID;
                    $appoint_copy->START_DATE = $start;
                    $appoint_copy->END_DATE = $end;
                    $appoint_copy->save();

                    $this->log('appoint', 'add_contract', $appoint_copy->APPOINT_ID, 'คัดลอกนัดหมายนัดหมาย');

                    foreach ($appoint->attenders as $attender) {
                        $attender_copy = $attender->replicate();
                        $attender_copy->APPOINT_ID = $appoint_copy->APPOINT_ID;

                        $attender_copy->save();
                    }
                }
            }
        }

        return redirect('/appoint/info/' . $request['APPOINT_ID']);
    }


    public function add_contract(Request $request)
    {
        if (!Auth::user()) {
            abort(401);
        }

        $appoint = Appoint::find($request['APPOINT_ID']);
        if (!(Auth::user()->can('appoint.is_owner', $appoint) || Auth::user()->can('appoint.is_attender', $appoint))) {
            abort(401);
        }
    //if (!Auth::user()->can('appoint.is_attender'))   abort(401);
    // if (!Auth::user()->can('appoint.is_owner'))   abort(401);

    if (isset($request['CONTRACT_ID']) || $request['CONTRACT_ID'] > 0) {
        $contract = Contract::find($request['CONTRACT_ID']);
        if (isset($contract)) {

        // TODO : must optimize speed.
        foreach ($contract->details as $item) {
            $staff = $item->staff;

            $appoint = AppointAttender::where('ATTENDER_STAFF_ID', $item->STAFF_ID)
            ->where('APPOINT_ID', $request['APPOINT_ID'])
            ->first();

            if (!isset($appoint)) {
                $attender = new AppointAttender;

                $attender->ATTENDER_STAFF_ID = $staff->STAFF_ID;
                $attender->EMAIL = $staff->EMAIL;

                $attender->APPOINT_ID = $request['APPOINT_ID'];
                $attender->ATTENDER_STATUS_ID = 0;
                $attender->ATTENDER_NAME = $staff->FULL_NAME;

                $attender->UPDATE_BY = Auth::user()->name;
                $attender->UPDATE_DATE = date("Y-m-d H:i:s");
                $attender->CREATE_BY = Auth::user()->name;
                $attender->CREATE_DATE = date("Y-m-d H:i:s");

                $attender->save();

                $this->log('appoint', 'add_contract', $attender->APPOINT_ID, 'เพิ่มผู้เข้าร่วมนัดหมาย : ' . $attender->ATTENDER_NAME);
            }
        }
        }
    }

        return redirect('/appoint/info/' . $request['APPOINT_ID']);
    }


    public function save(Request $request)
    {
        if (!Auth::user()) {
            abort(401);
        }
    // if (!(Auth::user()->can('appoint.is_owner') || Auth::user()->can('appoint.is_attender')))   abort(401);
    //if (!Auth::user()->can('appoint.is_attender'))   abort(401);

    $appoint = Appoint::find($request['APPOINT_ID']);
        if (isset($appoint)) {
            if (!Auth::user()->can('appoint.is_owner', $appoint)) {
                abort(401);
            }
        }

        $this->validate($request, [
        'APPOINT_TITLE' => 'required',
        'LOCATION_NAME' => 'required',
        'OWNER_NAME' => 'required',
        'START_DATE' => 'required',
        'END_DATE' => 'required',
    ]);

        $appoint = null;
        if (isset($request['APPOINT_ID']) || $request['APPOINT_ID'] > 0) {
            $appoint = Appoint::find($request['APPOINT_ID']);

            if (isset($appoint)) {
                $appoint->UPDATE_DATE = date("Y-m-d H:i:s");
                $appoint->UPDATE_BY = Auth::user()->name;
            }
        }

        $isNew = false;
        if (!isset($appoint)) {
            $isNew = true;
            $appoint = new Appoint;
            $appoint->CREATE_STAFF_ID = Auth::user()->id;
            $appoint->CREATE_BY = Auth::user()->name;
            $appoint->CREATE_DATE = date("Y-m-d H:i:s");
            $appoint->APPOINT_STATUS_ID = 1;
        }

        $appoint->APPOINT_TITLE = $request['APPOINT_TITLE'];
        $appoint->APPOINT_DESC = $request['APPOINT_DESC'];
        $appoint->LOCATION = $request['LOCATION_NAME'];


        $location = Location::where('LOCATION_NAME', $request['LOCATION_NAME'])->first();
        if (isset($location)) {
            $appoint->LOCATION_ID = $location->LOCATION_ID;
        } else {
            $appoint->LOCATION_ID = 0;
        }

        $appoint->START_DATE = $request['START_DATE'];
    //$appoint->START_TIME = $request['DEPARTMENT_ID'];
    $appoint->END_DATE = $request['END_DATE'];
    //$appoint->END_TIME = $request['DEPARTMENT_ID'];
    $appoint->OWNER_NAME = $request['OWNER_NAME'];
    //$appoint->IS_ALL_DAY = $request['DEPARTMENT_ID'];
    $appoint->save();

        if ($isNew) {
            $this->log('appoint', 'save', $appoint->APPOINT_ID, 'บันทึกรายการนัดหมาย');
        } else {
            $this->log('appoint', 'save', $appoint->APPOINT_ID, 'แก้ไขรายการนัดหมาย');
        }


        return redirect('/appoint/info/' . $appoint->APPOINT_ID);
    }


    public function edit($id)
    {
        if (!Auth::user()) {
            abort(401);
        }
    // if (!(Auth::user()->can('appoint.is_owner') || Auth::user()->can('appoint.is_attender')))   abort(401);
    //if (!Auth::user()->can('appoint.is_attender'))   abort(401);
    $appoint = Appoint::find($id);
        if (!Auth::user()->can('appoint.is_owner', $appoint)) {
            abort(401);
        }

        $staff_items = Staff::all();
        $location_items = Location::all();

        $staffs = array();
        foreach ($staff_items as $item) {
            $staffs[] = $item->FULL_NAME;
        }

        $locations = array();
        foreach ($location_items as $item) {
            $locations[] = $item->LOCATION_NAME;
        }

        return View('Appoint.edit')->with([
      'appoint' => $appoint
      , 'staffs' => json_encode($staffs)
      , 'locations'=>json_encode($locations)
    ]);
    }

    public function accept($id)
    {
        if (!Auth::user()) {
            abort(401);
        }
    // if (!(Auth::user()->can('appoint.is_owner') || Auth::user()->can('appoint.is_attender')))   abort(401);

    $appoint = Appoint::find($id);
        if (!Auth::user()->can('appoint.is_attender', $appoint)) {
            abort(401);
        }
    // if (!Auth::user()->can('appoint.is_owner'))   abort(401);

    $attender = AppointAttender::where('APPOINT_ID', $id)
        ->where('ATTENDER_STAFF_ID', Auth::user()->id)
        ->first();

        if (isset($attender)) {
            $attender->UPDATE_DATE = date("Y-m-d H:i:s");
            $attender->UPDATE_BY = Auth::user()->name;

            $attender->ATTENDER_STATUS_ID = 1;
            $attender->save();

            $this->log('appoint', 'accept', $id, 'ตอบรับเข้าร่วมนัดหมาย');
        }
        return redirect('/appoint/info/' . $id);
    }


    public function reject($id)
    {
        if (!Auth::user()) {
            abort(401);
        }
    // if (!(Auth::user()->can('appoint.is_owner') || Auth::user()->can('appoint.is_attender')))   abort(401);

    $appoint = Appoint::find($id);
        if (!Auth::user()->can('appoint.is_attender', $appoint)) {
            abort(401);
        }
    // if (!Auth::user()->can('appoint.is_owner'))   abort(401);

    $attender = AppointAttender::where('APPOINT_ID', $id)
        ->where('ATTENDER_STAFF_ID', Auth::user()->id)
        ->first();

        if (isset($attender)) {
            $attender->UPDATE_DATE = date("Y-m-d H:i:s");
            $attender->UPDATE_BY = Auth::user()->name;

            $attender->ATTENDER_STATUS_ID = 2;
            $attender->save();

            $this->log('appoint', 'reject', $id, 'ปฏิเสธเข้าร่วมนัดหมาย');
        }
        return redirect('/appoint/info/' . $id);
    }
}
