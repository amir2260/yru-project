<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Document;
use App\Model\DocumentComment;
use App\Model\Department;
use App\Model\Staff;
use App\Model\DocumentPrefix;
use App\Model\DocumentFooter;
use App\Model\DocumentPriority;
use App\Model\DocumentType;
use App\Model\DocumentStatus;
use App\Model\DocumentFile;
use App\Model\FileItem;
use App\Model\Box;
use App\Model\Path;
use App\Model\PathAction;
use App\Model\DocumentInBox;
use App\Model\DocumentPath;
use App\Model\ApplicationLog;
use App\Model\DocumentReceiveCode;

use App\Utils;




class CommandController extends Controller {
	public function __construct() {
		$this->middleware ( 'auth' );
	}



	public function create() {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = new Document ();
		$prefixs = DocumentPrefix::all ()->pluck ( 'DOC_PREFIX_NAME', 'DOC_PREFIX_ID' );
		$footers = DocumentFooter::all ()->pluck ( 'DOC_FOOTER_NAME', 'DOC_FOOTER_ID' );
		$priorities = DocumentPriority::all ()->pluck ( 'DOC_PRIORITY_NAME', 'DOC_PRIORITY_ID' );
		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();

		$department = Department::find(0);
		
		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();		

		return View ( 'Command.edit' )->with ( [
				'document' => $doc,
				'prefixs' => $prefixs,
				'footers' => $footers,
				'priorities' => $priorities,
				'staff' => $staff,
				'department' => $department,
				'departments' => $staff->getMasterDepartments()->all()
		] );
	}


	public function edit(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );

		if (isset ( $doc )) {
			if (! Auth::user ()->can ( 'document.access_item', $doc )) {
				abort ( 403, 'Unauthorized action.' );
			}
		} else {
			abort ( 403, 'Unauthorized action.' );
		}

		$prefixs = DocumentPrefix::all ()->pluck ( 'DOC_PREFIX_NAME', 'DOC_PREFIX_ID' );
		$footers = DocumentFooter::all ()->pluck ( 'DOC_FOOTER_NAME', 'DOC_FOOTER_ID' );
		$priorities = DocumentPriority::all ()->pluck ( 'DOC_PRIORITY_NAME', 'DOC_PRIORITY_ID' );
		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();

		return View ( 'Memo.edit' )->with ( [
				'document' => $doc,
				'prefixs' => $prefixs,
				'footers' => $footers,
				'priorities' => $priorities,
				'staff' => $staff, 
				'departments' => $staff->getMasterDepartments()->all()
		] );
	}


	public function info(Request $request, $id) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$doc = Document::find ( $id );
		if (isset ( $doc )) {
			if (! Auth::user ()->can ( 'document.access_item', $doc )) {
				abort ( 403, 'Unauthorized action.' );
			}
		}
		
		$staff = Staff::where('STAFF_ID',  Auth::user ()->id)->first();
		$ddlist = $staff->getMasterDepartments();
		$list = array();
		foreach ($ddlist as $d){
			if ($d->DEPARTMENT_ID)
				$list[] += $d->DEPARTMENT_ID;
		}
		
		$docReceices = DocumentReceiveCode::where('DOC_ID', $id)
		->whereHas('code', function($c) use ($list){
			$c->whereIn('DEPARTMENT_ID', $list);
		})->get();
		

		$staff = Staff::where ( 'STAFF_ID', Auth::user ()->id )->first ();
		$utils = new Utils;

		return View ( 'Command.info' )->with ( [
				'document' => $doc,
				'staff' => $staff,
				'utils' => $utils,
				'docReceices' => $docReceices
		] );
	}


	public function save(Request $request) {
		if (! Auth::user ()) {
			abort ( 403, 'Unauthorized action.' );
		}

		$id = $request ['DOC_ID'];
		$doc = Document::find ( $id );
		if (isset ( $doc )) {
			if (! Auth::user ()->can ( 'document.edit_item', $doc )) {
				abort ( 403, 'Unauthorized action.' );
			}
		} else {
			$doc = new Document ();
		}

		$this->validate ( $request, [
				'WRITE_AT' => 'required',
				'DOC_DATE' => 'required',
				'DOC_TITLE' => 'required',
				// 'TO_NAME' => 'required',
				'DOC_CONTENT' => 'required',
				'FROM_NAME' => 'required',
				'FROM_POSITION' => 'required'
		] );

		// TODO : do validate.
		$doc->STAFF_ID = Auth::user ()->id;
		$doc->WRITE_AT = $request ['WRITE_AT'];
// 		$doc->DOC_NO = $request ['DOC_NO'];
		$doc->DOC_DATE = $request ['DOC_DATE'];
		// $doc->DOC_PREFIX_ID = $request ['DOC_PREFIX_ID'];
		$doc->DOC_TITLE = $request ['DOC_TITLE'];
		$doc->TO_NAME = $request ['TO_NAME'];
		$doc->DOC_CONTENT = $request ['DOC_CONTENT'];
		// $doc->DOC_COMMENT = $request ['DOC_COMMENT'];
		$doc->FROM_NAME = $request ['FROM_NAME'];
		$doc->FROM_POSITION = $request ['FROM_POSITION'];
		// $doc->FROM_DEPARTMENT = $request ['FROM_DEPARTMENT'];
		// $doc->DOC_FOOTER_ID = $request ['DOC_FOOTER_ID'];
		$doc->DOC_STATUS_ID = 1; // 1 = draff
		$doc->DOC_TYPE_ID = 4; // 4 =  คำสั่ง
		$doc->DOC_PRIORITY_ID = $request ['DOC_PRIORITY_ID'];

		$doc->UPDATE_BY = Auth::user ()->name;
		$doc->UPDATE_DATE = date ( "Y-m-d H:i:s" );
		$doc->DEPARTMENT_ID = $request['DEPARTMENT_ID'];

		if (! (isset ( $doc->DOC_ID ) && $doc->DOC_ID > 0)) {
			$doc->CREATE_BY = Auth::user ()->name;
			$doc->CREATE_DATE = date ( "Y-m-d H:i:s" );
		}
		$doc->save ();

		$this->log('document', 'create', $doc->DOC_ID,  'บันทึกข้อมูลเอกสาร');

		return redirect ()->action ( 'DocumentController@info', $doc->DOC_ID );
	}




}
