<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Location;
use App\Model\Department;
use App\Model\Staff;


class LocationController extends Controller {
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $locations = Location::paginate(20);

        return View('Location.list')->with([
            'locations' => $locations ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request){
        $search = $request['TEXT_SEARCH'];

        $locations = Location::where('LOCATION_NAME', 'like', '%' . $search . '%')
        ->orWhere('BUILDING_NAME', 'like', '%' . $search . '%')
        ->paginate(20);

        return View('Location.list')->with([
            'locations' => $locations ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function info($id){
        $location = Location::find($id);
        return View("Location.info")->with([
            'location' => $location
        ]);
    }

    public function edit($id){
        $location = Location::find($id);
        return View("Location.edit")->with([
            'location' => $location
        ]);
    }

    public function delete($id){
        $location = Location::find($id);
        if (isset($location)){
            $location->delete();
        }

        return redirect('/location');
    }


    public function create()
    {
        $location = new Location;

        return View('Location.edit')->with([
            'location' => $location
        ]);
    }

    public function save(Request $request)
    {

        $this->validate($request, [
            'LOCATION_NAME' => 'required'
            , 'BUILDING_NAME' => 'required'
        ]);

        $id = $request['LOCATION_ID'];
        $location = new Location;

        $item = Location::find($id);
        if (isset($item)){
            $location = $item;
        }

        $location->LOCATION_NAME = $request['LOCATION_NAME'];
        $location->LOCATION_DESC =  $request['LOCATION_DESC'];
        $location->BUILDING_NAME =  $request['BUILDING_NAME'];

        $location->UPDATE_BY = Auth::user()->name;
        $location->UPDATE_DATE = date("Y-m-d H:i:s");

        if (isset($location->LOCATION_ID) && $location->LOCATION_ID == 0){
            $location->CREATE_BY = Auth::user()->name;
            $location->CREATE_DATE = date("Y-m-d H:i:s");
        }

        $location->save();

        return redirect('/location');
    }



    // public function memberinfo($id){
    //   $member = Member::find($id);
    //   $staffTypes = StaffType::all();
    //
    //   return View('Member.info')->with(
    //     [
    //       'member'=>$member
    //       ,'stafftypes' => $staffTypes]
    //   );
    //
    // }
    //
    // public function memberedit($id){
    //   $member = Member::find($id);
    //   $staffTypes = StaffType::all();
    //
    //   return View('Member.register')->with(
    //     [
    //       'member'=>$member
    //       ,'stafftypes' => $staffTypes]
    //   );
    //
    // }
    //
    // public function memberdelete($id){
    //   $member = Member::find($id);
    //   if ($member){
    //     $member->delete();
    //   }
    //
    //   return redirect('/member');
    // }

}
