<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Request;
use App\Model\ApplicationLog;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function log($controllerName, $actionName, $id, $logName, $logType = 'MESSAGE', $logDesc = null){

      $log = new ApplicationLog;

      $log->LOG_BY = 'Unknow user';
      $log->LOG_DATE = date('Y-m-d H:i:s');
      if (Auth::user() !== null){
        $log->LOG_BY = Auth::user()->id;
      }

      $log->LOG_TYPE = $logType;
      $log->IP_ADDRESS = Request::ip();

      $log->DATA_ID = $id;
      $log->CONTROLLER_NAME = $controllerName;
      $log->ACTION_NAME = $actionName;
      $log->LOG_NAME = $logName;
      $log->LOG_DESC = $logDesc;

      $log->save();
    }

    public function insertLog($logType, $ip, $actionName, $logName, $logDesc = null){

      $log = new ApplicationLog;

      $log->LOG_BY = 'Unknow user';
      $log->LOG_DATE = date('Y-m-d H:i:s');
      if (Auth::user() !== null){
        $log->LOG_BY = Auth::user()->name;
      }

      $log->LOG_TYPE = $logType;
      $log->IP_ADDRESS = $ip;
      $log->ACTION_NAME = $actionName;
      $log->LOG_NAME = $logName;
      $log->LOG_DESC = $logDesc;

      $log->save();
    }
}
