<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Manager;
use App\Model\Department;
use App\Model\Staff;


class ManagerController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $managers = Manager::paginate(20);

        return View('Manager.list')->with([
            'managers' => $managers ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function index_post(Request $request){
        $search = $request['TEXT_SEARCH'];

        $managers = Manager::whereHas('staff', function($q) use ($search) {
            $q->where('FULL_NAME', 'like', '%' . $search . '%');
        })->orWhereHas('department', function($q) use ($search) {
            $q->where('DEPARTMENT_NAME', 'like', '%' . $search . '%');
        })->paginate(20);

        return View('Manager.list')->with([
            'managers' => $managers ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }

    public function info($id){
        $manager = Manager::find($id);
        return View("Manager.info")->with([
            'manager' => $manager
        ]);
    }

    public function delete($id){
        $manager = Manager::find($id);
        if (isset($manager)){
            $manager->delete();
        }

        return redirect('/manager');
    }


    public function create(){
        $manager = new Manager;
        $staff_items = Staff::all();
        $departments = Department::where('MASTER_DEPARTMENT_ID', 0)
            ->where('DEPARTMENT_ID', '<>', 0)->get();

        $staffs = array();
        foreach ($staff_items as $item){
            $staffs[] = $item->FULL_NAME;
        }

        return View('Manager.edit')->with([
            'manager' => $manager
            , 'staffs' => json_encode($staffs)
            , 'departments'=>$departments
        ]);
    }

    public function save(Request $request){

        $this->validate($request, [
            'STAFF_NAME' => 'required|valid_staffname',
        ]);

        $staff = Staff::where('FULL_NAME', $request['STAFF_NAME'])->first();

        $manager = Manager::where('STAFF_ID', $staff->STAFF_ID);
        if (isset($manager)){
            $manager->delete();
        }

        $manager = Manager::where('STAFF_ID', $staff->STAFF_ID)
        ->where('DEPARTMENT_ID', $request['DEPARTMENT_ID'])->first();

        if (!isset($manager)){
            $manager = new Manager;

            $manager->STAFF_ID = $staff->STAFF_ID;
            $manager->DEPARTMENT_ID = $request['DEPARTMENT_ID'];

            $manager->CREATE_BY = Auth::user()->name;
            $manager->CREATE_DATE = date("Y-m-d H:i:s");

            $manager->save();
        }

        return redirect('/manager');
    }



    // public function memberinfo($id){
    //     $member = Member::find($id);
    //     $staffTypes = StaffType::all();
    //
    //     return View('Member.info')->with(
    //         [
    //             'member'=>$member
    //             ,'stafftypes' => $staffTypes]
    //         );
    //
    //     }
    //
    //     public function memberedit($id){
    //         $member = Member::find($id);
    //         $staffTypes = StaffType::all();
    //
    //         return View('Member.register')->with(
    //             [
    //                 'member'=>$member
    //                 ,'stafftypes' => $staffTypes]
    //             );
    //
    //         }
    //
    //         public function memberdelete($id){
    //             $member = Member::find($id);
    //             if ($member){
    //                 $member->delete();
    //             }
    //
    //             return redirect('/member');
    //         }

            // public function memberjson(){
            //   $members = Member::all();
            //   return response()->json($members);
            // }

}
