<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Model\Course;
use App\Model\Department;
use App\Model\Staff;
use App\Model\StaffCourse;


class CourseController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
        $courses = Course::paginate(20);

        return View('Course.list')->with([
            'courses' => $courses ,
            'text_search' => $request['TEXT_SEARCH']
        ]);

    }

    public function index_post(Request $request){
        $search = $request['TEXT_SEARCH'];


        $courses = Course::whereHas('staff', function($q) use ($search) {
            $q->where('FULL_NAME', 'like', '%' . $search . '%');
        })->orWhereHas('department', function($q) use ($search) {
            $q->where('DEPARTMENT_NAME', 'like', '%' . $search . '%');
        })->paginate(20);

        return View('Course.list')->with([
            'courses' => $courses ,
            'text_search' => $request['TEXT_SEARCH']
        ]);
    }
    public function delete($id){
        $course = Course::find($id);
        if (isset($course)){
            $course->delete();
        }
        return redirect()->action('CourseController@index');
    }

    public function edit($id){
        $course = Course::find($id);
        $departments = Department::where('MASTER_DEPARTMENT_ID', 0)
			->where('DEPARTMENT_ID', '>', 0)
			->get();

        if (isset($course)){
            return View('Course.edit')->with([
                'course' => $course
                , 'departments'=>$departments->pluck('DEPARTMENT_NAME', 'DEPARTMENT_ID')
            ]);
        }
    }

    public function info($id){
        $course = Course::find($id);
        $departments = Department::where('MASTER_DEPARTMENT_ID', 0)
			->where('DEPARTMENT_ID', '>', 0)
			->get();
        $staffs = Staff::get();

        if (isset($course)){
            return View('Course.info')->with([
                'course' => $course
                , 'departments'=>$departments->pluck('DEPARTMENT_NAME', 'DEPARTMENT_ID')
                , 'staffs' => $staffs->pluck('FULL_NAME', 'STAFF_ID')
            ]);
        }
    }


    public function create(){
        $course = new Course;
        $departments = Department::where('MASTER_DEPARTMENT_ID', 0)
			->where('DEPARTMENT_ID', '>', 0)
			->get();

        return View('Course.edit')->with([
            'course' => $course
            , 'departments'=>$departments->pluck('DEPARTMENT_NAME', 'DEPARTMENT_ID')
        ]);
    }

    public function save(Request $request){

        $this->validate($request, [
            'COURSE_NAME' => 'required',
        ]);

		$course = new Course;
		$item = Course::find ($request->input('COURSE_ID'));
		if (isset($item)){
			$course = $item;
		}

		$course->COURSE_NAME = $request->input('COURSE_NAME');
		$course->DEPARTMENT_ID = $request->input('DEPARTMENT_ID');
		$course->save();

        return redirect()->action('CourseController@index');
    }

    public function addStaff(Request $request){

        $this->validate($request, [
            'STAFF_ID' => 'required',
        ]);

        $staff = new StaffCourse;
        $staff->STAFF_ID = $request->input('STAFF_ID');
        $staff->COURSE_ID = $request->input('COURSE_ID');
		$staff->save();

        return redirect()->action('CourseController@info', $staff->COURSE_ID);
    }



    // public function memberinfo($id){
    //     $member = Member::find($id);
    //     $staffTypes = StaffType::all();
    //
    //     return View('Member.info')->with(
    //         [
    //             'member'=>$member
    //             ,'stafftypes' => $staffTypes]
    //         );
    //
    //     }
    //
    //     public function memberedit($id){
    //         $member = Member::find($id);
    //         $staffTypes = StaffType::all();
    //
    //         return View('Member.register')->with(
    //             [
    //                 'member'=>$member
    //                 ,'stafftypes' => $staffTypes]
    //             );
    //
    //         }
    //
    //         public function memberdelete($id){
    //             $member = Member::find($id);
    //             if ($member){
    //                 $member->delete();
    //             }
    //
    //             return redirect('/member');
    //         }

            // public function memberjson(){
            //   $members = Member::all();
            //   return response()->json($members);
            // }

}
