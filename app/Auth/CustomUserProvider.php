<?php

namespace App\Auth;
use App\User;
// use Carbon\Carbon;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

use GuzzleHttp\Client;

class CustomUserProvider implements UserProvider {
    /**
    * Retrieve a user by their unique identifier.
    *
    * @param  mixed $identifier
    * @return \Illuminate\Contracts\Auth\Authenticatable|null
    */
    public function retrieveById($identifier)
    {
        // Amir. do Step 1.
        $client = new Client;

        try{
            $response = $client->request('POST'
            , 'http://202.29.32.163/yruservice/staff/get_staff_by_staffid/format/json'
            , ['form_params' =>
            [ 'staffid'=>$identifier  ]
        ]
    );

    if ($response->getStatusCode() != 200)
    return null;

    $items = $response->getBody()->getContents();
    $staff = json_decode($items);

    if (isset($staff)){
        $user = new User;
        $user->name = $staff->SNAME;
        //$user->username = $credentials['username'];
        $user->id = $staff->STAFFID;
        //$user->password = 'qwerdf';
        //$
        if ($identifier == 1019)
            $user->name = 'User Name';

        return $user;
    }

}catch (\Exception $e){

}

return null;
}

/**
* Retrieve a user by by their unique identifier and "remember me" token.
*
* @param  mixed $identifier
* @param  string $token
* @return \Illuminate\Contracts\Auth\Authenticatable|null
*/
public function retrieveByToken($identifier, $token)
{
    // // TODO: Implement retrieveByToken() method.
    // $qry = UserPoa::where('admin_id','=',$identifier)->where('remember_token','=',$token);
    //
    // if($qry->count() >0)
    // {
    //     $user = $qry->select('admin_id', 'username', 'first_name', 'last_name', 'email', 'password')->first();
    //
    //     $attributes = array(
    //         'id' => $user->admin_id,
    //         'username' => $user->username,
    //         'password' => $user->password,
    //         'name' => $user->first_name . ' ' . $user->last_name,
    //     );
    //
    //     return $user;
    // }
    // return null;

}

/**
* Update the "remember me" token for the given user in storage.
*
* @param  \Illuminate\Contracts\Auth\Authenticatable $user
* @param  string $token
* @return void
*/
public function updateRememberToken(Authenticatable $user, $token)
{
    // TODO: Implement updateRememberToken() method.
    // $user->setRememberToken($token);
    //
    // $user->save();

}

/**
* Retrieve a user by the given credentials.
*
* @param  array $credentials
* @return \Illuminate\Contracts\Auth\Authenticatable|null
*/
public function retrieveByCredentials(array $credentials)
{
    // Amir. do Step 1.
    $client = new Client;

    try{
        $response = $client->request('POST'
        , 'http://202.29.32.163/yruservice/staff/authenticate/format/json'
        , ['form_params' =>
        ['username'=>$credentials['username'] , 'password'=>$credentials['password'] ]
    ]
);

/*
<STAFFID>935</STAFFID>
<CITIZENID>1960500042829</CITIZENID>
<STAFFSTATUS>20</STAFFSTATUS>
<WORKPOSITIONID>37</WORKPOSITIONID>
<WORKPOSITIONNAME>นักวิชาการคอมพิวเตอร์</WORKPOSITIONNAME>
<STAFFGROUP>2</STAFFGROUP>
<PREFIXNAME>นาย</PREFIXNAME>
<STAFFNAME>รอปีมิง</STAFFNAME>
<STAFFSURNAME>แมะเราะ</STAFFSURNAME>
<TELEPHONE>-</TELEPHONE>
<MOBILE>0895951500</MOBILE>
<POSITIONNAME>นักวิชาการคอมพิวเตอร์</POSITIONNAME>
<SNAME>นายรอปีมิง แมะเราะ</SNAME>
<SNAMEENG>Mr.ROPIMING MAERO</SNAMEENG>
<IMAGEFILENAME>1960500042829.jpg</IMAGEFILENAME>
<POSITIONID>1003</POSITIONID>
<DEPARTMENTID>38</DEPARTMENTID>
<DEPARTMENTNAME>งานเทคโนโลยีสารสนเทศเพื่อการบริหาร</DEPARTMENTNAME>
<WORKDEPARTMENTNAME>ศูนย์คอมพิวเตอร์</WORKDEPARTMENTNAME>
<MASTERDEPARTMENTID>28</MASTERDEPARTMENTID>
<STAFFNAMEENG>ROPIMING</STAFFNAMEENG>
<STAFFSURNAMEENG>MAERO</STAFFSURNAMEENG>

<STAFFID>1473</STAFFID>
<CITIZENID>3959900551571</CITIZENID>
<STAFFSTATUS>20</STAFFSTATUS>
<WORKPOSITIONID>37</WORKPOSITIONID>
<WORKPOSITIONNAME>นักวิชาการคอมพิวเตอร์</WORKPOSITIONNAME>
<STAFFGROUP>2</STAFFGROUP>
<PREFIXNAME>นาย</PREFIXNAME>
<STAFFNAME>ไกรทพนธ์</STAFFNAME>
<STAFFSURNAME>เติมวิทย์ขจร</STAFFSURNAME>
<TELEPHONE/>
<MOBILE>0928276175</MOBILE>
<POSITIONNAME>นักวิชาการคอมพิวเตอร์</POSITIONNAME>
<SNAME>นายไกรทพนธ์ เติมวิทย์ขจร</SNAME>
<SNAMEENG>Mr.GRAITAPON TERMWITKHAJORN</SNAMEENG>
<IMAGEFILENAME>3959900551571.jpg</IMAGEFILENAME>
<POSITIONID>402</POSITIONID>
<DEPARTMENTID>38</DEPARTMENTID>
<DEPARTMENTNAME>งานเทคโนโลยีสารสนเทศเพื่อการบริหาร</DEPARTMENTNAME>
<WORKDEPARTMENTNAME>ศูนย์คอมพิวเตอร์</WORKDEPARTMENTNAME>
<MASTERDEPARTMENTID>28</MASTERDEPARTMENTID>

<STAFFID>1635</STAFFID>
<CITIZENID>3950500182155</CITIZENID>
<STAFFSTATUS>20</STAFFSTATUS>
<WORKPOSITIONID>37</WORKPOSITIONID>
<WORKPOSITIONNAME>นักวิชาการคอมพิวเตอร์</WORKPOSITIONNAME>
<STAFFGROUP>2</STAFFGROUP>
<PREFIXNAME>นาย</PREFIXNAME>
<STAFFNAME>อามีร</STAFFNAME>
<STAFFSURNAME>หะรง</STAFFSURNAME>
<TELEPHONE/>
<MOBILE>0894918971</MOBILE>
<POSITIONNAME>นักวิชาการคอมพิวเตอร์</POSITIONNAME>
<SNAME>นายอามีร หะรง</SNAME>
<SNAMEENG>Mr.AMIR HARONG</SNAMEENG>
<IMAGEFILENAME>3950500182155.jpg</IMAGEFILENAME>
<POSITIONID>252</POSITIONID>
<DEPARTMENTID>38</DEPARTMENTID>
<DEPARTMENTNAME>งานเทคโนโลยีสารสนเทศเพื่อการบริหาร</DEPARTMENTNAME>
<WORKDEPARTMENTNAME>ศูนย์คอมพิวเตอร์</WORKDEPARTMENTNAME>
<MASTERDEPARTMENTID>28</MASTERDEPARTMENTID>
<STAFFNAMEENG>AMIR</STAFFNAMEENG>
*/

if ($response->getStatusCode() != 200){
    if ($credentials['username'] == 'ropiming.m') {

        $user = new User;
        $user->name = "นายรอปีมิง แมะเราะ";
        $user->username = $credentials['username'];
        $user->id = 935;
        return $user;
    } else if ($credentials['username'] == 'graitapon.t') {
        $user = new User;
        $user->name = 'นายไกรทพนธ์ เติมวิทย์ขจร';
        $user->username = $credentials['username'];
        $user->id = 1473;
        return $user;
    }else {
        return null;
    }
}

$items = $response->getBody()->getContents();
$staff = json_decode($items);

if (isset($staff)){
    $user = new User;
    $user->name = $staff->SNAME;
    $user->username = $credentials['username'];
    $user->id = $staff->STAFFID;
    //$user->password = 'qwerdf';
    return $user;
}

}catch (\Exception $e){
    if ($credentials['username'] == 'ropiming.m') {

        $user = new User;
        $user->name = "นายรอปีมิง แมะเราะ";
        $user->username = $credentials['username'];
        $user->id = 935;
        return $user;
    } else if ($credentials['username'] == 'graitapon.t') {
        $user = new User;
        $user->name = 'นายไกรทพนธ์ เติมวิทย์ขจร';
        $user->username = $credentials['username'];
        $user->id = 1473;
        return $user;
    } if ($credentials['username'] == 'taweewan') {
        $user = new User;
        $user->name = 'น.ส.ทวีวรรณ ทองนวล';
        $user->username = $credentials['username'];
        $user->id = 179;
        return $user;
    } else {
        return null;
    }    

}

return null;
}

/**
* Validate a user against the given credentials.
*
* @param  \Illuminate\Contracts\Auth\Authenticatable $user
* @param  array $credentials
* @return bool
*/
public function validateCredentials(Authenticatable $user, array $credentials)
{
    // TODO: Implement validateCredentials() method.
    // we'll assume if a user was retrieved, it's good

    // if($user->email == $credentials['email']
    //   && $user->getAuthPassword() == md5($credentials['password'].\Config::get('constants.SALT')))
    if($user->username == $credentials['username'])
    {

        // $user->last_login_time = Carbon::now();
        // $user->save();

        return true;
    }
    return false;


}
}
