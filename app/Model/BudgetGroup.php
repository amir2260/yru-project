<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class BudgetGroup extends Model
{
    protected $primaryKey = "BUDGET_GROUP_ID";
    protected $table = "tb_budget_group";

    public $timestamps = false;

    protected $fillable = [
        'BUDGET_GROUP_NAME'
    ];

}
