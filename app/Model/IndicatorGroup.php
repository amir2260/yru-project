<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class IndicatorGroup extends Model
{
    protected $primaryKey = "INDICATOR_GROUP_ID";
    protected $table = "tb_indicator_group";

    // public $timestamps = false;

    protected $fillable = [
        'INDICATOR_GROUP_NAME'
    ];

}
