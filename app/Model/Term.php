<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Term extends Model
{
    protected $primaryKey = "TERM_ID";
    protected $table = "tb_term";

    public $timestamps = false;

    protected $fillable = [
        'TERM_NAME'
    ];

    public function projects(){
      return $this->hasMany('App\Model\Project', 'TERM_ID', 'TERM_ID');
    }

}
