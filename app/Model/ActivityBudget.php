<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class ActivityBudget extends Model
{
	protected $primaryKey = "ACTIVITY_DETAIL_BUDGET_ID";
	protected $table = "tb_activity_detail_budget";
	
	//     public $timestamps = false;
		
	
	protected $fillable = [
			'ACTIVITY_DETAIL_ID'
			, 'BUDGET_GROUP_ID'			
			, 'BUDGET_AMOUNT'			
			, 'CREATE_BY'
			, 'UPDATE_BY'
	];
	
	
	public function activityDetail(){
		return $this->belongsTo('App\Model\ActivityDetail', 'ACTIVITY_DETAIL_ID', 'ACTIVITY_DETAIL_ID');
	}
	
	public function budgetGroup(){
		return $this->belongsTo('App\Model\BudgetGroup', 'BUDGET_GROUP_ID', 'BUDGET_GROUP_ID');
	}

	public function actions(){
		return $this->hasMany('App\Model\Action', 'ACTIVITY_DETAIL_ID', 'ACTIVITY_DETAIL_ID');
	}
		
	
}
