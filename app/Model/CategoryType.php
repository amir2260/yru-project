<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class CategoryType extends Model
{
    protected $primaryKey = "category_type_id";
    protected $table = "tb_category_type";

    public $timestamps = false;

    protected $fillable = [
        'category_name'
    ];

    public function categories(){
        return $this->hasMany('App\Model\Category', 'category_type_id', 'category_type_id');
    }


}
