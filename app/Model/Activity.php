<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


    class Activity extends Model
{
    protected $primaryKey = "ACTIVITY_ID";
    protected $table = "tb_activity";

//     public $timestamps = false;

    protected $fillable = [
        'ACTIVITY_NAME'
        , 'PROJECT_ID'
        , 'CREATE_BY'
        , 'UPDATE_BY'
    ];


    public function project(){
      return $this->belongsTo('App\Model\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    public function activityDetails(){
      return $this->hasMany('App\Model\ActivityDetail', 'ACTIVITY_ID', 'ACTIVITY_ID');
    }

}
