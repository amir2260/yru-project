<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Indicator extends Model
{
    protected $primaryKey = "INDICATOR_ID";
    protected $table = "tb_indicator";

    // public $timestamps = false;

    protected $fillable = [
		'PROJECT_ID'
		,'INDICATOR_GROUP_ID'
		,'INDICATOR_TYPE_ID'
		,'TARGET'
    ];


    public function indicatorGroup(){
      return $this->belongsTo('App\Model\IndicatorGroup', 'INDICATOR_GROUP_ID', 'INDICATOR_GROUP_ID');
	}
	
	public function indicatorType(){
		return $this->belongsTo('App\Model\IndicatorType', 'INDICATOR_TYPE_ID', 'INDICATOR_TYPE_ID');
	}

	public function getName(){
		return $this->indicatorGroup->INDICATOR_GROUP_NAME . ' มี' . $this->indicatorType->INDICATOR_TYPE_NAME;
	}

	public function getUnitName(){
		return $this->indicatorType->UNIT_NAME;
	}


}
