<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class _Program extends Model
{
    protected $primaryKey = "PROGRAM_ID";
    protected $table = "tb_program";

    public $timestamps = false;

    protected $fillable = [
        'PROGRAM_NAME'
    ];


}
