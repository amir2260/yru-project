<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * เอกสารแนบ
 */
class FileItem extends Model
{
    protected $primaryKey = "FILE_ID";
    protected $table = "tb_file";

    public $timestamps = false;

    protected $fillable = [
        'FILE_MIME'
        , 'FILE_SIZE'
        , 'FILE_DATA'
        , 'CREATE_DATE'
        , 'CREATE_BY'
    ];

}
