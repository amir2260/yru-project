<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class _School extends Model
{
    protected $primaryKey = "SCHOOL_ID";
    protected $table = "tb_school";

    public $timestamps = false;

    protected $fillable = [
        'SCHOOL_NAME'
        ,'SCHOOL_ADDRESS'
        ,'CONTACT_NAME'
        ,'PHONE'
        ,'EMAIL'
    ];


}
