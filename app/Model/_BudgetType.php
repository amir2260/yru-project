<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class _BudgetType extends Model
{
    protected $primaryKey = "BUDGET_TYPE_ID";
    protected $table = "tb_budget_type";

    public $timestamps = false;

    protected $fillable = [
        'BUDGET_TYPE_NAME'
    ];


}
