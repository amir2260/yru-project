<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class _ProgressStatus extends Model
{
    protected $primaryKey = "PROGRESS_STATUS_ID";
    protected $table = "tb_progress_status";

    public $timestamps = false;

    protected $fillable = [
        'PROGRESS_STATUS_NAME'
    ];


}
