<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class BudgetDetail extends Model
{
    protected $primaryKey = "BUDGET_DETAIL_ID";
    protected $table = "tb_budget_detail";

    public $timestamps = false;

    protected $fillable = [
        'BUDGET_DETAIL_NAME'
        , 'BUDGET_TYPE_ID'
    ];

    public function budgetType(){
        return $this->belongsTo('App\Model\BudgetType', 'BUDGET_TYPE_ID', 'BUDGET_TYPE_ID');
    }

}
