<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class _ContractStatus extends Model
{
    protected $primaryKey = "CONTRACT_STATUS_ID";
    protected $table = "tb_contract_status";

    public $timestamps = false;

    protected $fillable = [
        'CONTRACT_STATUS_NAME'
    ];


}
