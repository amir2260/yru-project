<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Action extends Model
{
    protected $primaryKey = "ACTION_ID";
    protected $table = "tb_action";

//     public $timestamps = false;

    protected $fillable = [
        'ACTIVITY_DETAIL_ID'
        , 'ACTION_NAME'
        , 'REF_NO'
        , 'ACTION_DETAIL'
        , 'CREATE_BY'
        , 'UPDATE_BY'
        , 'DEPARTMENT_ID'
    ];

    public function actionBudgets(){
      return $this->hasMany('App\Model\ActionBudget', 'ACTION_ID', 'ACTION_ID');
    }

    public function activityDetail(){
      return $this->belongsTo('App\Model\ActivityDetail', 'ACTIVITY_DETAIL_ID', 'ACTIVITY_DETAIL_ID');
    }

    public function course(){
        return $this->belongsTo('App\Model\Course', 'DEPARTMENT_ID', 'COURSE_ID');
    }

}
