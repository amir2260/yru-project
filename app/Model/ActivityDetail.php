<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class ActivityDetail extends Model
{
    protected $primaryKey = "ACTIVITY_DETAIL_ID";
    protected $table = "tb_activity_detail";

//     public $timestamps = false;


    protected $fillable = [
        'ACTIVITY_ID'
        , 'ACTIVITY_DETAIL_NAME'
    	  , 'TARGET_DATE'
    	  , 'BUDGET_DETAIL_ID'
        , 'CREATE_BY'
        , 'UPDATE_BY'
        , 'COURSE_ID'
    ];


    public function activity(){
      return $this->belongsTo('App\Model\Activity', 'ACTIVITY_ID', 'ACTIVITY_ID');
    }

    public function course(){
      return $this->belongsTo('App\Model\Course', 'COURSE_ID', 'COURSE_ID');
    }

    public function activityBudgets(){
      return $this->hasMany('App\Model\ActivityBudget', 'ACTIVITY_DETAIL_ID', 'ACTIVITY_DETAIL_ID');
    }

    public function actions(){
      return $this->hasMany('App\Model\Action', 'ACTIVITY_DETAIL_ID', 'ACTIVITY_DETAIL_ID');
    }

    public function budgetDetail(){
    	return $this->belongsTo('App\Model\BudgetDetail', 'BUDGET_DETAIL_ID', 'BUDGET_DETAIL_ID');
    }


}
