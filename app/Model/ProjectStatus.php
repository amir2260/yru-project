<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class ProjectStatus extends Model
{
    protected $primaryKey = "PROJECT_STATUS_ID";
    protected $table = "tb_project_status";

    public $timestamps = false;

    protected $fillable = [
        'PROJECT_STATUS_NAME'
    ];


}
