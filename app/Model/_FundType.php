<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class _FundType extends Model
{
    protected $primaryKey = "FUND_TYPE_ID";
    protected $table = "tb_fund_type";

    public $timestamps = false;

    protected $fillable = [
        'FUND_TYPE_NAME'
    ];


}
