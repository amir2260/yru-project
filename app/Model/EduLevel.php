<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class EduLevel extends Model
{
    protected $primaryKey = "EDU_LEVEL_ID";
    protected $table = "tb_edu_level";

    public $timestamps = false;

    protected $fillable = [
        'EDU_LEVEL_NAME'
    ];

    public function staff(){
      return $this->belongsTo('App\Model\Staff', 'STAFF_ID', 'STAFF_ID');
    }

}
