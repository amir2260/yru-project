<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class IndicatorType extends Model
{
    protected $primaryKey = "INDICATOR_TYPE_ID";
    protected $table = "tb_indicator_type";

    // public $timestamps = false;

    protected $fillable = [
        'INDICATOR_TYPE_NAME'
        , 'UNIT_NAME'
    ];

}
