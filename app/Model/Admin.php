<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * สารบัญกลาง
 */
class Admin extends Model
{
    protected $primaryKey = "ADMIN_ID";
    protected $table = "tb_admin";

    public $timestamps = false;

    protected $fillable = [
        'STAFF_ID'
        ,'IS_ADMIN'
        , 'CREATE_DATE', 'CREATE_BY'
    ];

    public function staff(){
      return $this->belongsTo('App\Model\Staff', 'STAFF_ID', 'STAFF_ID');
    }

}
