<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class ProjectCategory extends Model
{
    protected $primaryKey = "PROJECT_CATEGORY_ID";
    protected $table = "tb_project_category";

    public $timestamps = false;

    protected $fillable = [
        'PROJECT_ID'
        ,'CATEGORY_ID'
        ,'IS_SELECTED'
    ];

    public function project(){
        return $this->belongsTo('App\Model\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    public function category(){
        return $this->belongsTo('App\Model\Category', 'CATEGORY_ID', 'CATEGORY_ID');
    }

}
