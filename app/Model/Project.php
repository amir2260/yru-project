<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Project extends Model
{
    protected $primaryKey = "PROJECT_ID";
    protected $table = "tb_project";

    public $timestamps = false;

    protected $fillable = [
        'PROJECT_CODE'
        , 'PROJECT_NAME'
        , 'DEPARTMENT_ID'
        , 'COST'
        , 'REASON'
        , 'RESPECT'
        , 'TERM_ID'
        , 'PROJECT_STATUS_ID'
        , 'IS_TEMPLATE'

        , 'CREATE_DATE'
        , 'CREATE_BY'
        , 'UPDATE_DATE'
        , 'UPDATE_BY'

    ];

    public function getByStatus($userId, $statusId){
        $projects = Project::where('IS_TEMPLATE', 0)->where('PROJECT_STATUS_ID', $statusId);
        return $projects;
    }

    public function activities(){
    	return $this->hasMany('App\Model\Activity', 'PROJECT_ID', 'PROJECT_ID');
    }

    public function projectOwners(){
        return $this->hasMany('App\Model\ProjectOwner', 'PROJECT_ID', 'PROJECT_ID');
    }

    public function projectStatus(){
        return $this->belongsTo('App\Model\ProjectStatus', 'PROJECT_STATUS_ID', 'PROJECT_STATUS_ID');
    }

    public function term(){
        return $this->belongsTo('App\Model\Term', 'TERM_ID', 'TERM_ID');
    }

    public function department(){
        return $this->belongsTo('App\Model\Department', 'DEPARTMENT_ID', 'DEPARTMENT_ID');
    }

    public function categories(){
        return $this->hasMany('App\Model\ProjectCategory', 'PROJECT_ID', 'PROJECT_ID');
    }

    public function indicators(){
        return $this->hasMany('App\Model\Indicator', 'PROJECT_ID', 'PROJECT_ID');
    }

    //////

    // งบประมาณที่ได้ขอ/อนุมัติ
    public function getRequestAmount(){
        $amount = $this->activities->sum(function($ac){
            return $ac->activityDetails->sum(function($de){
                return $de->activityBudgets->sum('BUDGET_AMOUNT');
            });
        });
        return $amount;
    }

    // งบประมาณที่ใช้ไป
    public function getUsageAmount(){
        $amount = $this->activities->sum(function($ac){
            return $ac->activityDetails->sum(function($de){
                return $de->activityBudgets->sum(function($bud){
                    return $bud->actions->sum(function ($action) {
                        return $action->actionBudgets->sum('AMOUNT');
                    });
                });
            });
        });

        return $amount;
    }

}
