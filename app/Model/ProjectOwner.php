<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class ProjectOwner extends Model
{
    protected $primaryKey = "PROJECT_OWNER_ID";
    protected $table = "tb_project_owner";

    public $timestamps = false;

    protected $fillable = [
        'PROJECT_OWNER_NAME'
        ,'PROJECT_ID'
        ,'STAFF_ID'
        ,'IS_LEADER'
    ];

    public function project(){
        return $this->belongsTo('App\Model\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    public function staff(){
        return $this->belongsTo('App\Model\Staff', 'STAFF_ID', 'STAFF_ID');
    }

}
