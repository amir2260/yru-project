<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Course extends Model
{
    use SoftDeletes;
    
    protected $primaryKey = "COURSE_ID";
    protected $table = "tb_course";

    public $timestamps = false;

    protected $fillable = [
        'COURSE_ID'
        , 'DEPARTMENT_ID'
        , 'COURSE_NAME'
    ];

    public function staffsInCourse(){
    	return $this->hasMany('App\Model\StaffCourse', 'COURSE_ID', 'COURSE_ID');
    }

	public function department(){
    	return $this->belongsTo('App\Model\Department', 'DEPARTMENT_ID', 'DEPARTMENT_ID');
    }

    public function gets(){
        return Course::all();
    }


}
