<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class ApplicationLog extends Model
{
    protected $primaryKey = "LOG_ID";
    protected $table = "tb_log";

    public $timestamps = false;

    protected $fillable = [
        'LOG_DATE'
        , 'DATA_ID'
        , 'LOG_BY'
        , 'CONTROLLER_NAME'
        , 'ACTION_NAME'
        , 'LOG_NAME'
        , 'LOG_DESC'
        , 'LOG_TYPE'
        , 'IP_ADDRESS'
    ];

    // public function staff(){
    //   $this->belongsTo('App\Model\Staff',  'STAFF_ID', 'LOG_BY');
    // }

    public function staff(){
      return $this->belongsTo('App\Model\Staff', 'LOG_BY',  'STAFF_ID');
    }

  

    // public function department(){
    //   return $this->belongsTo('App\Model\Department', 'DEPARTMENT_ID');
    // }

}
