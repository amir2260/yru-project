<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class _StudentType extends Model
{
    protected $primaryKey = "STUDENT_TYPE_ID";
    protected $table = "tb_student_type";

    public $timestamps = false;

    protected $fillable = [
        'STUDENT_TYPE_NAME'
    ];


}
