<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;



class Budget extends Model
{
    protected $primaryKey = "BUDGET_ID";
    protected $table = "tb_budget";

    public $timestamps = false;

    protected $fillable = [
        'CONTRACT_ID'
        ,'BUDGET_TYPE_ID'
        ,'BUDGET_DESC'
        ,'AMOUNT'
        ,'BUDGET_DATE'
        ,'REF_NO'
        ,'CREATE_DATE'
        ,'CREATE_BY'
    ];

    public function contract(){
        return $this->belongsTo('App\Model\Contract', 'CONTRACT_ID', 'CONTRACT_ID');
    }

    public function budgetType(){
        return $this->belongsTo('App\Model\BudgetType', 'BUDGET_TYPE_ID', 'BUDGET_TYPE_ID');
    }

}
