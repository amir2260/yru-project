<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


// `PROGRESS_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
//   `REPORT_DATE` datetime DEFAULT NULL,
//   `PROGRESS_STATUS_ID` int(11) DEFAULT NULL,
//   `PROGRESS_DESC` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
//   `GPA` decimal(15,2) DEFAULT NULL,
//   `CREATE_DATE` datetime DEFAULT NULL,
//   `CREATE_BY` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
//
class _Progress extends Model
{
    protected $primaryKey = "PROGRESS_ID";
    protected $table = "tb_progress";

    public $timestamps = false;

    protected $fillable = [
        'CONTRACT_ID'
        ,'PROGRESS_STATUS_ID'
        ,'PROGRESS_DESC'
        ,'GPA'
        ,'PROGRESS_DATE'
        ,'CREATE_DATE'
        ,'CREATE_BY'
    ];

    public function contract(){
        return $this->belongsTo('App\Model\Contract', 'CONTRACT_ID', 'CONTRACT_ID');
    }

    public function progressStatus(){
        return $this->belongsTo('App\Model\ProgressStatus', 'PROGRESS_STATUS_ID', 'PROGRESS_STATUS_ID');
    }

}
