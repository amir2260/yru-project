<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * สารบัญกลาง
 */
class _Contract extends Model
{
    protected $primaryKey = "CONTRACT_ID";
    protected $table = "tb_contract";

    public $timestamps = false;

    protected $fillable = [
        'FUND_ID'
        ,'STUDENT_TYPE_ID'
        ,'CONTRACT_DATE'
        ,'CONTRACT_NO'
        ,'CONTRACT_START_DATE'
        ,'CONTRACT_FINISH_DATE'
        ,'CONTRACT_STATUS_ID'
        ,'ADVISOR'

        ,'CITIZEN_ID'
        ,'STUDENT_TYPE_NAME'
        ,'ADDRESS'
        ,'PHONE'
        ,'EMAIL'

        ,'UPDATE_DATE','UPDATE_BY'
        , 'CREATE_DATE', 'CREATE_BY'
    ];

    // public function student(){
    //   return $this->belongsTo('App\Model\Student', 'STUDENT_ID', 'STUDENT_ID');
    // }

    public function fund(){
      return $this->belongsTo('App\Model\Fund', 'FUND_ID', 'FUND_ID');
    }

    public function budgets(){
      return $this->hasMany('App\Model\Budget', 'CONTRACT_ID', 'CONTRACT_ID');
    }

    public function progresses(){
      return $this->hasMany('App\Model\Progress', 'CONTRACT_ID', 'CONTRACT_ID');
    }

    public function renews(){
      return $this->hasMany('App\Model\Renew', 'CONTRACT_ID', 'CONTRACT_ID');
    }

    public function studentType(){
      return $this->belongsTo('App\Model\StudentType', 'STUDENT_TYPE_ID', 'STUDENT_TYPE_ID');
    }

    public function contractStatus(){
      return $this->belongsTo('App\Model\ContractStatus', 'CONTRACT_STATUS_ID', 'CONTRACT_STATUS_ID');
    }

}
