<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class ActionBudget extends Model
{
    protected $primaryKey = "ACTION_BUDGET_ID";
    protected $table = "tb_action_budget";

//     public $timestamps = false;

    protected $fillable = [
        'ACTION_ID'
        , 'BUDGET_GROUP_ID'
        , 'AMOUNT'
    ];

    //
    // public function project(){
    //   return $this->belongsTo('App\Model\Project', 'PROJECT_ID', 'PROJECT_ID');
    // }
    //
    // public function activityDetails(){
    //   return $this->hasMany('App\Model\ActivityDetail', 'ACTIVITY_ID', 'ACTIVITY_ID');
    // }

/*
CREATE TABLE `` (
  `` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `` int(11) DEFAULT NULL,
  `` int(11) DEFAULT NULL,
  `` decimal(15,3) DEFAULT NULL,

  `CREATE_BY` int(11) DEFAULT NULL,
  `UPDATE_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ACTION_BUDGET_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 */
}
