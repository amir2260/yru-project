<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class _Fund extends Model
{
    protected $primaryKey = "FUND_ID";
    protected $table = "tb_fund";

    public $timestamps = false;

    protected $fillable = [
        'FUND_NAME'
        , 'FUND_TYPE_ID'
        , 'PROJECT_ID'
        , 'EDU_TERM_ID'
        , 'PROGRAM_ID'
        , 'SCHOOL_ID'
        , 'EDU_LEVEL_ID'
        , 'FUND_DESC'
        , 'REQUEST_AMOUNT'
        , 'BUDGET'
        , 'FUND_STATUS_ID'
        , 'CREATE_DATE', 'CREATE_BY'
        , 'UPDATE_DATE', 'UPDATE_BY'
    ];

    public function project(){
        return $this->belongsTo('App\Model\Project', 'PROJECT_ID', 'PROJECT_ID');
    }

    public function eduTerm(){
        return $this->belongsTo('App\Model\EduTerm', 'EDU_TERM_ID', 'EDU_TERM_ID');
    }

    public function fundType(){
        return $this->belongsTo('App\Model\FundType', 'FUND_TYPE_ID', 'FUND_TYPE_ID');
    }

    public function fundStatus(){
        return $this->belongsTo('App\Model\FundStatus', 'FUND_STATUS_ID', 'FUND_STATUS_ID');
    }

    public function program(){
        return $this->belongsTo('App\Model\Program', 'PROGRAM_ID', 'PROGRAM_ID');
    }

    public function school(){
        return $this->belongsTo('App\Model\School', 'SCHOOL_ID', 'SCHOOL_ID');
    }

    public function eduLevel(){
        return $this->belongsTo('App\Model\EduLevel', 'EDU_LEVEL_ID', 'EDU_LEVEL_ID');
    }


    // public function projec(){
    //     return $this->hasMany('App\Model\Fund', 'PROJECT_ID', 'PROJECT_ID');
    // }


}
