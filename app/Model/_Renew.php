<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;



class _Renew extends Model
{
    protected $primaryKey = "RENEW_ID";
    protected $table = "tb_renew";

    public $timestamps = false;

    protected $fillable = [
        'CONTRACT_ID'
        ,'RENEW_INDEX'
        ,'REQUEST_DATE'
        ,'START_DATE'
        ,'END_DATE'
        ,'REASON'
        ,'ACCEPTOR'
        ,'ACCEPTOR_DATE'
        ,'APPROVER'
        ,'APPROVER_DATE'
        ,'CREATE_DATE'
        ,'CREATE_BY'
        ,'UPDATE_DATE'
        ,'UPDATE_BY'
    ];

    public function contract(){
        return $this->belongsTo('App\Model\Contract', 'CONTRACT_ID', 'CONTRACT_ID');
    }


}
