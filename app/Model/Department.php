<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * หน่วยงานต้นสังกัด
 */
class Department extends Model
{
    protected $primaryKey = "DEPARTMENT_ID";
    protected $table = "tb_department";

    public $timestamps = false;

    protected $fillable = [
        'MASTER_DEPARTMENT_ID'
        , 'DEPARTMENT_NAME'
        , 'DEPARTMENT_DESC'
        , 'EX_POSITION_NAME'
        , 'USER_ID' // TODO : must change to STAFF_ID
        , 'CREATE_DATE', 'CREATE_BY'
        , 'UPDATE_DATE', 'UPDATE_BY'
    ];

    public function exStaff(){
      return $this->belongsTo('App\Model\Staff', 'USER_ID', 'STAFF_ID');
    }

    public function masterDepartment(){
      return $this->belongsTo('App\Model\Department', 'MASTER_DEPARTMENT_ID', 'DEPARTMENT_ID');
    }

    public function departments(){
      return $this->hasMany('App\Model\Department', 'MASTER_DEPARTMENT_ID', 'DEPARTMENT_ID');
    }

    public function staffs(){
      return $this->hasMany('App\Model\Staff', 'DEPARTMENT_ID');
    }

    /*
    public function getStaffDepartments($userId){
        $staff = Staff::where('STAFF_ID', $userId)->first();

//         $list = [];
//         $list[] = $staff->department;
//         if (isset($staff->department->masterDepartment)){
//             $list[] = $staff->department->masterDepartment;

//             if (isset($staff->department->masterDepartment->masterDepartment)){
//                 $list[] = $staff->department->masterDepartment->masterDepartment;
//             }
//         }

        $list = collect();
        if ($staff->department->DEPARTMENT_ID > 0) {
	        $list->push($staff->department);
	        if (isset($staff->department->masterDepartment)){
	        	if ($staff->department->masterDepartment->DEPARTMENT_ID > 0) {
		        	$list->push($staff->department->masterDepartment);

		        	if (isset($staff->department->masterDepartment->masterDepartment)){
		        		if ($staff->department->masterDepartment->masterDepartment->DEPARTMENT_ID > 0) {
		        			$list->push($staff->department->masterDepartment->masterDepartment);
		        		}
		        	}
	        	}
	        }
        }
        return $list;
    }
    */

}
