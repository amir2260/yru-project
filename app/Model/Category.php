<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    protected $primaryKey = "category_id";
    protected $table = "tb_category";

    public $timestamps = false;

    protected $fillable = [
        'category_name'
        ,'category_type_name'
    ];

    public function projectCategories(){
    	return $this->hasMany('App\Model\ProjectCategory', 'category_id', 'category_id');
    }
    // public function getByType($categoryTypeId){
    //     return Category::where('CATEGORY_TYPE_ID', $categoryTypeId);
    // }


}
