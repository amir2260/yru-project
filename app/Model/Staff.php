<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * บุคลากร
 */
class Staff extends Model
{
    protected $primaryKey = "ITEM_ID";
    protected $table = "tb_staff";

    public $timestamps = false;

    protected $fillable = [
        'STAFF_ID'
        ,'EMAIL'
        , 'CITIZEN_ID'
        , 'FIRST_NAME'
        , 'LAST_NAME'
        , 'FULL_NAME'
        , 'DEPARTMENT_NAME'

        , 'POSITION_NAME'
        , 'LOGIN_NAME'
        , 'DEPARTMENT_ID'

        , 'POSITION_ID'
        , 'PHONE'
        , 'WORKPOSITION_ID'
        , 'CREATE_DATE','CREATE_BY'
    ];

    public function gets(){
      	return Staff::all();
    }

    

    public function department(){
      	return $this->belongsTo('App\Model\Department', 'DEPARTMENT_ID');
    }

    public function exDepartments(){
      	return $this->hasMany('App\Model\Department', 'USER_ID', 'STAFF_ID');
    }

    public function managers(){
      	return $this->hasMany('App\Model\Manager', 'STAFF_ID', 'STAFF_ID');
    }

    public function documents(){
     	return $this->hasMany('App\Model\Document', 'STAFF_ID', 'STAFF_ID');
    }

    public function admin(){
      	return $this->belongsTo('App\Model\Admin', 'STAFF_ID', 'STAFF_ID');
    }

    public function staffInCourses(){
		return $this->hasMany('App\Model\StaffCourse', 'STAFF_ID', 'STAFF_ID');
    }

    public function getMasterDepartments(){
    	//$staff = Staff::where('STAFF_ID', $userId)->first();

    	//         $list = [];
    	//         $list[] = $staff->department;
    	//         if (isset($staff->department->masterDepartment)){
    	//             $list[] = $staff->department->masterDepartment;

    	//             if (isset($staff->department->masterDepartment->masterDepartment)){
    	//                 $list[] = $staff->department->masterDepartment->masterDepartment;
    	//             }
    	//         }

    	$list = collect();
    	if ($this->department->DEPARTMENT_ID > 0) {
    		$list->push($this->department);
    		if (isset($this->department->masterDepartment)){
    			if ($this->department->masterDepartment->DEPARTMENT_ID > 0) {
    				$list->push($this->department->masterDepartment);

    				if (isset($this->department->masterDepartment->masterDepartment)){
    					if ($this->department->masterDepartment->masterDepartment->DEPARTMENT_ID > 0) {
    						$list->push($this->department->masterDepartment->masterDepartment);
    					}
    				}
    			}
    		}
    	}
    	return $list;
	}
	
	public function canAccessActivity($activityDetailId){
		// 

		$course = Course::whereHas('staffsInCourse', function($sc){
			$sc->where('STAFF_ID', $this->STAFF_ID);
		})->pluck('COURSE_ID');

		$result = ActivityDetail::where('ACTIVITY_DETAIL_ID', $activityDetailId)
			->whereIn('COURSE_ID', $course);

		
		return ($result->count() > 0);
	  }
	  
	  public function getCourses(){
		$course = Course::whereHas('staffsInCourse', function($sc){
			$sc->where('STAFF_ID', $this->STAFF_ID);
		});

		return $course;
	  }

}
