<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class StaffCourse extends Model
{
    protected $primaryKey = "STAFF_COURSE_ID";
    protected $table = "tb_staff_course";

    public $timestamps = false;

    protected $fillable = [
        'COURSE_ID'
        , 'STAFF_ID'
    ];

    public function Course(){
    	return $this->belongsTo('App\Model\Course', 'COURSE_ID', 'COURSE_ID');
    }

	public function Staff(){
    	return $this->belongsTo('App\Model\Staff', 'STAFF_ID', 'STAFF_ID');
    }
        		

}
