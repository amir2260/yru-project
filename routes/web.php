<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');
// Route::get('/index', 'StartController@showname');
// Route::get('/index/{id}', 'StartController@showName');
// Route::get('/b', 'StartController@showLastName');
//
// Route::post('/save', 'StartController@save');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/home/soon', 'HomeController@soon');

// Member
// Route::get('/member', 'MemberController@index');
// Route::post('/member', 'MemberController@index_post');
//
// Route::get('/member/register', 'MemberController@register');
// Route::post('/member/register', 'MemberController@register_do');
// Route::get('/member/memberedit/{id}', 'MemberController@memberedit');
// Route::post('/member/memberedit', 'MemberController@register_do');
// Route::get('/member/memberinfo/{id}', 'MemberController@memberinfo');
// Route::get('/member/memberdelete/{id}', 'MemberController@memberdelete');
// Route::get('/member/memberjson', 'MemberController@memberjson');

Route::get('/category/category01', 'CategoryController@category01');
Route::get('/category/category02', 'CategoryController@category02');
Route::get('/category/category03', 'CategoryController@category03');
Route::get('/category/category04', 'CategoryController@category04');
Route::get('/category/category05', 'CategoryController@category05');
Route::get('/category/category06', 'CategoryController@category06');
Route::get('/category/category07', 'CategoryController@category07');
Route::get('/category/category08', 'CategoryController@category08');
Route::get('/category/category09', 'CategoryController@category09');
Route::get('/category/category10', 'CategoryController@category10');

Route::get('/term', 'TermController@index');

Route::get('/budget', 'BudgetController@index');
Route::get('/budget/budgettype', 'BudgetController@budgettype');


Route::get('/project', 'ProjectController@index');
Route::post('/project', 'ProjectController@index_post');

Route::get('/project/pending', 'ProjectController@pending');
Route::post('/project/pending', 'ProjectController@index_post');

Route::get('/project/approvelist', 'ProjectController@approvelist');
Route::post('/project/approvelist', 'ProjectController@approvelist');

Route::get('/project/rejectlist', 'ProjectController@rejectlist');
Route::post('/project/rejectlist', 'ProjectController@rejectlist');

Route::get('/project/returnlist', 'ProjectController@returnlist');
Route::post('/project/returnlist', 'ProjectController@returnlist');

Route::get('/project/projectlist', 'ProjectController@projectlist');
Route::post('/project/projectlist', 'ProjectController@projectlist');


Route::get('/project/info/{id}', 'ProjectController@info');
Route::get('/project/projectactivity/{id}', 'ProjectController@projectActivity');
Route::get('/project/delete/{id}', 'ProjectController@delete');
Route::get('/project/create/', 'ProjectController@create');
Route::post('/project/create/', 'ProjectController@save');
Route::get('/project/edit/{id}', 'ProjectController@edit');
Route::post('/project/edit/{id}', 'ProjectController@save');

Route::get('/project/selecttemplate', 'ProjectController@selectTemplate');
Route::post('/project/selecttemplate', 'ProjectController@selectTemplate_post');
Route::get('/project/selectinfo/{id}', 'ProjectController@templateInfo');
Route::get('/project/createfromtemplate/{id}', 'ProjectController@createFromTemplate');

Route::get('/project/addactivity/{id}', 'ProjectController@addActivity');
Route::post('/project/addactivity/{id}', 'ProjectController@saveActivity');
Route::get('/project/editactivity/{id}', 'ProjectController@editActivity');
Route::post('/project/editactivity/{id}', 'ProjectController@saveActivity');

Route::get('/project/addaction/{id}', 'ProjectController@addAction');
Route::post('/project/addaction/{id}', 'ProjectController@saveAction');

Route::get('/project/editaction/{id}', 'ProjectController@editAction');
Route::post('/project/editaction/{id}', 'ProjectController@saveAction');
Route::get('/project/deleteaction/{id}', 'ProjectController@deleteAction');


Route::get('/project/addactivitydetail/{id}', 'ProjectController@addActivityDetail');
Route::post('/project/addactivitydetail/{id}', 'ProjectController@saveActivityDetail');
Route::get('/project/editactivitydetail/{id}', 'ProjectController@editActivityDetail');
Route::post('/project/editactivitydetail/{id}', 'ProjectController@saveActivityDetail');

Route::get('/project/addindicator/{id}', 'ProjectController@addIndicator');
Route::post('/project/addindicator/{id}', 'ProjectController@saveIndicator');
Route::get('/project/editindicator/{id}', 'ProjectController@editIndicator');
Route::post('/project/editindicator/{id}', 'ProjectController@saveIndicator');

Route::get('/project/send/{id}', 'ProjectController@send');
Route::get('/project/approve/{id}', 'ProjectController@approve');
Route::get('/project/reject/{id}', 'ProjectController@reject');
Route::get('/project/return/{id}', 'ProjectController@returnProject');

Route::get('/course', 'CourseController@index');
Route::post('/course', 'CourseController@index_post');
Route::get('/course/info/{id}', 'CourseController@info');
Route::post('/course/info/{id}', 'CourseController@addStaff');
Route::get('/course/delete/{id}', 'CourseController@delete');
Route::get('/course/create/', 'CourseController@create');
Route::post('/course/create/', 'CourseController@save');
Route::get('/course/edit/{id}', 'CourseController@edit');
Route::post('/course/edit/{id}', 'CourseController@save');

Route::get('/indicator/group', 'IndicatorController@indicator_group');
Route::get('/indicator/groupadd', 'IndicatorController@add_group');
Route::get('/indicator/groupedit/{id}', 'IndicatorController@edit_group');
Route::post('/indicator/groupadd', 'IndicatorController@save_group');
Route::post('/indicator/groupedit/{id}', 'IndicatorController@save_group');


Route::get('/indicator/type', 'IndicatorController@indicator_type');
Route::get('/indicator/typeadd', 'IndicatorController@add_type');
Route::get('/indicator/typeedit/{id}', 'IndicatorController@edit_type');
Route::post('/indicator/typeadd', 'IndicatorController@save_type');
Route::post('/indicator/typeedit/{id}', 'IndicatorController@save_type');


///////////////
Route::get('/projectowner', 'ProjectOwnerController@index');
Route::post('/projectowner', 'ProjectOwnerController@index_post');
Route::get('/projectowner/info/{id}', 'ProjectOwnerController@info');
Route::get('/projectowner/delete/{id}', 'ProjectOwnerController@delete');

Route::get('/fund', 'FundController@index');
Route::post('/fund', 'FundController@index_post');
Route::get('/fund/info/{id}', 'FundController@info');
Route::get('/fund/delete/{id}', 'FundController@delete');
Route::get('/fund/deactive/{id}', 'FundController@deactive');
Route::get('/fund/create/', 'FundController@create');
Route::post('/fund/create/', 'FundController@save');
Route::get('/fund/edit/{id}', 'FundController@edit');
Route::post('/fund/edit/{id}', 'FundController@save');

Route::get('/school', 'SchoolController@index');
Route::post('/school', 'SchoolController@index_post');
Route::get('/school/info/{id}', 'SchoolController@info');
Route::get('/school/delete/{id}', 'SchoolController@delete');
Route::get('/school/create/', 'SchoolController@create');
Route::post('/school/create/', 'SchoolController@save');
Route::get('/school/edit/{id}', 'SchoolController@edit');
Route::post('/school/edit/{id}', 'SchoolController@save');


Route::get('/contract', 'ContractController@index');
Route::post('/contract', 'ContractController@index_post');
Route::get('/contract/all', 'ContractController@all');
Route::post('/contract/all', 'ContractController@all_post');
Route::get('/contract/info/{id}', 'ContractController@info');
Route::get('/contract/delete/{id}', 'ContractController@delete');
Route::get('/contract/create/', 'ContractController@create');
Route::post('/contract/create/', 'ContractController@save');
Route::get('/contract/edit/{id}', 'ContractController@edit');
Route::post('/contract/edit/{id}', 'ContractController@save');

Route::get('/contract/budget/{id}', 'ContractController@budget');
Route::post('/contract/budget/{id}', 'ContractController@budget_save');

Route::get('/contract/progress/{id}', 'ContractController@progress');
Route::post('/contract/progress/{id}', 'ContractController@progress_save');

Route::get('/contract/renew/{id}', 'ContractController@renew');
// Route::post('/contract/renew/{id}', 'ContractController@renew_save');

Route::get('/contract/renew_add/{id}', 'ContractController@renew_add');
Route::post('/contract/renew_add/{id}', 'ContractController@renew_save');
Route::get('/contract/renew_edit/{id}', 'ContractController@renew_edit');
Route::post('/contract/renew_edit/{id}', 'ContractController@renew_save');
Route::get('/contract/renew_delete/{id}', 'ContractController@renew_delete');


Route::get('/report/budget_month', 'ReportController@budget_month');
Route::post('/report/budget_month', 'ReportController@budget_month');

Route::get('/report/budget', 'ReportController@budget');
Route::post('/report/budget', 'ReportController@budget');
Route::get('/report/budget_contract/{id}', 'ReportController@budget_contract');


/**
 * Manager
 */
Route::get('/manager', 'ManagerController@index');
Route::post('/manager', 'ManagerController@index_post');
Route::get('/manager/info/{id}', 'ManagerController@info');
Route::get('/manager/delete/{id}', 'ManagerController@delete');
Route::get('/manager/create/', 'ManagerController@create');
Route::post('/manager/create/', 'ManagerController@save');


/**
 * holiday
 */
Route::get('/holiday', 'HolidayController@index');
Route::post('/holiday', 'HolidayController@index_post');
Route::get('/holiday/info/{id}', 'HolidayController@info');
Route::get('/holiday/delete/{id}', 'HolidayController@delete');
Route::get('/holiday/create/', 'HolidayController@create');
Route::post('/holiday/create/', 'HolidayController@save');
Route::get('/holiday/edit/{id}', 'HolidayController@edit');
Route::post('/holiday/edit/{id}', 'HolidayController@save');


Route::get('/admin', 'AdminController@index');
Route::post('/admin', 'AdminController@index_post');
Route::get('/admin/info/{id}', 'AdminController@info');
Route::get('/admin/delete/{id}', 'AdminController@delete');
Route::get('/admin/create/', 'AdminController@create');
Route::post('/admin/create/', 'AdminController@save');

Route::get('/location', 'LocationController@index');
Route::post('/location', 'LocationController@index_post');
Route::get('/location/info/{id}', 'LocationController@info');
Route::get('/location/delete/{id}', 'LocationController@delete');
Route::get('/location/create/', 'LocationController@create');
Route::post('/location/create/', 'LocationController@save');
Route::get('/location/edit/{id}', 'LocationController@edit');
Route::post('/location/edit/{id}', 'LocationController@save');

Route::get('/code', 'CodeController@index');
Route::post('/code', 'CodeController@index_post');
Route::get('/code/pending', 'CodeController@pending');
Route::get('/code/reservelist', 'CodeController@reservelist');
Route::get('/code/reserve', 'CodeController@reserve');
Route::get('/code/getcode/{id}', 'CodeController@getcode');
Route::get('/code/addcode/{id}/{docid}', 'CodeController@addcode');
Route::get('/code/codeout', 'CodeController@codeout');
Route::post('/code/codeout', 'CodeController@codeout_post');
Route::get('/code/codein', 'CodeController@codein');
Route::post('/code/codein', 'CodeController@codein_post');

/**
 * Staff
 */
 Route::get('/staff', 'StaffController@index');
 Route::post('/staff', 'StaffController@index_post');
 Route::get('/staff/info/{id}', 'StaffController@info');
 Route::get('/staff/edit/{id}', 'StaffController@edit');
 Route::post('/staff/edit/{id}', 'StaffController@save');

/**
 * Appoint
 */
 Route::get('/appoint', 'AppointController@index');
 Route::post('/appoint', 'AppointController@index_post');
 Route::get('/appoint/info/{id}', 'AppointController@info');
 Route::get('/appoint/history/{id}', 'AppointController@history');
 Route::get('/appoint/create', 'AppointController@create');
 Route::post('/appoint/create/', 'AppointController@save');
 Route::get('/appoint/edit/{id}', 'AppointController@edit');
 Route::post('/appoint/edit/{id}', 'AppointController@save');
 Route::get('/appoint/cancel/{id}', 'AppointController@cancel');
 Route::get('/appoint/remove_attender/{id}', 'AppointController@remove_attender');
 Route::post('/appoint/add_attender/', 'AppointController@add_attender');
 Route::post('/appoint/add_contract/', 'AppointController@add_contract');
 Route::post('/appoint/copy_appoint/', 'AppointController@copy_appoint');

 Route::get('/appoint/accept/{id}', 'AppointController@accept');
 Route::get('/appoint/reject/{id}', 'AppointController@reject');
 Route::get('/appoint/sendmail/{id}', 'AppointController@sendmail');

/**
 * Path  เส้นทางเอกสาร
 */
Route::get('/path/publicpath', 'PathController@publicpath');
Route::post('/path/publicpath', 'PathController@publicpath_post');
Route::get('/path/publicdetail/{id}', 'PathController@publicdetail');

Route::get('/path/managerpath', 'PathController@managerpath');
Route::post('/path/managerpath', 'PathController@managerpath_post');

Route::get('/path/personalpath', 'PathController@personalpath');
Route::post('/path/personalpath', 'PathController@personalpath_post');

Route::get('/path/publictodepartment/{id}', 'PathController@publictodepartment');
Route::get('/path/publictopersonal/{id}', 'PathController@publictopersonal');

Route::get('/path/pathdetail/{id}', 'PathController@pathdetail');
Route::get('/path/remove/{id}', 'PathController@remove');
Route::get('/path/edit/{id}', 'PathController@edit');

Route::get('/path/rename/{id}', 'PathController@rename');
Route::post('/path/rename/{id}', 'PathController@rename_post');

Route::get('/path/copytodepartment/{id}', 'PathController@copytodepartment');
Route::get('/path/copytopersonal/{id}', 'PathController@copytopersonal');

Route::get('/path/removeitem/{id}', 'PathController@removeItem');
Route::get('/path/edititem/{id}', 'PathController@editItem');
Route::post('/path/edititem/{id}', 'PathController@updateItem');

Route::post('/path/addstaff/', 'PathController@addStaff');
Route::post('/path/adddepartment/', 'PathController@addDepartment');

/**
 * Contract
 */
 Route::get('/contract', 'ContractController@index');
 Route::post('/contract', 'ContractController@index_post');
 Route::get('/contract/info/{id}', 'ContractController@info');

 Route::get('/contract/create/', 'ContractController@create');
 Route::post('/contract/create/', 'ContractController@save');
 Route::get('/contract/edit/{id}', 'ContractController@edit');
 Route::post('/contract/edit/{id}', 'ContractController@save');
 Route::get('/contract/delete/{id}', 'ContractController@delete');

 Route::post('/contract/addstaff/', 'ContractController@addStaff');

/*
* Box ตู้เก็บเอกสาร
*/
Route::get('/box', 'BoxController@index');
Route::post('/box', 'BoxController@index_post');
Route::get('/box/info/{id}', 'BoxController@info');

Route::get('/box/create/', 'BoxController@create');
Route::post('/box/create/', 'BoxController@save');
Route::get('/box/edit/{id}', 'BoxController@edit');
Route::post('/box/edit/{id}', 'BoxController@save');
Route::get('/box/delete/{id}', 'BoxController@delete');

Route::post('/box/addstaff/', 'BoxController@addStaff');

Route::get('/box/manager', 'BoxController@manager');
Route::post('/box/manager', 'BoxController@manager_post');

Route::post('/box/managercreate/', 'BoxController@managersave');
Route::get('/box/managerinfo/{id}', 'BoxController@managerinfo');

Route::get('/box/manageredit/{id}', 'BoxController@edit');
Route::post('/box/manageredit/{id}', 'BoxController@managersave');

/**
 * Document
 */
Route::get('/document/info/{id}', 'DocumentController@info');
Route::get('/document/remove/{id}', 'DocumentController@remove');

Route::get('/document/', 'DocumentController@index');
Route::post('/document/', 'DocumentController@index_post');

Route::get('/document/draff/', 'DocumentController@draff');
Route::post('/document/draff/', 'DocumentController@draff_post');

Route::get('/document/inbox/', 'DocumentController@inbox');
Route::post('/document/inbox/', 'DocumentController@inbox_post');

Route::get('/document/outbox/', 'DocumentController@outbox');
Route::post('/document/outbox/', 'DocumentController@outbox_post');

Route::get('/document/box/{id}/{docTypeId}', 'DocumentController@box');
Route::get('/document/box/{id}', 'DocumentController@box');
Route::post('/document/box/{id}/{docTypeId}', 'DocumentController@box_post');
Route::post('/document/box/{id}', 'DocumentController@box_post');

Route::get('/document/trash/', 'DocumentController@trash');
Route::post('/document/trash/', 'DocumentController@trash_post');

Route::post('/document/upload', 'DocumentController@upload');
Route::post('/document/comment', 'DocumentController@comment');

Route::get('/document/openfile/{id}', 'DocumentController@openfile');
Route::get('/document/removefile/{id}', 'DocumentController@removefile');

Route::get('/document/tobox/{id}', 'DocumentController@tobox');
Route::post('/document/tobox/{id}', 'DocumentController@tobox_post');
Route::get('/document/todepartmentbox/{id}', 'DocumentController@todepartmentbox');
Route::post('/document/todepartmentbox/{id}', 'DocumentController@todepartmentbox_post');

 // Route::get('/document/owner', 'DocumentController@owner');
 // Route::post('/document/owner', 'DocumentController@owner_post');

 Route::get('/document/draff', 'DocumentController@draff');
 Route::post('/document/draff', 'DocumentController@draff_post');

 Route::get('/document/setuser/{id}', 'DocumentController@setuser');
 Route::get('/document/selectpath/{id}/{pathId}', 'DocumentController@selectpath');
 Route::post('/document/selectpath/{id}/{pathId}', 'DocumentController@selectpath_post');

 Route::get('/document/updatepath/{id}', 'DocumentController@updatepath');
 Route::get('/document/resetpath/{id}', 'DocumentController@resetpath');
 Route::get('/document/removepathitem/{id}', 'DocumentController@removepathitem');
 Route::get('/document/editpathitem/{id}', 'DocumentController@editpathitem');
 Route::post('/document/editpathitem/{id}', 'DocumentController@updatepathitem');

 Route::post('/document/addstaffitem/', 'DocumentController@addStaffItem');
 Route::post('/document/adddepartmentitem/', 'DocumentController@addDepartmentItem');

 // Route::get('/document/memo/', 'DocumentController@memo');
 // Route::post('/document/memo/', 'DocumentController@memo_post');
 Route::get('/memo/create/', 'MemoController@create');
 Route::post('/memo/create/', 'MemoController@save');
 Route::get('/memo/info/{id}', 'MemoController@info');
 Route::get('/memo/edit/{id}', 'MemoController@edit');
 Route::post('/memo/edit/{id}', 'MemoController@save');

Route::get('/command/create/', 'CommandController@create');
Route::post('/command/create/', 'CommandController@save');
Route::get('/command/info/{id}', 'CommandController@info');
Route::get('/command/edit/{id}', 'CommandController@edit');
Route::post('/command/edit/{id}', 'CommandController@save');

Route::get('/leave/sick/', 'LeaveController@sick');
Route::post('/leave/sick/', 'LeaveController@save');
Route::get('/leave/errand/', 'LeaveController@errand');
Route::post('/leave/errand/', 'LeaveController@save');
Route::get('/leave/annual/', 'LeaveController@annual');
Route::post('/leave/annual/', 'LeaveController@save');
Route::get('/leave/info/{id}', 'LeaveController@info');
Route::get('/leave/edit/{id}', 'LeaveController@edit');
Route::post('/leave/edit/{id}', 'LeaveController@save');

Route::get('/leave/limit/', 'LeaveController@limit');
Route::post('/leave/limit/', 'LeaveController@limit_post');
Route::get('/leave/limitedit/{id}', 'LeaveController@limitedit');
Route::post('/leave/limitedit/{id}', 'LeaveController@limitsave');


Route::get('/note/create/', 'NoteController@create');
Route::post('/note/create/', 'NoteController@save');
Route::get('/note/info/{id}', 'NoteController@info');
Route::get('/note/edit/{id}', 'NoteController@edit');
Route::post('/note/edit/{id}', 'NoteController@save');


Route::get('/document/send/{id}', 'DocumentController@send');
Route::get('/document/share/{id}', 'DocumentController@share');
Route::post('/document/share/{id}', 'DocumentController@share_post');
Route::get('/document/removeshare/{id}', 'DocumentController@removeshare');

Route::get('/document/getback/{id}', 'DocumentController@getback');
Route::get('/document/dochistory/{id}', 'DocumentController@dochistory');
Route::get('/document/pathhistory/{id}', 'DocumentController@pathhistory');
Route::get('/document/commenthistory/{id}', 'DocumentController@commenthistory');

 Route::get('/mail', function(){
  // Mail::queue('mail.reminder', [],  function($mail){
  //   $mail->from('amir.h@yru.ac.th', 'Amir YRU');
  //   $mail->to('amir.h@hrg-dev.com', 'Amir hRG')->subject('นี่คือการทดสอบ');
  // });

  Mail::to('amir.h@hrg-dev.com', 'Amir hDev')
    ->send(new App\Mail\Reminder);
 });

// Contract
// // Deposit
// Route::get('/deposit', 'DepositController@index');
// Route::post('/deposit', 'DepositController@index_post');
// Route::get('/deposit/memberlist', 'DepositController@memberlist');
// Route::post('/deposit/memberlist', 'DepositController@memberlist_post');
// Route::get('/deposit/listbymember/{id}', 'DepositController@listbymember');

// Route::get('/deposit/info/{id}', 'DepositController@info');
//
// Route::get('/deposit/editincome/{id}', 'DepositController@editincome');
// Route::post('/deposit/editincome/{id}', 'DepositController@deposit_save');
//
// Route::get('/deposit/editwithdraw/{id}', 'DepositController@editwithdraw');
// Route::post('/deposit/editwithdraw/{id}', 'DepositController@withdraw_save');
//
// Route::get('/deposit/addnew', 'DepositController@addnew');
// Route::post('/deposit/addnew', 'DepositController@deposit_save');
// Route::get('/deposit/withdraw', 'DepositController@withdraw');
// Route::post('/deposit/withdraw', 'DepositController@withdraw_save');
//
// Route::get('/deposit/delete/{id}', 'DepositController@delete');
//
// // Request form
// Route::get('/requestform', 'RequestformController@index');
// Route::post('/requestform', 'RequestformController@index_post');
//
// Route::get('/requestform/pending', 'RequestformController@pending');
// Route::post('/requestform/pending', 'RequestformController@pending_post');
//
// Route::get('/requestform/create', 'RequestformController@create');
// Route::post('/requestform/create', 'RequestformController@requestform_save');
//
// Route::get('/requestform/edit/{id}', 'RequestformController@edit');
// Route::post('/requestform/edit/{id}', 'RequestformController@requestform_save');
//
// Route::get('/requestform/info/{id}', 'RequestformController@info');
// Route::post('/requestform/info/{id}', 'RequestformController@approve');
//
// Route::post('/requestform/makepayment', 'RequestformController@makepayment');
//
// Route::get('/requestform/delete/{id}', 'RequestformController@delete');
// Route::get('/requestform/reject/{id}', 'RequestformController@reject');
// Route::get('/requestform/cancel/{id}', 'RequestformController@cancel');
//
// Route::post('/requestform/previewdetail', 'RequestformController@previewdetail');
